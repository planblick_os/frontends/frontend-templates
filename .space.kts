/**
 * JetBrains Space Automation
 * This Kotlin-script file lets you automate build activities
 * For more info, see https://www.jetbrains.com/help/space/automation.html
 */

// STAGING
job("Build, push and deploy staging") {
    startOn {
        gitPush {
            anyBranchMatching  {
                +"staging"
            }
        }
    }

    container(displayName = "Build and push", image = "docker:dind") {
        shellScript {
            content = """
            	apk update
                apk add git
                git submodule update --init
                echo "${'$'}JB_SPACE_GIT_REVISION" > ./src/app/html/version.html
                find ./. -type f \( -name '*.html' -o -name '*.js' -o -name '*.tpl' \) -print0 | xargs -0 sed -i "s/\\[cs_version\\]/${'$'}JB_SPACE_GIT_REVISION/g"
                docker login -u a84baedd-a48f-41b6-845e-2c54dc21b6fa -p 25ecc99f3d89402db4abf545a7ac1ed48b48dab093a40f96dd1e656b4cd66399 planblick.registry.jetbrains.space
                docker build -t planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:staging -t planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:${'$'}JB_SPACE_GIT_REVISION .
                docker push planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:staging
                docker push planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:${'$'}JB_SPACE_GIT_REVISION
            """
        }
    }
    container(displayName = "Trigger deployment on staging kubernetes cluster", image = "registry.gitlab.com/crowdsoft-foundation/various/pytestkube:latest") {
        env["KUBECONFIG_STAGING"] = "{{ project:kubeconfig_staging }}"
        env["GIT_USER"] = "{{ project:git_user }}"
        env["GIT_PASSWORD"] = "{{ project:git_password }}"
        shellScript {
            content = """
                find ./. -type f \( -name '*.yaml' \) -print0 | xargs -0 sed -i "s#\[image\]#planblick.registry.jetbrains.space/p/crowdsoft/images/frontend\:${'$'}JB_SPACE_GIT_REVISION#g"
                    echo "${'$'}KUBECONFIG_STAGING" > ./kube_config.yaml
                    cat ./staging.yaml
                    kubectl apply -f ./staging.yaml --kubeconfig="./kube_config.yaml"
                    rm -f ./kube_config.yaml
                    git config --global user.email "info@planblick.com"
                    git config --global user.name "planBLICK CI/CD"
                    git clone https://${'$'}GIT_USER:${'$'}GIT_PASSWORD@git.jetbrains.space/planblick/planblick/service-manifests.git
                    cp ./staging.yaml ./service-manifests/staging/${'$'}JB_SPACE_GIT_REPOSITORY_NAME.yaml
                    cd service-manifests
                    git add --all
                    git commit -m "Automatic commit from CI/CD (staging) for ${'$'}JB_SPACE_GIT_REPOSITORY_NAME.yaml"
                    git push
            """
        }
    }
}

// PRODUCTION
job("Build, push and deploy production") {
    startOn {
        gitPush {
            anyBranchMatching  {
                +"production"
            }
        }
    }

    container(displayName = "Build and push", image = "docker:dind") {
        shellScript {
            content = """
            	apk update
                apk add git
                git submodule update --init
                echo "${'$'}JB_SPACE_GIT_REVISION" > ./src/app/html/version.html
                find ./. -type f \( -name '*.html' -o -name '*.js' -o -name '*.tpl' \) -print0 | xargs -0 sed -i "s/\\[cs_version\\]/${'$'}JB_SPACE_GIT_REVISION/g"
                docker login -u a84baedd-a48f-41b6-845e-2c54dc21b6fa -p 25ecc99f3d89402db4abf545a7ac1ed48b48dab093a40f96dd1e656b4cd66399 planblick.registry.jetbrains.space
                docker build -t planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:production -t planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:${'$'}JB_SPACE_GIT_REVISION .
                docker push planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:production
                docker push planblick.registry.jetbrains.space/p/crowdsoft/images/frontend:${'$'}JB_SPACE_GIT_REVISION
            """
        }
    }
    container(displayName = "Trigger deployment on production kubernetes cluster", image = "registry.gitlab.com/crowdsoft-foundation/various/pytestkube:latest") {
        env["KUBECONFIG_PRODUCTION"] = "{{ project:kubeconfig_production }}"
        env["GIT_USER"] = "{{ project:git_user }}"
        env["GIT_PASSWORD"] = "{{ project:git_password }}"
        shellScript {
            content = """
                find ./. -type f \( -name '*.yaml' \) -print0 | xargs -0 sed -i "s#\[image\]#planblick.registry.jetbrains.space/p/crowdsoft/images/frontend\:${'$'}JB_SPACE_GIT_REVISION#g"
                echo "${'$'}KUBECONFIG_PRODUCTION" > ./kube_config.yaml
                cat ./production.yaml
                kubectl apply -f ./production.yaml --kubeconfig="./kube_config.yaml"
                rm -f ./kube_config.yaml
                git config --global user.email "info@planblick.com"
                git config --global user.name "planBLICK CI/CD"
                git clone https://${'$'}GIT_USER:${'$'}GIT_PASSWORD@git.jetbrains.space/planblick/planblick/service-manifests.git
                cp ./production.yaml ./service-manifests/production/${'$'}JB_SPACE_GIT_REPOSITORY_NAME.yaml
                cd service-manifests
                git add --all
                git commit -m "Automatic commit from CI/CD (production) for ${'$'}JB_SPACE_GIT_REPOSITORY_NAME.yaml"
                git push
            """
        }
    }
}

