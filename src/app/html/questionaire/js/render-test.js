import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {stringToHslColor, md5} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbQuestionaires} from "./questionaires.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]";
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {ContactManager} from "../../contact-manager/js/contact-manager.js?v=[cs_version]";
export class RenderTest {
    contacts_list_sorted = []
    groupsandmembers = []
    attachedContacts = []

    constructor() {
        if (!RenderTest.instance) {

            this.tpl = new PbTpl()
            this.questionaires = new PbQuestionaires()
            RenderTest.instance = this
            RenderTest.instance.handledTaskIds = []
        }

        return RenderTest.instance
    }

    init() {
        let contact = new ContactManager()
        let promises = [this.questionaires.getQuestionaireConfig(), contact.init()]

        Promise.all(promises).then(function (return_values) {

            let contacts_list = contact.contacts_list
            let questionaireIDs = extractQuestionaireIDs(return_values[0]);
            let res = [...new Set(questionaireIDs)];

            $.each(res, function (key, value) {
                $("#questionaire_dropdown").append($("<option></option>").val(value).html(value));
            })
            $("#questionaire_dropdown").prepend($("<option></option>").text("-- Bitte wählen Sie einen Fragebogen --").val("").prop("selected", true));
            RenderTest.instance.renderContactList(contacts_list)
        }.bind(this))




        function extractQuestionaireIDs(data) {
            return data.map(item => item.data.value.questionaireID);
        }

        $(".datepicker").datetimepicker({
            format: CONFIG.DATEFORMAT,
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false,
            widgetPositioning: {horizontal: 'left', vertical: 'bottom'}
        });

        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }

        $(document).on('click', '#show_statistic', function (e) {
            let search_terms = $("#search_terms").val();

            if (!search_terms) {
                PbNotifications.instance.showHint("Es muss ein Fragebogen ausgewählt werden.", "error")
                $('#questionaire_dropdown_id').addClass('alert-danger');
                $('#questionaire_dropdown_id').removeClass('hidden');
            } else {
                $('#questionaire_dropdown_id').removeClass('alert-danger');
                $('#questionaire_dropdown_id').addClass('hidden');
            }

            let date = new Date();
            let start_date = null;
            let end_date = null;

            if ($("#start_date").val() === "") {
                start_date = new Date(date.getFullYear(), date.getMonth(), 1);
                start_date = moment(start_date).format('YYYY-MM-DD');
            } else {
                start_date = moment($("#start_date").val(), 'DD.MM.YYYY');
                start_date = start_date.format('YYYY-MM-DD');
            }

            if ($("#end_date").val() === "") {
                end_date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                end_date = moment(end_date).format('YYYY-MM-DD');
            } else {
                end_date = moment($("#end_date").val(), 'DD.MM.YYYY');
                end_date = end_date.format('YYYY-MM-DD');
            }


            let promises = [new PbQuestionaires().getQuestionaireStatistic(search_terms, start_date, end_date), new PbQuestionaires().getQuestionaireConfigById(search_terms)]


            Promise.all(promises).then(function (return_values) {
                new PbNotifications().blockForLoading('#statistic_container')

                // Extract mappings
                const questionMapping = {};
                const answerMapping = {};

                return_values[1].questions.forEach(q => {
                    questionMapping[q.question.id] = q.question.text;
                    q.answer.select_options?.forEach(option => {
                        answerMapping[option.id] = option.label;
                    });
                });

                for (let value of return_values[0]) {
                    let answer_label = answerMapping[value[5].replace('answer_value_', '')]
                    if (answer_label !== undefined) {
                        value[5] = answer_label
                    } else {
                        value[5] = value[6]
                    }

                    let question_label = questionMapping[value[4]]
                    if (question_label !== undefined) {
                        value[4] = question_label
                    }
                }

                PbTpl.instance.renderIntoAsync('questionaire/templates/statistic/answers.html', {
                    "statistic_data": return_values[0],
                    "search_terms": search_terms,
                    "start_date": start_date,
                    "end_date": end_date,
                    "chart_type": "bar"
                }, '#question_statistic').then(() => {
                    const statistic_data = return_values[0];

                    let buckets = {};

                    statistic_data.slice(1).forEach(entry => {
                        const questionId = entry[4];
                        const answerIds = Array.isArray(entry[5]) ? entry[5] : [entry[5]];

                        if (!buckets[questionId]) {
                            buckets[questionId] = {};
                        }

                        answerIds.forEach(answerId => {
                            if (!buckets[questionId][answerId]) {
                                buckets[questionId][answerId] = 0;
                            }
                            buckets[questionId][answerId] += 1;
                        });
                    });

                    function getRandomColor(string) {
                        return stringToHslColor(md5(string))
                    }

                    function hslToRgba(hsl, alpha = 0.2) {
                        const [h, s, l] = hsl.match(/\d+/g).map(Number);

                        return `hsla(${h}, ${s}%, ${l}%, ${alpha})`;
                    }

                    function createChart(ctx, labels, datasets) {
                        datasets.forEach(dataset => {
                            const backgroundColors = [];
                            const borderColors = [];

                            labels.forEach((item) => {

                                const color = getRandomColor(item);
                                backgroundColors.push(hslToRgba(color));
                                borderColors.push(color);
                            });

                            dataset.backgroundColor = backgroundColors;
                            dataset.borderColor = borderColors;
                        });

                        new Chart(ctx, {
                            type: "bar",
                            data: {
                                labels: labels,
                                datasets: datasets
                            },
                            options: {

                                indexAxis: 'y',
                                responsive: true,
                                maintainAspectRatio: false,
                                scales: {
                                    y: {
                                        ticks: {
                                            maxRotation: 0, // Adjust the rotation as needed
                                            minRotation: 0  // Adjust the rotation as needed
                                        }
                                    },
                                    x: {
                                        beginAtZero: true,
                                        title: {
                                            display: true,
                                            text: 'Count'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            callback: function (value) {
                                                if (Number.isInteger(value)) {
                                                    return value;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }

                    // Get the row to append canvas elements
                    const row = document.querySelector('#charts-container .row');

                    // Iterate over each bucket entry to create charts
                    Object.keys(buckets).forEach(bucketKey => {
                        const bucket = buckets[bucketKey];
                        const labels = Object.keys(bucket);
                        const data = Object.values(bucket);

                        // Create a new column
                        const col = document.createElement('div');
                        col.className = 'canvas-design chart-box';

                        // Create a new canvas element
                        const canvas = document.createElement('canvas');
                        col.appendChild(canvas);
                        row.appendChild(col);

                        // Get the context of the canvas
                        const ctx = canvas.getContext('2d');

                        // Create the dataset
                        const datasets = [{
                            label: bucketKey,
                            data: data,
                            borderWidth: 1
                        }];

                        createChart(ctx, labels, datasets);
                    });

                    new PbNotifications().unblock('#statistic_container')

                });
            }.bind(this))
                .catch(function (error) {
                    PbTpl.instance.renderIntoAsync('questionaire/templates/statistic/answers.html', {
                        "no_data": true,
                    }, '#question_statistic')
                });
        })
    }

        renderContactList(contact_list) {
            $("#azContactList").html("")
                //find out from the config settings, which details are to be shown in the list view
                let tmp_titles = {}
                let tmp_subtitles = {}
                for (let configEntry of ContactManager.instance.fieldconfig) {
                    if (configEntry.display_pos_in_list_h1 > 0) {
                        tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
                    }
                    if (configEntry.display_pos_in_list_h2 > 0) {
                        tmp_subtitles[configEntry.display_pos_in_list_h2] = configEntry.field_id
                    }
                }

            for (let entry of contact_list) {

                let contactTitleString = ""
                let contactSubtitleString = ""
                let contactTagArray = []
                let contactTagSnippet = ""

                for (let titlefield_key of Object.keys(tmp_titles)) {
                    if (entry.data[tmp_titles[titlefield_key]]) {
                        contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                    }
                }
                for (let titlefield_key of Object.keys(tmp_subtitles)) {
                    if (entry.data[tmp_subtitles[titlefield_key]]) {
                        contactSubtitleString += entry.data[tmp_subtitles[titlefield_key]] + " "
                    }
                }
                for (let titlefield_key of Object.keys(entry.data)) {
                    if (entry.data.tags) {
                        contactTagArray = entry.data.tags
                    }
                }

                let newListItem = $("#contactItem").clone()

                if (newListItem.length > 0) {
                    newListItem.attr("id", entry.contact_id)
                    let listItemContent = newListItem.html().replace("[{contact-title}]", contactTitleString).replace("[{contact-subtitle}]", contactSubtitleString)
                    newListItem.html(listItemContent)
                    $("#azContactList").append(newListItem)
                }
        }




    }

}