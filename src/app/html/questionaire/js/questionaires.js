import {getCookie, fireEvent} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"

export class PbQuestionaires {
    static baseconfig = {
        config: {apikey: getCookie("apikey"), api_base_url: CONFIG.API_BASE_URL},
        onBackendUpdateFunc: () => "",
        onCommandError: () => "",
        onEventError: () => "",
    }

    #data = {
        "id": undefined,
        "questionaireinstance_uuid": undefined,
        "questionaire_uuid": undefined,
        "questionaire_entries": [],
        "data_owners": [],
    }


    constructor(args = PbQuestionaires.baseconfig) {
        this.config = args.config
        this.onBackendUpdateFunc = args.onBackendUpdateFunc
        this.onCommandError = args.onCommandError
        this.onEventError = args.onEventError
        this.initBackendListeners()
        return this
    }

    initBackendListeners() {
        document.addEventListener("event", function _(e) {
            if (e.data && e.data.data && e.data.data.event == "questionaireAnswersAdded") {
                if (e.data.data?.args?.data?.id == this.#data.id) {
                    fireEvent("questionaireAnswersAdded", e.data.data)
                    this.onBackendUpdateFunc(e.data.data)
                }
            }
        }.bind(this))
    }

    async getQuestionairesById(id = "") {
        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/get_data_by_questionaire_id/${id}`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey
                    },
                    "success": async function (response) {

                        if (id == "") {
                            this.loadedQuestionaires = response
                        } else {
                            this.currentQuestionaire = {"answers": response}
                            if(response[0]) {
                                await this.getQuestionaireConfigById(response[0]["data"]["value"]["questionaireID"])
                            }
                        }
                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    }

    async getQuestionaireConfigById(id) {
        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/get_questionaire_config_by_id/${id}`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey
                    },
                    "success": function (response) {
                        this.currentQuestionaireConfig = response
                        this.buildAnswerIDTextMapping()
                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    }

    getQuestionaireStatistic(search_terms = null, start_date = null, end_date = null, transpose = false) {
        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/questionaires/statistic/${search_terms}?from=${start_date}&to=${end_date}&transpose=true`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey
                    },
                    "success": function (response) {
                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    }

    getQuestionAnswerMapping() {
        let map = {}
        for (let cur_answer of this.currentQuestionaire.answers) {
            //console.log("cur_answer", cur_answer)
            let cur_question_id = cur_answer["data"]["value"]["questionID"]
            for (let cur_question of this.currentQuestionaireConfig.questions) {
                //console.log("cur_question_id", cur_question)
                if (cur_question.question.id == cur_question_id) {
                    //console.log("FOUND",cur_question, cur_answer)
                    if (!map[cur_question.question.id]) {
                        map[cur_question.question.id] = {
                            "question": {
                                text: cur_question.question.text,
                                id: cur_question.question.id,
                                type: cur_question.answer.type
                            },
                            "answers": []
                        }
                    }

                    if (map[cur_question.question.id].question.type == "select" || map[cur_question.question.id].question.type == "multiselect") {
                        console.log("FUCK IT", this.getAnswerTextById(cur_answer["data"]["value"]["answerID"].replace("answer_value_", "")), cur_answer["data"]["value"]["answerID"].replace("answer_value_", ""))
                        map[cur_question.question.id]["answers"].push({
                            "answer_id": cur_answer["data"]["value"]["data"],
                            "answer_value": cur_answer["data"],
                            "text": this.getAnswerTextById(cur_answer["data"]["value"]["answerID"].replace("answer_value_", ""))
                        })
                    } else if (map[cur_question.question.id].question.type == "input") {
                        map[cur_question.question.id]["answers"].push({"text": cur_answer["data"]["value"]["data"]})
                    } else {
                        console.log("HELP", map[cur_question.question.id].question.type)
                    }
                }

            }
        }
        console.log("MAP", map)
        return map
    }

    buildAnswerIDTextMapping() {
        this.answerTextMapping = {}
        for (let entry of this.currentQuestionaireConfig.questions) {
            if (entry.answer.type == "select" || entry.answer.type == "multiselect") {
                for (let answer of entry.answer.select_options) {
                    this.answerTextMapping[answer.id] = answer.label
                }
            }
        }
    }

    getAnswerTextById(id = "") {
        return this.answerTextMapping[id]
    }

    exportQuestionaires(search_terms = null, start_date = null, end_date = null, transpose = false) {
        const url = `${this.config.api_base_url}/questionaires/csv_download/${search_terms}?from=${start_date}&to=${end_date}&transpose=${transpose ? 1 : 0}&apikey=${this.config.apikey}`;
        const a = document.createElement('a');
        a.href = url;
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    async getQuestionaireConfig(selectedValues = []) {
        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/get_data_by_questionaire_id/`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey
                    },
                    "success": function (response) {
                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    }

    async getQuestionaireConfigs(selectedValues = []) {
        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/get_questionaire_configs`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey
                    },
                    "success": function (response) {
                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    }

    async putQuestionaires(data = "") {
        // Check if data is already an object
        if (typeof data !== 'object') {
            data = JSON.parse(data);
        }

        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": `${this.config.api_base_url}/es/cmd/putQuestionaireConfig`,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.config.apikey,
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify({
                        "data": data,
                        "data_owners": []
                    }),
                    "success": async function (response) {

                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    }

    createInviteCode(id) {
        return new Promise (function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createQRCode",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "name": "Contact-Deep-Link",
                  "data": {
                    "payload": {
                      "type": "url",
                      "guest_login": true,
                      "value": CONFIG.APP_BASE_URL + '/questionaire/questionaire_form.html?questionaire_config_id='+ id
                    },
                    "error_correct": "H",
                    "kind": "default"
                  }
                }),
                "success": (response) => {
                    document.addEventListener("command", function (event) {
                        if (event.data.data.command === "qrCodeCreated") {
                            resolve(event.data.data.args.qr_code_id)
                        }
                    })
                },
                "error": reject,
            };

            $.ajax(settings)
          })
    }
}