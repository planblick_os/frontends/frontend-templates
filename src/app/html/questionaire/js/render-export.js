import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"

import {PbQuestionaires} from "./questionaires.js?v=[cs_version]"

export class RenderExport {
    constructor() {
        if (!RenderExport.instance) {
            this.tpl = new PbTpl()
            this.questionaires = new PbQuestionaires()
            RenderExport.instance = this
        }

        return RenderExport.instance
    }

    init() {

        let promises = [this.questionaires.getQuestionaireConfig()]

        Promise.all(promises).then(function (return_values) {
            let questionaireIDs = extractQuestionaireIDs(return_values[0]);
            let res = [...new Set(questionaireIDs)];

            $.each(res, function (key, value) {
                $("#questionaire_dropdown").append($("<option></option>").val(value).html(value));
            })
            $("#questionaire_dropdown").prepend($("<option></option>").text("-- Bitte wählen Sie einen Fragebogen --").val("").prop("selected", true));
            $("#questionaire_dropdown").on("change", function (element, data) {
                let questionaireID = $("#questionaire_dropdown").val();
                if(questionaireID && questionaireID.length > 0) {
                    let downloadURL = CONFIG.API_BASE_URL + "/questionaires/download_simplified_config_csv/" + questionaireID + "?apikey=" + getCookie("apikey")
                    $("#config_download_link").attr("href", downloadURL)
                    $("#config_download_link").prop("href", downloadURL)
                    $("#config_download_link").removeClass("hidden")
                } else {
                    $("#config_download_link").addClass("hidden")
                }
            });
        }.bind(this))

        function extractQuestionaireIDs(data) {
            return data.map(item => item.data.value.questionaireID);
        }

        $(".datepicker").datetimepicker({
            format: CONFIG.DATEFORMAT,
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false,
            widgetPositioning: {horizontal: 'left', vertical: 'bottom'}
        });

        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }

        $(document).on('click', '#export_questionare_button', function (e) {
            let searchTerms = $("#searchterms").val();

            let startTimeObject = moment($("#starttime").val(), 'DD.MM.YYYY');
            let formattedStartTimeDate = startTimeObject.format('YYYY-MM-DD');

            let endTimeObject = moment($("#endtime").val(), 'DD.MM.YYYY');
            let formattedEndTimeDate = endTimeObject.format('YYYY-MM-DD');

            if (!$("#starttime").val() || !$("#endtime").val()) {
                PbNotifications.instance.showHint("Das End-Datum und das Start-Datum dürfen nicht leer sein.", "error")
                $('#starttime').addClass('alert-danger');
                $('#endtime').addClass('alert-danger');
            }

            let transposeFlag = $("#transpose").prop('checked')

            if (startTimeObject.isValid() && endTimeObject.isValid()) {
                $('#starttime').removeClass('alert-danger');
                $('#endtime').removeClass('alert-danger');

                if (endTimeObject.isBefore(startTimeObject)) {
                    PbNotifications.instance.showHint("Das End-Datum darf nicht vor dem Start-Datum liegen.", "error")
                } else {
                    PbNotifications.instance.showHint(new pbI18n().translate("Ihr Dokument wird nun heruntergeladen."))
                    new PbQuestionaires().exportQuestionaires(encodeURIComponent(searchTerms), formattedStartTimeDate, formattedEndTimeDate, transposeFlag);
                }
            }
        })
    }
}