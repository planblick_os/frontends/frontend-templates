import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {
    getCookie,
    fireEvent
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"



export class PbFroala {
    userList = {}
    editors = {}
    socketio = undefined

    constructor(forceNewInstance= false) {
        if (!PbFroala.instance || forceNewInstance) {
            PbFroala.instance = this
            PbFroala.instance.socketio = new socketio()
        }

        return PbFroala.instance
    }

    init(node_selector="#editor", sync_with_room=null) {
        PbFroala.instance.socketio.joinRoom(sync_with_room)
        PbFroala.instance.editors[node_selector] = new FroalaEditor(node_selector, {
            key: CONFIG.FROALA_KEY,
            pluginsEnabled: ['align', 'charCounter', 'codeBeautifier', 'codeView', 'colors'
                , 'draggable', 'embedly', 'emoticons', 'entities', 'file', 'fontAwesome', 'fontFamily'
                , 'fontSize', 'fullscreen', 'image', 'imageTUI', 'inlineStyle', 'inlineClass'
                , 'lineBreaker', 'lineHeight', 'link', 'lists', 'paragraphFormat', 'paragraphStyle'
                , 'quickInsert', 'quote', 'save', 'table', 'url', 'video', 'wordPaste'],
            imageUpload: true,
            imagePaste: true,
            fileAllowedTypes: ['*'],
            // Set max file size to 20MB.
            fileMaxSize: 20 * 1024 * 1024,
            fileUploadURL: CONFIG.API_BASE_URL + '/froala/upload_file?apikey=' + getCookie("apikey"),
            imageUploadURL: CONFIG.API_BASE_URL + '/froala/upload_image?apikey=' + getCookie("apikey"),
            videoUploadURL: CONFIG.API_BASE_URL + '/froala/upload_video?apikey=' + getCookie("apikey"),
            iframe: false,
            height: "100%",
            width: "100%",
            events: {
                keyup: function () {
                    //$('#preview').html(this.html.get());
                    this.selection.save();
                    if(sync_with_room != null) {
                        console.log("FIRE EVENT TO ROOM", sync_with_room)
                        fireEvent("easy2getherProtocolUpdate", {
                            protocolContent: this.html.get(true),
                            editorName: getCookie("username")
                        }, sync_with_room, true)
                    }
                },
                contentChanged: function () {
                    //$('#preview').html(this.html.get());
                    this.selection.save();
                    if(sync_with_room != null) {
                        fireEvent("easy2getherProtocolUpdate", {
                            protocolContent: this.html.get(true),
                            editorName: getCookie("username")
                        }, sync_with_room, true)
                    }
                },
                'image.beforeUpload': function (images) {
                    // Return false if you want to stop the image upload.
                    console.log("IMAGES", images)
                    return true
                },
                'image.uploaded': function (response) {
                    console.log("IMAGES UPLOADED", response)
                    // Image was uploaded to the server.
                },
                'image.inserted': function ($img, response) {
                    console.log("IMAGES INSERTED", $img, response)
                    // Image was inserted in the editor.
                },
                'image.replaced': function ($img, response) {
                    console.log("IMAGES REPLACED", $img, response)
                    // Image was replaced in the editor.
                },
                'image.error': function (error, response) {
                    console.log("IMAGES ERROR", error, response)
                    // Bad link.
                    if (error.code == 1) {
                    }

                    // No link in upload response.
                    else if (error.code == 2) {
                    }

                    // Error during image upload.
                    else if (error.code == 3) {
                    }

                    // Parsing response failed.
                    else if (error.code == 4) {
                    }

                    // Image too text-large.
                    else if (error.code == 5) {
                    }

                    // Invalid image type.
                    else if (error.code == 6) {
                    }

                    // Image can be uploaded only to same domain in IE 8 and IE 9.
                    else if (error.code == 7) {
                    }

                    // Response contains the original server response to the request if available.
                }
            }
        })
    }
}