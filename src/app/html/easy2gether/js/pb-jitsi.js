import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {
    getCookie,
    fireEvent
} from "../../csf-lib/pb-functions.js?v=[cs_version]"


export class PbJitsi {
    domain = 'meet.jit.si';
    api = undefined

    constructor() {
        if (!PbJitsi.instance) {
            PbJitsi.instance = this
        }

        return PbJitsi.instance
    }

    init(node_selector, username, roomname = "planblick", height = "100%", width = "100%") {
        const options = {
            roomName: roomname,
            width: width,
            height: height,
            parentNode: document.querySelector(node_selector),
            userInfo: {
                email: '',
                displayName: username
            }

        }

        PbJitsi.instance.api = new JitsiMeetExternalAPI(PbJitsi.instance.domain, options)
    }

    setDisplayName(name) {
        PbJitsi.instance.api.executeCommand('displayName', name);
    }

    setLobbyMode(enabled = true) {
        PbJitsi.instance.api.executeCommand('toggleLobby', enabled);
    }

    closeCall() {
        PbJitsi.instance.api.executeCommand("hangup")
    }
}