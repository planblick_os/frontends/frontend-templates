import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {
    getCookie,
    setCookie,
    fireEvent,
    showNotification,
    getQueryParam,
    get_news_for,
    renderNews,
    getWelcome,
    getTourParam
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {RenderListSettings} from "./render-list-settings.js?v=[cs_version]"
import {RenderTemplateSettings} from "./render-template-settings.js?v=[cs_version]"
import {RenderResourceSettings} from "./render-resource-settings.js?v=[cs_version]"
import {RenderTemplateGroups} from "./render-template-groups.js?v=[cs_version]"
import {TaskmanagerHelp} from "./taskmanager-help.js?v=[cs_version]"
import {RenderGuestForm} from "./render-guest-form.js?v=[cs_version]"
import {RenderSearchList} from "./render-search-list.js?v=[cs_version]"


export class TaskManagerModule {
    actions = {}


    constructor() {
        if (!TaskManagerModule.instance) {
            this.initListeners()
            this.initActions()
            $.unblockUI()
            TaskManagerModule.instance = this
        }

        return TaskManagerModule.instance
    }

    initActions() {
        this.actions["taskManagerInitialised"] = function (myevent, callback = () => "") {
            new TaskManager().setOrderValueByIds([], true)

            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("taskmanager")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new TaskManager().init().then(function () {
                        if ($("#loader-overlay")) {
                            $("#loader-overlay").hide()
                            $("#contentwrapper").show()
                        }
                        new TaskmanagerHelp()
                        callback(myevent)

                    })
                }

            })
        }

        this.actions["taskManagerGuestInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("frontend.taskmanager.views.guestform")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    let template_id = getQueryParam("tid")
                    new TaskManager().initGuest(template_id).then(function (initialisation_data) {
                        new RenderGuestForm().fillInTemplateData(initialisation_data[0])
                        Fnon.Wait.Remove(500)
                        callback(myevent)
                    })
                }
            })
        }


        this.actions["startTaskManagerList"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {

                getWelcome("taskmanager")
                let current_queue_id = undefined
                let current_task_id = undefined
                let update_current_queue_id = false
                let update_current_task_id_source = false

                let current_tour = undefined
                if (window.location.toString().includes("tour")) {
                    current_tour = getQueryParam("tour")
                }

                current_queue_id = getCookie(Render.instance.cookie_queue_id)
                if (window.location.toString().includes("queue_id")) {
                    update_current_queue_id = true
                    current_queue_id = getQueryParam("queue_id")
                }

                current_task_id = getCookie(Render.instance.cookie_task_id)
                if (window.location.toString().includes("task_id")) {
                    update_current_task_id_source = true
                    current_task_id = getQueryParam("task_id")

                    // If comments shall be displayed on load of page instead of click on comments, uncomment this comment
                    TaskManager.instance.fetchCommentsByTaskId(current_task_id).then(() => {
                        Render.instance.showQueueEntryComments(TaskManager.instance.getComments())
                    })
                }
                Render.instance.renderTaskListView()

                if (current_queue_id != false) {
                    $('#task-list-section option').removeAttr('selected').filter(`[value=${current_queue_id}]`).prop('selected', true);
                    $('#task-list-section').trigger('click');
                    Render.instance.renderActiveQueueData()
                    Render.instance.renderQueueEntries()
                    $("#list-view").show()
                    $("#task-detail-view").hide()
                }

                if (current_task_id != false) {
                    Render.instance.tm.setActiveQueueEntryById(current_task_id).then(Render.instance.renderDetailData)
                    $("#list-view").hide()
                    $("#task-detail-view").show()
                }

                if (update_current_queue_id) {
                    setCookie(Render.instance.cookie_queue_id, current_queue_id, {expires: 365, path: '/'})
                }

                if (update_current_task_id_source) {
                    setCookie(Render.instance.cookie_task_id, current_task_id, {expires: 365, path: '/'})
                }

                Render.instance.renderURLfromCookie()

                if (current_tour) {
                    $('#' + current_tour).trigger("click");

                }

            })
        }

        this.actions["taskManagerListSettingsInit"] = function (myevent, callback = () => "") {
            callback(myevent)
        }

        this.actions["taskManagerListSettingsInitialised"] = function (myevent, callback = () => "") {
            new RenderListSettings().initial()
            getTourParam()
            callback(myevent)
        }

        this.actions["taskManagerTemplateSettingsInit"] = function (myevent, callback = () => "") {
            callback(myevent)
        }

        this.actions["taskManagerTemplateSettingsInitialised"] = function (myevent, callback = () => "") {
            new RenderTemplateSettings().initial()
            getTourParam()
            callback(myevent)
        }

        this.actions["taskManagerResourceSettingsInit"] = function (myevent, callback = () => "") {
            callback(myevent)
        }

        this.actions["taskManagerResourceSettingsInitialised"] = function (myevent, callback = () => "") {
            new RenderResourceSettings().initial()
            getTourParam()
            callback(myevent)
        }
        this.actions["taskManagerTemplateGroupSettingsInit"] = function (myevent, callback = () => "") {
            callback(myevent)
        }

        this.actions["taskManagerTemplateGroupSettingsInitialised"] = function (myevent, callback = () => "") {
            new RenderTemplateGroups().initial()
            getTourParam()
            callback(myevent)
        }

        this.actions["startTaskManagerSearch"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("frontend.taskmanager.views.search")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new RenderSearchList().initial()
                    getTourParam()
                    callback(myevent)
                }

            })

        }
    }


    initListeners() {
        $(document).on('click', '#button-task-history', function (e) {
            if ($('#button-task-history').attr('data-selector') === 'off') {

                Fnon.Wait.Circle('', {
                    svgSize: {w: '50px', h: '50px'},
                    svgColor: '#3a22fa',
                })

                Render.instance.tm.fetchActiveQueueEntryHistory()
                $('#button-task-history').attr('data-selector', 'on').removeClass("btn-light-pill").addClass("btn-primary-pill")
                Render.instance.clearComment()

            } else {
                Render.instance.clearHistory()
            }
        })

        $(document).on("showHistoryData", (data) => {
            new Render().showTaskEntryHistory(data)
        })

        $(document).on('click', '#button-task-comment', function (e) {
            if ($('#button-task-comment').attr('data-selector') === 'off') {
                $('#button-task-comment').attr('data-selector', 'on').removeClass("btn-light-pill").addClass("btn-primary-pill")
                Render.instance.clearHistory()
            } else {
                Render.instance.clearComment()
            }

            TaskManager.instance.fetchCommentsByTaskId(TaskManager.instance.getActiveQueueEntryId()).then(() => {
                    Render.instance.showQueueEntryComments(TaskManager.instance.getComments())
                }
            )
        })
        $(document).on('click', '#comment-send-button', () => {
            let comment = $('#comment-input').val()
            TaskManager.instance.createNewComment(comment, TaskManager.instance.getActiveQueueEntryId())
            $('#comment-input').val('')
        })

        $(document).on('click', 'button[data-selector="btn-delete-comment"]', function () {
            let id = $(this).attr('id')
            let idToDelete = id.slice(7)
            let deleteTitle = "Kommentar löschen"
            let deleteQuestion = "Wollen Sie den Kommentar wirklich löschen?"
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {

                    TaskManager.instance.deleteCommentById(idToDelete).then(
                        TaskManager.instance.fetchCommentsByTaskId(TaskManager.instance.getActiveQueueEntryId()).then(
                            Render.instance.showQueueEntryComments(TaskManager.instance.getComments())
                        )
                    )

                    Fnon.Hint.Success('Kommentar gelöscht')
                }
            })


        })

        // build and kill event listener because of necessity to render list setting on page load without setting list cookie and rendering the new url, triggered if getQueryParam("queue_id") is true
        $('#task-list-section').bind('click', function (e) {
            Render.instance.tm.active_queue_list_id = this.value
            Render.instance.renderTaskListView()
            Render.instance.tm.fetchTaskTemplates(Render.instance.tm.active_queue_list_id).then(function () {
                Render.instance.renderTaskTemplateDrowdown('list-view-template-dropdown')
                $('#task-list-section').unbind('click');
            })
        })

        $(document).on('change', '#task-list-section', function (e) {
            Render.instance.tm.active_queue_list_id = this.value
            Render.instance.renderTaskListViewWithCookies()
            Render.instance.tm.fetchTaskTemplates(Render.instance.tm.active_queue_list_id).then(function () {
                Render.instance.renderTaskTemplateDrowdown('list-view-template-dropdown')
            })
        })

        $('#button-new-task, #template_0').click(function (e) {
            e.stopPropagation()
            Render.instance.tm.setActiveQueueEntryById(undefined)
            Render.instance.renderDetailData()
            Render.instance.renderTagList()
        })


        $(document).on('click', 'li[data-selector="list-entry"]', function () {
            let id = $(this).attr('id')
            Render.instance.tm.setActiveQueueEntryById(id).then(Render.instance.renderDetailData)
            TaskManager.instance.fetchCommentsByTaskId(id)
            //set cookie value to active task entry
            setCookie(Render.instance.cookie_queue_id, Render.instance.tm.active_queue_list_id, {
                expires: 365,
                path: '/'
            })
            setCookie(Render.instance.cookie_task_id, Render.instance.tm.active_entry_id, {expires: 365, path: '/'})
            Render.instance.renderURLfromCookie(false)
        })


        $(document).on("eventsToLinkUpdated", (data) => {
            new Render().addEventsToEventsDropdown(data)
        })


        $(document).on('click', 'button[data-selector="btn-resource-edit"]', function () {
            let id = $(this).attr('data-update-resource-id')
            RenderResourceSettings.instance.modal_mode = "update"
            TaskManager.instance.setActiveResource(id)
            RenderResourceSettings.instance.renderModal()

        });
        $("#btn-create-resource").on('click', function () {
            RenderResourceSettings.instance.modal_mode = "new"
            TaskManager.instance.active_resource = undefined
            let id = ""
            RenderResourceSettings.instance.renderModal()
        });

        $('#btn-update-resource').on('click', function () {
            RenderResourceSettings.instance.saveChanges()
        })

    }
}