import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {validateForm} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"

export class RenderListSettings {
    contacts_list_sorted = []
    current_state = "initial"
    pending_tasks = []
    task_description = undefined
    

    constructor() {
        if (!RenderListSettings.instance) {
            RenderListSettings.instance = this
            RenderListSettings.instance.tm = new TaskManager()
            RenderListSettings.instance.permissions = new pbPermissions()
            RenderListSettings.instance.initListeners()
        }

        return RenderListSettings.instance
    }

    initial() {
        RenderListSettings.instance.renderPermissionEntries()
        RenderListSettings.instance.renderListEntries(RenderListSettings.instance.tm.queue_list)
        $("#button-list-delete").prop("disabled", true)
    }

    renderListEntries(queue_list) {
        $("#list-container").html("")
        $("#list-name").val("")
        for (let [key, value] of Object.entries(queue_list)) {
            let clone = $("#list-entry-snippet").children().first().clone()
            clone.attr('id', key);
            clone.html(value.queue_data.queue_name)
            $("#list-container").append(clone)
            clone.click(function () {
                fireEvent("queueSetToActive", {"queue_id": key})
            })
        }
    }

    renderActiveQueueDetails() {
        $("#list-name").val(RenderListSettings.instance.tm.getActiveQueueData() ? RenderListSettings.instance.tm.getActiveQueueData().queue_name : "")
        $('#list-permissions').val(RenderListSettings.instance.tm.getActiveQueueDataOwners()).change()
    }

    renderPermissionEntries() {
        $("#list-permissions").html("")
        let already_added_users = []
        let other_groups_object = RenderListSettings.instance.permissions.groups
        let other_groups= []
        if(other_groups_object != undefined){
            let foo = Object.entries(other_groups_object)
            let bar = foo.map(element => element[0])
            other_groups= bar
        }
        
        let groups = RenderListSettings.instance.permissions.my_groups
        groups.forEach(group => {
            RenderListSettings.instance.permissions.fetchGroupMembers(group).then(function (members) {
                members.sort((a, b) => a.username.toLowerCase() > b.username.toLowerCase()? 1 : -1).reverse()
                members.forEach(member => {
                    if(!already_added_users.includes(member.username)) {
                        already_added_users.push(member.username)
                        $("#list-permissions").prepend(
                            $('<option>', {value: "user." + member.username, text: new pbI18n().translate("User") + ": " + member.username})
                        )
                    }
                })
            })
        })
    
        const addGroups = () => {
            other_groups.forEach(group => {
                if(!groups.includes(group)) {
                    groups.push(group)
                }
            })
        } 
        addGroups()
        groups.sort((a, b) => a.toLowerCase() > b.toLowerCase()? 1 : -1)
        RenderListSettings.instance.permissions.my_groups.forEach(group => {
            if(!group.includes("user.")){
                $("#list-permissions").append(
                    $('<option>', {value: group, text: group.replace("group.", new pbI18n().translate("Group") + ": ")})
                )
            }
            
        })


    }

    initListeners() {
        document.addEventListener('queueSetToActive', function (args) {
            RenderListSettings.instance.tm.setActiveQueueById(args.data.queue_id)
            RenderListSettings.instance.renderActiveQueueDetails()

            if (args.data.queue_id) {
                $("#button-list-delete").prop("disabled", false)
            } else {
                $("#button-list-delete").prop("disabled", true)
            }
        }, false);


        $("#button-list-save").click(function () {
            if (validateForm({'list-name': 'input'})) {
                Fnon.Wait.CircleDots()
                let mode = undefined
                if (!RenderListSettings.instance.tm.getActiveQueueId()) {
                    mode = "new"
                } else {
                    mode = "update"
                }


                if (mode == "new") {
                    RenderListSettings.instance.tm.createNewQueue($("#list-name").val(), $('#list-permissions').val())
                } else {
                    RenderListSettings.instance.tm.active_queue_list_data.queue_name = $("#list-name").val()
                    RenderListSettings.instance.tm.active_queue_list_data_owners = $('#list-permissions').val()
                    RenderListSettings.instance.tm.saveChangesOnQueue()
                }
            }
        })

        $("#button-new-list").click(function () {
            RenderListSettings.instance.tm.setActiveQueueById(undefined)
            $("#button-list-delete").prop("disabled", true)
        })

        $("#button-list-delete").click(function () {
            let cancelQuestion = new pbI18n().translate("Do you really want to delete this list?")
            let cancelTitle = new pbI18n().translate("Delete list")
            let okay = new pbI18n().translate("Yes, delete this list")
            let abort = new pbI18n().translate("Cancel")
            if (RenderListSettings.instance.tm.getActiveQueueId()) {
                Fnon.Ask.Warning(cancelQuestion, cancelTitle, okay, abort, (result) => {
                    if (result == true) {
                        Fnon.Wait.CircleDots()
                        RenderListSettings.instance.tm.deleteActiveQueue()
                    }
                })
            }
        })

        RenderListSettings.instance.tm.socket.commandhandlers["refreshQueues"] = function (args) {
            RenderListSettings.instance.tm.fetchQueueData().then(function () {
                RenderListSettings.instance.renderListEntries(RenderListSettings.instance.tm.queue_list)
                $("#" + args.queue_id).click()
                Fnon.Hint.Success(new pbI18n().translate("Data has been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove()
            })

        }
    }
}

 