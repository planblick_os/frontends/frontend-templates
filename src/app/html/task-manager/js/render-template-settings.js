import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {getCookie, setCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"


export class RenderTemplateSettings {


    constructor() {
        if (!RenderTemplateSettings.instance) {
            RenderTemplateSettings.instance = this
            RenderTemplateSettings.instance.tm = new TaskManager()
            RenderTemplateSettings.instance.initListeners()
        }

        return RenderTemplateSettings.instance
    }

    initial() {
        RenderTemplateSettings.instance.renderQueueList()
        $("#template_element_list").html(new pbI18n().translate("No list selected"))

    }

    renderQueueList() {
        $("#task-list-select").html("")
        $('#task-list-select').append($('<option>',
            {
                value: "0",
                text: "Keine Liste ausgewählt"
            }))
        for (let [key, value] of Object.entries(RenderTemplateSettings.instance.tm.queue_list)) {
            $('#task-list-select').append($('<option>',
                {
                    value: key,
                    text: value.queue_data.queue_name
                }))
        }
        if(getCookie("ts_selected_list")) {
            $("#task-list-select").val(getCookie("ts_selected_list")).change()
        }
    }

    renderTaskTemplateList() {
        let shown_entries = 0
        $('#template_element_list').html("")
        let list = $('#template_element_list')
        let list_entry = $('#template_item').html()
        for (let [key, value] of Object.entries(RenderTemplateSettings.instance.tm.task_templates)) {
            shown_entries++
            const new_entry = list_entry.replaceAll("[{template_id}]", key).replace("[{template_name}]", value.entry_data.name)
            list.append(new_entry)
        }
        if (shown_entries == 0) {
            $("#template_element_list").html(new pbI18n().translate("Nothing to see here"))
        }
    }

    deleteTemplate() {
        //TO DO
    }


    initListeners() {
        $(document).on('click', 'button[name="qr_formgen"]', function (context) {
            context.stopPropagation()
            Fnon.Box.Circle(context.currentTarget, '', {
                svgSize: {w: '50px', h: '50px'},
                svgColor: '#3a22fa',
            })
            let deeplink = CONFIG.APP_BASE_URL + `/task-manager/index-guest.html?tid=${context.currentTarget.id.split("_").pop()}`

            let socket = new socketio()
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createQRCode",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"), "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "name": "Task " + context.currentTarget.id.split("_").pop(), "data": {
                        "payload": {
                            "type": "url",
                            "guest_login": true,
                            "value": deeplink
                        },
                        "error_correct": "H",
                        "kind": "logo",
                        "logo_url": "https://www.serviceblick.com/style-global/img/serviceblick_logo.gif"
                    }
                }),
                "success": function (response) {
                    socket.commandhandlers["qrCodeCreated"] = function (args) {
                        if (args.task_id == response.task_id) {
                            $("#qrform_modal").modal('show')
                            let image_url = CONFIG.API_BASE_URL + "/get_qrcode/" + args.qr_code_id + "?apikey=" + getCookie("apikey")
                            $("#qr_preview").attr("src", image_url)
                            $('button[name="open_qr_form_button"]').attr("id", args.qr_code_id)
                            Fnon.Box.Remove(context.currentTarget)
                        }
                    }
                }
            }
            $.ajax(settings)

        })

        $(document).on('click', 'button[name="open_qr_form_button"]', function (context) {
            context.stopPropagation()
            let title = encodeURIComponent($("#form_title").val())
            let desc = encodeURIComponent($("#form_description").val())
            let qrcode_id = $('button[name="open_qr_form_button"]').attr("id")
            let deeplink = CONFIG.APP_BASE_URL + `/task-manager/qr_formular.html?tid=${context.currentTarget.id.split("_").pop()}&title=${title}&desc=${desc}&qid=${qrcode_id}`

            window.open(deeplink, 'qr_form', 'width=640,height=480,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=yes')
        })


        $(document).on('click', 'button[name="qr_gen"]', function (context) {
            context.stopPropagation()
            Fnon.Box.Circle(context.currentTarget, '', {
                svgSize: {w: '50px', h: '50px'},
                svgColor: '#3a22fa',
            })
            let deeplink = CONFIG.APP_BASE_URL + `/task-manager/index-guest.html?tid=${context.currentTarget.id.split("_").pop()}`

            let socket = new socketio()
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createQRCode",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"), "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "name": "Task " + context.currentTarget.id.split("_").pop(), "data": {
                        "payload": {
                            "type": "url",
                            "guest_login": true,
                            "value": deeplink
                        },
                        "error_correct": "H",
                        "kind": "logo",
                        "logo_url": "https://www.serviceblick.com/style-global/img/serviceblick_logo.gif"
                    }
                }),
                "success": function (response) {
                    socket.commandhandlers["qrCodeCreated"] = function (args) {
                        if (args.task_id == response.task_id) {
                            let image_id = "qrimage_" + args.qr_code_id
                            $("#" + image_id).remove()
                            let image_url = CONFIG.API_BASE_URL + "/get_qrcode/" + args.qr_code_id + "?apikey=" + getCookie("apikey")
                            let img = document.createElement("img")
                            img.id=image_id
                            img.src = image_url
                            img.style = "width:150px;"
                            let src = document.getElementById(context.currentTarget.id.split("_").pop()).firstElementChild

                            src.innerHTML = src.innerHTML + img.outerHTML
                            Fnon.Box.Remove(context.currentTarget)
                        }
                    }
                }
            }

            $.ajax(settings)
        })

        $(document).on('change', '#task-list-select', function (e) {
            let selected_list = this.value

            if (selected_list != 0) {
                RenderTemplateSettings.instance.tm.active_queue_list_id = selected_list
                RenderTemplateSettings.instance.tm.fetchTaskTemplates(selected_list).then(function () {
                    RenderTemplateSettings.instance.renderTaskTemplateList()
                })

            } else {
                $("#template_element_list").html(new pbI18n().translate("No list selected"))
            }

            setCookie("ts_selected_list", selected_list)

        })

        $(document).on('click', 'button[data-selector="btn-field-delete"]', function () {
            let id = $(this).attr('data-field-delete-id')
            let deleteTitle = new pbI18n().translate("Delete template")
            let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the template?")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderTemplateSettings.instance.tm.deleteTaskTemplate(id)
                    Fnon.Hint.Success('Template gelöscht')
                }
            })

        })

        RenderTemplateSettings.instance.tm.socket.commandhandlers["refreshTemplates"] = function (args) {
            RenderTemplateSettings.instance.tm.fetchTaskTemplates(args.queue_id).then(function () {
                RenderTemplateSettings.instance.renderTaskTemplateList()
                Fnon.Hint.Success(new pbI18n().translate("Templates have been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })

        }


    }


}

 