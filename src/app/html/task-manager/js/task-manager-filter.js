export class TaskManagerFilter {
    constructor(task_manager) {
        if (!TaskManagerFilter.instance) {
            TaskManagerFilter.instance = this
            TaskManagerFilter.instance.tm = task_manager
        }

        return TaskManagerFilter.instance
    }

    get_filtered_list(search_in_properties, searchTerm, pre_filtered_list=undefined, invert_search=false,) {
        let filtered_list = pre_filtered_list || Object.assign({}, TaskManagerFilter.instance.tm.getQueueEntries()) || {}

        if(searchTerm && searchTerm.length > 0) {
            for (let [id, value] of Object.entries(filtered_list)) {
                let search_in_string = ""

                for (let search_in_property of search_in_properties) {
                    if (value.entry_data.hasOwnProperty(search_in_property)) {
                        search_in_string += " | " + TaskManagerFilter.instance.search_value_to_string(value.entry_data[search_in_property])
                    }
                    if (value.hasOwnProperty(search_in_property)) {
                        search_in_string += " | " + TaskManagerFilter.instance.search_value_to_string(value[search_in_property])
                    }
                }
                if (!TaskManagerFilter.instance.find(searchTerm, search_in_string, invert_search)) {
                    delete filtered_list[id]
                }
            }
        }

        return filtered_list
    }

    filter_for_not_assigned(pre_filtered_list) {
        for (let [id, value] of Object.entries(pre_filtered_list)) {
            let assigned_to = value.entry_data.assigned_to
            if(!(!Array.isArray(assigned_to) || assigned_to.length === 0)) {
                delete pre_filtered_list[id]
            }
        }
        return pre_filtered_list
    }

    remove_not_assigned(pre_filtered_list) {
        for (let [id, value] of Object.entries(pre_filtered_list)) {
            let assigned_to = value.entry_data.assigned_to
            if(!Array.isArray(assigned_to) || assigned_to.length === 0) {
                delete pre_filtered_list[id]
            }
        }
        return pre_filtered_list
    }

    find(searchTerm, searchstring, invert_search=false) {
        let result = searchstring.toLowerCase().includes(searchTerm.toLowerCase())
        if(invert_search) {
            return !result
        } else {
            return result
        }
    }

    search_value_to_string(search_value) {
        let sanitized_string = ""
        if(typeof search_value === 'string' || search_value instanceof String) {
            sanitized_string = search_value || ""
        } else if(Array.isArray(search_value)) {
            sanitized_string = search_value.join(";")
        } else if(typeof search_value === "object" && item !== null) {
            sanitized_string = search_value.join(";")
        }

        return sanitized_string
    }
}
