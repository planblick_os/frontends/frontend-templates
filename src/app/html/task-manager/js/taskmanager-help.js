import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"

export class TaskmanagerHelp {
    introguide = introJs()

    constructor() {
        if (!TaskmanagerHelp.instance) {
            TaskmanagerHelp.instance = this
            this.initListeners()
        }
        return TaskmanagerHelp.instance
    }
    
    createTourUrl(pathname, tourname) {
        let tour = `?tour=${tourname}`
        let shortPath = undefined //to deal with short url if index page is wanted
        let newURL = pathname + tour

        if (pathname.includes("index")){ 
            shortPath = pathname.replace('/index.html', '/')
        }

        if (!location.pathname.includes(pathname) && shortPath ) { //path is not found in current href
            //alert("short path is " + shortPath)
                if(location.pathname != shortPath){ 
                    window.location.assign(newURL)
                }
        }

        if (!location.pathname.includes(pathname) && !shortPath ) { //path is not found in current href
            window.location.assign(newURL)
        }

    }


    template_tour_steps = [
        {
            title: 'Vorlage QR-Codes',
            intro: 'Lassen Sie uns einige großartige <strong>Funktionen von Vorlagen und deren QR-Codes</strong> kennenlernen.  Diesen Tour zeigt Ihnen, wie das geht. Klicken Sie einfach auf die Pfeilschaltflächen, um durch die Tour zu gelangen. Klicken Sie auf <strong> x</strong>, um die Tour abzubrechen.'
        },
        {
            title: 'Wieso QR-Codes?',
            intro: "Jede Vorlage hat ihren eigenen QR-Code. <strong>Wenn Jemand diesen Code scannt</strong>, wird ein Eingabeformular geofnet, der aus dieser Vorlage erstellt wurde. Wird dieses Formular speichern, wird dann eine <strong>neuen Aufgabe in Ihrem Aufgabemanager erstellt</strong>.",
            position: "right"
        },
        {
            element: '#task-list-select',
            intro: "Wählen Sie eine Liste aus dem Menü aus, um ihre Vorlagen anzuzeigen.",
            myBeforeChangeFunction: function() { 
                // make list clickable
                $(".introjs-overlay").css('z-index', '1000')
                $(".introjs-helperLayer").css('z-index', '1000')
                $("#task-list-select").css('z-index', '999999') 
               }
        },
        {
            element: document.querySelector('#template_element_list'),
            intro: "Die Listenansicht gibt Ihnen einen schnellen Überblick über alle Ihre Vorlagen. Im <strong>Aktionbereich</strong>, rechts in jeder Vorlageneitrag, finden Sie eine Auswahl an Aktionen.",
            myBeforeChangeFunction: function() { 
            // make list actions clickable and modal appear on top if clicked
            $(".introjs-helperLayer").css('z-index', '1000')
            $(".modal").css('z-index', '10000000')
            $("#template_element_list").css('z-index', '1010') 
           }
        },

        {
            element: '#template_element_list',
            title: '<button class="btn btn-indigo btn-icon"><i class="fa fa-qrcode text-white"></i></button>',
            intro:  'Mit das <strong> QR-Code-Icon</strong>, wird den <strong>qr-code als Bild</strong> angezeigt, das Sie auf Ihrem lokalen Gerät speichern können. Dort können Sie es ausdrucken oder an eine Nachricht anhängen.',
            position: "right"
        },
        {
            element: '#template_element_list',
            title: '<button class="btn btn-indigo btn-icon"><i class="typcn typcn-printer text-white"></i></button>',
            intro: 'Mit das <strong>Drucker-Icon</strong>, wird ein Formular aufgerufen, in dem Sie eine <strong>druckbare Seite für Ihren QR-Code anpassen</strong> können. Dort können Sie Ihren eigenen Titel und eine Beschreibung eingeben. Senden Sie die generierte Seite an Ihren normalen Drucker oder drucken Sie sie als PDF aus.',
            position: "right"
        },
        {
            title: 'Großartig!',
            intro: "Jetzt kennen Sie sich mit Vorlagen und deren QR-Codes aus. <strong>Fertig</strong> bringt Sie zum Help Center, wo Sie weitere Touren finden"
        }
    ]




    template_group_tour_steps = [
        { //1
            'title': 'Autopilot',
            'intro': 'Es ist einfach, <b>neuer Aufgaben aus als Gruppen von Vorlagen mit einem Einstellungsintervall zum Generieren</b>. Klicken Sie einfach auf die Pfeilschaltflächen, um durch die Tour zu gelangen. Klicken Sie auf das <strong>x</strong>, um die Tour abzubrechen.', 
        },
        {//2
            'element': '.component-item',
            'intro': 'Erreichen Sie die Vorlagenpaket Einstellungen, entweder über das <b>Zahnradsymbol im Header</b> Ihres Aufgabenmanagers oder über das linke <b>Einstellungsmenü.</b>',
        },
        {//3
            'element': '#new_group_button',
            'intro': 'Erstellen Sie ein neues Aufgabenpaket, indem Sie auf <b>neues Aufgabenpaket</b> klicken. Die Seite Neues Aufgabenpaket wird angezeigt.',
            'myBeforeChangeFunction': function() { 
                //check if the clickable element has been triggered and show next step
                let object = document.querySelector('#new_group_button');
                if(object){
                    object.addEventListener('click',e => {
                    TaskmanagerHelp.instance.introguide.goToStep(4)
                       },{ once: true })
                   }
           }, 
        },
        {//4
            'element': '#group_name',
            'intro': 'Geben Sie Ihrem Aufgabenpaket einen <b>erkennbaren Namen</b>.',
            'myBeforeChangeFunction': function() { 
                $("#new_group_button").click()
            },
            'myAfterChangeFunction': function() {
                $("#group_name").val("Demo Aufgabenpaket")
            }
        },
        {//5
            'element': '#task-list-select',
            'intro': 'Sie können zwischen der Liste wechseln, um ihre Vorlagen anzuzeigen.',
        },
        {//6
            'element': '#check-list-template-container',
            'intro': '<b>Wählen Sie die Vorlage aus</b>, die Sie in dieses Paket aufnehmen möchten. Wenn Sie auf den Namen klicken, wird die Auswahl "umgeschaltet".',
        },
        {//7
            'element': '#overview_list',
            'intro': 'Ihre Auswahl wird in einer Liste zusammengefasst',
            'myAfterChangeFunction': function() {
                $("ul#check-list-template li:first label input").click()//make sure one template is checked
            }
        },
        {//8
            'element': '#edit_schedule_icon_button',
            'intro': 'Um Ihre Aufgaben automatisch zu generieren, müssen Sie ein Wiederholungsintervall einrichten. Klicken Sie <b>"bearbeiten"</b>, um das Popup-Fenster mit den Einstellungen für das Wiederholungsintervall zu öffnen.',
            'myBeforeChangeFunction': function() { 
                //check if the clickable element has been triggered and show next step
                let object = document.querySelector('#edit_schedule_icon_button');
                if(object){
                    object.addEventListener('click',e => {
                    TaskmanagerHelp.instance.introguide.goToStep(9)
                       },{ once: true })
                   }
           },
           'myAfterChangeFunction': function() { 
            // make modal clickable
            $(".introjs-overlay").css('z-index', '1000')
            $(".introjs-helperLayer").css('z-index', '1000')
            $("#modal-schedule").css('z-index', '999999') 
         }
        },
        {//9
            'element': '#date-fieldset',
            'intro': 'Wählen Sie ein Startdatum und dann Intervall, z. B. jede Woche, und die Häufigkeit, mit der es wiederholt werden soll. Danach, klicken Sie auf <b>Auswahl übernehmen</b>, um diese Einstellungen zu speichern',
            'position': 'bottom',
            'myBeforeChangeFunction': function() { 
                $("#edit_schedule_icon_button").click()

                //check if the clickable element has been triggered and show next step
                let object_a = document.querySelector('#save_schedule_button');
                let object_b = document.querySelector('#cancel_schedule_button')
                let object_c = document.querySelector('button.close')
                if(object_a){
                    object_a.addEventListener('click',e => { TaskmanagerHelp.instance.introguide.goToStep(10)},{ once: true })
                    object_b.addEventListener('click',e => { TaskmanagerHelp.instance.introguide.goToStep(10)},{ once: true })
                    object_c.addEventListener('click',e => { TaskmanagerHelp.instance.introguide.goToStep(10)},{ once: true })
                   }
            }
        },
        {//10
            'element': '#save_group_button',
            'intro': 'Wenn Sie alle gewünschten Einstellungen vorgenommen haben, klicken Sie auf <b>speichern</b>, um das Vorlagenpaket zu erstellen.',
            'myBeforeChangeFunction': function() { 
                $("#save_schedule_button").click()
                
                //check if the clickable element has been triggered and show next step
                let object = document.querySelector('#save_group_button');
                if(object){
                    object.addEventListener('click', function (e) {
                        TaskmanagerHelp.instance.introguide.goToStep(11)
                    },{ once: true })
                   }
            }

        },
        {//11
            'element': '.card-body',
            'intro': 'Ihre neues Aufgabenpaket wird <b>der Datentabelle hinzugefügt</b>. Dort haben Sie auch die Möglichkeit, Ihre vorhandenen Ressourcen zu <b>bearbeiten</b> oder zu <b>löschen.</b>. <b>Execute</b> führt das Vorlagenpaket sofort einmal aus',
            'myBeforeChangeFunction': function() {
                //check if we have saved already
                if($('#save_group_button').is(":visible")){
                    $("#save_group_button").click()
                } 
             }
 
        },
        {//12
            'title': 'Großartig!',
            'intro': 'Jetzt wissen Sie, wie Sie Aufgabenpaketen erstellen kann. <b>Fertig</b> bringt Sie zum Help Center, wo Sie weitere Touren finden',
        }

    ]


    resource_tour_steps = [
        {
            'title': 'Was ist eine </br> Ressource?',
            'intro': 'Es kann ein Hotelzimmer oder eine Kaffeemaschine sein. Es liegt an Ihnen. Lassen Sie uns <b>eine neue Ressource erstellen</b>. Klicken Sie einfach auf die Pfeilschaltflächen, um durch die Tour zu gelangen. Klicken Sie auf <strong> x</strong>, um die Tour abzubrechen.', 
        },
        {
            'element': '.component-item',
            'intro': 'Suchen Sie Ihre Ressourceneinstellungsseite entweder über das <b>Zahnradsymbol im Header</b> Ihres Aufgabenmanagers oder über das linke <b>Einstellungsmenü.</b>',
        },
        {
            'element': '#btn-create-resource',
            'intro': 'Erstellen Sie eine <b>neue Ressource</b>, indem Sie auf <b>Ressource anlegen</b> klicken. Dadurch wird ein leeres Dialogformular geöffnet.',
            'myBeforeChangeFunction': function() { 
                 //check if the clickable element has been triggered and show next step
                 let object = document.querySelector('#btn-create-resource');
                 if(object){
                     object.addEventListener('click',e => {
                     TaskmanagerHelp.instance.introguide.goToStep(3)
                        },{ once: true })
                    }
            }, 
            'myAfterChangeFunction': function() { 
               // make modal clickable
               //$(".introjs-overlay").css('z-index', '1000')
               $("#modalResourceDetails").css('z-index', '999999') 
            }
        },
        {
            'element': '#resource_name',
            'intro': 'Geben Sie Ihrer Ressource einen <b>erkennbaren Namen</b>, z. B. eine Adresse.',
            'myBeforeChangeFunction': function() { 
                $("#btn-create-resource").click()
            },
            'myAfterChangeFunction': function() {
                $("#resource_name").val("Demo Ressource 101")
            }
        },
        {
            'element': '#resource_tag',
            'intro': 'Fügen Sie alle <b>Tags</b> hinzu, die Ihnen bei der <b>Kategorisierung</b> dieser Ressource helfen.',
            'myAfterChangeFunction': function() {
                $("#resource_tag").val("1. Stock, Doppelzimmer")
            }
        },
        {
            'element': '#resource_type',
            'intro': 'Sie können aus voreingestellten <b>Ressourcentypen auswählen</b>, um Ihre Ressourcen weiter zu organisieren.',
        },
        {
            'element': '#btn-update-resource',
            'intro': 'Klicken Sie auf <b>Speichern</b>, um Ihre Ressource zu erstellen.',
            'myBeforeChangeFunction': function() { 
                //check if the clickable element has been triggered and show next step
                let object = document.querySelector('#btn-update-resource');
                if(object){
                    object.addEventListener('click',e => {
                    TaskmanagerHelp.instance.introguide.goToStep(8)
                       },{ once: true })
                   }
           }, 
           
        },
        {
            'element': '#resource-table_wrapper',
            'intro': 'Ihre neue Ressource wird </b>der Datentabelle hinzugefügt<b>, in der Sie alle Ihre Einträge sortieren und durchsuchen können.',
            'myBeforeChangeFunction': function() { 
                $("#btn-update-resource").click()
            },
            
        },
        {
            'element': '#resource-table',
            'intro': 'In der Tabelle haben Sie auch die Möglichkeit, Ihre vorhandenen Ressourcen zu <b>bearbeiten</b> oder zu <b>löschen.</b>',
        },
        {
            'title': 'Großartig!',
            'intro': 'Jetzt wissen Sie, wie Sie eine neue Ressource erstellen kann. <b>Fertig</b> bringt Sie zum Help Center, wo Sie weitere Touren finden',
        }
    ]


    task_tour_steps = [
        {//1
            'title': 'Aufgaben Tour',
            'intro': 'Lassen Sie uns entdecken, wie Sie <b>Aufgaben verwalten</b> können. Klicken Sie einfach auf die Pfeilschaltflächen, um durch die Tour zu gelangen. Klicken Sie auf <strong> x</strong>, um die Tour abzubrechen.', 
        },
        {//2
            'element': '#button-new-task',
            'intro': 'Aufgaben können <b>aus Vorlagen erstellt</b> werden, die sich unter dem Dropdown-Menü der Schaltfläche befinden, aber zuerst erstellen wir eine <b> leere neue Aufgabe</b>. Klicken Sie <b> Task anlegen</b>.',
            'myBeforeChangeFunction': function() { 
                //make sure the view is correct if the back button is used from the next step
                $("#list-view").show()
                $("#task-detail-view").hide()
               
                 //check if the clickable element has been triggered and show next step
                 let object = document.querySelector('#button-new-task');
                 if(object){
                     object.addEventListener('click',e => {
                     //alert("selection clicked")
                     TaskmanagerHelp.instance.introguide.goToStep(3)
                        },{ once: true })
                    }
            }, 

        },

        {//3
            'element': '#select-list-name-dropdown',
            'intro': 'Stellen Sie sicher, dass die richtige <b>Liste</b> ausgewählt ist, in der Ihre Aufgabe gespeichert wird.',
            'myBeforeChangeFunction': function() { 
                $("#button-new-task").click()
                $(".tab-pane.active").css('animation', 'slide-down 0s')
            },
        },
        {//4
            'element': '#select-task-template',
            'intro': ' Sie können aus vorhandenen <b>Vorlagen</b> auswählen, um Ihre Aufgabe vorab auszufüllen.',
        },
        {//5
            'element': '#generalSettings',
            'intro': ' Geben Sie Ihrer Aufgabe einen <b>Titel</b> und fügen Sie eine <b>Beschreibung</b> hinzu. Wie in der Beispielbeschreibung, erstellen Sie Checklisten mithilfe eckigen Klammern [ ] vor einem Listenelement.',
            'myBeforeChangeFunction': function() { 
                $("#task-title").val("Meine neue Demo Aufgabe, als Vorlage gespeichert")
                Render.instance.task_description.clipboard.dangerouslyPasteHTML(0, "<p>Dies sind die Schritte zum Erstellen einer Aufgabe als Vorlage:</p><ul data-checked=\"false\"><li>Neue Aufgabe erstellen</li><li>Füllen Sie die Details aus</li><li>Als Vorlage speichern</li></ul>")
            },
        },
        {//6
            'element': '#specificSettings',
            'intro': ' Viele andere schnelle Einstellungen wie <b>Status</b> oder <b>Fälligkeitsdatum</b> stehen Ihnen zur Verfügung, um Ihre Aufgaben weiter zu organisieren.',
        },
        {//7
            'element': '#resource_item',
            'intro': ' Wenn Sie diese Aufgabe mit einer bestimmten <b>Ressource </b> verbinden möchten, wählen Sie sie aus der Liste aus.',
        },
       
        {//8
            'element': '#contact_item',
            'intro': ' Sie können jeden Kontakt verknüpfen, der sich in Ihrer <b>Kontaktmanager-App</b> befindet.',
        },
        {//9
            'element': '#event_item',
            'intro': ' Sie können einen vorab eingegebenen Termin in der <b>Kalender-App</b> auswählen, um ihn mit dieser Aufgabe zu verknüpfen.',
        },
        
        {//10
            'element': '#btn-group-task-save-as-template',
            'intro': ' Jetzt können Sie auswählen, ob diese Aufgabe als <b>Vorlage</b> gespeichert werden soll. Klicken Sie auf den Pfeil, um die weitere Option <b>Als Vorlage speichern</b> anzuzeigen.',
            position: 'left',
            'myBeforeChangeFunction': function() { 
               
                 //check if the clickable element has been triggered and show next step
                 let object2 = document.querySelector('#select-save-as-template');

                if(object2){
                    object2.addEventListener('click',e => {
                        TaskmanagerHelp.instance.introguide.goToStep(11)
                           },{ once: true })
                       }
                },
                'myAfterChangeFunction': function() { 
                    // make menu clickable
                    $(".introjs-overlay").css('z-index', '10')
                    $(".introjs-helperLayer").css('z-index', '10')
                }
            
        }, 
            
        {//11
            'element': '#save_task_as_template',
            'intro': ' Geben Sie Ihrer Vorlage <b>einen Namen</b> und klicken Sie <b> Speichern</b>. Vorlagen können auf der Seite mit den Vorlageneinstellungen gelöscht werden.',

            'myBeforeChangeFunction': function() { 
                //make sure modal is visable
                $("#select-save-as-template").click()
                //set generic data in fields
                $("#template_name").val("Meine neue Vorlage")
                $("#request_make_task").prop('checked', true)
                
                //check if the clickable element has been triggered and show next step
                let object = document.querySelector('#button_save_template');

                if(object){
                    object.addEventListener('click',e => {
                        TaskmanagerHelp.instance.introguide.goToStep(12)
                        //alert("step 11 manual save click")
                           },{ once: true })
                       }
                },
                'myAfterChangeFunction': function() { 
                    // make modal clickable
                    $(".introjs-overlay").css('z-index', '1000')
                    $(".introjs-helperLayer").css('z-index', '1000')
                    $("#save_task_as_template").css('z-index', '9999999') 
                }
            
        },
        
        {//12
            "title": "Großartig!",
            'intro': ' Ihre neue Aufgabe wurde als Vorlage gespeichert und am Ende der Liste hinzugefügt. ',
            'position': 'bottom' ,
            'myBeforeChangeFunction': function() { 
                //alert("step 12 auto save click")
                $("#button_save_template").click()
            }
            
        }]

    startTour(steps) {
        console.log("TOURSTEPS", steps)
        let tour_steps = steps.filter(function (obj) {
            //filter elements hidden by permissions
            let list = $(obj.element).hasClass("hidden") === false
            console.log("List", list)
            return $(obj.element).hasClass("hidden") === false || obj.element == ".introjsFloatingElement"
        })
        console.log("STEPS", tour_steps)
        let introguide = introJs()
        introguide.setOptions({
            showBullets: false,
            nextLabel: ">",
            prevLabel: "<",
            showProgress: true,
            doneLabel: "Fertig",
            exitOnOverlayClick: false,
            disableInteraction: true,
            keyboardNavigation: false,
            tooltipClass: 'customTooltip',
            steps: tour_steps,
        })
        introguide.start()   

        introguide.onbeforeexit(function () {
            introJs().addHints().setOptions({
                hintButtonLabel: "Alles klar"
            }).hideHints().showHint(0).onhintclose(function() { 
                $('#modal_help').modal('toggle');
              });
        })

        introguide.oncomplete(function () {
            $('#modal_help').modal('toggle');
        })

    }

    startInteractiveTour(steps) {
        console.log("TOURSTEPS", steps)
        let tour_steps = steps.filter(function (obj) {
            //filter elements hidden by permissions
            let list = $(obj.element).hasClass("hidden") === false
            console.log("List", list)
            return $(obj.element).hasClass("hidden") === false || obj.element == ".introjsFloatingElement"
        })
        console.log("STEPS", tour_steps)
        TaskmanagerHelp.instance.introguide = introJs()
        TaskmanagerHelp.instance.introguide.setOptions({
            showBullets: false,
            nextLabel: ">",
            prevLabel: "<",
            showProgress: true,
            doneLabel: "Fertig",
            exitOnOverlayClick: false,
            disableInteraction: false,
            keyboardNavigation: false,
            tooltipClass: 'customTooltip',
            steps: tour_steps,
        })
        TaskmanagerHelp.instance.introguide.start()

        TaskmanagerHelp.instance.introguide.onbeforechange(function(){ 
            if(this._introItems[this._currentStep].myBeforeChangeFunction){
                this._introItems[this._currentStep].myBeforeChangeFunction();
            }
            }).onchange(function() { 
            if (this._introItems[this._currentStep].myChangeFunction){
                this._introItems[this._currentStep].myChangeFunction();
            }
            }).onafterchange(function() { 
            if (this._introItems[this._currentStep].myAfterChangeFunction){
                this._introItems[this._currentStep].myAfterChangeFunction();
            }
        })

        TaskmanagerHelp.instance.introguide.onbeforeexit(function () {
            introJs().addHints().setOptions({
                hintButtonLabel: "Alles klar"
            }).hideHints().showHint(0).onhintclose(function() { 
                $('#modal_help').modal('toggle');
              });
        })

        TaskmanagerHelp.instance.introguide.oncomplete(function () {
            $('#modal_help').modal('toggle');
        })
        
    }



    initListeners() {

    
        $('#help_tour_get_started, #tour_get_started').on('click', function (e) {
            e.preventDefault();
            //check if requested tour is on current page if not, create and launch correct url
            TaskmanagerHelp.instance.createTourUrl("/task-manager/index.html", "help_tour_get_started")

            var settings = {
                "url": CONFIG.APP_BASE_URL + "/tours/taskmanager-get-started.json",
                "method": "GET",
                "timeout": 0,
                "success": function (response) {
                    let tour_steps = JSON.parse(response)
                    $("#list-view").show()//set up correct view
                    $("#task-detail-view").hide()
                    $("#task-list-section").prop('selectedIndex', 1).change()//make sure a list is selected
                    TaskmanagerHelp.instance.startTour(tour_steps)
                    $('#modal_help').modal('hide');//close help menues
                    $('#taskmanager_welcome').hide();
                },
                "error": () => alert("Cannot load tour-data")
            };
            $.ajax(settings)
        });


        $('#help_tour_tasks').on('click', function (e) {
            e.preventDefault();
            // check if requested tour is on current page if not, create and launch correct url
            TaskmanagerHelp.instance.createTourUrl("/task-manager/index.html", "help_tour_tasks")
            $("#list-view").show()
            $("#task-detail-view").hide()
            TaskmanagerHelp.instance.startInteractiveTour(TaskmanagerHelp.instance.task_tour_steps)
            $('#modal_help').modal('hide');
            $('#taskmanager_welcome').hide();
        });

        $('#help_tour_lists').on('click', function (e) {
            e.preventDefault();
            // check if requested tour is on current page if not, create and launch correct url
            TaskmanagerHelp.instance.createTourUrl("/task-manager/list-settings.html", "help_tour_lists")

            var settings = {
                "url": CONFIG.APP_BASE_URL + "/tours/taskmanager-lists.json",
                "method": "GET",
                "timeout": 0,
                "success": function (response) {
                    let tour_steps = JSON.parse(response)
                    $("#button-new-list").click()//make sure form is empty
                    $("#list-container a.nav-link").removeClass('active')
                    TaskmanagerHelp.instance.startTour(tour_steps)
                    $('#modal_help').modal('hide');
                },
                "error": () => alert("Cannot load tour-data")
            };
            $.ajax(settings)

        });

        $('#help_tour_templates').on('click', function (e) {
            e.preventDefault();
            // check if requested tour is on current page if not, create and launch correct url
            TaskmanagerHelp.instance.createTourUrl("/task-manager/template-settings.html", "help_tour_templates")
            TaskmanagerHelp.instance.startInteractiveTour(TaskmanagerHelp.instance.template_tour_steps)
            $('#modal_help').modal('hide');

        });

        $('#help_tour_template_groups').on('click', function (e) {
            e.preventDefault();
            // check if requested tour is on current page if not, create and launch correct url
            TaskmanagerHelp.instance.createTourUrl("/task-manager/template-group-settings.html", "help_tour_template_groups")
            TaskmanagerHelp.instance.startInteractiveTour(TaskmanagerHelp.instance.template_group_tour_steps)
            $('#modal_help').modal('hide');
        });

        $('#help_tour_resources').on('click', function (e) {
            e.preventDefault();
            // check if requested tour is on current page if not, create and launch correct url
            TaskmanagerHelp.instance.createTourUrl("/task-manager/resource-settings.html", "help_tour_resources")
            TaskmanagerHelp.instance.startInteractiveTour(TaskmanagerHelp.instance.resource_tour_steps)
            $('#modal_help').modal('hide');
        });


    }



}