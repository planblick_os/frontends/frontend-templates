import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {getCookie, setCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class RenderGuestForm {
    constructor() {
        if (!RenderGuestForm.instance) {
            RenderGuestForm.instance = this
            RenderGuestForm.instance.tm = new TaskManager()
            RenderGuestForm.instance.task_id_handlers = {}
            RenderGuestForm.instance.initListeners()
        }
        if ($('#task_description').length) {
            RenderGuestForm.instance.task_description = new Quill('#task_description', {
                modules: {
                    toolbar: ['bold', 'italic']
                }, placeholder: new pbI18n().translate('Sagen sie uns hier was wir für sie tun können'), theme: 'bubble'
            })
        }

        RenderGuestForm.instance.checkForOpenTasks()

        return RenderGuestForm.instance
    }

    checkForOpenTasks() {
        $("#tasks_status_view").addClass("hidden")
        if (getCookie("guest_created_task_ids")) {
            Fnon.Box.Infinity('#tasks_status_view');
            let task_ids = getCookie("guest_created_task_ids").split("_")
            $("#task_status_list").html("")
            let promises = []
            for (let task_id of task_ids) {
                promises.push(RenderGuestForm.instance.showStatusOfTaskWithId(task_id))
            }
            Promise.all(promises).then(function (results) {
                $("#tasks_status_view").removeClass("hidden")
               Fnon.Box.Remove('#tasks_status_view')
            })
        }
    }

    showStatusOfTaskWithId(id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": `${CONFIG.API_BASE_URL}/tasks/by_id/${id}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": function (response) {
                    let new_entry = $("<li>")
                    let badge_class = ""
                    new_entry.html(`${response.task_data.title}<br>${response.task_data.description}<br>Status: ${RenderGuestForm.instance.displayListViewStatus(response.task_data.status)}<hr>`)
                    $("#task_status_list").append(new_entry)
                    resolve()
                },
                "error": ()=>reject
            }

            $.ajax(settings)
        })
    }

    displayListViewStatus(value) {
        let statusBadge
        statusBadge = `<span class="badge badge-success badge_${value}">${new pbI18n().translate(value)}</span>`
        return statusBadge
    }

    fillInTemplateData(template_data) {
        RenderGuestForm.instance.template_data = template_data
        RenderGuestForm.instance.tm.active_queue_list_id = template_data.queue_id
        $('<option/>')
            .val(template_data.queue_id)
            .text(template_data.queue_id)
            .prop('selected', true)
            .appendTo('#select-list-name-dropdown')

        $("#task-title").val(template_data.title)

        RenderGuestForm.instance.task_description.clipboard.dangerouslyPasteHTML(0, template_data.description)

        $('<option/>')
            .val(template_data.resource)
            .text(template_data.resource)
            .prop('selected', true)
            .appendTo('#select-task-resource')

        $('#select-task-resource').val(template_data.resource)

        $('#select-task-status').val(template_data.status)

        $('<option/>')
            .val(template_data.assigned_to)
            .text(template_data.assigned_to)
            .prop('selected', true)
            .appendTo('#select-task-responsible')
    }

    initListeners() {
        $(document).on('click', '#button-edit-task', function (e) {
            RenderGuestForm.instance.saveQueueEntry()

        })

        RenderGuestForm.instance.tm.socket.commandhandlers["refreshQueueEntries"] = function (args) {
            if (RenderGuestForm.instance.task_id_handlers[args.task_id]) {
                RenderGuestForm.instance.task_id_handlers[args.task_id](args)
            }
            RenderGuestForm.instance.checkForOpenTasks()
        }
    }

    saveQueueEntry() {
        Fnon.Wait.Init({
            fontFamily: '"Roboto", sans-serif',
            textColor: '#3a22fa',
            svgSize: {w: '50px', h: '50px'},
            svgColor: '#3a22fa',
            textSize: '12px',
            clickToClose: false,
        })
        Fnon.Wait.Circle('', {
            svgSize: {w: '50px', h: '50px'},
            svgColor: '#3a22fa',
        })

        let descriptionHTML = RenderGuestForm.instance.task_description.container.firstChild.innerHTML
        let assigned_to = $("#select-task-responsible option").filter(':selected').val()
        let due_date = RenderGuestForm.instance.template_data.due_date
        let due_time = null
        let order = 0
        let resource = $("#select-task-resource option").filter(':selected').val()
        let data_owners = []
        RenderGuestForm.instance.tm.createNewQueueEntry(
            $("#task-title").val(),
            descriptionHTML,
            $('#select-task-status').val(),
            assigned_to,
            due_date,
            due_time,
            order,
            resource,
            RenderGuestForm.instance.template_data.links,
            data_owners).then(function (data) {
            RenderGuestForm.instance.task_id_handlers[data] = (args) => {
                let guest_created_task_ids = getCookie("guest_created_task_ids")
                if (guest_created_task_ids) {
                    guest_created_task_ids += `_${args.queue_entry_id}`
                } else {
                    guest_created_task_ids = args.queue_entry_id
                }
                let currentDate = new Date();
                let expirationDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1, 0, 0, 0);
                setCookie("guest_created_task_ids", guest_created_task_ids, {path: '/', 'expires': expirationDate})
            }
            Fnon.Alert.Success(new pbI18n().translate("Ihre Anfrage ist bei uns eingegangen und wir kümmern uns schnellstmöglich darum. Wenn sie diese Seite aktualisieren können sie den Status ihrer Anfrage einsehen."), 'Übermittlung erfolgreich', "Ok", () => {
                location.reload()
            });

        }).catch(function () {
            Fnon.Alert.Danger({
                title: 'Ooooops...',
                message: new pbI18n().translate("Oops da ist etwas schief gegangen. Wir aktualisieren jetzt diese Seite und möchten sie bitten es anschließend noch einmal zu versuchen."),
                callback: () => {
                    location.reload(true)
                }
            });

        })
    }
}