{% for entry in entries %}
<li data-selector="list-entry" class="task-element tickets-card d-flex justify-content-start" id="{{task-id}}">
    <div class="w-100">
        <div class="row">
            <div class="tickets-details col-md-6 col-sm-12 align-self-center">
                <div class="wrapper">
                    <a target="_blank" href="./?queue_id={{entry.entry_data.queue_id}}&task_id={{entry.entry_id}}">{{entry.entry_data.title}}</a>
                </div>
                <div class="wrapper text-muted d-none d-md-block">
                    <span class="dotted-text"><p>{{entry.entry_data.description | safe}}</p></span>
                </div>
            </div>
            <div class="col-md-3 col-5 align-self-start">
                <span class="badge badge-secondary badge_{{entry.entry_data.status}}" data-i18n="{{entry.entry_data.status}}">{{entry.entry_data.status}}</span>
                <div class="text-muted text-flex">
                    <i class="far fa-clock"></i> {{entry.entry_data.create_time | datenotime}}
                </div>
            </div>
            <div class="col-md-3 col-7 text-right align-self-start">
                <span class="text-flex">
                    {% if entry.entry_data.assigned_to|is_array  %}
                        {% for assignee in entry.entry_data.assigned_to %}
                        <i class="far fa-user"></i> {{assignee}}
                        {% endfor %}
                    {% endif %}
                </span>
                <div class="text-flex text-muted">
                    {{entry.entry_data.resource_data.name}}
                    {% for id, tag in entry.entry_data.resource_data.tags %}
                        <span class="badge badge-data-tags">{{tag}}</span>
                    {% endfor %}

                </div>
                <div class="text-flex text-muted">
                    <span class="badge badge-data-tags">{{entry.entry_data.tags}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="action-dropdown dropdown">
        <button type="button" class="dropdown-toggle" id="portlet-action-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="icon ion-ios-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="portlet-action-dropdown">
            <a class="dropdown-item" href="#" data-id="{{entry.entry_id}}" data-name="quickaction_assigntome_search">Mir zuweisen</a>
            <a class="dropdown-item" href="#" data-id="{{entry.entry_id}}" data-name="quickaction_planned_search">Geplant</a>
            <a class="dropdown-item" href="#" data-id="{{entry.entry_id}}" data-name="quickaction_inprogress_search">In Bearbeitung</a>
            <a class="dropdown-item" href="#" data-id="{{entry.entry_id}}" data-name="quickaction_pending_search">Wartend</a>
            <a class="dropdown-item" href="#" data-id="{{entry.entry_id}}" data-name="quickaction_done_search">Erledigt</a>
            <a class="dropdown-item" href="#" data-id="{{entry.entry_id}}" data-name="quickaction_canceled_search">Verwerfen</a>
        </div>
    </div>
</li>

{% endfor %}