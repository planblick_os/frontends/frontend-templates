import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]";
import {getCookie, fireEvent, getQueryParam} from "../../csf-lib/pb-functions.js?v=[cs_version]";
import {validateForm} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {togglePassword} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"

export class LoginModule {
    actions = {}
    constructor() {
        if (!LoginModule.instance) {
            this.initListeners()
            this.initActions()
            LoginModule.instance = this;
        }

        return LoginModule.instance;
    }



    initActions() {
        this.actions["loginModuleInitialised"] = function (myevent, callback = () => "") {
            if(getCookie("apikey")) {
                location.href = CONFIG.DEFAULT_LOGIN_REDIRECT
            }

            if(CONFIG.REGISTER_URL && CONFIG.REGISTER_URL.length > 0) {
                $("#register_now_button").attr("href", CONFIG.REGISTER_URL)
            } else {
                $("#register_now_button").parent().hide()
            }

            if ($("#loader-overlay")) {
                $(".az-signin-wrapper").show()
                $(".az-footer").show()
            }
            callback(myevent)
        }

        this.actions["loginButtonClicked"] = function(myevent, callback=() => "") {
            let urlParams = new URLSearchParams(window.location.search)
            let redirect = true
            if(urlParams.has('r')) {
                redirect = urlParams.get('r')
            }
            let rememberMe = $("#remember_me").is(":checked")

            new PbAccount().getapikey(redirect,() => "", rememberMe).catch(()=>"")
            callback(myevent)
        }

        this.actions["staticLoginButtonClicked"] = function(myevent, callback=() => "") {
            let urlParams = new URLSearchParams(window.location.search)
            let redirect = true
            if(urlParams.has('r')) {
                redirect = urlParams.get('r')
            }
            let rememberMe = true

            new PbAccount().getapikey(redirect,() => "", rememberMe, "/login/static").catch(()=>"")
            callback(myevent)
        }

        this.actions["initPasswortReset"] = function(myevent, callback=() => "") {
            $("#loader-overlay").hide()
            $(".az-signin-wrapper").show()
            $(".az-footer").show()

            callback(myevent)
        }
    }

    initListeners() {
        $("#register_button").click(function(event) {
            event.preventDefault()
            fireEvent("loginButtonClicked", {"login": $("#login").val(), "password": $("#password").val()})
        })

        $("#static_login_button").click(function(event) {
            event.preventDefault()
            fireEvent("staticLoginButtonClicked", {"login": $("#login").val(), "password": $("#password").val()})
        })

        $("#reset_password_button").click(function(event) {
            event.preventDefault()
            $("#error-msg").hide()
            if (validateForm({'password': 'password'}) == false) {
                document.getElementById('error-msg').innerHTML = "<div class='alert alert-danger error_message'>*Das Passwort muss mindestens 8 Zeichen ohne Lehrzeichen, mit mindestens 1 Groß-, 1 Kleinbuchstaben, 1 Ziffer und 1 Sonderzeichen enthalten.*</div>"
                $("#error-msg").show()
            } else if (validateForm({ 'password_repeat': 'input' }) == false || $("#password").val() != $("#password_repeat").val()) {
                document.getElementById('error-msg').innerHTML = "<div class='alert alert-danger error_message'>*Das neues Passwort und das Passwort-Bestätigung stimmen nicht überein.*</div>"
                $("#error-msg").show()
            } else {
                $("#error-msg").hide()
                Fnon.Box.Circle('.az-signin-wrapper','', {
                    svgSize: { w: '50px', h: '50px' },
                    svgColor: '#3a22fa',
                })

                new PbAccount().changePassword(getQueryParam("u") || $.cookie("username"), $("#password").val()).then(()=> {
                    location.href = "/login/"
                }, ()=> {
                    Fnon.Box.Remove('.az-signin-wrapper')
                })
            }
        })

        $('#start_lost_password_process_button').click( e => {
            e.preventDefault()
            const callback = () => {}
            const login = $('#login').val()
            
            if(validateForm({'login': 'input'}) == true) {
                $('#successMessage').show()
                async function f() {
                    try {
                        await new PbAccount().startLostPasswordProcess(
                          login,
                          callback,
                          callback
                        );
                      } catch (e) {
                        /* console.error("Password reset failed", e.status, e.statusText); */
                        
                      }
                } 
                f()
                
            }
            
        })

        $('#login').focus(() => {
            if( $('#successMessage').is(':visible')){
                $('#successMessage').hide()
            }
        })

        $("#pw-toggle").click(function () {
            togglePassword("password", "pw-toggle")
        })
    }
    
}