apikey = $.cookie("apikey");
initiator = $.cookie("consumer_name")
updateTimer = 0
document_data = ""
current_task = false

var wrapper = document.getElementById("action_wrapper");
var dev_wrapper = document.getElementById("dev_tools");

current_message = ""

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}


function getTask(document_id) {
    //If current_task is not false we already have a document in process
    console.log("Getting task...")
    initiator = $.cookie("consumer_name") + "." + $.cookie("username");

    if (current_task != false)
        return;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": API_BASE_URL + "/engine-rest/task",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "apikey": $.cookie("apikey"),
            "Cache-Control": "no-cache"
        },
        "processData": false,
        "data": "{\"processInstanceId\": \"" + document_id + "\", \"taskDefinitionKey\": \"fill_out_document\",\r\n\t\"processVariables\":\r\n    [\r\n    \t{\r\n    \t\t\"name\": \"initiator\",\r\n    \t\t\"value\": \"" + initiator + "\",\r\n    \t\t\"operator\": \"eq\"\r\n    \t}\r\n    ],\r\n\t\"sorting\":\r\n    [\r\n    \t{\r\n    \t\t\"sortBy\": \"created\",\r\n    \t\t\"sortOrder\": \"asc\"\r\n    \t}\r\n    ]\r\n}"
    }

    $.ajax(settings).done(function (response) {
        if (response.length > 0) {
            console.log(response[0].id);
            current_task = response[0].id
            buildCanvasForTask(response[0].id)
            $.unblockUI()
            waitingPopUpActive = false;
        } else {
            //setBlockWaitingForDocuments();
            //showWaitingForDocumentsTimer = window.setTimeout(function() {setBlockWaitingForDocuments();}, 10000);
            $.blockUI({
                message: '<div class="responseMessage">Vielen Dank dass Sie Easy2Sign genutzt haben.<br/>Das Dokument wird jetzt dem Absender zugestellt.<br/>Sie können diese Seite nun <a href="#" onclick="window.close();">schließen</a>.</div>',
                timeout: 10000
            });
        }

    });
}

function buildCanvasForTask(task_id) {
    current_task = task_id
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": API_BASE_URL + "/engine-rest/task/" + task_id + "/variables?deserializeValues=false",
        "method": "GET",
        "headers": {
            "Content-Type": "application/json",
            "apikey": $.cookie("apikey"),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {
        if (response.pages.type == "Object") {
            return;
        }
        json_payload = JSON.parse(response.pages.value);
        document_data = json_payload
        if (typeof(json_payload.pages) != "undefined") {
            for (i = 0; i < json_payload.pages.length; i++) {
                //console.debug(json_payload.pages[i]);
                addTemplate(json_payload.pages[i]);
            }
            buildCanvasesFromTemplates(templates);
            $.unblockUI();
            wrapper.style.display = "block";
            $.blockUI({
                message: 'Weiter gehts!',
                timeout: 2000
            });
        }
    });
}

function completeTask() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": API_BASE_URL + "/engine-rest/task/" + current_task + "/complete",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "apikey": $.cookie("apikey"),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        current_task = false;
        $.blockUI({message: '<div class="responseMessage">Vielen Dank dass Sie Easy2Sign genutzt haben.<br/>Das Dokument wird jetzt dem Absender zugestellt.<br/>Sie können diese Seite nun schließen.</div>'});

        if(getCookie("returnUrl") && getCookie("returnUrl") != "null") {
            document.location = decodeURIComponent(getCookie("returnUrl"))
        }
    });
}
