apikey = $.cookie("apikey");


function changePasswordDialog() {
    $.blockUI({message: '<div class="password-change-dialog_wrapper">' +
            '<span class="new-password-input-label">Aktuelles Passwort:</span><input id="old_password" type="password"/><br/>' +
            '<span class="new-password-input-label">Neues Passwort:</span><input id="new_password" type="password"/><br/>' +
            '<span class="new-password-confirm-input-label">Neues Passwort Bestätigen:</span><input id="new_password_confirm" type="password"/><br/>' +
            '<br/><button onclick="submitPasswordDialog();">Passwort ändern</button><button onclick="$.unblockUI();">Abbrechen</button></div>'
    });

}

function submitPasswordDialog() {
    let func = function(apikey) {
        if(apikey == false) {
            $.blockUI({message: '<div class="responseMessage">Das angegebene Passwort stimmt nicht.</div>'});
            setTimeout(function(){ $.unblockUI(); }, 2000)
        } else if($("#new_password").val() != $("#new_password_confirm").val()) {
            $.blockUI({message: '<div class="responseMessage">Passwort und Passwort-Bestätigung stimmen nicht überein.</div>'});
            setTimeout(function(){ $.unblockUI(); }, 2000)
        } else if(!isStrongPwd($("#new_password").val()))  {
            $.blockUI({message: '<div class="password-not-safe-modal">Das Passwort ist nicht sicher. Es muss mindestens 8 Zeichen lang sein und mindestens einen Groß-, einen Kleinbuchstaben und eine Ziffer enthalten.<br/><br/><button onclick="$.unblockUI();">Okay</button></div>'});
        } else {
            $.unblockUI();
            changePassword($.cookie("username"), $("#new_password").val());
        }
    }
    getapikeyfor($.cookie("username"), $("#old_password").val(),func)
}

function changePassword(username, password, onsuccess=null, onfailure=null) {
    let data = {
        "username": username,
        "password": password
    }
    $.blockUI({message: '<div class="responseMessage">Passwort wird aktualisiert.</div>'});
    var settings = {
        "url": API_BASE_URL + "/update_credentials",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
            "apikey": $.cookie("apikey")
        },
        "data": JSON.stringify(data),
        "success": function() {
            $.blockUI({message: '<div class="responseMessage">Passwort wurde aktualisiert.</div>'});
            if(onsuccess != null) {
                onsuccess()
            } else {
                setTimeout(function(){ $.unblockUI(); }, 2000)
            }
        },
        "error": function() {
            $.blockUI({message: '<div class="responseMessage">Passwort konnte nicht aktualisiert werden, bitte versuchen sie es später noch einmal.</div>'});
            if(onsuccess != null) {
                onfailure()
            } else {
                setTimeout(function(){ $.unblockUI(); }, 2000)
            }
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
    });


}

function isStrongPwd(password) {

     var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
     var lowercase = "abcdefghijklmnopqrstuvwxyz";
     var digits = "0123456789";
     var splChars ="!@#$%&*()";
     var ucaseFlag = contains(password, uppercase);
     var lcaseFlag = contains(password, lowercase);
     var digitsFlag = contains(password, digits);
     var splCharsFlag = contains(password, splChars);

     if(password.length>=8 && ucaseFlag && lcaseFlag && digitsFlag && splCharsFlag)
           return true;
     else
           return false;

  }

function contains(password, allowedChars) {
    for (i = 0; i < password.length; i++) {
            var char = password.charAt(i);
             if (allowedChars.indexOf(char) >= 0) { return true; }
         }
     return false;
}

function checkLogin(isLoginPage = false, checkagainTime = 30000, health_endpoint = "/has_valid_login") {
    if (typeof (apikey) == "undefined") {
        redirect_file = "index.html"
        current_url = window.location.pathname;
        current_filename = current_url.substring(current_url.lastIndexOf('/') + 1);
        if (redirect_file != current_filename) {
            $.blockUI({
                message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>',
                timeout: 3000,
                onUnblock: function () {
                    $(location).attr('href', '/login/')
                }
            });
        }
    } else {
        var settings = {
            "async": false,
            "crossDomain": true,
            "url": API_BASE_URL + health_endpoint,
            "method": "GET",
            "headers": {
                "apikey": $.cookie("apikey"),
                "Cache-Control": "no-cache"
            }
        }

        $.ajax(settings).success(function (response) {
            setTimeout(checkLogin, checkagainTime, isLoginPage, checkagainTime);
            if (isLoginPage) {
                redirectDialog()
            }
        });
        $.ajax(settings).error(function (response) {
            if (!isLoginPage) {
                $.blockUI({
                    message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>',
                    timeout: 3000,
                    onUnblock: function () {
                        $(location).attr('href', '/login/')
                    }
                });
            }
        });
    }
}

function logout() {

    apikey = $.cookie("apikey");

    result = false

    postUrl = API_BASE_URL + "/logout";
    $.ajax({
        type: "GET",
        url: postUrl,
        contentType: 'application/json;charset=UTF-8',
        data: "",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('apikey', apikey);
        },
        success: function (response) {
            if (typeof (response) != "undefined") {
                $.removeCookie("apikey", {path: '/'});
                $.blockUI({
                    message: '<div class="responseMessage">Sie wurden abgemeldet.</div>',
                    timeout: 3000,
                    onUnblock: function () {
                        $(location).attr('href', '/login/')
                    }
                });
            } else {
                $.removeCookie("apikey", {path: '/'});
                $.blockUI({message: '<div class="responseMessage">Sie wurden  abgemeldet.</div>'});

            }
        },
        error: function () {
            $.removeCookie("apikey", {path: '/'});
            $.blockUI({message: '<div class="responseMessage">Ihr Browser wurde  abgemeldet.</div>'});
        }
    }).done(function (o) {

    });


}

function getapikey(success_redirect = null) {
    apikey = $.cookie("apikey");
    console.debug(apikey)
    username = document.getElementById("login").value;
    password = document.getElementById("password").value;

    $.blockUI({message: '<div class="responseMessage">Ihre Zugangsdaten werden überprüft ...</div>'});
    postUrl = API_BASE_URL + "/login";

    $.ajax({
        type: "GET",
        url: postUrl,
        contentType: 'application/json;charset=UTF-8',
        data: "",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
        },
        success: function (response) {
            if (typeof (response) != "undefined") {
                $.blockUI({message: '<div class="responseMessage">Login successfull</div>', timeout: 1000});
                $.cookie("apikey", response.apikey, {path: '/'});
                $.cookie("consumer_name", response.consumer_name, {path: '/'});
                $.cookie("consumer_id", response.consumer.id, {path: '/'})
                $.cookie("username", response.username, {path: '/'});
                //$(location).attr('href', 'beta.html')
                if (success_redirect) {
                    redirectDialog(success_redirect)
                } else {
                    redirectDialog(DEFAULT_LOGIN_REDIRECT)
                }
            } else {
                $.removeCookie("apikey", {path: '/'});
                $.unblockUI()
                $.blockUI({
                    message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',
                    timeout: 3000
                });
            }
        },
        error: function () {
            $.removeCookie("apikey", {path: '/'});
            $.unblockUI()
            $.blockUI({
                message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',
                timeout: 3000
            });

        }
    }).done(function (o) {

    });
}

function redirectDialog(url = null) {
    if (url == null) {
        location.href = "/login/"
    } else {
        location.href = url
    }
}

function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

function perform_login() {
    urlParams = new URLSearchParams(window.location.search)
    if (urlParams.has('r')) {
        if(isValidHttpUrl(urlParams.get('r'))) {
            getapikey(urlParams.get('r'))
        } else {
            getapikey(APP_BASE_URL + urlParams.get('r'))
        }
    } else {
        getapikey()
    }
}

function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === "http:" || url.protocol === "https:";
}

function start_password_reset(account) {
    var settings = {
      "url": API_BASE_URL + "/init_credentials_lost_process",
      "method": "POST",
      "timeout": 0,
      "headers": {
        "Content-Type": "application/json"
      },
      "data": JSON.stringify({"username":account}),
    };

    $.ajax(settings).done(function (response) {
      console.log(response);
      if(response.result == true) {
          $.blockUI({message: '<div class="responseMessage">Halten Sie in Ihrem Posteingang nach einer Email von no-reply@planblick.com ausschau. Die Zustellung kann einige Minuten dauern.<br/>Folgen sie dann den Anweisungen in der Email um ihr Passwort zurück zu setzen.<br/><br/><strong>Tip:</strong> Email nicht erhalten? Überprüfen sie bitte ihren Spamordner.</div>'});
      } else {
          console.log(response)
          $.blockUI({message: '<div class="responseMessage">Ups, da hat etwas nicht funktioniert. Bitte versuchen sie es später noch einmal oder wenden sie sich an unseren Support. (Fehlercode: "'+response.error_code+'")</div>'});
          setTimeout(function(){ $.unblockUI(); }, 7000)
      }
    });
}

function set_new_passwort(password, password_repeat) {

    if(!isStrongPwd(password))  {
        $.blockUI({message: '<div class="password-not-safe-modal">Das Passwort ist nicht sicher. Es muss mindestens 8 Zeichen lang sein und mindestens einen Groß-, einen Kleinbuchstaben und eine Ziffer enthalten.<br/><br/><button onclick="$.unblockUI();">Okay</button></div>'});
        setTimeout(function(){ $.unblockUI(); }, 2000)
    } else if(password != password_repeat) {
        $.blockUI({message: '<div class="responseMessage">Passwort und Passwort-Bestätigung stimmen nicht überein.</div>'});
        setTimeout(function(){ $.unblockUI(); }, 2000)
    } else {
        redirect_funct = function() { setTimeout(function() { window.location.href="/login/" }, 2000) }
        changePassword($.cookie("username"), password, redirect_funct)
    }
}

function getapikeyfor(username, password, callback) {
    let postUrl = API_BASE_URL + "/login";
    $.ajax({
        type: "GET",
        url: postUrl,
        contentType: 'application/json;charset=UTF-8',
        data: "",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
        },
        success: function (response) {
            //console.log("RESPONSE", response)
            if (typeof (response) != "undefined") {
                callback(response.apikey)
            } else {
                callback(response.apikey)
            }
        },
        error: function () {
            callback(false)
        }
    }).done(function (o) {

    });
}
