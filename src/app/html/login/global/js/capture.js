const videoElem = document.getElementById("video");
const logElem = document.getElementById("log");
const startElem = document.getElementById("start");
const stopElem = document.getElementById("stop");
const video = document.querySelector('#video');
const canvas = window.canvas = document.querySelector('#canvasOutput');
const context = canvas.getContext('2d')
const canvas2 = window.canvas = document.querySelector('#canvasTest');
//const context2 = canvas2.getContext('2d')
var FPS = 3
canvas.width = 1920;
canvas.height = 1080;

// Options for getDisplayMedia()

var displayMediaOptions = {
    video: {
        cursor: "always"
    },
    audio: false
};

// Set event listeners for the start and stop buttons
if (startElem) {
    startElem.addEventListener("click", function (evt) {
        startCapture();
    }, false);
}

if (stopElem) {
    stopElem.addEventListener("click", function (evt) {
        stopCapture();
    }, false);
}

var processVideoStatus = false

async function startCapture() {
    try {
        videoElem.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
        videoElem.play()
        processVideoStatus = true
        processVideo()
        dumpOptionsInfo();
    } catch (err) {
        console.error("Error: " + err);
    }
}

function stopCapture(evt) {
    let tracks = videoElem.srcObject.getTracks();
    processVideoStatus = false
    tracks.forEach(track => track.stop());
    videoElem.srcObject = null;
}

function dumpOptionsInfo() {
    const videoTrack = videoElem.srcObject.getVideoTracks()[0];

    console.info("Track settings:");
    console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
    console.info("Track constraints:");
    console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
}

function processVideo() {
    if (processVideoStatus == false) {
        return
    }
    let begin = Date.now();

    context.drawImage(video, 0, 0, video.width, video.height);
    //let pixels = context.getImageData(0, 0, video.width, video.height)
    pixels = canvas.toDataURL()
    globalSocket.emit('screenshare', pixels);


    // schedule next one.
    let delay = 1000 / FPS - (Date.now() - begin);
    setTimeout(processVideo, delay);
}

function setFPS() {
    FPS = document.querySelector('#fps').value
    document.querySelector('#current_fps').innerHTML = FPS
}

function startScreenRender() {
    globalSocket.emit("join", {room: "screenshare"})
    // globalSocket.on('streamdata') , function (data) {
    //     console.log("GOT MESSAGE", data)
    //     //image = document.querySelector('#streamed_image');
    //     //image.src=data
    //     //context2.putImageData(data, 0, 0);
    // }
    image = document.querySelector('#streamed_image');
    globalSocket.on('streamdata', function (msg) {
        //console.log("STREAMDATA", msg)
        image.src = msg
        //context2.putImageData(data, 0, 0);
    });
}