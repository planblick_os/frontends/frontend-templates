function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function encrypt(value, passphrase) {
    return CryptoJS.AES.encrypt(value, passphrase)
}

function decrypt(value, passphrase) {
    return CryptoJS.AES.decrypt(value, passphrase).toString(CryptoJS.enc.Utf8)
}