globalSocket = null
document.addEventListener("DOMContentLoaded", function () {
    // Use a "/test" namespace.
    // An application can open a connection on multiple namespaces, and
    // Socket.IO will multiplex all those connections on a single
    // physical channel. If you don't care about multiple channels, you
    // can set the namespace to an empty string.
    namespace = "/";

    // Connect to the Socket.IO server.
    // The connection URL has the following format, relative to the current page:
    //     http[s]://<domain>:<port>[/<namespace>]
    console.log("Listening for messages on:" + SOCKET_SERVER + namespace)
    var socket = io(SOCKET_SERVER + namespace, {path: '/socket/socket.io', query: {apikey: getCookie("apikey")}});
    globalSocket = socket
    socket.on('connect', function () {
        console.log("Connected")
        //socket.emit('join', {room: "calendar_users"});
        socket.emit('join', {room: getCookie("consumer_name")});
        socket.emit('join', {room: decodeURIComponent(getCookie("username"))});
    });

    socket.on('notification', function (msg) {
        console.log("Received notification:")
        //console.log(msg)
    });
    socket.on('command', function (msg) {
        console.log("Received command:")
        console.log(msg)
        if (typeof (msg) == "string") {
            msg = JSON.parse(msg)
        }

        var payload = null;
        if (typeof (msg.data) == "undefined" && msg["data"]) {
            payload = JSON.parse(msg["data"])
        } else {
            payload = msg.data;
        }
        if (!payload) {
            payload = msg
        }

        commandhandlers[payload.command](payload.args)
    });
    socket.on('my_response', function (msg) {
        console.log("Received my_response:")
        console.log(msg)
    });

});


commandhandlers = {
    fireEvent: function (args) {
            //fireEvent(args.eventname, args.eventdata)
    },
    showAlert: function (args) {
        alert(args.text)
    },
    reloadCalendar: function (args) {
        calendar.refetchEvents();
    },
    addEvent: function (args) {
        console.log("Adding event")
        calendar.addEvent(args);
    },
    removeEvent: function (args) {
        console.log("Removing event");
        event = calendar.getEventById(args.event_id);
        if (event) {
            event.remove();
        }
    },
    forcereload: function (args) {
        console.log(args)
        let reason = args.reason
        alert("Die Seite muss wegen " + reason + " aktualisiert werden.\nNach bestätigen dieser Meldung erledigen wir das automatisch für Sie.")
        location.reload();

    }
}