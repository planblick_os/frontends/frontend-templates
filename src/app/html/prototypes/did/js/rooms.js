export function getRooms() {
    let rooms = [
        {
            "room_data": {
                "room_id": "room_01",
                "room_name": "Room 01"
            }
        },
        {
            "room_data": {
                "room_id": "room_02",
                "room_name": "Room 2"
            }
        },
        {
            "room_data": {
                "room_id": "room_03",
                "room_name": "Room 3"
            }
        },
        {
            "room_data": {
                "room_id": "room_04",
                "room_name": "Room 4"
            }
        },
        {
            "room_data": {
                "room_id": "room_05",
                "room_name": "Room 5"
            }
        },
        {
            "room_data": {
                "room_id": "room_06",
                "room_name": "Room 6"
            }
        },
        {
            "room_data": {
                "room_id": "room_07",
                "room_name": "Room 7"
            }
        },
        {
            "room_data": {
                "room_id": "room_08",
                "room_name": "Room 8"
            }
        },
        {
            "room_data": {
                "room_id": "room_09",
                "room_name": "Room 9"
            }
        },
        {
            "room_data": {
                "room_id": "room_10",
                "room_name": "Room 10"
            }
        }
    ]
    return rooms
}

