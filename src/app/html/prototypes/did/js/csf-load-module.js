import {PbAccount} from "../../../csf-lib/pb-account.js?v=[cs_version]"
import {getCookie, getQueryParam} from "../../../csf-lib/pb-functions.js?v=[cs_version]"
import {getRooms} from "./rooms.js?v=[cs_version]"
import {CONFIG} from "../../../pb-config.js?v=[cs_version]"
import {socketio} from "../../../csf-lib/pb-socketio.js?v=[cs_version]"
import {PbQrcode} from "../../../csf-lib/pb-qrcode.js?v=[cs_version]"

"./rooms.js?v=[cs_version]";


export class PrototypeModule {
    actions = {}

    constructor() {
        if (!PrototypeModule.instance) {
            PrototypeModule.instance = this
            PrototypeModule.instance.socket = new socketio()
            PrototypeModule.instance.initListeners()
            PrototypeModule.instance.initActions()
        }

        return PrototypeModule.instance
    }


    initActions() {
        PrototypeModule.instance.socket.commandhandlers["refreshQueueEntries"] = (data) => {
            PrototypeModule.instance.renderRooms()
        }

        this.actions["protoypeModuleInitialised"] = function (myevent, callback = () => "") {
            callback(myevent)
        }

        this.actions["protoypeModuleIndex"] = function (myevent, callback = () => "") {
            PrototypeModule.instance.renderRooms(callback, myevent)
            let baseUrl = location.href.replace(/[^/]*$/, '')
            let ids = ["qrcode_1", "qrcode_2", "qrcode_3", "qrcode_4"]
            let i = 0

            for (let id of ids) {
                i++
                let link = baseUrl + "room.html?room=room_0" + i
                var qrcode = new QRCode(document.getElementById(id), {
                    text: link,
                    width: 256,
                    height: 256,
                    border: 4,
                    colorDark: "#000000",
                    colorLight: "#ffffff",
                    correctLevel: QRCode.CorrectLevel.M //L,M,Q,H
                })
                document.getElementById(id).parentNode.href = link
                document.getElementById("desc_" + i).append("Room 0" + i)
            }
        }

        this.actions["protoypeModuleRoom"] = function (myevent, callback = () => "") {

        }


    }

    renderRooms(callback = () => "", myevent = null) {
        let newRoomSnippet = $("#roomSnippet")
        let colorNode = undefined
        let colors = ["green"]
        $("#roomContainer").empty()
        for (let room of getRooms()) {
            let newRoomElement = newRoomSnippet.clone()
            newRoomElement.attr("id", "room_container-" + room.room_data.room_id)
            newRoomElement.find('[name="room_color_node"]').addClass(colors[Math.floor(Math.random() * colors.length)]).attr("id", "room_color-" + room.room_data.room_id)
            newRoomElement.find('[name="room_title"]').text(room.room_data.room_name)
            newRoomElement.find('[name="room_task_container"]').attr("id", "room_task_container-" + room.room_data.room_id)
            $("#roomContainer").append(newRoomElement)
        }
        PrototypeModule.instance.fetchAllTasks().then(() => {
            callback(myevent)
        })
    }


    fetchAllTasks() {
        return new Promise((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/queues/list",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
            };

            $.ajax(settings).done(function (response) {

                for (let queue_id of Object.keys(response.queues)) {
                    var settings = {
                        "url": CONFIG.API_BASE_URL + `/get_queue_entries/${queue_id}`,
                        "method": "GET",
                        "timeout": 0,
                        "headers": {
                            "apikey": getCookie("apikey")
                        },
                    };

                    $.ajax(settings).done(function (queue_response) {
                        for (let queue_id of Object.keys(queue_response.queue_entries)) {
                            let entry_links = queue_response.queue_entries[queue_id].links
                            let color_mapping = {"open": "red", "closed": "green", "pending": "yellow"}
                            let color = "orange"
                            for (let entrylink of entry_links) {
                                console.log("ARG", queue_response.queue_entries[queue_id].entry_data.status)
                                if (entrylink.resolver == "roommanager") {
                                    if (queue_response.queue_entries[queue_id] != undefined) {
                                        color = color_mapping[queue_response.queue_entries[queue_id].entry_data.status]
                                        console.log("COLOR SELECTION BASE ON ", queue_response.queue_entries[queue_id].entry_data.status, color)
                                    } else {
                                        color = "orange"
                                    }

                                    PrototypeModule.instance.changeStatus(entrylink.id, color)
                                }
                            }


                        }
                        resolve()
                    })
                }
            })
        })

    }

    changeStatus(room_id, status) {
        let room = $(`#room_color-${room_id}`)
        room.removeClass("red yellow green")
        room.addClass(status)
    }


    updateQueueEntry(entry) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/updateQueueEntry",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify(entry)
        };

        $.ajax(settings).done(function (response) {

        });
    }

    updateQueueEntryStatusForRoom(room_id, status) {
        return new Promise((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/queues/list",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
            };

            $.ajax(settings).done(function (response) {

                for (let queue_id of Object.keys(response.queues)) {
                    var settings = {
                        "url": CONFIG.API_BASE_URL + `/get_queue_entries/${queue_id}`,
                        "method": "GET",
                        "timeout": 0,
                        "headers": {
                            "apikey": getCookie("apikey")
                        },
                    };

                    $.ajax(settings).done(function (queue_response) {
                        for (let queue_entry_id of Object.keys(queue_response.queue_entries)) {
                            let entry_links = queue_response.queue_entries[queue_entry_id].links
                            for (let entrylink of entry_links) {
                                if (entrylink.resolver == "roommanager" && entrylink.id == room_id) {
                                    queue_response.queue_entries[queue_entry_id].entry_data.status = status
                                    queue_response.queue_entries[queue_entry_id]["entry_id"] = queue_entry_id
                                    PrototypeModule.instance.updateQueueEntry(queue_response.queue_entries[queue_entry_id])
                                }
                            }
                        }
                        resolve()
                    })
                }
            })
        })

    }

    initListeners() {
        $(".btn-test").click(function (event) {
            //PrototypeModule.instance.changeStatus("room_09", "red")
            console.log("EVENT", event.target.innerHTML.toLowerCase())
            PrototypeModule.instance.updateQueueEntryStatusForRoom(getQueryParam("room"), event.target.innerHTML.toLowerCase())

        })

        $(".btn-create").click(function (event) {
            console.log("EVENT", event.target.innerHTML.toLowerCase())
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewQueueEntry",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "queue_id": "43b1ab3f-2119-4410-bf6f-64d10564f77c",
                    "entry_data": {
                        "queue_id": "ad9d538b-00e5-4235-b373-3bbaba1df0e2",
                        "title": $("#task-title").val(),
                        "description": "",
                        "status": "open",
                        "assigned_to": [
                            ""
                        ],
                        "order": 1,
                        "due_date": "2022-05-19 10:06:37"
                    },
                    "data_owners": [],
                    "links": [
                        {
                            "resolver": "roommanager",
                            "type": "room",
                            "id": getQueryParam("room")
                        }
                    ]
                }),
            };

            $.ajax(settings).done(function (response) {
                alert("Task wurde angelegt")
            });

        })
    }
}