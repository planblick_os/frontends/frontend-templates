document.addEventListener('DOMContentLoaded', () => {
    const column1 = document.getElementById('column1');
    const column2 = document.getElementById('column2');
    const column3 = document.getElementById('column3');
    const resizer1 = document.getElementById('resizer1');
    const resizer2 = document.getElementById('resizer2');
    const rightActionMenu = document.getElementById('rightActionMenu');
    const leftActionMenu = document.getElementById('leftActionMenu');
    const pollButtonRight = document.getElementById('pollButtonRight');
    const pollButtonLeft = document.getElementById('pollButtonLeft');
    const fixColumn1Button = document.getElementById('fixColumn1');
    const fixColumn3Button = document.getElementById('fixColumn3');

    let isColumn1Fixed = false;
    let isColumn3Fixed = false;

    // Load saved widths from localStorage
    if (localStorage.getItem('column1Width')) {
        column1.style.width = localStorage.getItem('column1Width');
    }
    if (localStorage.getItem('column2Width')) {
        column2.style.width = localStorage.getItem('column2Width');
    }
    if (localStorage.getItem('column3Width')) {
        column3.style.width = localStorage.getItem('column3Width');
    }

    const resizers = document.querySelectorAll('.resizer');
    let currentResizer;
    let startX, startWidthCurrent, startWidthNext;

    resizers.forEach(resizer => {
        resizer.addEventListener('mousedown', (e) => {
            e.preventDefault();
            if ((resizer === resizer1 && isColumn1Fixed) || (resizer === resizer2 && isColumn3Fixed)) return;
            currentResizer = resizer;
            startX = e.clientX;

            const currentColumn = currentResizer.previousElementSibling;
            const nextColumn = currentResizer.nextElementSibling;

            startWidthCurrent = currentColumn.getBoundingClientRect().width;
            startWidthNext = nextColumn.getBoundingClientRect().width;

            document.addEventListener('mousemove', mousemove);
            document.addEventListener('mouseup', mouseup);
        });

        resizer.addEventListener('touchstart', (e) => {
            e.preventDefault();
            if ((resizer === resizer1 && isColumn1Fixed) || (resizer === resizer2 && isColumn3Fixed)) return;
            currentResizer = resizer;
            startX = e.touches[0].clientX;

            const currentColumn = currentResizer.previousElementSibling;
            const nextColumn = currentResizer.nextElementSibling;

            startWidthCurrent = currentColumn.getBoundingClientRect().width;
            startWidthNext = nextColumn.getBoundingClientRect().width;

            document.addEventListener('touchmove', touchmove);
            document.addEventListener('touchend', touchend);
        });
    });

    function mousemove(e) {
        const dx = e.clientX - startX;
        resizeColumns(dx);
    }

    function touchmove(e) {
        const dx = e.touches[0].clientX - startX;
        resizeColumns(dx);
    }

    function resizeColumns(dx) {
        const currentColumn = currentResizer.previousElementSibling;
        const nextColumn = currentResizer.nextElementSibling;

        const newWidthCurrent = startWidthCurrent + dx;
        const newWidthNext = startWidthNext - dx;

        // Ensure minimum width of 250px
        if (newWidthCurrent > 250 && newWidthNext > 250) {
            currentColumn.style.width = `${newWidthCurrent}px`;
            nextColumn.style.width = `${newWidthNext}px`;
        }
    }

    function mouseup() {
        document.removeEventListener('mousemove', mousemove);
        document.removeEventListener('mouseup', mouseup);
        saveWidths();
    }

    function touchend() {
        document.removeEventListener('touchmove', touchmove);
        document.removeEventListener('touchend', touchend);
        saveWidths();
    }

    function saveWidths() {
        localStorage.setItem('column1Width', column1.style.width);
        localStorage.setItem('column2Width', column2.style.width);
        localStorage.setItem('column3Width', column3.style.width);
    }

    let isColumn1Visible = true;
    let isColumn3Visible = true;

    document.getElementById('toggleColumn1').addEventListener('click', () => {
        if (isColumn1Visible) {
            column1.style.display = 'none';
            resizer1.style.display = 'none';
        } else {
            column1.style.display = 'block';
            resizer1.style.display = 'flex';
        }
        isColumn1Visible = !isColumn1Visible;
        updateActionMenuVisibility();
        recalculateColumnWidths();
    });

    document.getElementById('toggleColumn3').addEventListener('click', () => {
        if (isColumn3Visible) {
            column3.style.display = 'none';
            resizer2.style.display = 'none';
        } else {
            column3.style.display = 'block';
            resizer2.style.display = 'flex';
        }
        isColumn3Visible = !isColumn3Visible;
        updateActionMenuVisibility();
        recalculateColumnWidths();
    });

    pollButtonRight.addEventListener('click', () => {
        if (!isColumn3Visible) {
            column3.style.display = 'block';
            resizer2.style.display = 'flex';
            isColumn3Visible = true;
        }
        updateActionMenuVisibility();
        recalculateColumnWidths();
    });

    pollButtonLeft.addEventListener('click', () => {
        if (!isColumn1Visible) {
            column1.style.display = 'block';
            resizer1.style.display = 'flex';
            isColumn1Visible = true;
        }
        updateActionMenuVisibility();
        recalculateColumnWidths();
    });

    function updateActionMenuVisibility() {
        if (!isColumn1Visible) {
            leftActionMenu.style.display = 'block';
        } else {
            leftActionMenu.style.display = 'none';
        }

        if (!isColumn3Visible) {
            rightActionMenu.style.display = 'block';
        } else {
            rightActionMenu.style.display = 'none';
        }
    }

    function recalculateColumnWidths() {
        const totalWidth = document.getElementById('statistic_container').clientWidth;
        let availableWidth = totalWidth;

        if (isColumn1Visible) {
            availableWidth -= column1.clientWidth + resizer1.clientWidth;
        }
        if (isColumn3Visible) {
            availableWidth -= column3.clientWidth + resizer2.clientWidth;
        }

        column2.style.width = `${availableWidth}px`;
    }

    // Recalculate column widths on window resize
    // window.addEventListener('resize', (element) => {
    //     console.log("ELement", element)
    //     recalculateColumnWidths();
    //     updateActionMenuPosition();
    // });

    function updateActionMenuPosition() {
        const column2Rect = column2.getBoundingClientRect();
        rightActionMenu.style.top = `${column2Rect.top + 10}px`;
        leftActionMenu.style.top = `${column2Rect.top + 10}px`;
    }

    // Initial positioning of action menus
    updateActionMenuPosition();

    // Event listeners for fixing column sizes
    fixColumn1Button.addEventListener('click', () => {
        isColumn1Fixed = !isColumn1Fixed;
        if (isColumn1Fixed) {
            localStorage.setItem('fixedColumn1Width', column1.style.width);
            resizer1.style.pointerEvents = 'none'; // Disable resizing
            column1.classList.add('fixed-column'); // Add highlighting
        } else {
            localStorage.removeItem('fixedColumn1Width');
            resizer1.style.pointerEvents = 'auto'; // Enable resizing
            column1.classList.remove('fixed-column'); // Remove highlighting
        }
    });

    fixColumn3Button.addEventListener('click', () => {
        isColumn3Fixed = !isColumn3Fixed;
        if (isColumn3Fixed) {
            localStorage.setItem('fixedColumn3Width', column3.style.width);
            resizer2.style.pointerEvents = 'none'; // Disable resizing
            column3.classList.add('fixed-column'); // Add highlighting
        } else {
            localStorage.removeItem('fixedColumn3Width');
            resizer2.style.pointerEvents = 'auto'; // Enable resizing
            column3.classList.remove('fixed-column'); // Remove highlighting
        }
    });

    // Apply fixed column widths from localStorage
    if (localStorage.getItem('fixedColumn1Width')) {
        isColumn1Fixed = true;
        column1.style.width = localStorage.getItem('fixedColumn1Width');
        resizer1.style.pointerEvents = 'none';
        column1.classList.add('fixed-column'); // Add highlighting
    }
    if (localStorage.getItem('fixedColumn3Width')) {
        isColumn3Fixed = true;
        column3.style.width = localStorage.getItem('fixedColumn3Width');
        resizer2.style.pointerEvents = 'none';
        column3.classList.add('fixed-column'); // Add highlighting
    }
});


// When the user scrolls the page, execute myFunction
window.onscroll = function () {
    setStickyClass()
};



// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function setStickyClass() {
    // Get the header
let header = document.getElementById("az-header");

// Get the offset position of the navbar
let sticky = header.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}
