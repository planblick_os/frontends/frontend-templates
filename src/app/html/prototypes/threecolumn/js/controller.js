export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init(ThreecolumnModuleInstance) {
        switch (window.location.pathname) {
            case "/prototypes/threecolumn/index.html":
                import("./threecolumn-renderer.js?v=[cs_version]").then((Module) => {
                    new Module.ThreecolumnRenderer(Controller.instance.socket, ThreecolumnModuleInstance).init()
                })
                break;
        }
    }
}
