import {PbAccount} from "../../../csf-lib/pb-account.js?v=[cs_version]"
import {socketio} from "../../../csf-lib/pb-socketio.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../../csf-lib/pb-functions.js?v=[cs_version]";
import {pbI18n} from "../../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]";
import {ThreecolumnRenderer} from "./threecolumn-renderer.js?v=[cs_version]";


export class ThreecolumnModule {
    actions = {}
    config = []

    constructor() {
        if (!ThreecolumnModule.instance) {
            this.initListeners()
            this.initActions()
            ThreecolumnModule.instance = this;
        }

        return ThreecolumnModule.instance;
    }

    initActions() {
        this.actions["threecolumnModuleInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(async () => {
                if (!new PbAccount().hasPermission('frontend')) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."));
                } else {
                    new Controller(ThreecolumnModule.instance.socket).init(ThreecolumnModule.instance)
                    callback(myevent);
                }
            });
        }
    }

    initListeners() {
        document.addEventListener("InPageEvent", function (args) {
            console.log("GOT InPageEvent WITH ARGS", args)
        })

        fireEvent("InPageEvent", "init")
    }
}