import {PbAccount} from "/csf-lib/pb-account.js?v=[cs_version]";
import {setCookie, getCookie, fireEvent, getQueryParam, md5} from "/csf-lib/pb-functions.js?v=[cs_version]";
import {socketio} from "../../../csf-lib/pb-socketio.js?v=[cs_version]";

export class E2OFormModule {
    actions = {}

    constructor() {
        if (!E2OFormModule.instance) {
            this.initListeners()
            this.initActions()
            E2OFormModule.instance = this;
        }

        return E2OFormModule.instance;
    }

    initActions() {
        this.actions["checkWhetherServiceIsOnline"] = function (myevent, callback = () => "") {
            fireEvent("easy2order.isserviceonline", {}, "tmp_global")
            document.onlinecheck = window.setInterval(function () {
                fireEvent("easy2order.isserviceonline", {}, "tmp_global")
                document.offlineTimeout = window.setTimeout(function () {
                        $("#onlineCheck").show()
                        $("#orderFormContainer").hide()
                    }, 1000
                )
            }, 1000)
            callback(myevent)
        }

        this.actions["easy2order.isserviceonline"] = function (myevent, callback = () => "") {
            fireEvent("easy2order.serviceisonline", {}, "tmp_global")
            callback(myevent)
        }

        this.actions["easy2order.serviceisonline"] = function (myevent, callback = () => "") {
            $("#onlineCheck").hide()
            $("#orderFormContainer").show()
            //window.clearInterval(document.onlinecheck)
            window.clearTimeout(document.offlineTimeout)
            callback(myevent)
        }

        this.actions["choicesItemClicked"] = function (myevent, callback = () => "") {
            let parts = myevent.data.choice.split("_")
            document.getElementById("cb_" + parts[1]).click()
            callback(myevent)
        }
        this.actions["orderSubmitted"] = function (myevent, callback = () => "") {
            console.debug("ORDER SUBMITTE", myevent.data)
            fireEvent("easy2order.ordersubmitted", myevent.data, "tmp_global")
            callback(myevent)
        }
        this.actions["easy2order.ordersubmitted"] = function (myevent, callback = () => "") {
            console.debug(myevent.data)
            let btn = document.getElementById("confirmButtonTemplate").cloneNode(true)
            btn.id = myevent.data.order_entry_id
            document.getElementById("entryWrapper").innerHTML = '<div id="wrapper_for_' + myevent.data.order_entry_id + '">' + myevent.data.chamber + ": <br> - " + myevent.data.choices.join("<br> - ") + "&nbsp;<br>" + btn.outerHTML + '</div>' + "<hr>" + document.getElementById("entryWrapper").innerHTML
            $(".confirmButton").click(function (event) {
                fireEvent("easy2order.orderconfirmed", {
                    "order_entry_id": event.target.id,
                    "order_confirmed_by": getCookie("username")
                }, "tmp_global")
                fireEvent("easy2order.orderconfirmed.entry", {
                    "order_entry_id": event.target.id,
                    "order_confirmed_by": getCookie("username")
                }, "tmp_global")
            })
            callback(myevent)
        }
        this.actions["easy2order.orderconfirmed"] = function (myevent, callback = () => "") {
            console.debug(myevent.data)
            if (myevent.data.order_entry_id == document.order_entry_id) {
                document.order_entry_id = null
                $.unblockUI()
                $.blockUI({
                    message: '<div class="responseMessage">' + myevent.data.order_confirmed_by + ' hat ihre Bestellung bestätigt und kümmert sich sofort darum.</div>',
                    baseZ: 9999999999999999,
                    onUnblock: function () {
                        location.reload()
                    }
                })
            }
            callback(myevent)
        }
        this.actions["easy2order.orderconfirmed.entry"] = function (myevent, callback = () => "") {
            console.log("MYEVENT DATA", myevent.data)
            $("#" + myevent.data.order_entry_id).remove()
            $("#confirmedByWrapper_" + myevent.data.order_entry_id).remove()
            $("#wrapper_for_" + myevent.data.order_entry_id).html($("#wrapper_for_" + myevent.data.order_entry_id).html() + '<span id="confirmedByWrapper_'+myevent.data.order_entry_id+'">Bestätigt von ' + myevent.data.order_confirmed_by + '</span>')
            callback(myevent)
        }


    }

    initListeners() {

        $(".choicesItem").click(function (event) {
            event.preventDefault()
            fireEvent("choicesItemClicked", {"choice": event.target.id})
        })

        $(".submitButton").click(function (event) {
            event.preventDefault()
            let checkedIds = []
            $('input[type=checkbox]').each(function () {
                if (this.checked) {
                    checkedIds.push($(this).attr('id'))
                }
            });
            console.debug("Checked", checkedIds)
            let order_entry_id = md5(getQueryParam("room") + "_" + (Date.now() / 1000 | 0))
            document.order_entry_id = order_entry_id
            fireEvent("orderSubmitted", {
                "choices": checkedIds,
                "chamber": getQueryParam("room"),
                "order_entry_id": order_entry_id
            })
            $.blockUI({
                message: '<div class="responseMessage">Ihre Bestellung wird an einen unserer Mitarbeiter übermittelt.<br>Bitte haben sie einen Augenblick Geduld...</div>',
                //timeout: 3000,
                baseZ: 9999999999999999,
            });
        })


    }
}