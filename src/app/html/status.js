document.addEventListener("DOMContentLoaded", init, false);
setInterval(init, 1000);

function init() {
    if (!navigator.onLine) {
        const statusElem = document.querySelector(".page-status");
        console.log("ENTERED OFFLINE MODE")
        //statusElem.innerHTML = "<span style='color:red'>offline</span>";
    } else {
        const statusElem = document.querySelector(".page-status");
        console.log("ENTERED ONLINE MODE")
        //statusElem.innerHTML = "<span style='color:green'>online</span>";
    }
}
