import {getQueryParam} from "../../csf-lib/pb-functions.js?v=[cs_version]"


export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init() {
        switch (window.location.pathname) {
            case "/calendar/":
                import("./renderEventDialogPage.js?v=[cs_version]").then((Module) => {
                    new Module.RenderEventDialogPage(Controller.instance.socket).init(getQueryParam("event_id"))
                })
                break;
            case "/calendar/different/":
                import("./renderEventDialogPage.js?v=[cs_version]").then((Module) => {
                    alert("THIS IS A DIFFERENT THING")
                })
                break;
            default:
                import("./renderEventDialogPage.js?v=[cs_version]").then((Module) => {
                    new Module.RenderEventDialogPage(Controller.instance.socket).init(getQueryParam("event_id"))
                })
                break;
        }
    }
}
