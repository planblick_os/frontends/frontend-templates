<div class="d-flex align-items-start">
    <span class="d-inline-block pr-2">
        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip" title="Write a comment"
             data-original-title="Write a comment">
            <i class="fas fa-sticky-note tx-16"></i>
        </div>
    </span>
<textarea data-propertyname="extended_props.comment" id="event_comment" name="" class="form-control" rows="3"
          placeholder="Notiz schreiben...">{{comment}}</textarea>
</div>