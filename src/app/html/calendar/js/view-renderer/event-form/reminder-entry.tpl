<div data-reminder-id="{{ reminder.id }}" class="d-flex pb-2">
    <div class="px-0" data-reminder-container="mode_select">
        <select data-reminder-data="{{ reminder.id }}" name="mode" class="form-control" id="reminder_mode_select_{{ reminder.id }}">
            <option value="gui" selected="selected">GUI</option>
            <option value="email">Email</option>
        </select>
    </div>
    <div class="px-0" data-reminder-container="time_amount">
        <input data-reminder-data="{{ reminder.id }}" name="value" class="form-control" id="reminder_time_value_{{ reminder.id }}" type="number" min="0" style="width: 70px;" value="{{ reminder.value }}"/>
    </div>
    <div class="mr-auto" data-reminder-container="time_unit_select">
        <select data-reminder-data="{{ reminder.id }}" name="unit" id="reminder_unit_select_{{ reminder.id }}" class="form-control">
            <option value="minutes" selected="selected">Minuten</option>
            <option value="hours">Stunden</option>
            <option value="days">Tage</option>
        </select>
    </div>
    <div class="pl-2" data-reminder-container="delete_reminder_button">
        <button data-reminder-delete-button="{{ reminder.id }}" name="reminder_delete_button" class="btn btn-outline-secondary btn-with-icon">
            <i class="typcn typcn-trash"></i>
        </button>
    </div>
</div>