<div class="d-flex align-items-center mg-b-10">
    <span class="d-inline-block pr-2">
        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip"
             title="Select start date"
             data-original-title="Select start date">
            <i class="fas fa-calendar-alt tx-16"></i>
        </div>
    </span>
    <div class="w-100 dateselect">
        <label data-i18n="Start date">Start date</label>
        <input data-propertyname="starttime" type="text" id="starttime" name="starttime" class="form-control datepicker" value="{{start}}" required readonly=""/>
    </div>
</div>


<div class="d-flex align-items-center mg-b-0">
    <span class="d-inline-block pr-2">
        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip"
             title="Select end date"
             data-original-title="Select end date">
            <i class="fas fa-calendar-alt tx-16"></i>
        </div>
    </span>
    <div class="w-100 dateselect">
        <label data-i18n="End date">End date</label>
        <input data-propertyname="endtime" type="text" id="endtime" name="endtime" class="form-control datepicker" value="{{end}}" required readonly=""/>
    </div>
</div>

<div class="d-flex mg-l-30 mg-t-10">
    <div class="dateselect mg-l-10">
        <label data-i18n="All day" class="no-wrap">All day</label>
        <input data-propertyname="allDay" type="checkbox" id="allDayCheckbox" name="" class="" value="" />
    </div>
    <div class="dateselect mg-l-30 hidden" data-permission="developer_access">
        <label data-i18n="Series">Series</label>
        <input data-propertyname="" type="checkbox" id="" name="" class="" value="" />
    </div>
</div>
