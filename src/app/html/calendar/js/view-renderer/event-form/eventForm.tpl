<div class="pd-5 p-lg-3 bd-lg ">
    <form id="eventForm" method="POST">
        <div class="row">
            <!-- start left side -->
            <div class="col-12 col-lg-6 ">
                <!-- Title -->
                <div id="title_container" class="bg-light form-group pt-3 px-3 mb-0">

                </div>
                <!-- Date -->
                <div id="date_selects_container" class="bg-light form-group pt-3 px-3 mb-0">

                </div>

                <!-- Comment -->
                <div class="bg-light form-group pt-3 px-3 mb-0" id="comment_container">

                </div>

                <div class="bg-light form-group py-3 px-3 mb-3">
                    <div class="d-flex align-items-center">
                    <span class="d-inline-block pr-2">
                        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip" title="Visible for" data-original-title="Visible for"><i class="fa fa-lock tx-16"></i></div>
                    </span>
                        <div id="dataOwnersContainer" class="d-flex w-100"></div>
                    </div>
                </div>

                <!-- Column/Resource -->
                <div class="bg-light form-group py-3 px-3 mb-3">
                    <div class="d-flex align-items-center">
                        <span class="d-inline-block pr-2">
                            <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip"
                                 title="Zuordnung zu Kalenderspalte" data-original-title="Zuordnung zu Kalenderspalte">
                                <i class="fas fa-link tx-16"></i>
                            </div>
                        </span>
                        <div id="ressources_1st_dimension" class="d-flex w-100">

                        </div>
                    </div>
                </div>


                <!-- end accordian -->
            </div>
            <!-- end left side -->
            <!-- start right side -->
            <div class="col-12 col-lg-6">
<div class="accordion" role="tablist" aria-multiselectable="true">
                    <!-- start teilnehmer -->
                    <div class="card">
                        <div class="card-header fs-1" role="tab" id="heading_participants">
                            <a data-toggle="collapse" href="#eep" aria-expanded="false"
                               aria-controls="eep" class="collapsed" data-i18n="Participants">

                            </a>
                        </div>

                        <div id="eep" class="collapse" role="tabpanel" aria-labelledby="heading_participants" style="">
                            <div class="card-body px-3 py-2">
                                <div class="d-flex align-items-center mg-b-10">
                                    <span class="d-inline-block pr-2">
                                        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip"
                                             title="Add calendar members"
                                             data-original-title="Add calendar members">
                                            <i class="fas fa-user tx-16"></i>
                                        </div>
                                    </span>
                                    <div id="ressources_2nd_dimension" class="d-flex w-100">

                                    </div>
                                </div>
                                <div id="contact_wrapper" class="d-flex align-items-center mg-b-10" data-permission='frontend.contactmanager.entries.read'>
                                    <span class="d-inline-block pr-2">
                                        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip"
                                             title="Add contacts" data-original-title="Add contacts">
                                            <i class="fas fa-address-book tx-16"></i>
                                        </div>
                                    </span>
                                    <div id="contactsContainer" class="d-flex w-100"></div>
                                    <a target="_blank" class="btn btn-outline-secondary btn-with-icon ml-2" type="button" id="new_contact_button">
                                        <i class="typcn typcn-plus-outline"></i>
                                        <span data-i18n="New Contact" class="d-none d-md-block no-wrap">neuen Kontakt</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end teilnehmer -->
                </div>
                <div class="accordion" role="tablist" aria-multiselectable="true">
                    <!-- start erweit. detail -->
                    <div class="card">
                        <div class="card-header fs-1" role="tab" id="headingDetails">
                            <a class="collapsed" data-toggle="collapse" href="#collapseDetails" aria-expanded="false" aria-controls="collapseDetails" data-i18n="Extended details">

                            </a>
                        </div>
                        <div id="collapseDetails" class="collapse" role="tabpanel" aria-labelledby="headingDetails" style="">
                            <div class="card-body px-3 py-2">
                                <div class="d-flex align-items-center mg-b-10">
                                    <span class="d-inline-block pr-2">
                                        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip" title="Select a tag or create a new one by typing and then pressing enter"><i class="fa fa-tag tx-16"></i>
                                        </div>
                                    </span>
                                    <div id="tag_container" class="d-flex align-items-center w-100"></div>
                                </div>

                                <div class="d-flex align-items-center mg-b-10">
                                    <span class="d-inline-block pr-2">
                                        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip" title="Select status" data-original-title="Select status"><i class="fa fa-calendar-check tx-16"></i>
                                        </div>
                                    </span>
                                    <div id="status_container" class="d-flex w-100"></div>
                                </div>
                                <div class="d-flex align-items-center mg-b-10">
                                    <span class="d-inline-block pr-2">
                                        <div class="az-avatar-no-status avatar-sm bg-primary" data-toggle="tooltip" title="Select type" data-original-title="Select type"><i class="fa fa-calendar-check tx-16"></i>
                                        </div>
                                    </span>
                                    <div id="type_container" class="d-flex w-100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end accordian -->
                <div class="accordion" role="tablist" aria-multiselectable="true">
                    <!-- start custom -->
                    <!--                   <div class="card">-->
                    <!--                       <div class="card-header fs-1" role="tab" id="headingCustom">-->
                    <!--                        <a class="collapsed" data-toggle="collapse" href="#collapseCustom" aria-expanded="false"-->
                    <!--                               aria-controls="collapseNotifications" data-i18n="Custom">-->
                    <!--                    -->
                    <!--                        </a>-->
                    <!--                       </div>-->
                    <!--                       <div id="collapseCustom" class="collapse" role="tabpanel" aria-labelledby="headingCustom"-->
                    <!--                            style="">-->
                    <!--                           <div class="card-body px-3 py-2">-->
                    <!--                               <div id="custom_fields_container">-->
                    <!--                                    Loading custom-fields...-->
                    <!--                               </div>-->
                    <!--                           </div>-->
                    <!--                       </div>-->
                    <!--                   </div>-->
                    <!-- end custom-->
                </div>
                <!-- end accordian -->
                <div class="accordion" role="tablist" aria-multiselectable="true">
                    <!-- start alert -->
                    <div class="card">
                        <div class="card-header fs-1" role="tab" id="headingNotifications">
                            <a class="collapsed" data-toggle="collapse" href="#collapseNotifications" aria-expanded="false"
                               aria-controls="collapseNotifications" data-i18n="Notifications">

                            </a>
                        </div>
                        <div id="collapseNotifications" class="collapse" role="tabpanel" aria-labelledby="headingNotifications" style="">
                            <div class="card-body px-3 py-2">
                                <!--                                <div id="reminder_form" style="">-->
                                <!--                                    <div id="reminder_data_set">-->
                                <!--                                        -->
                                <!--                                        <div data-reminder-id="35d76544-5408-4a03-abae-1df2107beb32" class="d-flex row justify-content-between pb-2 px-3">-->
                                <!--                                            <div class="px-0" data-reminder-container="mode_select"><select class="form-control" id="reminder_mode_select_35d76544-5408-4a03-abae-1df2107beb32">-->
                                <!--                                                <option value="gui">GUI</option>-->
                                <!--                                                <option value="email" selected="selected">Email</option>-->
                                <!--                                            </select></div>-->
                                <!--                                            <div class="px-0" data-reminder-container="time_amount"><input class="form-control" id="reminder_time_value_35d76544-5408-4a03-abae-1df2107beb32" type="number" min="0" style="width: 70px;"></div>-->
                                <!--                                            <div class="px-0" data-reminder-container="time_unit_select"><select id="reminder_unit_select_35d76544-5408-4a03-abae-1df2107beb32" class="form-control">-->
                                <!--                                                <option value="min" selected="selected">Minuten</option>-->
                                <!--                                                <option value="hour">Stunden</option>-->
                                <!--                                                <option value="day">Tage</option>-->
                                <!--                                            </select></div>-->
                                <!--                                            <div class="px-0" data-reminder-container="delete_reminder_button">-->
                                <!--                                                <button name="reminder_delete_button" class="btn btn-outline-secondary btn-icon" id="reminder_delete_button_35d76544-5408-4a03-abae-1df2107beb32"><i class="typcn typcn-trash"></i></button>-->
                                <!--                                            </div>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="px-3 row justify-content-center">-->

                                <!--                                        <button id="new_reminder_button" type="button" class="btn btn-indigo-white mb-md-0 mb-3"><i class="typcn typcn-plus tx-18 pr-1"></i> Neue Benachrichtigung-->
                                <!--                                        </button>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <div id="reminder_form">
                                    <div id="reminder_data_set">

                                    </div>

                                    <div class="px-3 row justify-content-center">
                                        <button id="new_reminder_button" type="button" class="btn btn-indigo-white mb-md-0" data-i18n="Add new notification">

                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end alert -->
                </div>
                <!-- end accordian -->
            </div>
            <!-- end right side -->
        </div>
    </form>
</div>
<div id="interactions" class="">
    <div id="activity-nav">
        <nav class="d-flex align-items-center pd-t-10">
            <h6 class="mg-t-7 ml-3">Interaktionen: </h6>
            <div class="" data-permission="frontend.documentation.views.contact" data-selector="pill_entry"
                 id="documents">
                <a id="pilllink_documents"
                   href=""
                   target="documentation" class="btn-light-pill ml-2"> <span class="d-none d-lg-block"
                                                                             data-i18n="Dokumentation">Dokumentation</span> <span class="d-block d-lg-none"><i
                        class="far fa-file-alt mr-1 tx-14"></i></span></a>
            </div>

            <!--            <div class="" data-permission="frontend.contactmanager.views.history" data-selector="pill_entry"-->
            <!--                 id="history">-->
            <!--                <a id="pilllink_history" class="btn-light-pill ml-2" data-toggle="true"> <span-->
            <!--                        class="d-none d-lg-block" data-i18n="Revisionsverlauf">Revisionsverlauf</span> <span-->
            <!--                        class="d-block d-lg-none"><i class="fa fa-history mr-1 tx-14"></i></span></a>-->
            <!--            </div>-->
        </nav>
    </div>
</div>

