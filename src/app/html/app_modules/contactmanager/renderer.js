import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";
import {BaseRenderer} from "../base-renderer.js?v=[cs_version]"

export class Renderer extends BaseRenderer {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor(AppInstance, targetselectors = Controller.targetselectors) {
        super();
        this.targetselectors = targetselectors

        if (!Renderer.instance) {
            Renderer.instance = this
            Renderer.instance.appInstance = AppInstance
            Renderer.instance.tpl = new PbTpl()
            Renderer.instance.notifications = new PbNotifications()
            Renderer.instance.appInstance.layout.lazyLoadCSS("/app_modules/contactmanager/css/custom.css?v=[cs_version]")

        }

        return Renderer.instance
    }

    renderListView(data = [], current_task_id = undefined) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/contactmanager/templates/list.html', {"data": data}, this.targetselectors.list).then(function () {
                $("#contactmanager_refresh_list_button").on("click", function () {
                    Renderer.instance.fire_event("contactmanager_refresh_list", {})
                })

                $("[data-contact-id]").on("click", function () {
                    let contact_id = $(this).attr("data-contact-id")
                    Renderer.instance.fire_event("contact_select_btn_clicked", {"contact_id": contact_id})
                })

                $("#contact-list-button-cancel").on("click", function () {
                    Renderer.instance.fire_event("contact-list-button-cancel", {})
                })
                resolve()
            })
        });
    }

    renderDetailView(data) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/contactmanager/templates/detail.html', {"data": {data}}, this.targetselectors.detail).then(function () {
                resolve()
            }.bind(this))
        });
    }

    renderFormView(data) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/contactmanager/templates/form.html', {"data": {data}}, this.targetselectors.detail).then(function () {
                resolve()
            }.bind(this))
        });
    }
}
