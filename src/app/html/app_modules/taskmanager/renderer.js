import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";
import {PbEditor} from "../../csf-lib/pb-editor.js?v=[cs_version]";
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {BaseRenderer} from "../base-renderer.js?v=[cs_version]"

export class Renderer extends BaseRenderer {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor(AppInstance, targetselectors = Controller.targetselectors) {
        super();
        this.targetselectors = targetselectors

        if (!Renderer.instance) {
            Renderer.instance = this
            Renderer.instance.appInstance = AppInstance
            Renderer.instance.tpl = new PbTpl()
            Renderer.instance.notifications = new PbNotifications()
            Renderer.instance.appInstance.layout.lazyLoadCSS("/app_modules/taskmanager/css/custom.css?v=[cs_version]")
        }

        return Renderer.instance
    }

    renderListView(data = [], current_task_id = undefined) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/taskmanager/templates/list.html', {"data": data, "current_task_id": current_task_id}, this.targetselectors.list).then(function () {
                $("#taskmanager_refresh_list_button").on("click", function () {
                    Renderer.instance.fire_event("taskmanager_refresh_list", {})
                })
                $("[data-quickactiondropdownbutton]").dropdown()

                $('[data-quickaction-set-status]').on('click', function (e) {
                    let status = $(e.currentTarget).data('quickaction-set-status')
                    let quickactionTaskId = $(e.currentTarget).data('taskid')
                    alert("NYI: Set status of task " + quickactionTaskId + " to: " + status)
                    $("[data-quickactiondropdownbutton]").dropdown('hide')
                })

                $('[data-quickaction-assigntome]').on('click', function (e) {
                    alert("NYI: Assign to me")
                    $("[data-quickactiondropdownbutton]").dropdown('hide')
                })

                resolve()
            })
        });
    }

    renderDetailView(data) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/taskmanager/templates/detail.html', {"data": {data}}, this.targetselectors.detail).then(function () {
                if (this.targetselectors.list == this.targetselectors.detail) {
                    $("#button-cancel").show()
                } else {
                    $("#button-cancel").hide()
                }

                resolve()
            }.bind(this))
        });
    }

    renderDetailViewContacts(data) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/taskmanager/templates/snippets/detail_contact.html', {"data": {data}}, "#contact_entries").then(function () {
                resolve()
            }.bind(this))
        });
    }

    renderFormView(data) {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/taskmanager/templates/form.html', {"data": {data}}, this.targetselectors.detail).then(function () {
                $('[data-select2-selector]').select2()
                $('[data-propertyname="tags"]').select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    selectOnClose: true
                })

                $("#taskmanager_select_contact_button").on("click", function () {
                    Renderer.instance.fire_event("taskmanager_select_contact_button_click", {})
                })

                new PbEditor("froala", "#task_description_editor", undefined, undefined).then((pbeditor) => {
                    pbeditor.init().then((initialisedEditor) => {
                        Renderer.instance.editor = initialisedEditor;

                        // Set the beautified JSON content in the editor.
                        Renderer.instance.editor.setContent(`${data.entry_data.description}`);
                    });
                });

                $("input").on("change", function (e) {
                    Renderer.instance.fire_event("taskmanager_form_input_change", e)
                })

                resolve()
            }.bind(this))
        });
    }

    removeTodoFromTask(todoId) {
        $('li[data-todoid="' + todoId + '"]').remove();
    }

    addTodoToTask(value) {
        $("#todoList ul").append('<li style="list-style-type: none;" class="mb-0 w-100 d-flex justify-content-between align-items-center"><div class="d-flex align-items-center"><input type="checkbox" class="list-item-checkbox" disabled ><span class="ml-2 list-item-text" data-selector="" >' + value + '</span></div></li>');
    }
}
