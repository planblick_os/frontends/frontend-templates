import {pbI18n} from "../../../csf-lib/pb-i18n.js?v=[cs_version]"

export class CrmAppBase {
    actions = {}
    loaded_submodule_controllers = {}

    async getAppModuleControllerClass(module_name) {
        await new CrmAppBase().lazyLoadTranslations(module_name)

        return new Promise((resolve, reject) => {
            import(`/app_modules/${module_name}/controller.js?v=[cs_version]`).then((Module) => {
                resolve(Module.Controller)
            })
        })

    }

    lazyLoadTranslations(module_name, language=null) {
        let i18n = new pbI18n()
        if (language == null || language == undefined) {
            language = i18n.getAvailableBrowserLanguage()
        }

        let url = "/app_modules/" + module_name + "/i18n/" + language + ".json?v=[cs_version]"
        return new Promise((resolve, reject) => {
            i18n.loadLanguageFile(url, i18n.setI18n, true).then(() => {
                resolve()
            })
        })
    }
}