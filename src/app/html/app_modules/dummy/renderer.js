import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";
export class Renderer {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor(AppInstance, targetselectors = Controller.targetselectors) {
        this.targetselectors = targetselectors

        if (!Renderer.instance) {
            Renderer.instance = this
            Renderer.instance.appInstance = AppInstance
            Renderer.instance.tpl = new PbTpl()
            Renderer.instance.notifications = new PbNotifications()
        }

        return Renderer.instance
    }

    fire_event(name, data) {
        let custom_event = new CustomEvent(name, {
            detail: data
        });
        document.dispatchEvent(custom_event);
    }

    renderListView(data = []) {

        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/dummy/templates/list.html', {"data": data}, this.targetselectors.list).then(function () {
                $("#dummy_refresh_list_button").on("click", function () {
                        Renderer.instance.fire_event("dummy_refresh_list", {})
                })
                resolve()
            })
        });
    }

    renderDetailView() {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/dummy/templates/detail.html', {"data": {}}, this.targetselectors.detail).then(function () {
                resolve()
            })
        });
    }

    renderFormView() {
        return new Promise((resolve, reject) => {
            Renderer.instance.tpl.renderIntoAsync('app_modules/dummy/templates/form.html', {"data": {}}, this.targetselectors.form).then(function () {
                resolve()
            })
        });
    }
}
