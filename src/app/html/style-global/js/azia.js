
  onload = function loader() {
    // checks if id is present to highlight active link in menu
    if(document.getElementById('use-activate-menu') !== null){
      activateMenu()
    }
  
  }

  // This template is mobile first so active menu in navbar
  // has submenu displayed by default but not in desktop
  // so the code below will hide the active menu if it's in desktop
  if(window.matchMedia('(min-width: 992px)').matches) {
    $('.az-navbar .active').removeClass('show');
    $('.az-header-menu .active').removeClass('show');
  }

  // Shows header dropdown while hiding others
  $('.az-header .dropdown > a').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });

  // Showing submenu in navbar while hiding previous open submenu
  $('.az-navbar .with-sub').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });

  // this will hide dropdown menu from open in mobile
  $('.dropdown-menu .az-header-arrow').on('click', function(e){
    e.preventDefault();
    $(this).closest('.dropdown').removeClass('show');
  });

  // this will show navbar in left for mobile only
  $('#azNavShow, #azNavbarShow').on('click', function(e){
    e.preventDefault();
    $('body').addClass('az-navbar-show');
  });

  // this will hide currently open content of page
  // only works for mobile
  $('#azContentLeftShow').on('click touch', function(e){
    e.preventDefault();
    $('body').addClass('az-content-left-show');
  });

  // This will hide left content from showing up in mobile only
  $('#azContentLeftHide').on('click touch', function(e){
    e.preventDefault();
    $('body').removeClass('az-content-left-show');
  });

  // this will hide content body from showing up in mobile only
  $('#azContentBodyHide').on('click touch', function(e){
    e.preventDefault();
    $('body').removeClass('az-content-body-show');
  })

  // navbar backdrop for mobile only
  $('body').append('<div class="az-navbar-backdrop"></div>');
  $('.az-navbar-backdrop').on('click touchstart', function(){
    $('body').removeClass('az-navbar-show');
  });

  // Close dropdown menu of header menu
  $(document).on('click touchstart', function(e){
    e.stopPropagation();

    // closing of dropdown menu in header when clicking outside of it
    var dropTarg = $(e.target).closest('.az-header .dropdown').length;
    if(!dropTarg) {
      $('.az-header .dropdown').removeClass('show');
    }

    // closing nav sub menu of header when clicking outside of it
    if(window.matchMedia('(min-width: 992px)').matches) {

      // Navbar
      var navTarg = $(e.target).closest('.az-navbar .nav-item').length;
      if(!navTarg) {
        $('.az-navbar .show').removeClass('show');
      }

      // Header Menu
      var menuTarg = $(e.target).closest('.az-header-menu .nav-item').length;
      if(!menuTarg) {
        $('.az-header-menu .show').removeClass('show');
      }

      if($(e.target).hasClass('az-menu-sub-mega')) {
        $('.az-header-menu .show').removeClass('show');
      }

    } else {

      //
      if(!$(e.target).closest('#azMenuShow').length) {
        var hm = $(e.target).closest('.az-header-menu').length;
        if(!hm) {
          $('body').removeClass('az-header-menu-show');
        }
      }
    }

  });

  $('#azMenuShow').on('click', function(e){
    e.preventDefault();
    $('body').toggleClass('az-header-menu-show');
  })

  $('.az-header-menu .with-sub').on('click', function(e){
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  })

  $('.az-header-menu-header .close').on('click', function(e){
    e.preventDefault();
    $('body').removeClass('az-header-menu-show');
  })


  $('[data-toggle="tooltip"]').tooltip();

  // colored tooltip
  $('[data-toggle="tooltip-primary"]').tooltip({
    template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  });

  $('[data-toggle="tooltip-secondary"]').tooltip({
    template: '<div class="tooltip tooltip-secondary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  });

  $('#window-expand').on('click', function(e){

    let element = document.documentElement

    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    }
  
    return false;
  })

  

function getClosestMatch(elem, selector) {
    
  // Element.matches() polyfill
  if (!Element.prototype.matches) {
      Element.prototype.matches =
          Element.prototype.matchesSelector ||
          Element.prototype.mozMatchesSelector ||
          Element.prototype.msMatchesSelector ||
          Element.prototype.oMatchesSelector ||
          Element.prototype.webkitMatchesSelector ||
          function (s) {
              var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                  i = matches.length;
              while (--i >= 0 && matches.item(i) !== this) { }
              return i > -1;
          };
  }

  // Get the closest matching element
  for (; elem && elem !== document; elem = elem.parentNode) {
      if (elem.matches(selector)) return elem;
  }
  return null;

}

function activateMenu() {
  
  let menuItems = document.getElementsByClassName("nav-sub-link")
  if (menuItems.length !== 0){
      menuItems = menuItems
  } else {
      menuItems = document.getElementsByClassName("nav-link")
 }

  if (menuItems) {

      var matchingMenuItem = null;
      for (var idx = 0; idx < menuItems.length; idx++) {
          if (menuItems[idx].href === window.location.href) {
              matchingMenuItem = menuItems[idx];
          }
      }

      if (matchingMenuItem) {
          matchingMenuItem.classList.add('active');
          var immediateParent = getClosestMatch(matchingMenuItem, 'nav-item');
          if (immediateParent) {
              immediateParent.classList.add('active');
          }

          var parent = getClosestMatch(matchingMenuItem, '.nav-sub-item');
          if (parent) {
              parent.classList.add('active');
              var parentMenuitem = parent.querySelector('.nav-item');
              if (parentMenuitem) {
                  parentMenuitem.classList.add('active');
              }
              var parentOfParent = getClosestMatch(parent, '.nav-item');
              if (parentOfParent) {
                  parentOfParent.classList.add('active');
              }
          } else {
              var parentOfParent = getClosestMatch(matchingMenuItem, '.nav-item');
              if (parentOfParent) {
                  parentOfParent.classList.add('active');
              }
          }
      }
  }
}



