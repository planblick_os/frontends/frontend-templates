import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {makeId, getNextParentElement, getCookie} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"
import {pbI18n} from "/csf-lib/pb-i18n.js?v=[cs_version]"

export class Render {
    constructor() {
        if (!Render.instance) {
            Render.instance = this
            Render.instance.notifications = new PbNotifications()
            Render.instance.handledTaskIds = []
            Render.instance.i18n = new pbI18n()
            Render.instance.tpl = new PbTpl()
            Render.instance.timers = {}
            this.initListeners()

            setInterval(() => {

            })
        }

        return Render.instance
    }

    init(ModuleInstance) {
        this.moduleInstance = ModuleInstance
        this.renderStart().then(() => {
            $("#loader-overlay").hide()
        })
    }

    renderStart() {
        return new Promise((resolve, reject) => {
            resolve();
        })
    }

    initListeners() {
        document.addEventListener("event", function _(e) {
            if (e.data && e.data.data && e.data.data.event == "cruisesUpdated") {
                this.renderStart()
            }
        }.bind(this))
    }
}
