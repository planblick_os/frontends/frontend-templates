
export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init(CruisesModuleInstance) {
        switch (window.location.pathname) {
            case "/cruises/cruises_contracts.html":
                import("./render-iternary-list.js?v=[cs_version]").then((Module) => {
                    new Module.RenderIternaryList(Controller.instance.socket).init(CruisesModuleInstance)
                })
                break;
            default:
                import("./render.js?v=[cs_version]").then((Module) => {
                    new Module.Render(Controller.instance.socket).init(CruisesModuleInstance)
                })
                break;
        }
    }
}

