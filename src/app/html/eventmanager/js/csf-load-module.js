import {Eventmanager} from "./eventmanager.js?v=[cs_version]"
import {Event} from "./event.js?v=[cs_version]"
import {RenderEventmanager} from "./render-eventmanager.js?v=[cs_version]"
import {RenderEventmanagerScanner} from "./render-eventmanager-scanner.js?v=[cs_version]"
import {RenderGuestForm} from "./render-eventmanager-guestform.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {getWelcome} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"

export class EventManagerModule {
    actions = {}


    constructor() {
        if (!EventManagerModule.instance) {
            EventManagerModule.instance = this
            this.initListeners()
            this.initActions()
            EventManagerModule.instance.em = new Eventmanager()
        }

        return EventManagerModule.instance
    }

    initActions() {
        this.actions["eventManagerGuestInitialised"] = function (myevent, callback = () => "") {
            EventManagerModule.instance.rgf = new RenderGuestForm()

            let data = undefined
            const setData = (input) => {
                data = input
            }

            let url_string = window.location.href;
            let url = new URL(url_string);
            let event_id = url.searchParams.get("event_id")
            let init_event = new Event()
            init_event.getEventById(event_id).then(response => {
                setData(Object.entries(response.events))
                let guest_event = new Event(data[0][0], data[0][1])
                EventManagerModule.instance.rgf.setInviteEvent(guest_event)
                if (EventManagerModule.instance.rgf.checkEventTime()) {
                    EventManagerModule.instance.rgf.renderFormDetails()
                } else {
                    EventManagerModule.instance.rgf.renderFormDetails()
                    EventManagerModule.instance.rgf.showEventOver()
                }

            })


            new PbAccount().init().then(() => {
                if ($("#loader-overlay")) {
                    $("#loader-overlay").hide()
                    $("#contentwrapper").show()
                }
                callback(myevent)
            })
        }

        this.actions["eventManagerScannerInitialised"] = function (myevent, callback = () => "") {
            EventManagerModule.instance.rem = new RenderEventmanagerScanner()
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("eventmanager")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    EventManagerModule.instance.rem.init().then(() => {
                        if ($("#loader-overlay")) {
                            $("#loader-overlay").hide()
                            $("#contentwrapper").show()
                        }
                        callback(myevent)
                    })
                }
            })
        }

        this.actions["eventManagerInitialised"] = function (myevent, callback = () => "") {
            EventManagerModule.instance.rem = new RenderEventmanager()
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("eventmanager")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    EventManagerModule.instance.em.init().then(() => {
                        const urlParams = new URLSearchParams(window.location.search);
                        let event_id = urlParams.get("event_id")
                        EventManagerModule.instance.rem.init().renderEventListView()
                        if (event_id && event_id != "") {
                            RenderEventmanager.instance.em.setActiveEvent(urlParams.get("event_id")).then(RenderEventmanager.instance.renderEventDetail)
                        }
                        if ($("#loader-overlay")) {
                            $("#loader-overlay").hide()
                            $("#contentwrapper").show()
                        }
                        // new EventManagerHelp()
                        callback(myevent)
                    })
                }

            })
        }

        this.actions["startEventmanagerList"] = function (myevent, callback = () => "") {
            //EventManagerModule.instance.render.renderEventListView()
            //getWelcome("eventmanager")
            new PbGuide("./tours/user_getting_started.js")
            callback(myevent)
        }

        this.actions["eventManagerEmbeddedInitialised"] = function (myevent, callback = () => "") {
            EventManagerModule.instance.rem = new RenderEventmanager()
            new PbAccount().init().then(() => {
                EventManagerModule.instance.em.init().then(() => {
                    EventManagerModule.instance.rem.init().renderEventListView()
                    if ($("#loader-overlay")) {
                        $("#loader-overlay").hide()
                        $("#contentwrapper").show()
                    }
                    callback(myevent)
                })
            })
        }
    }

    initListeners() {
    }
}

