import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class Event {

    constructor(event_id=undefined, event_data={}) {

        Event.instance = this
        Event.instance.event_id = event_id
        /* Event.instance.external_id = external_id */
        
        let data = event_data?.data || {}
        Event.instance.title = data?.name
        Event.instance.location = data?.location
        Event.instance.desc = data?.desc
        Event.instance.participants = data?.participants || {}
        Event.instance.organisers = data?.organisers || {}
        Event.instance.start = data?.startdatetime
        Event.instance.end = data?.enddatetime
        Event.instance.status = data?.status
        Event.instance.max_participants = data?.max_participants
        Event.instance.tags = data?.tags
        Event.instance.send_participant_reminders = data?.send_participant_reminders
        Event.instance.state_machine_template = data?.state_machine_template
        Event.instance.additional_text = data?.additional_text
    }
    
    deleteParticipant() {

    }
    
    setParticipants(participants) {
        Event.instance.participants = participants
    }

    getParticipants() {
        return Event.instance.participants
    }

    signUpOrganiser() {
        Event.instance.organisers.push(participant_object)
        return this
    }

    deleteOrganiser() {

    }

    setOrganisers(organisers) {
        Event.instance.organisers  = organisers
    }

    getDataFormattedForApiCall() {
        let payload = {
                "event_id": Event.instance.event_id,
                "data": {
                    "name": Event.instance.title,
                    "location": Event.instance.location,
                    "desc":Event.instance.desc,
                    "status": "planned",
                    "startdatetime": Event.instance.start,
                    "enddatetime": Event.instance.end,
                    "max_participants": Event.instance.max_participants,
                    "organisers": Event.instance.organisers,
                    "participants": Event.instance.participants,
                    "tags": Event.instance.tags || [],
                    "send_participant_reminders": Event.instance.send_participant_reminders,
                    "state_machine_template": Event.instance.state_machine_template || null,
                    "additional_text": Event.instance.additional_text || null
                },
                "links": [],
                "data_owners": []
            }

        if(!Event.instance.event_id || Event.instance.event_id.length == 0) {
            delete payload["event_id"]
        }
        return payload
    }

    

    getOrganisers() {
        return Event.instance.organisers
    }

    setTitle(title) {
        Event.instance.title = title
    }

    getTitle(){
        return Event.instance.title
    }

    setDescription(desc) {
        Event.instance.desc = desc
    }
    
    getDescription() {
        return Event.instance.desc
    }

    setMaxCapacity(max) {
        Event.instance.max_participants = max
    }
    
    getMaxCapacity(){
        return Event.instance.max_participants
    }

    getNumberofParticipants(){
        return Event.instance.participants.length
    }

    setStartTime(start) {
        Event.instance.start = start
    }

    setEndTime(end) {
        Event.instance.end = end
    }
    
    setLocation(location) {
        Event.instance.location = location
    }
    

    addEvent(addPayload) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewEvent",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(addPayload),
                "success": (response) => resolve(response),
                "error": reject,
            };

            $.ajax(settings)
        })
    }

    create() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewEvent",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(Event.instance.getDataFormattedForApiCall()),
                "success": (response) => resolve(response),
                "error": reject,
            };

            $.ajax(settings)
        })
    }

    update() {
        let payload = Event.instance.getDataFormattedForApiCall()

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateEvent",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => resolve(response),
                "error": reject,
            };

            $.ajax(settings)
        })
    }

    delete() {
        return new Promise(function (resolve, reject) {
            if(Event.instance.event_id){
                let payload = {
                "event_id": Event.instance.event_id
                }
                
                var settings = {
                    "url": CONFIG.API_BASE_URL + "/es/cmd/deleteEvent",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                    },
                    "data": JSON.stringify(payload),
                    "success": (response) => resolve(response),
                    "error": reject,
                };

                $.ajax(settings)
            } else {
                reject({"error_code": "no valid event_id provided"})
            }
        })
    }

    
    signUpParticipant(participantPayload) {
        if(Event.instance.state_machine_template) {
            return Event.instance.startStateMachineConfigProcess(participantPayload.personal_data.raw.firstname, participantPayload.personal_data.raw.lastname, participantPayload.personal_data.raw.email, participantPayload.personal_data.raw.phone)
        } else {
            let payload = {}
            payload['event_id'] = Event.instance.event_id ? Event.instance.event_id : ''
            payload['data'] = participantPayload

            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": CONFIG.API_BASE_URL + "/es/cmd/signUpForEvent",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey"),
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify(payload),
                    "success": (response) => resolve(response),
                    "error": reject,
                };

                $.ajax(settings)
            })
        }
    }

    startStateMachineConfigProcess(firstname, lastname, email, phone="") {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/statemachine/create_state_machine_from_template/" + Event.instance.state_machine_template,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "phone": phone,
                    "info": {"phone": phone},
                    "email": email,
                    "firstname": firstname,
                    "lastname": lastname,
                    "redirect_url_success": encodeURIComponent(CONFIG.APP_BASE_URL + "/eventmanager/registration-success.html"),
                    "redirect_url_failure": encodeURIComponent(CONFIG.APP_BASE_URL + "/eventmanager/registration-failure.html"),
                    "eventdata": {
                        "event_id": Event.instance.event_id,
                        "desc": Event.instance.desc,
                        "enddatetime": Event.instance.end,
                        "name": Event.instance.title,
                        "startdatetime": Event.instance.start
                    }
                }),
                "success": function (response) {
                    console.log("Response", response);
                    Event.instance.moveToFirstStepOfConfig(response.meta.persistent_id).then(function(){
                        resolve(response)
                    })

                },
                "error": function () {
                    reject()
                }
            };

            $.ajax(settings)
        })
    }

    moveToFirstStepOfConfig(id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + `/statemachine/state_machine_instance/${id}/go_to_next_state`,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({}),
                "success": function (response) {
                    resolve()
                },
                "error": function () {reject()}
            };

            $.ajax(settings)
        })
    }
    
    getEventById(event_id) {
        return new Promise(function(resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_events/" + event_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey")
                },
                "success": (response) => resolve(response),
                "error": reject,
              };
  
              $.ajax(settings)
            })
    }

    

}