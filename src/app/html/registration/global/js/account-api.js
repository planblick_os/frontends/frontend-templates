apikey = $.cookie("apikey");

function checkLogin(isLoginPage=false, checkagainTime=30000, health_endpoint="/has_valid_login")
{
    if(typeof(apikey)=="undefined")
    {
        redirect_file = "index.html"
        current_url = window.location.pathname;
        current_filename = current_url.substring(current_url.lastIndexOf('/')+1);
        if(redirect_file != current_filename) {
            $.blockUI({ message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>', timeout: 3000, onUnblock: function() {
                    $(location).attr('href', '/login/')
                }});
        }
    }
    else
    {
        var settings = {
          "async": false,
          "crossDomain": true,
          "url": API_BASE_URL + health_endpoint,
          "method": "GET",
          "headers": {
            "apikey": $.cookie("apikey"),
            "Cache-Control": "no-cache"
          }
        }

        $.ajax(settings).success(function (response) {
            setTimeout(checkLogin, checkagainTime, isLoginPage, checkagainTime);
            if(isLoginPage) {
                redirectDialog()
            }
        });
        $.ajax(settings).error(function (response) {
            if(!isLoginPage)
            {
                 $.blockUI({ message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>', timeout: 3000, onUnblock: function() {
                    $(location).attr('href', '/login/')
                }});
            }
        });
    }
}

function logout()
{

    apikey = $.cookie("apikey");

    result = false

    postUrl = API_BASE_URL + "/logout";
    $.ajax({
        type: "GET",
        url: postUrl,
        contentType: 'application/json;charset=UTF-8',
        data: "",
        beforeSend: function (xhr){
            xhr.setRequestHeader('apikey', apikey);
        },
        success: function (response) {
            if(typeof(response) != "undefined")
            {
                $.removeCookie("apikey", {path: '/'});
                $.blockUI({ message: '<div class="responseMessage">Sie wurden abgemeldet.</div>', timeout: 3000, onUnblock: function() {
                    $(location).attr('href', '/login/')
                }});
            }
            else {
                $.removeCookie("apikey", {path: '/'});
                $.blockUI({ message: '<div class="responseMessage">Sie wurden  abgemeldet.</div>'});

            }
        },
        error: function(){
            $.removeCookie("apikey", {path: '/'});
            $.blockUI({ message: '<div class="responseMessage">Ihr Browser wurde  abgemeldet.</div>'});
        }
      }).done(function(o) {

      });


}

function getapikey(success_redirect = null)
{
  apikey = $.cookie("apikey");
  console.debug(apikey)
  username = document.getElementById("login").value;
  password = document.getElementById("password").value;

  $.blockUI({ message: '<div class="responseMessage">Ihre Zugangsdaten werden überprüft ...</div>' });
  postUrl = API_BASE_URL + "/login";

  $.ajax({
    type: "GET",
    url: postUrl,
    contentType: 'application/json;charset=UTF-8',
    data: "",
    beforeSend: function (xhr){
        xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
    },
    success: function (response) {
        if(typeof(response) != "undefined")
        {
            $.blockUI({ message: '<div class="responseMessage">Login successfull</div>', timeout: 1000 });
            $.cookie("apikey", response.apikey, {path: '/'});
            $.cookie("consumer_name", response.consumer_name, {path: '/'});
            $.cookie("consumer_id", response.consumer.id, {path: '/'})
            $.cookie("username", response.username, {path: '/'});
            //$(location).attr('href', 'beta.html')
            if(success_redirect) {
                redirectDialog(success_redirect)
            } else {
                redirectDialog(DEFAULT_LOGIN_REDIRECT)
            }
        }
        else {
           $.removeCookie("apikey", {path: '/'});
           $.unblockUI()
           $.blockUI({ message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',timeout: 3000 });
        }
    },
    error: function(){
        $.removeCookie("apikey", {path: '/'});
        $.unblockUI()
        $.blockUI({ message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',timeout: 3000 });

    }
  }).done(function(o) {

  });
}

function redirectDialog(url=null)
{
    if(url == null) {
        location.href = "/login/"
    }
    else {
        location.href = url
    }
}

function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

function perform_login() {
    urlParams = new URLSearchParams(window.location.search)
    if(urlParams.has('r')) {
        getapikey(APP_BASE_URL + urlParams.get('r'))
    } else {
        getapikey()
    }
}

function getapikeyfor(username, password, callback) {
        let postUrl = API_BASE_URL + "/login";
        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
            },
            success: function (response) {
                //console.log("RESPONSE", response)
                if (typeof (response) != "undefined") {
                    callback(response.apikey)
                } else {
                   callback(response.apikey)
                }
            },
            error: function () {
               callback(false)
            }
        }).done(function (o) {

        });
    }
