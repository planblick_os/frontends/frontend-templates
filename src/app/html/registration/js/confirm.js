document.addEventListener("DOMContentLoaded", function () {
    var url_string = window.location.href
    var url = new URL(url_string);
    var k = url.searchParams.get("k");

    var data = null;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            console.log(this.responseText);
        }
    });

    xhr.open("GET", API_BASE_URL + "/confirm_registration?k=" + k);
    xhr.setRequestHeader("cache-control", "no-cache");

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                document.getElementById("register").innerHTML = "Registrierung bestätigt"
                document.getElementById("register_content").innerHTML = 'Vielen Dank für Deine Registrierung.<br/>Deine Registrierung ist jetzt abgeschlossen und Du kannst Dich mit Deinen Daten <a href="/login/">anmelden.</a> '
            } else {
                document.getElementById("register").innerHTML = "Bestätigung fehlgeschlagen"
                document.getElementById("register_content").innerHTML = "Leider kann die Bestätigung gerade nicht durchgeführt werden. Bitte Versuche es etwas später erneut. "
            }
        }

    }

    xhr.send(data);


});

