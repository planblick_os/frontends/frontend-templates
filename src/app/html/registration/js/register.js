import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {validateForm, isEmail, fadeIn} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {fireEvent, encrypt} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {RegistrationModule} from "./csf-load-module.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"


function validate() {

    let return_value = true

    if (validateForm({ 'login': 'input' }) == false) {

        document.getElementById('login-error-msg').innerHTML = '<div class="alert alert-danger error_message">* Die Benutzername muss länger als 4 Zeichen sein und darf nur Buchstaben, Zahlen und Unterstriche enthalten – keine Leerzeichen. *</div>'
        fadeIn("login-error-msg")
        return_value = false

    } else if (validateForm({ 'email': 'email' }) == false || isEmail($("#email").val()) == false) {

        document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>* Das angegebene E-Mail stimmt nicht. *</div>"
        fadeIn("login-error-msg")
        return_value = false

    } else if (validateForm({ 'password': 'password' }) == false) {

        document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>* Das Passwort muss mindestens 8 Zeichen ohne Lehrzeichen, mit mindestens 1 Groß-, 1 Kleinbuchstaben, 1 Ziffer und 1 Sonderzeichen enthalten. *</div>"
        fadeIn("login-error-msg")
        return_value = false

    } else if (validateForm({ 'password_confirmation': 'input' }) == false || $("#password").val() != $("#password_confirmation").val()) {

        document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>* Das Passwort und das Passwort-Bestätigung stimmen nicht überein. *</div>"
        fadeIn("login-error-msg")
        return_value = false

    } else if (!$("#privacy_agreement").is(':checked')) {

        document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>* Sie müssen unsere Datenschutzerklärung akzeptieren um sich registrieren zu können. *</div>"
        fadeIn("login-error-msg")
        return_value = false

    }

    return return_value


}



export function validateInviteForm() {

    if (validate() == true) {

        fireEvent("registerInviteButtonClicked", {
            "login": $("#login").val(),
            "email": $("#email").val(),
            "password": $("#password").val()
        })

    }

}


export function validateRegistrationForm() {
   
    if (validate() == true) {

        fireEvent("registerButtonClicked", {
            "login": $("#login").val(),
            "email": $("#email").val(), 
            "password": $("#password").val()
        })

    }

}


export function registerInvite(apikey, username, password, email) {

    Fnon.Box.Circle('form','', {
        svgSize: { w: '50px', h: '50px' },
        svgColor: '#3a22fa',
   })
    var settings = {
        "url": CONFIG.API_BASE_URL + "/newlogin",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "apikey": apikey,
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "username": username,
            "password": password,
            "email": email
        }),
        "success": (response) => {
            new RegistrationModule().task_id_handlers[response.taskid] = function(args) {
                delete new RegistrationModule().task_id_handlers[response.taskid]
                Fnon.Box.Remove('form')
                if(args.data.data.args["error_message"]) {
                    Fnon.Hint.Danger(new pbI18n().translate("Login wasn't created") + " <br/>" + new pbI18n().translate(args.data.data.args["error_message"]), {displayDuration: 6000}, function () {

                    })
                } else {
                    Fnon.Hint.Success(new pbI18n().translate('Login has been created'), function () {
                        document.location = CONFIG.LOGIN_URL + "?login=" + encodeURI(username) + "&r=/dashboard/profile-settings.html"
                    })
                }
            }
        }
    };

    $.ajax(settings)
}

