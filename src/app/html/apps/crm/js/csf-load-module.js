import {PbAccount} from "../../../csf-lib/pb-account.js?v=[cs_version]"
import {socketio} from "../../../csf-lib/pb-socketio.js?v=[cs_version]"
import {getCookie, fireEvent, getQueryParam} from "../../../csf-lib/pb-functions.js?v=[cs_version]";
import {pbI18n} from "../../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Layout} from "./layout.js?v=[cs_version]";
import {CrmAppBase} from "/app_modules/csf-load-module-base.js?v=[cs_version]"


export class CrmApp extends CrmAppBase {
    actions = {}
    loaded_submodule_controllers = {}

    constructor() {
        super();
        if (!CrmApp.instance) {
            this.initListeners()
            this.initActions()
            CrmApp.instance = this;
            CrmApp.instance.layout = undefined;
        }

        return CrmApp.instance;
    }

    initActions() {
        this.actions["crmAppInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(function () {
                if (!new PbAccount().hasPermission('frontend')) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."));
                } else {
                    let layout = getQueryParam("layout") || "threecolumn"
                    CrmApp.instance.layout = new Layout(layout, "#contentwrapper")
                    CrmApp.instance.layout.render().then(function () {
                        let module_promises = []

                        let dummyModulePromise = CrmApp.instance.getAppModuleControllerClass("dummy")
                        let taskmanagerModulePromise = CrmApp.instance.getAppModuleControllerClass("taskmanager")
                        let contactmanagerModulePromise = CrmApp.instance.getAppModuleControllerClass("contactmanager")

                        module_promises.push(dummyModulePromise, taskmanagerModulePromise, contactmanagerModulePromise)

                        dummyModulePromise.then(function (dummyModuleController) {
                            let dummyModuleInstance = new dummyModuleController(CrmApp.instance, {
                                "list": "#column1-content",
                                "detail": "#column2-content",
                                "form": "#column3-content"
                            })
                            CrmApp.instance.loaded_submodule_controllers["dummy"] = dummyModuleInstance
                            //dummyModuleInstance.renderListView()
                            //dummyModuleInstance.renderDetailView()
                            //dummyModuleInstance.renderFormView()
                        })

                        taskmanagerModulePromise.then(function (taskmanagerModuleController) {
                            let taskmanagerControllerInstance = new taskmanagerModuleController(CrmApp.instance, {
                                "list": "#column1-content",
                                "detail": "#column2-content",
                                "form": "#column3-content"
                            })

                            CrmApp.instance.loaded_submodule_controllers["taskmanager"] = taskmanagerControllerInstance
                            taskmanagerControllerInstance.init().then(() => {
                                taskmanagerControllerInstance.renderListView()
                                taskmanagerControllerInstance.renderDetailView()
                                //taskmanagerModuleInstance.renderFormView()
                            })
                        })

                        contactmanagerModulePromise.then(function (contactModuleController) {
                            let contactControllerInstance = new contactModuleController(CrmApp.instance, {
                                "list": "#column3-content",
                                "detail": "#column2-content",
                                "form": "#column3-content"
                            })
                            CrmApp.instance.loaded_submodule_controllers["contact"] = contactControllerInstance
                            contactControllerInstance.init().then(() => {
                                //contactControllerInstance.renderListView()
                                //contactControllerInstance.renderDetailView()
                                //contactControllerInstance.renderFormView()
                            })
                        })


                        Promise.all(module_promises).then(function (return_values) {
                            CrmApp.instance.layout.ready_for_user()
                        })
                    })
                }
            });
        }
    }
    initListeners() {
        document.addEventListener("dummy_refresh_list", function (args) {
            CrmApp.instance.loaded_submodule_controllers["contact"].renderListView()
            //CrmApp.instance.loaded_submodule_controllers["taskmanager"].renderListView()
        })
        document.addEventListener("taskmanager_refresh_list", function (args) {
            CrmApp.instance.loaded_submodule_controllers["contact"].renderListView()
            //CrmApp.instance.loaded_submodule_controllers["dummy"].renderListView()
        })
        document.addEventListener("contactmanager_refresh_list", function (args) {
            //CrmApp.instance.loaded_submodule_controllers["taskmanager"].renderDetailView()
            $("#column3-content").html("")
        })

        document.addEventListener("taskmanager_select_contact_button_click", function (args) {
            //$("#toggleColumn3").click()
            CrmApp.instance.loaded_submodule_controllers["contact"].renderListView()
        })

        document.addEventListener("contact_select_btn_clicked", function (args) {
            $(CrmApp.instance.loaded_submodule_controllers["contact"].targetselectors.list).html("")
        })

        document.addEventListener("contact-list-button-cancel", function (args) {
            $(CrmApp.instance.loaded_submodule_controllers["contact"].targetselectors.list).html("")
        })
    }
}