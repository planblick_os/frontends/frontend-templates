import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]";
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]";
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]";

export class Easy2TrackModule {
    actions = {}
    constructor() {
        if (!Easy2TrackModule.instance) {
            this.initListeners()
            this.initActions()
            Easy2TrackModule.instance = this;
        }

        return Easy2TrackModule.instance;
    }

    initActions() {
        this.actions["moduleEasy2TrackVisitorsInitialized"] = function(myevent, callback=() => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("easy2track")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new PbAccount().handlePermissionAttributes()
                    callback(myevent)
                }
            })
            callback(myevent)
        }

        this.actions["loginButtonClicked"] = function(myevent, callback=() => "") {
            new PbAccount().getapikey()
            callback(myevent)
        }
    }

    initListeners() {
    }
}