import {CONFIG} from '../pbcdn/config.js?v=[cs_version]';
import {STATE} from '../pbcdn/globalState.js?v=[cs_version]';
import {init_console} from '../pbcdn/logger.js?v=[cs_version]';
import {pb_keys} from './keys.js?v=[cs_version]'
import {add_pb_listeners} from './eventlistener.js?v=[cs_version]'
import {fireEvent, waitFor} from '../pbcdn/functions.js?v=[cs_version]';
import {Account} from "../pbcdn/account-api.js?v=[cs_version]";
import {socketio} from "./sockets.js?v=[cs_version]";
import {setTemporaryGuestKey} from "./app.js?v=[cs_version]";


if (CONFIG.SEND_CONSOLE_LOG) {
    init_console()
}

export class Init {
    constructor() {
        this.loading_files = {}
        this.needed_js_files = [
            {name: "jqueryblockUI", path: "./pbcdn/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
            {name: "jquery_cookie", path: "./pbcdn/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
            {name: "jquery_blockUI", path: "./pbcdn/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
            {name: "semanticui_js", path: "./pbcdn/vendor/semanticui/semantic.min.js?v=[cs_version]", loaded: null},
            {name: "socketio", path: "./pbcdn/vendor/socket.io.js?v=[cs_version]", loaded: null},
            {name: "easyqrcode_js", path: "./pbcdn/vendor/easy.qrcode.min.js?v=[cs_version]", loaded: null},
            {name: "moment_js", path: "./pbcdn/vendor/moment-with-locales.min.js?v=[cs_version]", loaded: null},
            {name: "moment-timezone_js", path: "./pbcdn/vendor/moment-timezone.js?v=[cs_version]", loaded: null},
            {name: "aes_js", path: "./pbcdn/vendor/aes.js?v=[cs_version]", loaded: null},

        ]
        if (!Init.instance) {
            Init.instance = this;
        }

        document.addEventListener("js_sources_loaded", function () {
            let isLoginPage = false
            let checkagainTime = 30000
            let health_endpoint = "/has_valid_login"
            let onfailure=function() {$('#login_modal').modal('setting', 'closable', false).modal('show')}
            new Account().checkLogin(isLoginPage, checkagainTime, health_endpoint, onfailure)
            new socketio().init()
            new Account().getapikeyfor("guest", "guest", setTemporaryGuestKey)
            $("#spinner").css("display", "none")
            if(document.getElementById("explain_video")) {
                document.getElementById("explain_video").play();
            }

        }, false);

        return Init.instance;
    }

    load() {
        add_pb_listeners()
        this.load_files()
    }

    init_submodules() {
        new pb_keys().init()
        fireEvent('init_submodules')
    }

    load_files() {
        $.ajaxSetup({
            cache: CONFIG.JS_CACHE_ACTIVE
        });
        for (let file of this.needed_js_files) {
            this.load_file(file)
        }

        waitFor(this.finished_loading_files, Init.instance.files_loaded)
    }

    files_loaded() {
        let evt = new Event('js_sources_loaded');
        document.dispatchEvent(evt);
    }

    finished_loading_files() {
        let still_loading = false
        for (let key of Object.keys(Init.instance.loading_files)) {
            console.log(key)
            if (Init.instance.loading_files[key] == true) {
                still_loading = true
            }
        }
        console.log(still_loading, Init.instance.loading_files)
        return !still_loading
    }

    load_file(file) {
        Init.instance.loading_files[file.name] = true
        $.getScript(file.path)
            .done(function (script, textStatus) {
                fireEvent(file.name + '_loaded')
                Init.instance.loading_files[file.name] = false
            })
            .fail(function (jqxhr, settings, exception) {
                file.loaded = false
                console.warn(file.path, exception);
                fireEvent('js_file_load_failed', file)
            });
    }
}
