import {Init} from './init.js?v=[cs_version]'
import {pb_events} from './events.js?v=[cs_version]'
import {i18n} from '../pbcdn/i18n.js?v=[cs_version]'
import {CONFIG} from '../pbcdn/config.js?v=[cs_version]';
import {STATE} from '../pbcdn/globalState.js?v=[cs_version]';
import {Account} from '../pbcdn/account-api.js?v=[cs_version]';
import {socketio} from "./sockets.js?v=[cs_version]";
import {qrcode} from "./qrcode.js?v=[cs_version]"
import {setTemporaryGuestKey} from "./app.js?v=[cs_version]";
import {createQRCode, automatic_mode_toggle, abort_scan} from './app.js?v=[cs_version]';
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]";



export function add_pb_listeners() {
    document.addEventListener("DOMContentLoaded", function (event) {

        if (CONFIG.ENFORCE_SSL && location.protocol !== 'https:') {
            location.replace(`https:${location.href.substring(location.protocol.length)}`);
        }
        new pb_events().content_switched("qrcode")
    })

    document.addEventListener("content_switched", function (el) {
        let content_id = el.detail.content_id;
        $('[data-content]').removeClass("active")
        $('[data-content="' + content_id + '"]').addClass("active")
    }, false);

    document.addEventListener("js_sources_loaded", function () {
        new Init().init_submodules()
        //loadAds()
    }, false);

    document.addEventListener("jquery_cookie_loaded", function () {
        new i18n().get_translations()
    }, false);

    document.addEventListener("i18n_ready", function () {
        new i18n().replace_i18n()
    }, false);

    document.addEventListener("js_file_load_failed", function _js_file_load_failed(event) {
        if (!STATE["file_load_tries"][event.data.name]) {
            STATE["file_load_tries"][event.data.name] = 0
        }

        STATE["file_load_tries"][event.data.name] += 1

        if (STATE["file_load_tries"][event.data.name] < CONFIG.FILE_LOAD_FAILED_MAX_RETRIES) {
            setTimeout(function () {
                Init.instance.load_file(event.data)
            }, CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT)
        } else {
            console.error("File" + event.data.path + " cannot be loaded after " + STATE["file_load_tries"] + " Giving up now")
        }

    }, false);

    $("#login_modal_button").click(function() {
        new Account().getapikey(function() {
            location.reload()
        })
    })
   

    $("#logout_button").click(function (event) {
        new PbAccount().logout()
            
    })
   
    $("#correct_image_button").click(function () {
        console.log("correcting form")
        qrcode.correctQR('#lastname')
    })

    $("#save_image_button").click(function () {
        console.log("correcting form")
        qrcode.saveQRCode()
    })

    $("#print_image_button").click(function () {
        console.log("correcting form")
        qrcode.openQRForPrint('qr_image')
    })

    $("#contact_agb").click(function() {
        $("#qrcode_generate_button").attr('disabled', !$("#qrcode_generate_button").attr('disabled'))
    })

    $("#qrcode_generate_button").click(function () {
        createQRCode()
    })
    

    $("#save_data").click(function () {
       // $('#save_data').prop('checked', !$('#save_data').prop('checked'))
            let save_data = $("#save_data").prop("checked")
            console.log("what is my save_date value" + save_data)
    })

    $("#privacy_agreement").click(function () {
        $("#save_contact_button").attr('disabled', !$("#save_contact_button").attr('disabled'))
    })


}


