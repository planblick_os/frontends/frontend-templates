export function copyToClipBoard(element_id) {
    var copyText = document.getElementById(element_id);
    copyText.select();
    document.execCommand("copy");
}

export function waitFor(condition, callback) {
    if(!condition()) {
        console.log('waiting');
        window.setTimeout(waitFor.bind(null, condition, callback), 100); /* this checks the flag every 100 milliseconds*/
    } else {
        console.log('done');
        callback();
    }
}

export function renderNews(event) {
    if ($.cookie("read_news") && $.cookie("read_news").includes(event.content_id))
        return
    $("#news_bell").addClass("active")
    let doc = new DOMParser().parseFromString(event.data, "text/xml");
    let news_content = doc.getElementById(event.content_id)

    let approve_button = $('<div class="ui approve button news_read_button" onclick="setNewsCookieRead(\'' + event.content_id + '\')">Als gelesen markieren</div>')

    $("#news_container").append(news_content.innerHTML)
    $("#news_container").append(approve_button)
    $("#news_container").append("<br/><br/><br/>")

    if (event.force_display) {
        $("#news_modal").modal('show')
    }
}

export function setNewsCookieRead(news_content_id) {
    $("#news_bell").removeClass("active")
    let cookie_content = $.cookie("read_news")
    if (!cookie_content)
        cookie_content = ""
    cookie_content += "/" + news_content_id
    $.cookie("read_news", cookie_content, { expires: 730, path: '/' })
    $("#news_modal").modal('hide')
}

export function toggleOverlay() {
    if ($.cookie("e2s_show_extended_props") == "true") {
        $('#extended_props_wrapper').show();
    }
    if (document.getElementById("overlay").style.display == "block") {
        document.getElementById("overlay").style.display = "none";
    } else {
        document.getElementById("overlay").style.display = "block";
    }
}

export function post(url, data, callback) {
    var data = JSON.stringify(data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status == 200 || this.status == 204) {
                callback(this.responseText);
            } else {
                alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");
                calendar.refetchEvents();
            }
        }
    });

    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("apikey", getCookie("apikey"));
    xhr.send(data);
}

export function get(url, callback, ignoreErrors) {
    var xhr = new XMLHttpRequest();
    xhr.url = url;
    xhr.withCredentials = true;
    xhr.ignoreErrors = ignoreErrors

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status == 200 || this.status == 204 || ignoreErrors) {
                callback(this.responseText);
            } else {
                alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");
            }
        }
    });

    xhr.open("GET", url);
    xhr.setRequestHeader("apikey", $.cookie("apikey"));
    xhr.send();
}

export function enterFullscreen(element) {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    }
}

export function showHelp() {
    $('#legend_wrapper').hide("slide")
    if (document.getElementById("extended_form_help")) {
        $("#custom_help_wrapper").html(document.getElementById("extended_form_help").innerHTML)
    }
    $("#helpFormWrapper").toggle("slide")
}

export function fireEvent(event_name, data={}) {
    let evt = new Event(event_name);
    evt.data = data
    document.addEventListener(event_name, function _event_logger(event) {
               console.log("Event fired", event_name, event.data)
                document.removeEventListener(event_name, _event_logger)
        }, false);
    document.dispatchEvent(evt);
}

export function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

export function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

export function encrypt(value, passphrase) {
    return CryptoJS.AES.encrypt(value, passphrase)
}

export function decrypt(value, passphrase) {
    return CryptoJS.AES.decrypt(value, passphrase).toString(CryptoJS.enc.Utf8)
}