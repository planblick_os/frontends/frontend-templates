import {CONFIG} from './config.js?v=[cs_version]';
import {STATE} from './globalState.js?v=[cs_version]';
import {init_console} from './logger.js?v=[cs_version]';
// import {pb_keys} from './keys.js?v=[cs_version]'
import {add_pb_listeners} from './eventlistener.js'
import {fireEvent, waitFor} from './functions.js';

if (CONFIG.SEND_CONSOLE_LOG) {
    init_console()
}

export class Init {
    constructor() {
        this.loading_files = {}
        this.needed_js_files = [
            {name: "jqueryblockUI", path: "../global/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
            {name: "jquery_cookie", path: "../global/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
            {name: "jquery_blockUI", path: "../global/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
            {name: "semanticui_js", path: "../global/vendor/semanticui/semantic.min.js?v=[cs_version]", loaded: null},
        ]
        if (!Init.instance) {
            Init.instance = this;
        }

        return Init.instance;
    }

    load() {
        add_pb_listeners()
        this.load_files()
    }

    init_submodules() {
        new pb_keys().init()
    }

    load_files() {
        $.ajaxSetup({
            cache: CONFIG.JS_CACHE_ACTIVE
        });
        for (let file of this.needed_js_files) {
            this.load_file(file)
        }

        waitFor(this.finished_loading_files, Init.instance.files_loaded)
    }

    files_loaded() {
        let evt = new Event('js_sources_loaded');
        document.dispatchEvent(evt);
    }

    finished_loading_files() {
        let still_loading = false
        for (let key of Object.keys(Init.instance.loading_files)) {
            console.log(key)
            if (Init.instance.loading_files[key] == true) {
                still_loading = true
            }
        }
        console.log(still_loading, Init.instance.loading_files)
        return !still_loading
    }

    load_file(file) {
        Init.instance.loading_files[file.name] = true
        $.getScript(file.path)
            .done(function (script, textStatus) {
                fireEvent(file.name + '_loaded')
                Init.instance.loading_files[file.name] = false
            })
            .fail(function (jqxhr, settings, exception) {
                file.loaded = false
                console.warn(file.path, exception);
                fireEvent('js_file_load_failed', file)
            });
    }
}
