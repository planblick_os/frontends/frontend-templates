window.tour_config = {
    header: "Touren im Online Kalender",
    intro: "Schnellstarthilfe für den Kalender",
    tours: {
        firstSteps: {
            id: "firstSteps",
            title: "Erste Schritte im Online Kalender",
            desc: "Lerne die Oberfläche des Online Kalenders kennen.",
            startPage: "/easy2schedule/index.html",
            startStep: 1,
            prevTour: null,
            nextTour: "resourceSettings",
            permission: "easy2schedule"
        },
        resourceSettings:
            {
                id: "resourceSettings",
                title: "Neue Kalenderressourcen anlegen, verwalten und löschen",
                desc: "Lerne, wie man eine Kalenderressource anlegen, bearbeiten und wieder löschen kann.",
                startPage: "/easy2schedule/resource-settings.html",
                startStep: 11,
                prevTour: "firstSteps",
                nextTour: "columnSettings",
                permission: "easy2schedule"
            },
        columnSettings:
            {
                id: "columnSettings",
                title: "Kalenderspalten konfigurieren",
                desc: "Lerne, wie man Kalenderansichten und -spalten individuell konfigurieren kann.",
                startPage: "/easy2schedule/column-settings.html",
                startStep: 19,
                prevTour: "resourceSettings",
                nextTour: null,
                permission: "easy2schedule"
            }
    },
    tourSteps: [
        {//1
            title: 'Willkommen',
            disableInteraction: true,
            intro: 'Willkommen im <strong>Online Kalender</strong>.<br/> Diese Tour führt dich durch die drei wesentliche Inhaltselemente des Kalenders.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>',
        },
        {//2
            title: 'Aktionsmenü',
            element: '#side_menu_wrapper',
            disableInteraction: true,
            intro: 'Auf der linken Seite wird dir das Menü mit einer Auswahl der verschiedenen Kalenderansichten und Aktionen angezeigt.',
            'myBeforeChangeFunction': function (PbGuide) {
            },
        },
        {//3
            title: 'Suche',
            disableInteraction: true,
            intro: 'Über der Kalenderansicht oder im Navigationsmenü unter mobilen Ansicht, befindet sich ein Eingabefeld, mit dem Sie Ihre Termine durchsuchen können. Wenn du nach etwas suchst, werden nur noch die Termine angezeigt in denen der Suchbegriff vorkommt. Um wieder alle Einträge angezeigt zu bekommen, leere einfach das Suchfeld wieder.'
        },
        {//4
            title: 'Kalenderansicht mit Terminen',
            disableInteraction: true,
            intro: 'Kommen wir jetzt zum Herzstück, das Kalenderfeld und deinen Terminen. Wechsel zwischen den Tages-, Wochen- und Monatsansichten, um den Hauptbereich des Kalenders zu ändern.',
            'myBeforeChangeFunction': function (PbGuide) {
                let icon_nav = document.querySelector('#icon_side_menu_toggleMenu')
                if ($("#icon_side_menu_toggleMenu").is(":visible")){
                    icon_nav.click()
                }
            }
        },
        {//5
            title: 'Termin erstellen',
            element: '#create-appointment-button',
            disableInteraction: true,
            intro: 'Mit einem Klick auf die <strong>Neuer Termin</strong>-Schaltfläche kannst du einen neuen Termin erstellen. Alternativ klappt das auch mit einem Doppel-Klick direkt in das Kalenderfeld.',
            'myBeforeChangeFunction': function (PbGuide) {
                let object1 = document.querySelector('#create-appointment-button')
                object1.click()
            },
        },
        {//6
            title: 'Termin-Dialog',
            disableInteraction: false,
            intro: 'Im Termin-Dialog kannst du alle relevanten Informationen zu einem Termin speichern. In den folgenden Schritten lernst du die häufigst genutzten Funktionen kennen.',
            'myAfterChangeFunction': function (PbGuide) {
                    let lrg_nav = document.querySelector('#menue_header_icon')
                    if ($(window).width() < 767) { 
                        lrg_nav.click()
                        PbGuide.updateIntro("Im Termin-Dialog kannst du alle relevanten Informationen zu einem Termin speichern. Gib deinem Termin einen Titel, füge Teilnehmer aus deinen Kontakten oder den Kalendermitgliedern hinzu und klick auf die <strong>Anlegen</strong>-Schaltfläche um deinen Termin zu erstellen.")
                        PbGuide.setNextButton("Weiter", function () {
                            PbGuide.introguide.goToStepNumber(10)
                    })
                        }
            },
        },
        {//7
            title: 'Title',
            element: '#title',
            disableInteraction: true,
            intro: 'Gib deinem Termin einen Titel.',
            'myBeforeChangeFunction': function (PbGuide) {
            },
        },

        {//8
            title: 'Teilnehmer',
            element: '#appointment-participants',
            disableInteraction: true,
            intro: 'Füge Teilnehmer aus deinen Kontakten oder den Kalendermitgliedern hinzu.',
            'myBeforeChangeFunction': function (PbGuide) {
            },
        },
        {//9
            title: 'Erstellen',
            element: '#save',
            disableInteraction: true,
            intro: 'Klick auf die <strong>Anlegen</strong>-Schaltfläche um deinen Termin zu erstellen.',
            'myBeforeChangeFunction': function (PbGuide) {
            },
        },
        {//10
            title: 'Tour abgeschlossen',
            intro: 'Damit hast du unseren Rundgang abgeschlossen. Wenn du möchtest, können wir aber auch gleich mit der nächsten Tour weitermachen. Du kannst diese aber auch später starte,n indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong>',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("easy2schedule.resourceSettings")
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.firstSteps.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            },
            'myAfterChangeFunction': function (PbGuide) {
                let object = document.querySelector('#cancel')
                object.click()
            }
        },
        {//11
            title: 'Kalenderressource anlegen',
            intro: 'Diese Tour zeigt dir interaktiv, wie du ganz einfach eine <strong>Kalenderressource anlegen</strong>, <strong>verwalten</strong> und wieder <strong>löschen</strong> kannst.<br/> Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>',
            'myBeforeChangeFunction': function (PbGuide) {
                if (!PbGuide.ensureUrl(PbGuide.tourConfig.tours.resourceSettings.startPage, "resourceSettings")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//12
            title: 'Kalenderressource anlegen',
            element: '#btn-create-resource',
            intro: 'Um eine neue Kalenderressource anzulegen, klicke jetzt bitte auf den <strong>Ressource anlegen</strong>-Button.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()

                let object = document.querySelector('#btn-create-resource');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//13
            title: 'Kalenderressource anlegen',
            intro: 'Jetzt wird dir ein Eingabe-Dialog angezeigt in den du die Daten der Ressource eintragen kann.',
            'myBeforeChangeFunction': function (PbGuide) {

            },
        },
        {//14
            title: 'Kalenderressource anlegen',
            element: '#resource_name',
            disableInteraction: false,
            intro: 'Hier kannst du den Namen der Ressource eintragen. Dieser wird dann im "Termin anlegen"-Dialog angezeigt.',
            'myBeforeChangeFunction': function (PbGuide) {

            },
        },
        {//15
            title: 'Kalenderressource anlegen',
            element: '#resource_type',
            disableInteraction: false,
            intro: 'Hier kannst du den Typ der Ressource auswählen. Dieser wird dann im "Termin anlegen"-Dialog angezeigt.',
            'myBeforeChangeFunction': function (PbGuide) {

            },
        },
        {//16
            title: 'Kalenderressource anlegen',
            element: '#colorpicker-wrapper',
            disableInteraction: false,
            intro: 'Hier kannst du der Ressource eine bestimmte Farbe zuweisen, um diese im "Termin anlegen"-Dialog besonders kenntlich zu machen.',
            'myBeforeChangeFunction': function (PbGuide) {

            },
        },
        {//17
            title: 'Neue Kalenderressource speichern',
            element: '#btn-update-resource',
            intro: 'Um die neue Ressource verwenden zu können, müsstest du jetzt noch die Änderungen mit einem Klick auf die <strong>Speichern</strong>-Schaltfläche abspeichern.',
            'myChangeFunction': function (PbGuide) {
            },
        },
        {//18
            title: 'Tour abgeschlossen',
            intro: 'Sehr gut! Damit hast du die Tour zum Anlegen und Löschen von Kalenderressourcen abgeschlossen. Wenn du möchtest, können wir aber auch gleich mit der nächsten Tour weitermachen. Du kannst diese aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong>',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("easy2schedule.firstSteps")
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
                let object = document.querySelector('#close-button')
                object.click()

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.resourceSettings.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            },
        },
        {//19
            title: 'Kalenderspalte konfigurieren',
            intro: 'Diese Tour zeigt dir interaktiv, wie du einen <strong>Kalender bearbeiten</strong> kannst.<br/> Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>',
            'myBeforeChangeFunction': function (PbGuide) {
                if (!PbGuide.ensureUrl(PbGuide.tourConfig.tours.columnSettings.startPage, "columnSettings")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//20
            title: 'Kalenderspalte konfigurieren',
            element: '#calendar_view_selection',
            intro: 'Um eine Kalenderspalte zu bearbeiten, wähle bitte zuerst eine Kalenderansicht in der Liste aus.'
        },
        {//21
            title: 'Neue Kalenderspalte hinzufügen',
            element: '#new_column_button',
            intro: 'Um eine neue Spalte hinzuzufügen, klicke den <strong>neue Spalte anlegen</strong>-Button an.'
        },
        {//22
            title: 'Kalenderspalten konfigurieren',
            element: '#edit_columns_button',
            intro: 'Um die Reihenfolge und Bezeichnung der Spalten zu konfigurieren, klicke bitte auf den <strong>Bearbeiten</strong> Button.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#edit_columns_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//23
            title: 'Kalenderspalten konfigurieren',
            element: '#list-view',
            intro: 'Jetzt kannst du die Reihenfolge und Bezeichnung der Spalten konfigurieren. Um deine Änderungen zu speichern, klicke bitte auf den <strong>Übernehmen</strong> Button.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#save_columns_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//24
            title: 'Tour abgeschlossen',
            intro: 'Sehr gut! Damit hast du die Tour zum Konfigurieren der Kalenderspalten abgeschlossen.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("easy2schedule.editContact")
                PbGuide.hidePreviousButton()

                // PbGuide.setNextButton("Weiter", function () {
                //     let nextTour = PbGuide.tourConfig.tours.firstSteps.nextTour
                //     let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                //     PbGuide.introguide.goToStepNumber(nextStartStep)
                // })
            },
        },
    ]
}