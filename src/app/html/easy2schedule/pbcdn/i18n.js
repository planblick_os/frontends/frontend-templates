import {get} from './functions.js?v=[cs_version]'

export class i18n {
    constructor() {
        if (!i18n.instance) {
            i18n.instance = this;
        }

        return i18n.instance;
    }

    getLanguage() {
        let available_languages = ["de", "en"]
        let lang = navigator.languages ? navigator.languages[0] : navigator.language;
        let ISOA2 = lang.substr(0, 2);
        if (available_languages.includes(ISOA2)) {
            return ISOA2
        } else {
            return "de"
        }
    }

    get_translations() {
        let language = this.getLanguage()
        let url = "i18n/" + language + ".json";
        get(url, this.set_i18n)
        url = "i18n/" + $.cookie("consumer_name") + "_" + language + ".json";
        get(url, this.set_i18n, true)
    }

    translate(vocab) {
        if (this.i18n[vocab]) {
            return this.i18n[vocab]
        } else {
            return vocab
        }
    }

    set_i18n(response) {
        //console.log("RESPONSE i18n", response)
        try {
            if (!i18n.instance.vocabs) {
                i18n.instance.vocabs = []
            }
            let data = JSON.parse(response)
            i18n.instance.vocabs = [i18n.instance.vocabs, data].reduce(function (r, o) {
                Object.keys(o).forEach(function (k) {
                    r[k] = o[k];
                });
                i18n.instance.vocabs = data
                let evt = new Event('i18n_ready');
                document.dispatchEvent(evt);
                return r;
            }, {});
        } catch (err) {

        }
    }

    replace_i18n() {
        for (let vocab of Object.keys(i18n.instance.vocabs)) {
            try {
                //console.log("TRY TO FIND VOCAB", $(vocab))
                let data_attr_selector = "*[data-i18n='" + vocab + "']"
                $(data_attr_selector).html(i18n.instance.vocabs[vocab])
                $(vocab).html(i18n.instance.vocabs[vocab])
            } catch (err) {

            }
        }
    }
}
