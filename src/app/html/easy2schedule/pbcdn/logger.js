import {CONFIG} from './config.js?v=[cs_version]';
import {fireEvent} from './functions.js?v=[cs_version]';

export function init_console() {
    let cookie = function(x){return ""}
    if($.cookie) {
        let cookie = $.cookie
    }

    if (console.everything === undefined) {
        console.everything = [];

        console.defaultLog = console.log.bind(console);
        console.log = function () {
            console.everything.push({
                "location": window.location.href,
                "consumer": cookie("consumer_name"),
                "username": cookie("username"),
                "type": "log",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            });
            console.defaultLog.apply(console, arguments);
        }
        console.defaultError = console.error.bind(console);
        console.error = function () {
            let data = {
                "location": window.location.href,
                "consumer": cookie("consumer_name"),
                "username": cookie("username"),
                "type": "error",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            }
            console.everything.push(data);
            fireEvent("console_log_error", data)
            console.defaultError.apply(console, arguments);
        }
        console.defaultWarn = console.warn.bind(console);
        console.warn = function () {
            let data = {
                "location": window.location.href,
                "consumer": cookie("consumer_name"),
                "username": cookie("username"),
                "type": "warn",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            }
            console.everything.push(data);
            fireEvent("console_log_warning", data)
            console.defaultWarn.apply(console, arguments);
        }
        console.defaultDebug = console.debug.bind(console);
        console.debug = function () {
            console.everything.push({
                "location": window.location.href,
                "consumer": cookie("consumer_name"),
                "username": cookie("username"),
                "type": "debug",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            });
            console.defaultDebug.apply(console, arguments);
        }
    }
    window.setInterval(sendConsoleLog, 10000)
};

export function sendConsoleLog() {
    var seen = [];
    console.log("Consolelog-Connection-Check")
    if (!CONFIG.SEND_CONSOLE_LOG && typeof globalSocket != "undefined" && console.everything.length > 0) {
        try {
            globalSocket.emit('consolelog', JSON.stringify({data: console.everything}, function (key, val) {
                if (val != null && typeof val == "object") {
                    if (seen.indexOf(val) >= 0) {
                        return;
                    }
                    seen.push(val);
                }
                return val;
            }))
        } catch (err) {
            console.log("Cannot send console log to debug-server")
            console.log(err)
        }
    }
    console.everything = []
}
