if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    API_BASE_URL = "http://localhost:8000"
    APP_BASE_URL = "http://localhost:7000"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "http://localhost:8000"
    COOKIE_DOMAIN = "localhost"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
} else if (document.location.href.indexOf("testblick.de") != -1) {
    console.log("Using NEW-LIVE-Config")
    API_BASE_URL = "https://odin.planblick.com"
    APP_BASE_URL = "https://testblick.de"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "https://odin.planblick.com"
    COOKIE_DOMAIN = "testblick.de"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
}  else if (document.location.href.indexOf("stagingapp.familienplaner-online") != -1) {
    console.log("Using STAGING-FAMILY-Config")
    API_BASE_URL = "https://odin.planblick.com"
    APP_BASE_URL = "https://stagingapp.familienplaner-online.de"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "https://odin.planblick.com"
    COOKIE_DOMAIN = "stagingapp.familienplaner-online.de"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
    USE_CUSTOM_CONSOLE_LOG = false
} else if (document.location.href.indexOf("app.familienplaner-online") != -1) {
    console.log("Using PRODUCTION-FAMILY-Config")
    API_BASE_URL = "https://thor.planblick.com"
    APP_BASE_URL = "https://app.familienplaner-online.de"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "https://thor.planblick.com"
    COOKIE_DOMAIN = "app.familienplaner-online.de"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
    USE_CUSTOM_CONSOLE_LOG = false
} else {
    console.log("Using LIVE-Config")
    API_BASE_URL = "https://thor.planblick.com"
    APP_BASE_URL = "https://www.serviceblick.com"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "https://thor.planblick.com"
    COOKIE_DOMAIN = "www.serviceblick.com"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
}

document.addEventListener("DOMContentLoaded", function () {
    if (SYSTEM_MESSAGE.length > 0) {
        let system_message_element = document.getElementById("system_message")
        if (system_message_element) {
            system_message_element.innerHTML = SYSTEM_MESSAGE
        }
    }
})

function copyToClipBoard(element_id) {
    var copyText = document.getElementById(element_id);
    copyText.select();
    document.execCommand("copy");
}

news_config = [
    /*{
        news_id: "2020_04_16",
        show_on_date: "2020_04_16",
        content_uri: "../news/index.html",
        content_id: "2020_04_16",
        show_in_apps: ["easy2schedule"],
        force_display: true
    },*/
]

get_news_for = function (app) {
    for (let news of news_config) {
        if (news.show_in_apps.includes(app)) {
            $.ajax({
                type: 'GET',
                url: news.content_uri,
                dataType: 'html',
                success: function (data) {
                    let evt = new Event('news_available');
                    evt.content_id = news.content_id
                    evt.force_display = news.force_display
                    evt.data = data
                    document.dispatchEvent(evt);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ERROR NEWS", errorThrown)
                }
            });

        }
    }
}
