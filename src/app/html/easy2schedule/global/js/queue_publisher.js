apikey = $.cookie("apikey");
initiator = $.cookie("consumer_name") + "." + $.cookie("username");

function sendMessage(payload) {
    mail_recipient = false;
    recipient_select = document.getElementById("client_selection")

    if (recipient_select == undefined) {
        /*mail_recipient = document.getElementById("document_signer").value;*/
        mail_recipient = "placeholder"
    }
    else {
        var recipient = recipient_select.options[recipient_select.selectedIndex].value;
    }
    initiator = $.cookie("consumer_name") + "." + $.cookie("username");

    apikey = $.cookie("apikey");

    var event = new Date();
    document_identifier = "quicksign";
    payload_json = JSON.parse(payload)
    payload_json.document_id = document_identifier
    payload_json.initiator = initiator

    payload = JSON.stringify(payload_json)

    payload = {
        "variables":
            {
                "pages": {"value": JSON.stringify(payload_json), "type": "Json"},
                "initiator": {"value": initiator, "type": "String"}
            }
    }
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": API_BASE_URL + "/engine-rest/process-definition/key/signature_pad_default_step1/start",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "apikey": apikey,
            "Cache-Control": "no-cache"
        },
        "processData": false,
        "data": JSON.stringify(payload)
    }

    $.ajax(settings).done(function (response, status, request) {
        cid = request.getResponseHeader('x-correlation-id')

        namespace = '/' + escape(cid);
        console.log("Waiting for info about quicklink: " + namespace)
        let socket = io(SOCKET_SERVER + namespace);

        // Event handler for server sent data.
        // The callback function is invoked whenever the server emits data
        // to the client. The data is then displayed in the "Received"
        // section of the page.
        socket.on('my_response', function (msg) {

            //$('#log').prepend('<br>' + $('<div/>').text('Received #' + msg.count + ': ' + msg.data).html());
            $('#log').prepend($('<div/>').text(msg.data).html() + '<br>');
            $('#log').show().delay(5000).fadeOut();
        });

        if (mail_recipient != false) {
            document_signing_url = APP_BASE_URL + "/quicksign/sign.html?document_id=" + response.id + "&key=" + apikey + "&consumer_name=" + btoa($.cookie("consumer_name")) + "&username=" + btoa($.cookie("username")) + "&cid=" + cid
            $.blockUI({
                message: $('#quicksign_link_overlay'),
                css: {width: '600px'},
                onBlock: setQuicksignLink(document_signing_url)
            });
        }
    });
}

function setQuicksignLink(document_signing_url) {
    document.getElementById("qs_link").innerHTML = document_signing_url
    //document.getElementById("qs_link").href = document_signing_url
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}