export class NotificationsFilter {
    event_filter = {
        owned: false,
        assigned: false,
        other: false
    }

    constructor() {
        if (!NotificationsFilter.instance) {
            NotificationsFilter.instance = this
        }

        return NotificationsFilter.instance
    }

    getReminderEmailFilter() {
        return NotificationsFilter.instance.event_filter
    }

    setReminderEmailFilter(value) {
        NotificationsFilter.instance.setFilterOwnedEvents(value.owned)
        NotificationsFilter.instance.setFilterAssignedEvents(value.assigned)
        NotificationsFilter.instance.setFilterOtherEvents(value.other)
    }

    setFilterOwnedEvents(value) {
        NotificationsFilter.instance.event_filter.owned = value
        return NotificationsFilter.instance
    }

    getFilterOwnedEvents() {
        return NotificationsFilter.instance.event_filter.owned
    }

    setFilterAssignedEvents(value) {
        NotificationsFilter.instance.event_filter.assigned = value
        return NotificationsFilter.instance
    }

    getFilterAssignedEvents() {
        return NotificationsFilter.instance.event_filter.assigned
    }

    setFilterOtherEvents(value) {
        NotificationsFilter.instance.event_filter.other = value
        return NotificationsFilter.instance
    }

    getFilterOtherEvents() {
        return NotificationsFilter.instance.event_filter.other
    }
}