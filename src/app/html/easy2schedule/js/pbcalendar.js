checkLogin(isLoginPage = false, checkagainTime = 30000, health_endpoint = "/cb/healthz");

moment.locale(navigator.language)
datetimeFormat = "DD.MM.YYYY HH:mm";
dateFormat = "DD.MM.YYYY";
mysqldatetimeFormat = "YYYY-MM-DD HH:mm:ss";

//Placeholder for resources which are in current view
resources = []


var colors = []
get(API_BASE_URL + "/cb/ressources", fetchRessources)
get(API_BASE_URL + "/cb/columns", fetchColumns)
get(API_BASE_URL + "/cb/account_headers", fetchHeaders)
get(API_BASE_URL + "/cb/extended_properties", fetchExtendedProperties)
get(API_BASE_URL + "/cb/account_config", fetchAccountConfiguration)


anonymize = false
if (GetURLParameter("anonymize")) {
    anonymize = true
}

eventSource = [
    {
        url: API_BASE_URL + '/cb/events?apikey=' + getCookie("apikey") + (anonymize ? "&anonymize=true" : ""),
        method: 'GET',
        extraParams: {
            custom_param1: 'something',
            custom_param2: 'somethingelse'
        },
        failure: function () {
            window.setTimeout(function () {
                calendar.refetchEvents();
            }, 5000)
        },
        color: 'yellow',   // a non-ajax option
        textColor: 'black' // a non-ajax option
    }
]


systemsReady = {
    ressources: false,
    persons: false,
    extended_props: false,
    headers: false,
    account_configuration: false,
    i18n: false
}

fixMonthViewHeight = function () {
    setTimeout(function () {
        let viewmode = $.cookie("e2s_menue_mode")
        if (calendar.view.type.startsWith("dayGridMonth")) {
            if(viewmode == "full")
                $("#calendar").css('width', 'calc(100% - 219px)');
            else
                $("#calendar").css('width', 'calc(100% - 75px)');
            calendar.setOption('height', calendar.getOption('height'));
        } else if (calendar.view.type.startsWith("resourceTimeGridDay")) {
            if(viewmode == "full")
                $("#calendar").css('width', 'calc(100% - 219px)');
            else
                $("#calendar").css('width', 'calc(100% - 75px)');
        } else if (calendar.view.type.startsWith("timeGridDay")) {
            if(viewmode == "full")
                $("#calendar").css('width', 'calc(100% - 219px)');
            else
                $("#calendar").css('width', 'calc(100% - 75px)');
        }
        else {
            $("#calendar").removeAttr('style');
        }
    }, 200)
}

calendar = null;
headers_left = null
readOnlyUser = false

runCalendar = function () {
    console.log("ReadyForCalendarLoad")
    var calendarEl = document.getElementById('calendar');
    //Fullcalendar Version 4
    calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        droppable: false,
        editable: false,
        plugins: ['interaction', 'resourceTimeGrid', 'resourceDayGrid', 'dayGridPlugin'],
        timeZone: 'Europe/Berlin',
        locale: getLanguage(),
        defaultView: 'resourceTimeGridMultiDay',
        datesAboveResources: true,
        weekends: true,
        selectable: true,
        minTime: "08:00:00",
        maxTime: '23:59:59',
        nextDayThreshold: "00:00:00",
        height: "auto",
        slotDuration: '00:30:00', //For every of this timespan an own row is rendered
        snapDuration: '00:15:00',
        selectOverlap: true,
        //timezone: 'local',
        allDaySlot: true,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        selectMinDistance: 5,
        buttonIcons: {
            prev: 'fc-icon-chevrons-left',
            next: 'fc-icon-chevrons-right',
        },
        // navLinkDayClick: function (date, jsEvent) {
        //     console.log('day', date.toISOString());
        //     console.log('coords', jsEvent.pageX, jsEvent.pageY);
        //     calendar.changeView("")
        // },
        loading: function (bool) {
            if (bool) {
                console.log("loading events")
            } else {
                filter_events()
                checkForEventTimesNotInView()
                make_event_unique()
                sortToOthersResource()
                fixMonthViewHeight();
            }
            //Possibly call you feed loader to add the next feed in line
        },
        viewSkeletonRender: function (info) {
            /*if (info.view.type.startsWith("resourceTimeGridDay")) {
                $("#calendar").css('width', '98%');
            } else {
                $("#calendar").removeAttr('style');
            }*/


            calendar.refetchResources()
            sortToOthersResource()
            addResourcesToDropdown()
            replace_i18n()

            $(".fc-center").click(selectViewDate)
            if ($.cookie("e2s_menue_mode")) {
                toggleMenu($.cookie("e2s_menue_mode"))
            }
            let evt = new Event('viewSkeletonRender_finished');
            window.dispatchEvent(evt);
            fixMonthViewHeight();
        },
        customButtons: {
            nextday: {
                icon: "fc-icon-chevron-right",
                click: function () {
                    let cal = $('#calendar');
                    calendar.incrementDate({day: -100});
                    calendar.incrementDate({day: +101});
                }
            },
            previousday: {
                icon: "fc-icon-chevron-left",
                click: function () {
                    calendar.incrementDate({day: -1});
                }
            },
            selectdate: {
                icon: " calendar alternate outline icon left",
                click: function () {
                    selectViewDate()
                }
            }
        },
        header: {
            left: "prev,previousday,today,nextday,next,selectdate",
            center: 'title',
            right: ''
        },
        buttonText: {
            resourceTimeGridDay: "Tagesansicht Räume",
            resourceTimeGridMultiDay: "Wochenansicht Räume",
            resourceTimeGridDay2: "Tagesansicht Termine",
            resourceTimeGridMultiDay3: "Wochenansicht Termine",
            dayGridMonth: "Monatsansicht"
        },
        businessHours: {
            // days of week. an array of zero-based day of week integers (0=Sunday)
            daysOfWeek: [1, 2, 3, 4, 5, 6, 0], // Monday - Sunday
            startTime: '08:00',
            endTime: '23:59',
        },
        views: {
            resourceTimeGridMultiDay: {
                type: 'resourceTimeGrid',
                duration: {days: 7},
                titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
            },
            resourceTimeGridDay: {
                type: 'resourceTimeGrid',
                duration: {days: 1},
                titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
            },
            resourceTimeGridDay2: {
                type: 'resourceTimeGrid',
                duration: {days: 1},
                titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
            },
            resourceTimeGridMultiDay3: {
                type: 'resourceTimeGridDay',
                duration: {days: 7},
                titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
            },
            dayGridMonth: {
                type: 'dayGridMonth',
                titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'},
                eventLimit: 999
            },
            resourceTimelineFourDays: {
              type: 'resourceTimeline',
              duration: { days: 4 }
            }
        },
        refetchResourcesOnNavigate: true,
        resources: function (fetchInfo, successCallback, failureCallback) {
            if (!calendar) {
                resources = getRessourceColumns("resourceTimeGridMultiDay")
                successCallback(getRessourceColumns("resourceTimeGridMultiDay"))
            } else {
                let view_id = calendar.view.viewSpec.type
                console.log("Current View Id", calendar.view.viewSpec.type)
                if (getRessourceColumns(view_id)) {
                    resources = getRessourceColumns(view_id)
                    successCallback(getRessourceColumns(view_id))
                } else {
                    failureCallback()
                }
            }

        },
        eventClick: function (event) {
            if (event.event._def.resourceEditable != false) {
                click_doublick_switch(event, click_function = handleEventClick.bind(event), doubleclick_function = handleEventDoubleClick.bind(event));
            }
        },
        dateClick: function (info) {
            if (info.resource._resource.id == "others") {
                alert("Für diese Spalte können keine Termine direkt angelegt werden.")
                return false;
            }
            click_doublick_switch(info, click_function = handleCalendarClick.bind(info), doubleclick_function = handleCalendarDoubleClick.bind(info));
        },
        select: function (info) {
            if (info.resource._resource.id == "others") {
                alert("Für diese Spalte können keine Termine direkt angelegt werden.")
                return false;
            }
            handleSelectRange(info);
        },
        eventSources: eventSource,
        eventRender: function (event, element) {

            filter_event(event.event)
            fixMonthViewHeight();
            event.event = customize_event(event.event)
            //disable possibility to modify event:
            //event.event._def.resourceEditable=false

            let participants = []
            let attachedRessources = event.event._def.extendedProps.attachedRessources
            if (attachedRessources != null) {
                for (res of attachedRessources) {
                    res = JSON.parse(res)
                    participants.push(res.name)
                }
            }

            if (event.event._def.rendering == 'background') {
                event.el.append(event.event._def.title);
            } else {

                var br = document.createElement("br");
                //event.el.firstChild.firstChild.firstChild.append(br);

                if (event.event._def.allDay && event.event._def.allDay == true) {
                    event.el.firstChild.innerHTML = participants.join(", ") + "<br>" + event.el.firstChild.innerHTML
                } else {
                    if (event.el.firstChild.firstChild.firstChild.nodeName == "SPAN")
                        event.el.firstChild.firstChild.firstChild.append(" " + participants.join(", "));
                    else if (event.el.firstChild.firstChild.nodeName == "SPAN")
                        event.el.firstChild.firstChild.append(" " + participants.join(", "));
                }
            }

            if(event.event._def.originalTitle) {
                event.event._def.title = event.event._def.originalTitle
            }

            let tooltip_content = ""

            tooltip_content += translate("Titel:") + " " + event.event._def.title + "<br/>"

            let show_tooltip = false
            if (participants.length > 1) {
                show_tooltip = true
                tooltip_content += translate("Teilnehmer:") + " " + participants.join(", ") + "<br/>"
            }




            if(event.event._def.resourceIds) {
                let ressourceTitles = []
                if(calendar.view.type.startsWith("dayGridMonth")) {
                    resources = []
                    resources.push(...getRessourceColumns("resourceTimeGridDay"))
                    resources.push(...getRessourceColumns("resourceTimeGridDay2"))
                }

                for(let oneRessource of resources) {
                    if(event.event._def.resourceIds.includes(oneRessource.id) && !ressourceTitles.includes(oneRessource.title)) {
                        if(oneRessource.id != "others") {
                            ressourceTitles.push(oneRessource.title)
                        }
                    }
                }
                event.event._def.title = event.event._def.title.replaceAll( " (" + ressourceTitles.join(", ") + ")", "")

                if(calendar.view.type.startsWith("dayGridMonth")) {
                    if(event.event._def.allDay) {
                        event.el.style.border="3px outset " + adjustColor(event.event._def.ui.backgroundColor, -40)
                        event.el.style.opacity=1
                        event.el.style.marginBottom = "3px";
                    }
                    if(ressourceTitles.length > 0) {
                            event.event._def.originalTitle = event.event._def.title
                            event.event._def.title = ressourceTitles.join(", ")
                    }
                }
            }


            for ([key, value] of Object.entries(event.event._def.extendedProps)) {
                if (key == "attachedRessources") {
                    continue
                }


                if (value && value.length > 0) {
                    let selector = "#" + key;
                    if ($(selector).length != 0) {
                        if ($(selector).prop("tagName") == "SELECT") {
                            value = $(selector + " option[value='" + value + "']").text()
                        }
                    }

                    tooltip_content += value + "<br>"
                    show_tooltip = true
                }
            }

            if (show_tooltip) {
                // http://qtip2.com
                $(event.el).qtip({
                    hide: {
                        fixed: true,
                        delay: 100
                    },
                    content: {
                        text: decodeURIComponent(tooltip_content)
                    },
                    position: {
                        at: 'top center',
                        my: "bottom center",
                        target: false
                    },
                    style: {
                        classes: 'qtip-dark qtip-shadow qtip-rounded'
                    },
                    events: {
                        show: function (event, api) {
                            api.reposition(event)
                        }
                    }

                });


            }
        },
        eventDrop: function (info) {
            console.log(info)
            alert(info.event.title + " was dropped on " + info.event.start.toISOString());

            if (!confirm("Are you sure about this change?")) {
                info.revert();
            }
        }
    });

    calendar.render();
}

