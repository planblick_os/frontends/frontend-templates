import {get, fireEvent, getCookie} from "./pbjs/pb-functions.js?v=[cs_version]";
import {CONFIG} from './pb-config.js?v=[cs_version]';
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"


export class pbEventEntries {
    possible_groups = []
    group_members = []
    constructor() {
        if (!pbEventEntries.instance) {
            pbEventEntries.instance = this;
        }
        return pbEventEntries.instance;
    }

    init() {
        return new Promise(function (resolve, reject) {
            new pbPermissions().fetchGroupsFromApi().then(function(my_groups) {
                my_groups = Object.keys(my_groups)
                pbEventEntries.instance.possible_groups = my_groups

                //loop over groups and get members
                let promises = []
                pbEventEntries.instance.possible_groups.forEach(group => {
                    promises.push(new pbPermissions().fetchGroupMembers(group).then(function (members) {
                            members.forEach(member => {
                                if(!pbEventEntries.instance.group_members.includes("user." + member.username)) {
                                    pbEventEntries.instance.group_members.push("user." + member.username)
                                }
                            })
                            
                        })
                    )
                })
                if( !pbEventEntries.instance.group_members.includes("user." + getCookie("username"))) {
                    pbEventEntries.instance.group_members.push("user." + getCookie("username"))
                }
                Promise.all(promises)
                    .then(function () {
                        resolve()
                    })
                    .catch(function (exception) {
                        console.log("EXCEPTION", exception)
                        reject()
                    })
            })
        })
    }

    
}