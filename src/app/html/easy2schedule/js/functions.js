i18n = {}

renderNews = function (event) {
    if ($.cookie("read_news") && $.cookie("read_news").includes(event.content_id))
        return
    $("#news_bell").addClass("active")
    let doc = new DOMParser().parseFromString(event.data, "text/xml");
    news_content = doc.getElementById(event.content_id)

    let approve_button = $('<div class="ui approve button news_read_button" onclick="setNewsCookieRead(\'' + event.content_id + '\')">Als gelesen markieren</div>')

    $("#news_container").append(news_content.innerHTML)
    $("#news_container").append(approve_button)
    $("#news_container").append("<br/><br/><br/>")

    if (event.force_display) {
        $("#news_modal").modal('show')
    }
}

setNewsCookieRead = function (news_content_id) {
    $("#news_bell").removeClass("active")
    cookie_content = $.cookie("read_news")
    if (!cookie_content)
        cookie_content = ""
    cookie_content += "/" + news_content_id
    $.cookie("read_news", cookie_content, { expires: 730, path: '/' })
    $("#news_modal").modal('hide')
}

getLanguage = function () {
    let available_languages = ["de", "en"]
    let lang = navigator.languages ? navigator.languages[0] : navigator.language;
    let ISOA2 = lang.substr(0, 2);
    if (available_languages.includes(ISOA2)) {
        return ISOA2
    } else {
        return "de"
    }
}

get_translations = function () {
    language = getLanguage()
    url = "i18n/" + language + ".json";
    get(url, set_i18n)
    url = "i18n/" + $.cookie("consumer_name") + "_" + language + ".json";
    get(url, set_i18n, true)
}

translate = function (vocab) {
    if (i18n[vocab]) {
        return i18n[vocab]
    } else {
        return vocab
    }
}

set_i18n = function (response) {
    //console.log("RESPONSE i18n", response)
    try {
        data = JSON.parse(response)
        i18n = [i18n, data].reduce(function (r, o) {
            Object.keys(o).forEach(function (k) {
                r[k] = o[k];
            });
            return r;
        }, {});
    } catch (err) {

    }
    systemReadyForCalendarLoad("i18n")
}

replace_i18n = function (response) {
    for (vocab of Object.keys(i18n)) {
        try {
            let data_attr_selector = "*[data-i18n='" + vocab + "']"
            $(data_attr_selector).html(i18n[vocab])
            $(vocab).html(i18n[vocab])
        } catch (err) {

        }
    }
}

getById = function (id) {
    return document.getElementById(id);
}

toggleExtendedProps = function () {
    console.log("EXTENDED PROPS TOGGLED", userConfig)
    if (!userConfig["readonly_user"]) {
        $('#extended_props_wrapper').toggle("slide", function () {
            $.cookie("e2s_show_extended_props", $('#extended_props_wrapper').is(':visible'), {expires: 365});
        });
    }
}

initCalendar = function () {
    console.log("initCalendar")
    systemReadyForCalendarLoad("ressources")
}

fnAppointmentsubmit = function (evt) {
    var fields = getEventFormElements()
    evt.preventDefault()

    if (evt.target.name == "copy")
        fields["event_id"].value = ""

    if (fields["event_id"].value == "") {
        url = API_BASE_URL + "/es/cmd/createNewAppointment";
    } else {
        url = API_BASE_URL + "/es/cmd/changeAppointment";
    }

    let attachedRessources = []
    for (attachedRessource of fields["colors"].selectedOptions) {
        attachedRessources.push(attachedRessource.getAttribute("data-ressource"))
    }

    let allDay = false
    if (fields["allday"].value == "true") {
        allDay = true
    }

    data = {
        event_id: fields["event_id"].value,
        resourceId: fields["resource"].options[fields["resource"].selectedIndex].value,
        textColor: "", //NYI
        color: "", //NYI
        attachedRessources: attachedRessources,
        starttime: moment.utc(fields["starttime"].value, datetimeFormat).format(mysqldatetimeFormat),
        endtime: moment.utc(fields["endtime"].value, datetimeFormat).format(mysqldatetimeFormat),
        title: fields["title"].value,
        savekind: fields["save"].name,
        extended_props: {"creator_login": getCookie("username")},
        allDay: allDay
    }
    let extended_props = document.querySelectorAll("[data-event-property='extended_prop']")
    for (prop of extended_props) {
        prop_name = prop.id
        prop_value = prop.value
        data["extended_props"][prop_name] = prop_value
    }
    post(url, data, function (response) {
        toggleOverlay()
    });

    renderRange = calendar.state.dateProfile.renderRange
    view_start = moment(renderRange.start)
    view_end = moment(renderRange.end)

    let new_event_date = moment.utc(fields["starttime"].value, datetimeFormat)

    if (!new_event_date.isBetween(view_start, view_end) && confirm("Möchten Sie die Ansicht auf den neu angelegten Termin umstellen?")) {
        setViewDate(fields["starttime"].value)
    }

}

handleSelectRange = function (event) {
    setFormFields("appointment", event);
}

handleEventClick = function (event) {
    console.log("EventClick");
    if (event.event._def.resourceIds.includes("others")) {
        //alert("Dieser Eintrag kann in dieser Ansicht nicht geändert werden. Bitte verwenden Sie eine andere Ansicht.")
        return;
    }
    unselectEvents();
    selectEvent(event);
    jsevent = event.jsEvent
    //if(true) //This works only for chrome on android, but not safari || typeof(jsevent.sourceCapabilities) != "undefined" && typeof(jsevent.sourceCapabilities.firesTouchEvents) != "undefined" && jsevent.sourceCapabilities.firesTouchEvents == true)
    //{
    //    setFormFields("appointment", event);
    //}
    //Do not handle single clicks on events;
    //setFormFields("appointment", event);
}

handleEventDoubleClick = function (event) {
    if (event.event._def.resourceIds.includes("others")) {
        alert("Dieser Eintrag kann in dieser Ansicht nicht geändert werden. Bitte verwenden Sie eine andere Ansicht.")
        return;
    }
    if (event.event._def.extendedProps.readonly && event.event._def.extendedProps.creator_login != getCookie("username")) {
        alert("Dieser Eintrag kann nicht geändert werden.")
        return;
    }
    setFormFields("appointment", event);

}

handleCalendarClick = function (info) {
    unselectEvents();
}

handleCalendarDoubleClick = function (info) {
    setFormFields("appointment", info);
}

unselectEvents = function () {
    events = document.getElementsByClassName("fc-event")
    for (singleEvent of events) {
        singleEvent.style.border = "none";
    }
    selectedEvent = null;
}

deleteEvent = function (event = null) {
    if (event == null) {
        event = selectedEvent
    }
    if (!event) {
        alert("Bitte wählen Sie den Termin erst aus (Antippen)")
    } else {
        if (!confirm("Wollen Sie den ausgewählten Termin löschen?")) {
            return
        }

        if (event.event._def.extendedProps.readonly && selectedEvent.event._def.extendedProps.creator_login != getCookie("username")) {
            alert("Sie können diesen Eintrag nicht löschen.")
        } else {
            $(event.el).tooltip("hide")
            selectedEventId = event.event._def.publicId;
            url = API_BASE_URL + "/es/cmd/deleteAppointment";
            data = {
                "event_id": selectedEventId
            }
            post(url, data, function (response) {
                event.event.remove()
                //calendar.refetchEvents();
                selectedEvent = null;
            });
        }
    }
}

updateEvent = function () {
    var fields = getEventFormElements()

    url = API_BASE_URL + "/es/cmd/changeAppointment";
    data = {
        id: fields["id"].value,
        resourceId: fields["resource"].options[fields["resource"].selectedIndex].value,
        textColor: fields["colors"].options[fields["colors"].selectedIndex].value.split("||")[0],
        color: fields["colors"].options[fields["colors"].selectedIndex].value.split("||")[1],
        starttime: moment.utc(fields["starttime"].value, datetimeFormat).format(mysqldatetimeFormat),
        endtime: moment.utc(fields["endtime"].value, datetimeFormat).format(mysqldatetimeFormat),
        title: fields["title"].value,
        savekind: fields["save"].name
    }
    post(url, data, function (response) {
        toggleOverlay()
    });
    evt.preventDefault();

}

selectedEvent = null;

selectEvent = function (event) {
    event.el.style.border = "1px solid black";
    selectedEvent = event;
}

handleKeyPress = function (e) {
    var evtobj = e
    var keyCode = e.key;
    console.log(evtobj.keyCode)
    if (keyCode == "Delete" && selectedEvent != null) {
        deleteEvent(selectedEvent);
    }
    if (keyCode == "Escape" && $("#overlay").is(":visible")) {
        $("#overlay").toggle()
    }
    if (evtobj.keyCode == 78 && evtobj.altKey && !$("#overlay").is(":visible")) {
        setFormFields()
    }
    if (keyCode == "F1") {
        if ($("#overlay").is(":visible")) {
            showHelp()
            $('#legend_wrapper').hide("slide")
        } else {
            $('#legend_wrapper').toggle("slide");
        }
    }
}

alternativResources = {}
getRessourceColumns = function (view_id) {
    if (document.getElementById("column_count_specific_css")) {
        document.getElementById("column_count_specific_css").parentNode.removeChild(document.getElementById("column_count_specific_css"))
    }
    var style = document.createElement('style');
    style.id = "column_count_specific_css"
    style.type = 'text/css';
    if (view_id == "timeGridDay") {
        view_id = "resourceTimeGridDay"
        //OFF
        //$(".fc-view-container").css("width", "36%")
        $("html").scrollLeft(0)
        $('*[data-calendarview]').removeClass("active")
        $("*[data-calendarview='timeGridDay']").addClass("active")
    } else {
        $(".fc-view-container").css("width", "100%")
    }
    if (alternativResources[view_id]) {
        columns_count = alternativResources[view_id].length
        style.innerHTML = 'th:nth-child(' + columns_count + 'n+1), td:nth-child(' + columns_count + 'n+1) {border-right: 1px solid #7987a1;}';
        document.getElementsByTagName('head')[0].appendChild(style);

        return alternativResources[view_id]
    } else {
        // Fetch all ressources since we need them for the dropdown anyway
        let allressources = []
        console.log("Fetch all ressources since we need them for the dropdown anyway")
        for (a_ressource in alternativResources) {
            allressources.push(...alternativResources[a_ressource])
        }

        return allressources
    }
}

fetchRessources = function (response) {
    response = JSON.parse(response)
    for (entry of response) {
        let dataset = {
            id: entry.id,
            name: entry.display_name,
            fontColor: entry.props.eventFontColor,
            backgroundColor: entry.props.eventBackgroundColor,
            loginId: entry.props.login_id
        }
        colors.push(dataset)
    }

    var legend = getById("colorLegend");
    for (item of colors) {
        legend.innerHTML += '<div style="background-color: ' + item.backgroundColor + ';color:' + item.fontColor + '">' + item.name + '</div>'
    }

    addResourcesToDropdown()
}

fetchColumns = function (response) {
    console.log("Got data for columns")
    response = JSON.parse(response)
    alternativResources = response
    systemReadyForCalendarLoad("persons")
}

userConfig = null
fetchAccountConfiguration = function (response) {
    console.log("Got configuration")
    response = JSON.parse(response)
    userConfig = response["user_config"]
    console.log("USERCONFIG", userConfig)
    if (userConfig["readonly_user"]) {
        let extended_properties_toggle_element = getById("extended_props_toggle")
        extended_properties_toggle_element.parentNode.removeChild(extended_properties_toggle_element)
    }
    systemReadyForCalendarLoad("account_configuration")
}

headers_left = null
fetchHeaders = function (response) {
    console.log("Got headers")
    response = JSON.parse(response)
    headers_left = response["headers_left"]
    console.log("HEADERS", headers_left)
    removeNotGrantedViewsFromMenu()
    systemReadyForCalendarLoad("headers")
}

removeNotGrantedViewsFromMenu = function () {
    for (button of $(".calendar_view_button")) {
        if (!headers_left.includes(button.dataset.calendarview)) {
            button.style.display = "None";
            button.parentNode.removeChild(button)
        }
    }
}

readOnlyUser = false
fetchExtendedProperties = function (response) {
    console.log("Got data for columns")
    getById("extended_props_wrapper").innerHTML = response
    systemReadyForCalendarLoad("extended_props")
}


addResourcesToDropdown = function () {
    var dropdown = getById("resource");
    dropdown.innerHTML = "";

    for (item of resources) {
        if (item.id != "others") {
            let option = document.createElement("option");
            option.text = item.title;
            option.value = item.id;
            dropdown.add(option);
        }
    }

    var dropdown = getById("colors");
    dropdown.innerHTML = "";

    for (item of colors) {
        let option = document.createElement("option");
        option.text = item.name;
        option.value = item.id//JSON.stringify(item)
        option.setAttribute("data-ressource", JSON.stringify(item))
        dropdown.add(option);
    }
}


getEventFormElements = function () {
    var elements = {
        starttime: getById("starttime"),
        endtime: getById("endtime"),
        title: getById("title"),
        resource: getById("resource"),
        save: getById("save"),
        event_id: getById("event_id"),
        colors: getById("colors"),
        allday: getById("allday")
    }
    return elements;
}

resetFormFields = function () {
    var fields = getEventFormElements();
    for (field in fields) {
        fields[field].value = ""
    }

    let extended_props = document.querySelectorAll("[data-event-property='extended_prop']")
    for (prop of extended_props) {
        if (prop.tagName == "SELECT" && prop.hasAttribute("data-default-value")) {
            let options = prop.childNodes
            for (option of options) {
                if (option.value == prop.getAttribute("data-default-value")) {
                    option.selected = true
                }
            }
        } else {
            prop.value = ""
        }
    }
}

edit_event = function (event) {
    if (!event) {
        event = selectedEvent
    }
    if (event == null) {
        alert("Kein Termin ausgewählt")
        return
    }
    setFormFields('appointment', event)
}

setFormFields = function (form_id, info = undefined, startdate = undefined, enddate = undefined, title = "") {
    if (info && info.event && info.event._def.extendedProps.readonly && info.event._def.extendedProps.creator_login != getCookie("username")) {
        alert("Dieser Eintrag kann nicht geändert werden.")
        return;
    }
    var fields = getEventFormElements();
    //$(fields["starttime"]).off()
    //$(fields["endtime"]).off()
    //$(fields["starttime"]).off()
    //$(fields["endtime"]).off()
    //$(fields["resource"]).off()
    //$(fields["colors"]).off()
    toggleOverlay();
    resetFormFields()


    var starttime = "";
    var endtime = "";

    var title_input_value = title
    var timezoneoffset = 0;
    fields["save"].name = "create"
    fields["save"].value = "Anlegen"

    // Check whether we shall open an existing even
    if (info && typeof (info.event) != "undefined") {
        $("#copy").show()
        //Existing event
        fields["allday"].value = info.event._def.allDay
        timezoneoffset = info.event._instance.range.start.getTimezoneOffset();
        starttime = info.event._instance.range.start;
        endtime = info.event._instance.range.end;
        title_input_value = info.event._def.title
        fields["save"].name = "update";
        fields["save"].value = "Update";
        fields["event_id"].value = info.event._def.publicId;

        for (option of fields["resource"]) {
            if (info.event._def.resourceIds.includes(option.value)) {
                option.selected = true
            }
            if (info.event._def.resourceIds.includes("others")) {
                document.getElementById("save").disabled = true
            } else {
                document.getElementById("save").disabled = false
            }
        }

        let participants = []
        let attachedRessources = info.event._def.extendedProps.attachedRessources
        if (attachedRessources != null) {
            for (res of attachedRessources) {
                res = JSON.parse(res)
                participants.push(res.id)
            }
        }
        for (option of fields["colors"]) {
            option.selected = false
            if (participants.includes(JSON.parse(option.getAttribute("data-ressource")).id)) {
                option.selected = true
            }
        }
        for ([key, value] of Object.entries(info.event._def.extendedProps)) {
            if (document.getElementById(key)) {
                document.getElementById(key).value = value
            }
        }
    } else {
        $("#copy").hide()
        if (!info) {
            info = {
                allDay: 0,
                startStr: moment.utc().add(120, 'minutes'),
                endStr: moment.utc().add(180, 'minutes')
            }
        }
        console.log(info)
        //New event
        fields["event_id"].value = "";
        fields["allday"].value = info.allDay

        if (typeof (info.startStr) != "undefined") {
            starttime = info.startStr;
        } else {
            starttime = info.dateStr;
        }
        if (typeof (info.endStr) != "undefined") {
            endtime = info.endStr;
        } else {
            endtime = moment.utc(info.dateStr).add(60, 'minutes');
        }
        let at_least_one_selected = false
        for (option of fields["resource"]) {
            if (info.resource && info.resource._resource.id == option.value) {
                at_least_one_selected = true
                option.selected = true
            }
        }

        if (!at_least_one_selected) {
            console.log("Default-Ressource", fields["resource"])
            if (fields && fields["resource"] && fields["resource"][0]) {
                fields["resource"][0].selected = true
            }
        }
    }

    fields["starttime"].value = moment.utc(starttime).format(datetimeFormat);
    if (startdate) {
        fields["starttime"].value = startdate
    }
    addDatepickerToInput(fields["starttime"])
    fields["starttime"].readOnly = true
    fields["endtime"].value = moment.utc(endtime).format(datetimeFormat);
    if (enddate) {
        fields["endtime"].value = enddate
    }
    addDatepickerToInput(fields["endtime"])
    fields["endtime"].readOnly = true
    fields["title"].value = title_input_value


    $(fields["starttime"]).on('dp.change', show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
    $(fields["endtime"]).on('dp.change', show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
    $(fields["starttime"]).on('change', show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
    $(fields["endtime"]).on('change', show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
    $(fields["resource"]).on('change', show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
    $(fields["colors"]).on('change', show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))


    show_collisions.call(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"])

    $("#colors").dropdown({
        clearable: true,
    })


    $("select").dropdown({
        clearable: false
    })

    for (let select of $("select")) {
        for (let option of select.options) {
            if (option.selected) {
                $("#" + select.id).dropdown('set selected', option.value);
            } else {
                $("#" + select.id).dropdown('remove selected', option.value);
            }
        }
    }

}

show_collisions = function (starttime_node, endtime_node, ressource_node, participants_node, delayed_recheck = true) {
    $(".ERROR").removeClass("ERROR")
    let event = this
    starttime = $(starttime_node).val()
    starttime = moment.utc(starttime, datetimeFormat)
    endtime = $(endtime_node).val()
    endtime = moment.utc(endtime, datetimeFormat)
    ressources = $(ressource_node).val()
    participants = $(participants_node).val()
    starttime_node.removeAttribute('style')
    endtime_node.removeAttribute('style')
    ressource_node.removeAttribute('style')
    participants_node.removeAttribute('style')

    if (!calendar)
        return
    let allEvents = calendar.getEvents()
    let starttimeconflicts = []
    let endtimeconflicts = []

    //console.log("THIS EVENT", this.event)
    for (ev of allEvents) {
        if (typeof (event) != "undefined" && typeof (event.event) != "undefined") {
            // console.log("Comparing event_ids wheter same:", event.event._def.defId, ev._def.defId)
            if ((event.event && (event.event._def.defId == ev._def.defId)) || (event.event._def.title == ev._def.title)) {
                //console.log("!!!SKIPPED!!!", event.event._def)
                continue
            }
        }

        evStarttime = moment.utc(ev._instance.range.start)
        evEndtime = moment.utc(ev._instance.range.end)

        if (starttime.isBetween(evStarttime, evEndtime, null, '[)')) {
            starttimeconflicts.push(ev)
            //console.log("STARTTIME intersects with event", ev._def.title, evStarttime)
        }
        if (endtime.isBetween(evStarttime, evEndtime, null, '[)')) {
            endtimeconflicts.push(ev)
            //console.log("ENDTIME intersects with event", ev._def.title, evEndtime)
        }
        if (evStarttime.isBetween(starttime, endtime, null, '[)')) {
            starttimeconflicts.push(ev)
            //console.log("evSTARTTIME intersects with event", ev._def.title, evStarttime)
        }
        if (evStarttime.isBetween(starttime, endtime, null, '[)')) {
            endtimeconflicts.push(ev)
            //console.log("evENDTIME intersects with event", ev._def.title, evEndtime)
        }
    }
    conflicts = {}
    $("#" + ressource_node.id + "_label").removeClass("ERROR")
    $("#" + participants_node.id + "_label").removeClass("ERROR")

    if (starttimeconflicts.length > 0 || endtimeconflicts.length > 0) {
        for (starttimeconflict of starttimeconflicts) {
            //console.log("starttimeconflict", starttimeconflict)
            result = getRealCollisionData(starttimeconflict, ressources, participants)

            if (Object.entries(result).length > 0) {
                conflicts["starttime"] = result
                starttime_node.style.border = "1px solid red"

                if (result["withResources"]) {
                    $('*[data-value="' + result["withResources"] + '"]').addClass("ERROR")
                    if ($(ressource_node.parentNode).hasClass("selection ui dropdown")) {
                        $(ressource_node.parentNode).addClass("ERROR")
                        $(ressource_node.parentNode).click(function () {
                            $(ressource_node.parentNode).removeClass("ERROR")
                        })
                    }
                }

                if (result["withPersons"]) {
                    for (error_value of result["withPersons"]) {
                        $('*[data-value="' + error_value + '"]').addClass("ERROR")
                        $(ressource_node.parentNode).click(function () {
                            $(ressource_node.parentNode).removeClass("ERROR")
                        })
                    }

                    for (colidingParticipantOption of result["withPersons"]) {
                        for (option of participants_node.options) {
                            if (colidingParticipantOption == option.value) {
                                //$(option).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
                            }
                        }
                    }
                }
            }
        }

        for (endtimeconflict of endtimeconflicts) {
            //console.log("endtimeconflict", endtimeconflict)
            result = getRealCollisionData(endtimeconflict, ressources, participants)
            if (Object.entries(result).length > 0) {
                conflicts["endtime"] = result
                endtime_node.style.border = "1px solid red"

                if (result["withResources"]) {
                    $('*[data-value="' + result["withResources"] + '"]').addClass("ERROR")
                    if ($(ressource_node.parentNode).hasClass("selection ui dropdown")) {
                        $(ressource_node.parentNode).addClass("ERROR")
                    }
                }

                if (result["withPersons"]) {
                    for (error_value of result["withPersons"]) {
                        $('*[data-value="' + error_value + '"]').addClass("ERROR")
                    }


                    for (colidingParticipantOption of result["withPersons"]) {
                        for (option of participants_node.options) {
                            //console.log("COLIDING OPTIONS", colidingParticipantOption, option.value)
                            if (colidingParticipantOption == option.value) {
                                //$(option).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
                            }
                        }
                    }
                }
            }
        }
    }
    if (delayed_recheck) {
        setTimeout(function () {
            show_collisions.call(event, starttime_node, endtime_node, ressource_node, participants_node, false)
        }, 200)
    }

    //console.log("CONFLICTS", conflicts)
    return conflicts
}


function selectViewDate() {
    renderRange = calendar.state.dateProfile.renderRange
    view_start = moment(renderRange.start)
    console.log("VIEWSTART", view_start)
    addDatepickerToInput("#view_date_input", dateFormat, view_start.startOf("day"))
    $('#view_date_input').on('dp.change', function (e) {
        setViewDate($('#view_date_input').val());
        $("#view_date_input").datetimepicker("hide")
        $('#view_date_modal').modal('hide');
    })
    $("#view_date_modal").modal('show', function () {
        $("#view_date_input").datetimepicker("show")
    })
}

setViewDate = function (date) {

    renderRange = calendar.state.dateProfile.renderRange
    view_start = moment(renderRange.start)
    view_end = moment(renderRange.end)
    let current_view_date = moment(calendar.getDate())
    let new_view_date = moment.utc(date, dateFormat)

    console.log("DATE", new_view_date.startOf('day'), current_view_date.startOf('day'))
    let diff = new_view_date.startOf('day').diff(current_view_date.startOf('day'), 'days')
    if (diff < 0)
        diff--;

    back = 1000
    forward = diff + back
    console.log("Diff between dates: ", diff)

    calendar.incrementDate({day: -back})
    //calendar.incrementDate({day: forward})
    calendar.gotoDate(new_view_date.toDate())
}

function addDatepickerToInput(element, format = datetimeFormat, initialtime = null) {
    var datePickerObjectStarttime = $(element).data("DateTimePicker");
    if (typeof datePickerObjectStarttime !== "undefined") {
        // it's already been Initialize . Just update the date.
        if (initialtime) {
            datePickerObjectStarttime.date(initialtime);
        } else {
            datePickerObjectStarttime.date(element.value);
        }
    } else {
        // it hasn't been initialized yet. Initialize it with the date.
        $(element).datetimepicker({
            format: format,
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false,
            widgetPositioning: {horizontal: 'left', vertical: 'bottom'}
        });

        if (initialtime) {
            console.log("INITIALTIME", initialtime)
            $(element).data("DateTimePicker").date(initialtime)
        } else {
            $(element).data("DateTimePicker").date(element.value)
        }
    }
}

toggleOverlay = function () {
    if ($.cookie("e2s_show_extended_props") == "true") {
        $('#extended_props_wrapper').show();
    }
    if (document.getElementById("overlay").style.display == "block") {
        document.getElementById("overlay").style.display = "none";
    } else {
        document.getElementById("overlay").style.display = "block";
    }
}

readyforcalendarload_fired = false
systemReadyForCalendarLoad = function (system, value = true) {
    systemsReady[system] = value
    console.log("systemReadyForCalendarLoad")
    for (systemReady in systemsReady) {
        console.log(systemReady, systemsReady[systemReady])
        if (systemsReady[systemReady] != true) {
            console.log("NOT READY", systemReady)
            return false
        }
        console.log("READY", systemReady)
    }
    $("body").removeClass("loading")
    $("#spinner").css("display", "none")
    $("#content").css("display", "block")
    var evt = new Event('readyforcalendarload');
    if (readyforcalendarload_fired == false) {
        readyforcalendarload_fired = true
        document.dispatchEvent(evt);
    }
}


isSameDay = function (d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
        d1.getMonth() === d2.getMonth() &&
        d1.getDate() === d2.getDate();
}

isClicked = false;
isDblClicked = false;
click_doublick_switch = function (info, click_function = null, doubleclick_function = null) {
    if (isClicked) {
        isDblClicked = true;
        isClicked = false;
    } else {
        isClicked = true;
        isDblClicked = false
        setTimeout(function () {
            if (isClicked) {
                click_function(info)
            }
            if (isDblClicked) {
                doubleclick_function(info)
            }
            isClicked = false;
            isDblClicked = false;
        }, 250);
    }

}

make_event_unique = function () {
    let hits = []
    if (!calendar)
        return
    let allEvents = calendar.getEvents()
    for (event of allEvents) {
        for (ev of allEvents) {

            if (ev._def.publicId == event._def.publicId) {
                if (hits.includes(event._def.publicId)) {
                    ev.remove()
                    calendar.refetchEvents()
                }
                hits.push(ev._def.publicId)
            }

        }
    }

}


blink = function () {
    $(".calculate-btn").animate({
        opacity: '0'
    }, function () {
        $(this).animate({
            opacity: '1'
        }, blink);
    });
}

getRealCollisionData = function (event, ressource, participants) {

    let conflicts = {}
    if (participants == null)
        participants = []
    if (event._def.resourceIds.includes(ressource)) {
        conflicts["withResources"] = ressource
    }

    intersection_result = participants.filter(value => event._def.extendedProps.attachedRessources.join("-").includes(value))

    if (intersection_result.length > 0) {
        conflicts["withPersons"] = intersection_result
    }
    return conflicts
}

activate_filter_input = function (node) {
    if (!document.getElementById("filter_text")) {
        let input = document.createElement("input")
        input.type = "text"
        input.id = "filter_text_old"
        input.placeholder = "Suchbegriff"
        input.onkeyup = filter_events;
        node.parentNode.append(input)
    } else {
        reset_filter()
        let input = document.getElementById("filter_text");
        input.parentNode.removeChild(input)

    }
}


filter_events = function (events = null) {
    console.log("Filter", calendar.getEvents())
    for (event of calendar.getEvents()) {
        filter_event(event)
    }
    calendar.rerenderEvents()
}

calendarStarttime = "08:00:00"
calendarCustomStarttime = calendarStarttime
useCustomStarttime = false

checkForEventTimesNotInView = function() {
    calendarCustomStarttime = calendarStarttime
    for (event of calendar.getEvents()) {
        if(event._def.allDay != true) {
                let eventStart = event.start.toLocaleTimeString()
                let eventStartTime = new Date()

                eventStartTime = eventStartTime.setHours(eventStart.split(":")[0], eventStart.split(":")[1], eventStart.split(":")[2]);
                let minTime = new Date()
                minTime.setHours(calendar.getOption("minTime").split(":")[0], calendar.getOption("minTime").split(":")[1], calendar.getOption("minTime").split(":")[2])

                if (eventStartTime < minTime) {
                    if(eventStart.split(":")[0] < calendarCustomStarttime.split(":")[0]) {
                        calendarCustomStarttime = (eventStart.split(":")[0] -2) + ":00:00"
                        useCustomStarttime = true

                        calendar.setOption("minTime", calendarCustomStarttime)
                        calendar.refetchEvents()
                    }
                }
            }
    }
}

customize_event = function (event) {
    //MIM
    console.log()
    if ($.cookie("consumer_id") == "bf6b7cdf-af3a-4d4e-9450-eb71d2791da4" && event._def.extendedProps && event._def.extendedProps.ep_terminok && event._def.extendedProps.ep_terminok == 0) {
        event._def.rendering = ""
        event._def.ui.backgroundColor = "#555555"
        event._def.ui.textColor = "#990000"
    }
    return event

}

function adjustColor(color, amount) {
    return '#' + color.replace(/^#/, '').replace(/../g, color => ('0' + Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)).substr(-2));
}

filter_event = function (event, search = null) {
    if (search == null && document.getElementById("filter_text") != null) {
        search = document.getElementById("filter_text").value
    }

    let hidden = false
    // if still null, we have no search going on
    if (search == null) {
        return hidden;
    }

    if (search == "") {
        event._def.ui.classNames = ["defaultEvent"]

    } else {
        if (event._def.title.toUpperCase().indexOf(search.toUpperCase()) == -1 && JSON.stringify(event._def.extendedProps.attachedRessources).toUpperCase().indexOf(search.toUpperCase()) == -1) {
            event._def.ui.classNames = ["hiddenEvent"]
            hidden = true
        } else {
            event._def.ui.classNames = ["defaultEvent"]
        }

        if (search.toUpperCase().includes("RAUM:")) {
            let search_term = search.toUpperCase().replace("RAUM:", "")

            for (ressourceId of event._def.resourceIds) {
                console.log("RessourceID", ressourceId.toUpperCase(), "SearchTERM", search_term)
                if (ressourceId.toUpperCase().indexOf(search_term) != -1) {
                    console.log("ROOMSEARCH FOUND")
                    event._def.ui.classNames = ["defaultEvent"]
                    hidden = false
                } else {
                    console.log("ROOMSEARCH NOT FOUND")
                    event._def.ui.classNames = ["hiddenEvent"]
                    hidden = true
                }

            }
        }
    }

    return hidden
}


reset_filter = function () {
    for (event of calendar.getEvents()) {
        event._def.ui.classNames = ["defaultEvent"]
    }
    calendar.rerenderEvents()
}

isResourceOthers = function (resource_id) {
    if (resource_id == "others") {
        return false
    }
    for (res of resources) {
        if (resource_id == res.id) {
            return false
        }
    }
    return true
}

sortToOthersResource = function () {
    if (!calendar)
        return
    for (event of calendar.getEvents()) {
        event._def.resourceIds = removeElementFromArray(event._def.resourceIds, "others")
        //event._def.resourceIds = removeElementFromArray(event._def.resourceIds, "eb")
        //console.log("AFTER",event._def.title, event._def.resourceIds)
        for (id of event._def.resourceIds) {
            if (isResourceOthers(id)) {
                event._def.resourceIds.push("others")
            }

        }
    }
    calendar.rerenderEvents()
}

removeElementFromArray = function (arrOriginal, elementToRemove) {
    return arrOriginal.filter(function (el) {
        return el !== elementToRemove
    });
}

post = function (url, data, callback) {
    var data = JSON.stringify(data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status == 200 || this.status == 204) {
                callback(this.responseText);
            } else {
                //alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");
                calendar.refetchEvents();
            }
        }
    });

    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("apikey", getCookie("apikey"));
    xhr.send(data);
}

get = function (url, callback, ignoreErrors) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.ignoreErrors = ignoreErrors

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            if (this.status == 200 || this.status == 204 || ignoreErrors) {
                callback(this.responseText);
            } else {
                //alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");
                calendar.refetchEvents();
            }
        }
    });

    xhr.open("GET", url);
    xhr.setRequestHeader("apikey", getCookie("apikey"));
    xhr.send();
}

enterFullscreen = function (element) {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    }
}


var timer
var touchduration = 1000; //length of time we want the user to touch before we do something

touchstart = function (e) {
    timer = setTimeout(onlongtouch.bind(null, e), touchduration);
}

touchend = function () {
    //stops short touches from firing the event
    if (timer) {
        clearTimeout(timer); // clearTimeout, not cleartimeout..
    }
}

onlongtouch = function (e) {
    if (e.srcElement.className && e.srcElement.className.indexOf("fc-event") >= 0 || e.srcElement.className && e.srcElement.className.indexOf("fc-time") >= 0) {
        if (!selectedEvent) {
            alert("Bitte wählen Sie den Termin erst aus (Antippen)")
        } else if (confirm("Wollen Sie den ausgewählten Termin löschen?")) {
            deleteEvent(selectedEvent);
        }
    }
};

function showHelp() {
    $('#legend_wrapper').hide("slide")
    if (document.getElementById("extended_form_help")) {
        $("#custom_help_wrapper").html(document.getElementById("extended_form_help").innerHTML)
    }
    $("#helpFormWrapper").toggle("slide")
}

$(function () {
    $("#eventFormWrapper").mousedown(handle_mousedown);
});

function handle_mousedown(e) {
    console.log(e)
    if (e.target.tagName != "H2" && e.target.id != "icon_menu_wrapper" && !e.target.classList.contains("dragger"))
        return;
    window.my_dragging = {};
    my_dragging.pageX0 = e.pageX;
    my_dragging.pageY0 = e.pageY;
    my_dragging.elem = this;
    my_dragging.offset0 = $(this).offset();

    function handle_dragging(e) {
        var left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0);
        var top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
        $(my_dragging.elem)
            .offset({top: top, left: left});
    }

    function handle_mouseup(e) {
        $('body')
            .off('mousemove', handle_dragging)
            .off('mouseup', handle_mouseup);
    }

    $('body')
        .on('mouseup', handle_mouseup)
        .on('mousemove', handle_dragging);
}

toggleMenu = function (mode, callback = function () {
}) {
    $.cookie("e2s_menue_mode", mode);
    if (mode == "icons") {
        $("#calendar").animate({marginLeft: 74, complete: callback});
        $("#calendar").addClass("small_menu")
        $("#calendar").removeClass("wide_menu")
        $("#icon_menue").show("slideDown")
        $(".menue_icon_only").show("slideDown")
        $("#opened_menue").hide("slideUp")
        $("#menue_header_icon").hide("slideUp")
        $('#side_menu_wrapper').animate({width: 75, complete: callback});
        $(".fc-axis").addClass("small_menu")
        $(".fc-left").addClass("small_menu")
        $(".fc-axis").removeClass("wide_menu")
    }
    if (mode == "full") {
        $("#calendar").animate({marginLeft: 219, complete: callback});
        $("#calendar").removeClass("small_menu")
        $("#calendar").addClass("wide_menu")
        $(".fc-axis").addClass("wide_menu")
        $("#icon_menue").hide("slideUp")
        $(".menue_icon_only").hide("slideUp")
        $("#opened_menue").show("slideDown")
        $("#menue_header_icon").show("slideDown")
        $('#side_menu_wrapper').animate({width: 220}, {complete: callback});
        $(".fc-axis").removeClass("small_menu")
        $(".fc-left").removeClass("small_menu")
    }
    fixMonthViewHeight()
}

focus_search = function () {
    $('#filter_text').focus()
}

function setActiveClassForMenu(clickedElement) {
    $('*[data-calendarview]').removeClass("active")
    let view = $(clickedElement).attr("data-calendarview")
    $("*[data-calendarview='" + view + "']").addClass("active")
}
