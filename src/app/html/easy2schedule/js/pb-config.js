import {BASECONFIG} from "./pbjs/pb-config.js?v=[cs_version]";
export const CONFIG = BASECONFIG

CONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "./js/pbjs/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "./js/pbjs/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
    {name: "socket.io", path: "./js/pbjs/vendor/socket.io.js?v=[cs_version]", loaded: null},
]

if (document.location.href.indexOf("http://localhost") == 0) {
    CONFIG.CIRCUIT_BASE_URL = "http://localhost:8080/easy2schedule/circuits/"
    CONFIG.APP_BASE_URL = "http://localhost:7000"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.SOCKET_SERVER = "https://thor.planblick.com"
} else if (document.location.href.indexOf("testblick.de") != -1) {
    CONFIG.CIRCUIT_BASE_URL = "//testblick.de/easy2schedule/circuits/"
    CONFIG.APP_BASE_URL = "//testblick.de"
    CONFIG.API_BASE_URL = "//odin.planblick.com"
    CONFIG.SOCKET_SERVER = "//odin.planblick.com"
    CONFIG.LOGIN_URL = "//testblick.de/login/"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"

} else {
    CONFIG.APP_BASE_URL = "https://www.serviceblick.com/easy2schedule/"
    CONFIG.LOGIN_URL = "https://www.serviceblick.com/login/"
}