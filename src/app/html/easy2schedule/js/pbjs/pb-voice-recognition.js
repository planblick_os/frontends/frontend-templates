import {fireEvent} from "./pb-functions.js?v=[cs_version]";
import {pbDecisionmaker} from "./pb-decisionmaker.js?v=[cs_version]";
import {PbEventActions} from "../pb-event-actions.js?v=[cs_version]";
import {pbMedia} from "./pb-media.js?v=[cs_version]";

export class PbVoiceRecognition {
    constructor() {
        this.debug = false
        this.isRecording = false
        this.notUnderstandTimer = undefined
        this.stream = null

        this.audioContext = new AudioContext();
        if (!PbVoiceRecognition.instance) {
            document.addEventListener("startVoiceRecognition", function (event) {
                PbVoiceRecognition.instance.startRecording(event)
            })
            document.addEventListener("noAudioInWhenExpected", function (event) {
                console.log("LISTENER noAudioInWhenExpected", event)
                PbEventActions.instance.getByName("noAudioInWhenExpected")(event.data)
            })

            document.addEventListener("noIntentionFound", function (event) {
                console.log("LISTENER noIntentionFound", event)
                PbEventActions.instance.getByName("noIntentionFound")(event.data)
            })

            document.addEventListener("audioInputNotUnderstood", function (event) {
                console.log("LISTENER audioInputNotUnderstood", event)
                PbEventActions.instance.getByName("audioInputNotUnderstood")(event.data)
            })

            PbVoiceRecognition.instance = this;
        }


        return PbVoiceRecognition.instance;
    }

    startRecording(myevent) {
        PbVoiceRecognition.instance.stream_recorder = new RecordRTC(new pbMedia().getStream(), {
            type: 'audio', mimeType: 'audio/wav',
            recorderType: StereoAudioRecorder, numberOfAudioChannels: 1,
            desiredSampRate: 16000
        });


        $("#overlay").show()
        $(".listening.block").show()
        PbVoiceRecognition.instance.notUnderstandTimer = setTimeout(function () {
                console.log("HARD STOPPED RECORDING FOR", myevent)
                PbVoiceRecognition.instance.stopRecording()
                fireEvent("noAudioInWhenExpected", myevent)
            }
            , 10000)

        PbVoiceRecognition.instance.isRecording = true
        console.log("START RECORDING FOR", myevent)


        let options = {"interval_in_ms": 100};
        // https://github.com/otalk/hark
        PbVoiceRecognition.instance.speechEvents = hark(new pbMedia().getStream(), options);
        PbVoiceRecognition.instance.record()
        /*PbVoiceRecognition.instance.speechEvents.on('speaking', function () {
            PbVoiceRecognition.instance.record()
        });*/
        PbVoiceRecognition.instance.speechEvents.on('stopped_speaking', function () {
            PbVoiceRecognition.instance.stream_recorder.stopRecording(function () {
                PbVoiceRecognition.instance.stopRecording();
                PbVoiceRecognition.instance.transcribeLastRecording(myevent);
            })
        });
        PbVoiceRecognition.instance.speechEvents.on('volume_change', function (volume, threshold) {
            //console.log("Volume change", volume, threshold)
        });
        console.log('Started recording');

    }

    stopRecording() {
        $("#overlay").hide()
        $(".listening.block").hide()
        clearTimeout(PbVoiceRecognition.instance.notUnderstandTimer)
        if (PbVoiceRecognition.instance.isRecording == false) {
            return
        }
        PbVoiceRecognition.instance.speechEvents.off('stopped_speaking')
        PbVoiceRecognition.instance.stream_recorder.stopRecording();
        PbVoiceRecognition.instance.data_blob = PbVoiceRecognition.instance.stream_recorder.getBlob()

        if (PbVoiceRecognition.instance.debug === true) {
            PbVoiceRecognition.instance.stream_recorder.getDataURL(function (dataUrl) {
                let recordingAudio
                if (document.getElementById("lastRecording")) {
                    recordingAudio = document.getElementById("lastRecording")
                } else {
                    recordingAudio = document.createElement("audio");
                }
                recordingAudio.controls = "controls"
                recordingAudio.autobuffer = "autobuffer"
                recordingAudio.autoplay = "autoplay"
                recordingAudio.id = "lastRecording"
                $("body").prepend(recordingAudio)

                let recordingSource
                if (document.getElementById("recordingSource")) {
                    recordingSource = document.getElementById("recordingSource")
                    recordingSource.src = dataUrl
                } else {
                    recordingSource = document.createElement("source");
                    recordingSource.src = dataUrl
                    $(recordingAudio).append(recordingSource)
                }

            })
        }
        PbVoiceRecognition.instance.isRecording = false
        console.log('Stopped recording.');

    }

    record() {
        PbVoiceRecognition.instance.isRecording = true
        PbVoiceRecognition.instance.stream_recorder.startRecording();
    }

    transcribeLastRecording(sourceEvent) {
        let task_id = sourceEvent.type + "_" + Date.now()
        new pbDecisionmaker().decideForSpeechRecognitionResult(task_id)
        fireEvent("streamForTranscription", {
            "source_event": {"type": sourceEvent.type, "data": sourceEvent.data},
            "task_id": task_id,
            "data_blob": PbVoiceRecognition.instance.data_blob
        })
    }
}