import {CONFIG} from '../pb-config.js?v=[cs_version]';
import {getCookie, fireEvent} from './pb-functions.js?v=[cs_version]';

export class pbStatistics {
    constructor() {
        if (pbStatistics.instance) {
            return pbStatistics.instance;
        }
        pbStatistics.instance = this;
    }

    async logVisit() {
        fpJS.load().then(fp => {
            fp.get().then(result => {
                // This is the visitor identifier:
                const visitorId = result.visitorId;
                let data = {
                    consumer: CONFIG.PAGEOWNER,
                    login: getCookie("username") || visitorId,
                    record_type: "websiteVisit",
                    record_data: {
                        time: new Date().toLocaleString(),
                        fingerprint: visitorId,
                        url: window.location.href,
                        browser_locale: navigator.language,
                        referer: document.referrer
                    }
                }
                pbStatistics.instance.addStatisticalData(data)
            });
        });
    }

    async logContactFormSubmitted(inputValuesToLog = "") {
        fpJS.load().then(fp => {
            fp.get().then(result => {
                // This is the visitor identifier:
                const visitorId = result.visitorId;
                let data = {
                    consumer: CONFIG.PAGEOWNER,
                    login: getCookie("username") || visitorId,
                    record_type: "contactFormSubmitted",
                    record_data: {
                        time: new Date().toLocaleString(),
                        fingerprint: visitorId,
                        url: window.location.href,
                        browser_locale: navigator.language,
                        referer: document.referrer,
                        inputValues: escape(inputValuesToLog)
                    }
                }
                pbStatistics.instance.addStatisticalData(data)
            });
        });
    }

    addStatisticalData(data) {
        if (CONFIG.ENABLESTATISTICS) {
            var data = JSON.stringify(data);

            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;

            xhr.addEventListener("readystatechange", function () {
                if (this.readyState === 4) {
                    console.log(this.responseText);
                }
            });

            xhr.open("POST", CONFIG.API_BASE_URL + "/es/cmd/addStatisticalData");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("apikey", CONFIG.PUBLIC_KEY);

            xhr.send(data);
        }
    }
}