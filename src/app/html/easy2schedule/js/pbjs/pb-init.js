import {CONFIG} from '../pb-config.js?v=[cs_version]';
import {fireEvent, getCookie, waitFor} from "./pb-functions.js?v=[cs_version]";
import {socketio} from "./pb-socketio.js?v=[cs_version]";

export class pbInit {
    constructor() {
        this.systems_ready = {
            "js_sources_loaded": false,
        }

        if(getCookie("apikey")) {
            this.systems_ready["socketio_connected"] = false
        }

        this.loading_files = {}
        this.needed_js_files = CONFIG.NEEDED_JS_FILES
        if (!pbInit.instance) {
            pbInit.instance = this;
        }

        return pbInit.instance;
    }

    now() {
        document.addEventListener("js_sources_loaded", function (event) {
            if(getCookie("apikey")) {
                new socketio().init()
            }
        })
        document.addEventListener("socketio_connected", function (event) {
            pbInit.instance.systems_ready["socketio_connected"] = true
            document.addEventListener("streamForTranscription", function (event) {
                socketio.instance.socket.emit('streamForTranscription', event.data)
            })
        })

        this.load_needed_js_files()


        waitFor(this.is_system_ready, pbInit.instance.system_ready)
    }

    system_ready() {
        fireEvent("system-initialized")
    }

    load_needed_js_files() {
        $.ajaxSetup({
            cache: CONFIG.JS_CACHE_ACTIVE
        });
        for (let file of this.needed_js_files) {
            this.load_file(file)
        }

        waitFor(this.finished_loading_files, pbInit.instance.files_loaded)

    }

    load_file(file) {
        pbInit.instance.loading_files[file.name] = true
        $.getScript(file.path)
            .done(function (script, textStatus) {
                fireEvent(file.name + '_loaded')
                pbInit.instance.loading_files[file.name] = false
            })
            .fail(function (jqxhr, settings, exception) {
                file.loaded = false
                console.warn(file.path, exception);
                fireEvent('js_file_load_failed', file)
            });
    }

    finished_loading_files() {
        let still_loading = false
        for (let key of Object.keys(pbInit.instance.loading_files)) {
            if (pbInit.instance.loading_files[key] == true) {
                still_loading = true
            }
        }
        console.log("Files still loading: ", still_loading, pbInit.instance.loading_files)
        return !still_loading
    }

    is_system_ready() {
        let still_loading = false
        for (let key of Object.keys(pbInit.instance.systems_ready)) {
            if (pbInit.instance.systems_ready[key] == false) {
                still_loading = true
            }
        }
        console.log("System ready: ", still_loading, pbInit.instance.systems_ready)
        return !still_loading
    }

    files_loaded() {
        pbInit.instance.systems_ready["js_sources_loaded"] = true
        fireEvent("js_sources_loaded")
    }
}