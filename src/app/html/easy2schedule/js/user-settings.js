import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {getQueryParam, getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import { NotificationsFilter } from "./notifications-filter.js?v=[cs_version]"


export class UserSettings {
    reminder_email_configuration = {
        footer: "",
        header: "",
        recipients: [],
        event_filter: new NotificationsFilter().getReminderEmailFilter()
    }

    constructor() {
        if (!UserSettings.instance) {
            UserSettings.instance = this
        }

        return UserSettings.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            var arr = [UserSettings.instance.fetchEmailConfigFromApi()]

            Promise.all(arr)
                .then(function (res) {
                    let [config] = res
                    UserSettings.instance.setEmailConfig(config)
                    resolve(UserSettings.instance)
                })
                .catch(function (err) {
                    console.log("ERROR", err)
                    reject()
                }) // This is executed
        })
    }

    fetchEmailConfigFromApi() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/email_config",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "error": reject
            };

            $.ajax(settings).done(function (response) {
                resolve(response)
            });
        })
    }

    setEmailConfig(config) {
        if(!config.email_config) {
            config["email_config"] = UserSettings.instance.reminder_email_configuration
        }
        UserSettings.instance.setEmailConfigFooter(config.email_config.footer ? config.email_config.footer:"")
        UserSettings.instance.setEmailConfigHeader(config.email_config.header ? config.email_config.header:"")
        UserSettings.instance.setEmailConfigRecipients(config.email_config.recipients ? config.email_config.recipients:"")

        new NotificationsFilter().setReminderEmailFilter(config.email_config.event_filter ? config.email_config.event_filter : {
            filter_user: false,
            filter_assignee: false,
            filter_permissions: false
        }
     )

    }

    getEmailConfig() {
        return UserSettings.instance.reminder_email_configuration
    }

    setEmailConfigHeader(value) {
        UserSettings.instance.reminder_email_configuration.header = value
        return UserSettings.instance
    }

    getEmailConfigHeader(value) {
        return UserSettings.instance.reminder_email_configuration.header
    }

    setEmailConfigFooter(value) {
        UserSettings.instance.reminder_email_configuration.footer = value
        return UserSettings.instance
    }

    getEmailConfigFooter(value) {
        return UserSettings.instance.reminder_email_configuration.footer
    }

    setEmailConfigRecipients(value) {
        UserSettings.instance.reminder_email_configuration.recipients = value
        return UserSettings.instance
    }

    getEmailConfigRecipients(value) {
        return UserSettings.instance.reminder_email_configuration.recipients
    }

    saveEmailConfig(email_config) {
        return new Promise(function (resolve, reject) {
            let payload = {
                data: {
                    reminder_email_configuration: UserSettings.instance.reminder_email_configuration
                }
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/setEasy2ScheduleReminderEmailConfiguration",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "error": reject
            };

            $.ajax(settings).done(function (response) {
                resolve(response)

            })
        })
    }
}