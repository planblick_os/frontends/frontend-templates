import {PbAccount} from '../../csf-lib/pb-account.js?v=[cs_version]';
import {
    handle_mousedown, getCookie, fireEvent,
} from '../../csf-lib/pb-functions.js?v=[cs_version]';
import {pbI18n} from '../../csf-lib/pb-i18n.js?v=[cs_version]';
import {PbEasy2schedule} from './pb-easy2schedule.js?v=[cs_version]';
import {UserSettings} from './user-settings.js?v=[cs_version]';
import {RenderUserSettings} from './render_usersettings.js?v=[cs_version]';
import {ContactManager} from '../../contact-manager/js/contact-manager.js?v=[cs_version]';
import {pbEventEntries} from './pb-event-entries.js?v=[cs_version]';
import {NotificationsFilter} from './notifications-filter.js?v=[cs_version]';
import {RessourceSettings} from './ressource-settings.js?v=[cs_version]'
import {RenderRessourceSettings} from './render_ressourcesettings.js?v=[cs_version]';
import {ColumnSettings} from './column-settings.js?v=[cs_version]';
import {RenderColumnSettings} from './render_columnsettings.js?v=[cs_version]';
import {RenderBookingList} from './render_booking_list.js?v=[cs_version]';
import {RenderStatistic} from "./render-statistic.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]";
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]";
import {RenderListView} from "./render_listview.js?v=[cs_version]";
import {PbEventRenderer} from "./eventRenderer.js?v=[cs_version]";


let make_event_unique = function (calendar) {
    let hits = [];
    if (!calendar) return;
    let allEvents = calendar.getEvents();
    for (let event of allEvents) {
        for (let ev of allEvents) {
            if (ev._def.publicId == event._def.publicId) {
                if (hits.includes(event._def.publicId)) {
                    ev.remove();
                    calendar.refetchEvents();
                }
                hits.push(ev._def.publicId);
            }
        }
    }
};

export class Easy2ScheduleModule {
    actions = {};
    addedEvents = [];
    handledTaskIds = []

    constructor() {
        if (!Easy2ScheduleModule.instance) {
            this.initListeners();
            this.initActions();
            $.unblockUI();
            Easy2ScheduleModule.instance = this;
        }

        return Easy2ScheduleModule.instance;
    }

    initActions() {
        this.actions['moduleEasy2ScheduleBookingInitialized'] = function (myevent, callback = () => '') {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission('easy2schedule')) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."));
                } else {
                    new PbAccount().handlePermissionAttributes();
                    if ($('#loader-overlay')) {
                        $('#loader-overlay').hide();
                        $('#contentwrapper').show();
                    }

                    new PbEasy2schedule().init().then(() => {
                        let startMoment = moment().startOf('day')
                        let start = startMoment.format(PbEasy2schedule.instance.mysqldatetimeFormat)
                        let endMoment = moment().endOf('day')
                        let end = endMoment.format(PbEasy2schedule.instance.mysqldatetimeFormat)

                        PbEasy2schedule.instance.fetchPersonalEntriesByTimes(start, end).then((entries) => {
                            new RenderBookingList().renderBookingEntries(entries, startMoment);
                        })


                        document.addEventListener('command', function (args) {
                            if (args.data.data.command == 'addEvent') {
                                RenderBookingList.instance.changeViewDate(RenderBookingList.instance.current_view_data.startOf("day"))

                            } else if (args.data.data.command == 'removeEvent') {
                                if (!Easy2ScheduleModule.instance.handledTaskIds.includes(args.data.data.args.event_id)) {
                                    Easy2ScheduleModule.instance.handledTaskIds.push(args.data.data.args.event_id)
                                    Fnon.Hint.Success('Eine Buchung wurde storniert', {displayDuration: 5000})
                                    RenderBookingList.instance.changeViewDate(RenderBookingList.instance.current_view_data.startOf("day"))
                                } else {
                                    console.log("Skipped double removeEvent with id", args.data.data.args.event_id)
                                }
                            }
                        }, false);
                    });
                    callback(myevent);
                }
            });
        };


        this.actions['moduleEasy2ScheduleInitialized'] = function (myevent, callback = () => '') {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission('easy2schedule')) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."));
                } else {
                    new PbAccount().handlePermissionAttributes();
                    if ($('#loader-overlay')) {
                        $('#loader-overlay').hide();
                        $('#contentwrapper').show();
                    }
                    new PbEasy2schedule().init().then(() => {
                        var settings = {
                            "url": CONFIG.API_BASE_URL + "/cb/calendar_options",
                            "method": "GET",
                            "timeout": 0,
                            "headers": {
                                "apikey": getCookie("apikey")
                            },
                            "success": (response) => {
                                response.eventRenderFunction = PbEventRenderer.getRenderer(response.eventRenderFunction)
                                PbEasy2schedule.instance.run(response)
                            },
                            "error": () => {
                                let response = {
                                    "displayEventEnd": true,
                                    "eventRenderFunction": "participants_title"
                                }
                                PbEasy2schedule.instance.run(PbEventRenderer.getRenderer(response.eventRenderFunction))
                            }
                        };
                        $.ajax(settings)

                        $('#eventFormWrapper').mousedown(handle_mousedown)
                        new PbGuide("./tours/user_getting_started.js")

                        // $('[data-tour-start-link="getting_started_link"]').click(()=>{
                        //     new PbGuide("./tours/user_getting_started.js").init().then(instance => instance.startTourFromQueryParam("firstSteps"))

                        // })
                        document.addEventListener('command', function (args) {
                            if (args.data && args.data.data && args.data.data.command == 'addEvent') {
                                PbEasy2schedule.instance.calendar.addEvent(args.data.data.args);
                                // For no specific reason, sometimes the first addEvent does not produce an event shown in the calendar
                                // So we call it twice, sicne then it's always shown and we have to make the events unique anyway again
                                // because Fullcalendar sometimes produces duplicates on it's own
                                PbEasy2schedule.instance.calendar.addEvent(args.data.data.args);
                                PbEasy2schedule.instance.checkForEventTimesNotInView();
                                make_event_unique(PbEasy2schedule.instance.calendar);
                                //Easy2ScheduleModule.instance.addedEvents.push(args.data.data.args.id)
                            } else if (args.data.data && args.data.data.command == 'removeEvent') {
                                event = PbEasy2schedule.instance.calendar.getEventById(args.data.data.args.event_id);
                                if (event) {
                                    event.remove();
                                }
                                PbEasy2schedule.instance.checkForEventTimesNotInView();
                            }
                        }, false);
                    });

                    new ContactManager().init().then(function () {
                        PbEasy2schedule.instance.contacts_list_sorted = new ContactManager().contacts_list;
                        PbEasy2schedule.instance.renderContactsList();
                    });

                    new pbEventEntries().init().then(function () {
                        let eventEntries = new pbEventEntries();
                        let sorted_groups = eventEntries.possible_groups;
                        sorted_groups.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);

                        let sorted_members = eventEntries.group_members;
                        sorted_members.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
                        let groupsandmembers = sorted_groups.concat(sorted_members);

                        PbEasy2schedule.instance.groupsandmembers = groupsandmembers

                        PbEasy2schedule.instance.addEntriesToAccessrightsDropdown();
                    });

                    $('#eventFormWrapper').on('mousedown', function (ev) {
                        if (ev.target.nodeName == 'A') {
                            for (let contact of PbEasy2schedule.instance.contacts_list_sorted) {
                                if (contact.contact_id == ev.target.dataset.value) {
                                    window.open('/contact-manager/?ci=' + ev.target.dataset.value, '_blank');
                                    break;
                                }
                            }
                        }
                    });

                    callback(myevent);
                }
            });
        };

        this.actions['initEasy2ScheduleUserSettings'] = function (myevent, callback = () => '') {
            new UserSettings().init().then(function () {
                console.log('FETCHED DATA', new UserSettings().reminder_email_configuration)
                callback(myevent)
            })
        }

        this.actions['easy2ScheduleUserSettingsInitialized'] = function (myevent, callback = () => '') {
            if (window.location.pathname.includes("field-settings.html")) {
                import("./controller.js?v=[cs_version]").then(function (ModuleImport) {
                    new ModuleImport.Controller().init(this).then(() => callback(myevent))
                }.bind(this))
            } else if (window.location.pathname.includes("status-settings.html")) {
                import("./controller.js?v=[cs_version]").then(function (ModuleImport) {
                    new ModuleImport.Controller().init(this).then(() => callback(myevent))
                }.bind(this))
            } else {
                new RenderUserSettings(UserSettings.instance).init()
                new PbGuide("./tours/user_getting_started.js")
                if ($('#loader-overlay')) {
                    $('#loader-overlay').hide()
                    $('#contentwrapper').show()
                }
                callback(myevent)
            }

        };

        this.actions['initEasy2ScheduleRessourceSettings'] = function (myevent, callback = () => '') {
            if (!new PbAccount().hasPermission('easy2schedule.ressources')) {
                new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
            } else {
                new RessourceSettings().init().then(function () {
                    console.log('FETCHED DATA', new RessourceSettings().ressources)
                    callback(myevent)
                })
            }
        }

        this.actions['easy2ScheduleRessourceSettingsInitialized'] = function (myevent, callback = () => '') {
            new RenderRessourceSettings().init()
            new PbGuide("./tours/user_getting_started.js")
            if ($('#loader-overlay')) {
                $('#loader-overlay').hide();
                $('#contentwrapper').show();
            }
            callback(myevent);
        };

        this.actions["initEasy2ScheduleColumnSettings"] = function (myevent, callback = () => '') {
            if (!new PbAccount().hasPermission('easy2schedule.columns')) {
                new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
            } else {
                new ColumnSettings().init().then(function () {
                    console.log('FETCHED DATA', new ColumnSettings().columns)
                    callback(myevent)
                })
            }
        }

        this.actions["easy2ScheduleColumnSettingsInitialized"] = function (myevent, callback = () => '') {
            new RenderColumnSettings().init()
            new PbGuide("./tours/user_getting_started.js")
            if ($('#loader-overlay')) {
                $('#loader-overlay').hide();
                $('#contentwrapper').show();
            }
            callback(myevent)
        }

        this.actions["initEasy2ScheduleListView"] = function (myevent, callback = () => '') {
            if (!new PbAccount().hasPermission('easy2schedule')) {
                new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
            } else {
                callback(myevent)
            }
        }

        this.actions["easy2ScheduleListViewInitialized"] = function (myevent, callback = () => '') {
            new RenderListView().init()
            if ($('#loader-overlay')) {
                $('#loader-overlay').hide();
                $('#contentwrapper').show();
            }
            callback(myevent)
        }

        this.actions["easy2ScheduleStatisticModuleInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("beta_access")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    import("./controller.js?v=[cs_version]").then(function (ModuleImport) {
                        new ModuleImport.Controller().init(this).then(() => callback(myevent))
                    }.bind(this))
                }
            })
        }
    }

    initListeners() {
        let socket = new socketio()
        socket.commandhandlers["refreshContacts"] = function (args) {
            ContactManager.instance.fetchContactList().then(function () {
                PbEasy2schedule.instance.contacts_list_sorted = new ContactManager().contacts_list
                PbEasy2schedule.instance.renderContactsList()
            })
        }

        $(document).on('click', 'button[data-selector="btn-save-booking"]', function (event) {
            let colors = {"demo_1": "#20bc22", "demo": "#20bc22", "demo_3": "#3161f3"}
            let attachedRessources = {
                "id": 1,
                "name": getCookie("username"),
                "fontColor": "#000000",
                "backgroundColor": colors[getCookie("username")],
                "loginId": "a440326d-de94-4af8-af13-fe7f76433d82"
            }
            const column_config = new ColumnSettings().columns
            let column_set = []
            for (let view in column_config) {
                for (let ressource of column_config[view]) {
                    column_set[ressource.id] = ressource
                }
            }
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewAppointment", "success": () => {
                    $('#available_rooms_list').hide();
                    $('#available_rooms_list_table > tbody').html("")
                    let starttime = moment.utc($(event.target).data("starttime"), PbEasy2schedule.instance.mysqldatetimeFormat).format(PbEasy2schedule.instance.datetimeFormat)
                    let endtime = moment.utc($(event.target).data("endtime"), PbEasy2schedule.instance.mysqldatetimeFormat).format(PbEasy2schedule.instance.datetimeFormat)
                    Fnon.Alert.Success(`Der Raum "${column_set[$(event.target).data("ressourceid")].title}" wurde von ${starttime} bis ${endtime} gebucht.`, `Raumbuchung erfolgreich.`, 'Ok', () => {
                        location.href = "/easy2schedule/booking.html"
                        $('#overlay').hide()

                    });
                }, "method": "POST", "timeout": 0, "headers": {
                    "Content-Type": "application/json", "apikey": getCookie("apikey")
                }, "data": JSON.stringify({
                    "event_id": "",
                    "resourceId": $(event.target).data("ressourceid"),
                    "textColor": "",
                    "color": "",
                    "attachedRessources": [JSON.stringify(attachedRessources)],
                    "starttime": $(event.target).data("starttime"),
                    "endtime": $(event.target).data("endtime"),
                    "title": `${getCookie("username")}/${column_set[$(event.target).data("ressourceid")].title} am ${new Date().toLocaleTimeString('de-DE')}`,
                    "savekind": "create",
                    "extended_props": {
                        "creator_login": "demo_1"
                    },
                    "allDay": false,
                    "data_owners": [],
                    "reminders": [{
                        "type": "gui", "seconds_in_advance": 180, "recipients": ["demo_1"]
                    }]
                }),
            };

            $.ajax(settings)
        });

        if (document.getElementById('search_booking')) {
            document.getElementById('search_booking').addEventListener('click', function (event) {
                event.preventDefault()
                event.stopPropagation()
                var fields = PbEasy2schedule.instance.getEventFormElements()

                let starttime = moment.utc(fields["starttime"].value, PbEasy2schedule.instance.datetimeFormat).format(PbEasy2schedule.instance.mysqldatetimeFormat)
                let endtime = moment.utc(fields["endtime"].value, PbEasy2schedule.instance.datetimeFormat).format(PbEasy2schedule.instance.mysqldatetimeFormat)
                let room_type = $("#room_type").val()
                new ColumnSettings().init().then(() => {
                    let column_set = {}
                    console.log("FIELDS", starttime, endtime, room_type, new ColumnSettings().columns)
                    const column_config = new ColumnSettings().columns
                    for (let view in column_config) {
                        for (let ressource of column_config[view]) {
                            column_set[ressource.id] = ressource
                        }
                    }

                    new PbEasy2schedule().fetchEntriesByTimes(starttime, endtime).then((results) => {
                        console.log("DATE-ENTRIES", results)
                        for (let result of results) {
                            console.log("RESULT", result)
                            delete column_set[result.resourceId]
                        }

                        for (let column in column_set) {
                            if (!column.startsWith(room_type)) {
                                delete column_set[column]
                            }
                        }


                        $('#available_rooms_list').hide()
                        $('#available_rooms_list_table > tbody').html("")
                        let available_ressources = 0
                        let clicked_button = undefined
                        for (let column in column_set) {
                            available_ressources++
                            let data = column_set[column]
                            $('#available_rooms_list_table > tbody').append(`<tr><td style="border:none;"><button data-selector="btn-save-booking" class="btn btn-primary" data-ressourceId='${data.id}' data-starttime='${starttime}' data-endtime='${endtime}'>${data.title} jetzt buchen</button></td></tr>`);
                            if ($("#autobook").is(":checked")) {
                                clicked_button = $('button[data-selector="btn-save-booking"]')
                                clicked_button.click()
                                break;
                            }
                        }
                        if (!$("#autobook").is(":checked")) {
                            $('#available_rooms_list').show()
                        }

                        Fnon.Hint.Init({
                            displayDuration: 6000
                        })

                        if (available_ressources == 0) {
                            Fnon.Hint.Danger('Für den ausgewählten Zeitraum stehen keine Räume zur Verfügung.', 'Keine Räume verfügbar');
                            $('#overlay').show()
                        } else {
                            if (!$("#autobook").is(":checked")) {
                                $('#overlay').hide()
                            }
                        }


                    })

                })
            }, false);
        }

        if (document.getElementById('appointment')) {
            document
                .getElementById('appointment')
                .addEventListener('submit', new PbEasy2schedule().fnAppointmentsubmit, false);
            document.getElementById('full_side_menu_newEvent').addEventListener('click', function () {
                new PbEasy2schedule().setFormFields('appointment', null);
                $("#series-container").hide()
            }, false);
            if (document.getElementById('full_side_menu_newSeriesEvent')) {
                document.getElementById('full_side_menu_newSeriesEvent').addEventListener('click', function () {
                    new PbEasy2schedule().setFormFields('appointment', null);
                    $("#series-container").show()
                }, false)
            }
            document.getElementById('full_side_menu_editEvent').addEventListener('click', function () {
                new PbEasy2schedule().edit_event();
            }, false);
            document.getElementById('full_side_menu_deleteEvent').addEventListener('click', function () {
                new PbEasy2schedule().deleteEvent();
            }, false);

            document.getElementById('icon_side_menu_newEvent').addEventListener('click', function () {
                new PbEasy2schedule().setFormFields('appointment', null);
                $("#series-container").hide()
            }, false);
            if (document.getElementById('icon_side_menu_newSeriesEvent')) {
                document.getElementById('icon_side_menu_newSeriesEvent').addEventListener('click', function () {
                    new PbEasy2schedule().setFormFields('appointment', null);
                    $("#series-container").show()
                }, false);
            }
            if (document.getElementById('icon_side_menu_newBooking')) {
                document.getElementById('icon_side_menu_newBooking').addEventListener('click', function () {
                    new PbEasy2schedule().setFormFields('appointment', null);
                }, false);
            }

            document.getElementById('icon_side_menu_editEvent').addEventListener('click', function () {
                new PbEasy2schedule().edit_event();
            }, false);
            document.getElementById('icon_side_menu_deleteEvent').addEventListener('click', function () {
                new PbEasy2schedule().deleteEvent();
            }, false);

            document.addEventListener('keydown', new PbEasy2schedule().handleKeyPress, false);
            window.addEventListener('touchstart', new PbEasy2schedule().touchstart, false);
            window.addEventListener('touchend', new PbEasy2schedule().touchend, false);
        }

        $('#logout_button').click(function (event) {
            fireEvent('logoutButtonClicked', {
                login: getCookie('username'), consumer: getCookie('consumer_name'),
            });
        });
        $("#new_reminder_button").click(function (event) {
            new PbEasy2schedule().addNewReminderForm()
        })
        $('#update_weekly_mailer_button').on('click', function () {
            RenderUserSettings.instance.updateReminderEmailConfig();

            NotificationsFilter.instance.setFilterOwnedEvents($('#filter-user').prop('checked') ? true : false);
            NotificationsFilter.instance.setFilterAssignedEvents($('#filter-assignee').prop('checked') ? true : false);
            NotificationsFilter.instance.setFilterOtherEvents($('#filter-permissions').prop('checked') ? true : false);
            console.log('UPDATEDFILTER', NotificationsFilter.instance.reminder_email_filter);
        });

        $(document).on('click', 'button[data-selector="btn-resource-edit"]', function () {
            let id = $(this).attr('data-update-resource-id')
            RenderRessourceSettings.instance.renderModal(id)
        });
        $("#btn-create-resource").on('click', function () {
            RenderRessourceSettings.instance.rs.setActiveRessource(undefined);
            RenderRessourceSettings.instance.active_resource.ressource_id = undefined
            RenderRessourceSettings.instance.renderModal(undefined)
        });

        $('#btn-update-resource').on('click', function () {
            RenderRessourceSettings.instance.saveChanges()
        });

        $(document).on('click', 'button[data-selector="btn-resource-delete"]', function () {
            let id = $(this).attr('data-delete-resource-id')
            let deleteTitle = new pbI18n().translate("Delete")
            let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the resource?")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RessourceSettings.instance.deleteRessource(id)
                    Fnon.Hint.Success('Ressource gelöscht')
                }
            })


            new RessourceSettings().fetchRessourcesFromApi().then(function () {
                RenderRessourceSettings.instance.renderTable(RessourceSettings.instance.ressources[0])

            })
        });
        document.addEventListener("notification", function (event) {
            if (event.data.data.event == "calendarRessourceAdded") {
                new RessourceSettings().init().then(function () {
                    RenderRessourceSettings.instance.renderTable(RessourceSettings.instance.ressources[0])
                    Fnon.Hint.Success('Ressource erstellt')
                })

            }
            if (event.data.data.event == "calendarRessourceDeleted") {
                new RessourceSettings().init().then(function () {
                    RenderRessourceSettings.instance.renderTable(RessourceSettings.instance.ressources[0])
                })
            }
            if (event.data.data.event == "calendarRessourceUpdated") {
                new RessourceSettings().init().then(function () {
                    RenderRessourceSettings.instance.renderTable(RessourceSettings.instance.ressources[0])
                    Fnon.Hint.Success('Ressource geupdated')
                })
            }
            if (event.data.data.event == "easy2scheduleRessourceUpdateFailed") {
                new RessourceSettings().init().then(function () {
                    console.log('FETCHED DATA AFTER uPDATE fAIL', RessourceSettings.instance.ressources[0])
                    RenderRessourceSettings.instance.renderTable(RessourceSettings.instance.ressources[0])
                })
            }
            if (event.data.data.event == "easy2ScheduleCalendarOptionsSet") {
                var settings = {
                    "url": CONFIG.API_BASE_URL + "/cb/calendar_options",
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey")
                    },
                    "success": (response) => {
                        response.eventRenderFunction = PbEventRenderer.getRenderer(response.eventRenderFunction)
                        PbEasy2schedule.instance.run(response)
                    },
                    "error": () => {
                        let response = {
                            "displayEventEnd": true,
                            "eventRenderFunction": "participants_title"
                        }
                        PbEasy2schedule.instance.run(PbEventRenderer.getRenderer(response.eventRenderFunction))
                    }
                };
                $.ajax(settings)
            }
        })


    }
}
