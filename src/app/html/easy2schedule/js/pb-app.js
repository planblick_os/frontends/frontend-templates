import {get, fireEvent, getCookie} from "./pbjs/pb-functions.js?v=[cs_version]";
import {CONFIG} from './pb-config.js?v=[cs_version]';

export class pbApp {
    constructor() {
        if (!pbApp.instance) {
            pbApp.instance = this;
            pbApp.instance.ressources = []
            pbApp.instance.init()
        }
        return pbApp.instance;
    }

    init() {
        get(CONFIG.API_BASE_URL + "/cb/ressources", pbApp.instance.fetchRessources)
        
        $('#openNewCardForm').click(function () {
            $("#todoFormModal").modal()
        })

        $('#createNewCardButton').click(function () {
            //let data = pbApp.instance.randomSet()
            let data = pbApp.instance.getFromForm("#todoFormModal")
            console.log("DATA", data)
            fireEvent("newTaskCreated", data, getCookie("consumer_name"))
            //pbApp.instance.addNewContact(data)
        })

    }

    fetchRessources = function (response) {
        fireEvent("calendarRessourcesFetched", response)
    }

    addNewContact(data) {
        let datetimeFormat = "DD.MM.YYYY HH:mm";
        let callback_at_date = moment(data["todo_callback_date"], datetimeFormat).toDate()
        let firstcontact_at_date = moment(data["todo_firstcontact_date"], datetimeFormat).toDate()

        let newRequest = $('#requestTemplate').clone()
        newRequest.attr("id", Math.random())

        newRequest.html(newRequest.html().replaceAll("[phone]", data["todo_phone"]))
        newRequest.html(newRequest.html().replaceAll("[name]", data["todo_name"]))
        newRequest.html(newRequest.html().replaceAll("[email]", data["todo_email"]))
        newRequest.html(newRequest.html().replaceAll("[format]", data["todo_format"]))
        newRequest.html(newRequest.html().replaceAll("[assigned-to]", pbApp.instance.getRessourceNameById(data["todo_firstcontact_assignee"]) ? pbApp.instance.getRessourceNameById(data["todo_firstcontact_assignee"]).display_name : ""))
        newRequest.html(newRequest.html().replaceAll("[callback-at]", callback_at_date!="Invalid Date" ? callback_at_date.toLocaleDateString('de-DE') + " " + callback_at_date.toLocaleTimeString(): ""))
        newRequest.html(newRequest.html().replaceAll("[callback-at-calendar-link]", callback_at_date.toLocaleDateString('de-DE')))
        newRequest.html(newRequest.html().replaceAll("[callback-by]", pbApp.instance.getRessourceNameById(data["todo_callback_assignee"]) ? pbApp.instance.getRessourceNameById(data["todo_callback_assignee"]).display_name: ""))
        newRequest.html(newRequest.html().replaceAll("[comment]", data["todo_comment"]))
        newRequest.html(newRequest.html().replaceAll("[firstcontact-at]", firstcontact_at_date!="Invalid Date" ? firstcontact_at_date.toLocaleDateString('de-DE') + " " + firstcontact_at_date.toLocaleTimeString():""))
        newRequest.html(newRequest.html().replaceAll("[pseudonym]", data["todo_pseudonym"]))
        $('#requestsContainer').append(newRequest)
    }

    getRessourceNameById(id) {
        for (let i = 0; i < pbApp.instance.ressources.length; i++) {
            if (pbApp.instance.ressources[i].id == id) {
                return (pbApp.instance.ressources[i])
            }
        }
    }

    getFromForm(selectorToSearchForInputs) {
        let fields = $(selectorToSearchForInputs).find("select, textarea, input")
        let data = {}
        $.each(fields, function (i, field) {
            data[field.id] = $(field).val()
            //$(field).val("")
        });
        return data
    }

    randomDate() {
        var startDate = new Date(2020, 0, 1).getTime();
        var endDate = new Date(2021, 0, 1).getTime();
        var spaces = (endDate - startDate);
        var timestamp = Math.round(Math.random() * spaces);
        timestamp += startDate;
        return new Date(timestamp);
    }
}


class myButtonFunctions {

}
