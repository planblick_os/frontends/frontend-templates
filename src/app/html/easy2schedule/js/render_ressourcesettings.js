import {RessourceSettings} from './ressource-settings.js?v=[cs_version]';
import {ContactManager} from '../../contact-manager/js/contact-manager.js?v=[cs_version]';
import {PbEasy2schedule} from './pb-easy2schedule.js?v=[cs_version]';
import {PbAccount} from '../../csf-lib/pb-account.js?v=[cs_version]';
import {clearForm} from '../../csf-lib/pb-formchecks.js?v=[cs_version]';
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]";

export class RenderRessourceSettings {
    active_resource = {};
    contacts_list_sorted = [];
    contactTitles = [];
    logins = undefined;
    resourceTable

    constructor() {
        if (!RenderRessourceSettings.instance) {
            RenderRessourceSettings.instance = this;
        }
        RenderRessourceSettings.instance.rs = new RessourceSettings();
        RenderRessourceSettings.instance.es = new PbEasy2schedule();
        return RenderRessourceSettings.instance;
    }

    init() {
        let promises = [new ContactManager().init(), new PbAccount().init()]
        Promise.all(promises).then(function (response) {
            RenderRessourceSettings.instance.contacts_list_sorted = new ContactManager().contacts_list
            RenderRessourceSettings.instance.renderContactsList()
            RenderRessourceSettings.instance.logins = new PbAccount().logins
            RenderRessourceSettings.instance.renderLoginsList()
            RenderRessourceSettings.instance.renderTable(RenderRessourceSettings.instance.rs.ressources[0])
        })
    }

    renderModal(value) {
        if (value) {
            RenderRessourceSettings.instance.rs.setActiveRessource(value);
            RenderRessourceSettings.instance.active_resource = RenderRessourceSettings.instance.rs.getActiveRessource();
            const {
                display_name,
                type,
                props: {eventBackgroundColor, eventFontColor, login_id, contact_id},
            } = RenderRessourceSettings.instance.active_resource;
            console.log("Contact id of active resource when modal rendered", contact_id)
            $('#resource_name').val(display_name);
            $('#resource_type').val(type);

            $('#colorpicker-background').spectrum({
                color: eventBackgroundColor,
                showInitial: true,
                showInput: true,
            });

            $('#colorpicker-text').spectrum({
                color: eventFontColor,
                showInitial: true,
                showInput: true,
            });
            $('#colorpicker_preview').css('color', `${eventFontColor}`);
            $('#colorpicker_preview').css(
                'background-color',
                `${eventBackgroundColor}`
            );

            $('#resource_contact').val(contact_id)
            $('#resource_login').val(login_id);

        } else {
            RenderRessourceSettings.instance.setModaltoDefaultValues()
        }

        $('#colorpicker-background').change(function () {
            RenderRessourceSettings.instance.changeColorPickerPreview();
        });
        $('#colorpicker-text').change(function () {
            RenderRessourceSettings.instance.changeColorPickerPreview();
        });
        var background = $('#colorpicker-background').spectrum('get').toHexString();
        $('#colorpicker_preview').html(`${background}`);
    }

    renderContactsList() {
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.name
            }
        }

        let contactObject = {}
        for (let entry of RenderRessourceSettings.instance.contacts_list_sorted) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
                else if (entry.data[tmp_titles[titlefield_key].toLowerCase()]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key].toLowerCase()] + " "
                }
            }
            contactObject = {
                contact_id: contactId,
                title: contactTitleString
            }
            RenderRessourceSettings.instance.contactTitles.push(contactObject)
        }

        let tmp = RenderRessourceSettings.instance.contactTitles;


        tmp.sort(function (a, b) {
                a = a.title.toLowerCase()
                b = b.title.toLowerCase()
                return a < b ? -1 : 1
            }
        );

        RenderRessourceSettings.instance.contacts_list_sorted = tmp;
        RenderRessourceSettings.instance.addContactsToContactList();
    }

    addContactsToContactList() {
        var dropdown = document.getElementById('resource_contact')
        dropdown.innerHTML = ''
        let option = document.createElement('option')
        option.text = new pbI18n().translate("No contact linked")
        option.value = '' //JSON.stringify(item)
        option.setAttribute('data-contact-id', '')
        dropdown.add(option)
        for (let item of RenderRessourceSettings.instance.contacts_list_sorted) {
            let option = document.createElement('option');
            option.text = item.title;
            option.value = item.contact_id; //JSON.stringify(item)
            option.setAttribute('data-contact-id', item.contact_id);
            dropdown.add(option);
        }
    }

    renderLoginsList() {
        if (RenderRessourceSettings.instance.logins) {
            RenderRessourceSettings.instance.logins.sort(function (a, b) {
                let textA = a.username.toUpperCase();
                let textB = b.username.toUpperCase();
                return textA < textB ? -1 : textA > textB ? 1 : 0;
            });
            RenderRessourceSettings.instance.addLoginsToLoginList();
        }
    }

    addLoginsToLoginList() {
        var dropdown = document.getElementById('resource_login');
        dropdown.innerHTML = '';
        let option = document.createElement('option');
        option.text = new pbI18n().translate("No login linked")
        option.value = ''; //JSON.stringify(item)
        option.setAttribute('data-login-id', '');
        dropdown.add(option);

        for (let item of RenderRessourceSettings.instance.logins) {
            let option = document.createElement('option');
            option.text = item.username;
            option.value = item.userid; //JSON.stringify(item)
            option.setAttribute('data-login-id', item.userid);
            dropdown.add(option);
        }
    }

    changeColorPickerPreview() {
        var previewFontColor = $('#colorpicker-text').spectrum('get');
        var previewFontColorHex = previewFontColor.toHexString();
        var previewBackgroundColor = $('#colorpicker-background').spectrum('get');
        var previewBackgroundColorHex = previewBackgroundColor.toHexString();

        $('#colorpicker_preview').css('color', `${previewFontColorHex}`);
        $('#colorpicker_preview').css(
            'background-color',
            `${previewBackgroundColorHex}`
        );

        $('#colorpicker_preview').html(`${previewBackgroundColorHex}`);
    }

    saveChanges() {
        var name = $('#resource_name').val();
        var contact = $('#resource_contact').val();
        var background = $('#colorpicker-background').spectrum('get').toHexString();
        var fontcolor = $('#colorpicker-text').spectrum('get').toHexString();
        var login = $('#resource_login').val();
        var ressource_id =
            RenderRessourceSettings.instance.active_resource.ressource_id;
        var type = $('#resource_type').val();

        RenderRessourceSettings.instance.rs.updateActiveRessource(
            name,
            contact,
            background,
            fontcolor,
            login,
            ressource_id,
            type
        );
        RenderRessourceSettings.instance.rs.saveActiveRessource();
        RenderRessourceSettings.instance.setModaltoDefaultValues();
        $('#modalResourceDetails').modal('hide');
    }

    setModaltoDefaultValues() {
        $('#resource_name').val('');
        $('#colorpicker-background').spectrum({
            color: '#000000',
            showInitial: true,
            showInput: true,
        });

        $('#colorpicker-text').spectrum({
            color: '#ffffff',
            showInitial: true,
            showInput: true,
        });
        $('#colorpicker_preview').css('color', '#ffffff');
        $('#colorpicker_preview').css('background-color', '#000000');

        $('#resource_login').val('');
        $('#resource_contact').val('');
        $('#resource_type').val('person');
    }

    renderTable(data) {
        if (RenderRessourceSettings.instance.resourceTable) {
            RenderRessourceSettings.instance.resourceTable.destroy()
        }
        data.forEach((element) => {
            element.colors = [];
            element.colors.push(element.props.eventBackgroundColor);
            element.colors.push(element.props.eventFontColor);
            element.login_id = element.props.login_id;
            element.contact_id = element.props.contact_id
        });

        RenderRessourceSettings.instance.resourceTable = $('#resource-table').DataTable({
            pageLength: 100,
            retrieve: true,
            data: data,
            columns: [
                {data: 'display_name'},
                {
                    data: 'login_id',
                    render: function (data) {
                        let logintmp = {}
                        if (data == '') {
                            return '-';
                        } else {
                            if (RenderRessourceSettings.instance.logins) {
                                logintmp = RenderRessourceSettings.instance.logins.find(element => element.userid == data)
                                let result = logintmp ? logintmp.username : "ID: " + data
                                return result
                            } else {
                                return '-';
                            }
                        }

                    },
                },
                {
                    data: 'type',
                    render: function (ressource_type) {
                        return new pbI18n().translate(ressource_type)
                    }
                },
                {
                    data: 'colors',
                    render: function (colors) {
                        return `<div class="cp-preview pd-y-10 text-center" style="background-color: ${colors[0]};color: ${colors[1]};">${colors[0]}</div>`;
                    },
                },
                {
                    data: 'ressource_id',
                    render: function (data) {
                        return `<div class="btn-icon-list"><a href="" data-toggle="modal" data-target="#modalResourceDetails"><button class="btn btn-indigo btn-icon mg-r-10" data-toggle="tooltip" data-original-title="bearbeiten" data-selector="btn-resource-edit" data-update-resource-id=${data}><i class="typcn typcn-edit mg-l-5"></i></button></a>
                        <button class="btn btn-outline-indigo btn-icon" data-toggle="tooltip" data-original-title="löschen" data-selector="btn-resource-delete" data-delete-resource-id="${data}"><i class="typcn typcn-trash"></i></button> </div>`;
                    },
                },
            ],
            responsive: true,
            pagingType: 'first_last_numbers',
            lengthChange: false,
            language: {
                searchPlaceholder: 'Suchen...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            },
        });


    }


}
