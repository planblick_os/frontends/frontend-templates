import {PbEasy2schedule} from './pb-easy2schedule.js?v=[cs_version]'
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbEvents} from "../../csf-lib/pbapi/calendar/pb-events.js?v=[cs_version]"
import {CONFIG} from "../../../../pb-config.js?v=[cs_version]"
import {fixDateRangeIfNeeded, sortObject} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbEventStatus} from "../../csf-lib/pbapi/calendar/pb-event-status.js?v=[cs_version]"
import {getCookie, setCookie, stringToHslColor, md5} from "../../csf-lib/pb-functions.js?v=[cs_version]"


export class RenderListView {
    constructor() {
        if (!RenderListView.instance) {
            RenderListView.instance = this
            RenderListView.instance.socket = new socketio()
            RenderListView.instance.notifications = new PbNotifications()
            RenderListView.instance.i18n = new pbI18n()
            RenderListView.instance.easy2schedule = new PbEasy2schedule()
            RenderListView.instance.starttime = moment().startOf('day')
            RenderListView.instance.endtime = moment(RenderListView.instance.starttime).add(1, 'weeks')
            RenderListView.instance.sort_order = "ascending"
            RenderListView.instance.appointments = []
            RenderListView.instance.eventModel = new PbEvents()
            RenderListView.instance.eventStatusModel = new PbEventStatus()
            RenderListView.instance.autoExpandTimeframeForMultiDayEvents = false
        }

        return RenderListView.instance
    }

    init() {
        let self = this
        return this.updateDataFromApi().then(() => {
            self.initListeners()
            self.setTimeFrame()
            self.enableUpdateIntervall()
        })
    }

    updateDataFromApi() {
        let self = this
        return RenderListView.instance.eventModel.loadFromApi(RenderListView.instance.starttime.format("YYYY-MM-DDTHH:mm"), RenderListView.instance.endtime.endOf('day').format("YYYY-MM-DDTHH:mm")).then(eventModel => {
            RenderListView.instance.eventStatusModel.load().then(
                RenderListView.instance.renderAppointmentList.bind(self))
        })
    }

    setTimeFrame() {
        $('#starttime').val(RenderListView.instance.starttime.format(CONFIG.DATEFORMAT))
        $('#endtime').val(RenderListView.instance.endtime.format(CONFIG.DATEFORMAT))
    }

    toggleSortOrder() {
        if (RenderListView.instance.sort_order === "ascending") {
            RenderListView.instance.sort_order = "descending"
        } else {
            RenderListView.instance.sort_order = "ascending"
        }
    }

    toggleSortIcons() {
        if ($('#sort-icon').hasClass('typcn-arrow-sorted-up')) {
            $('#sort-icon').removeClass('typcn-arrow-sorted-up').addClass('typcn-arrow-sorted-down')
        } else {
            $('#sort-icon').removeClass('typcn-arrow-sorted-down').addClass('typcn-arrow-sorted-up')
        }
    }

    enableUpdateIntervall() {
        setInterval(() => {
            RenderListView.instance.updateDataFromApi()
            console.log("Update")
        }, 60000 * 5)
    }

    renderAppointmentList(autoExpandTimeframeForMultiDayEvents = undefined, search= undefined) {
        if(autoExpandTimeframeForMultiDayEvents !== undefined) {
            RenderListView.instance.autoExpandTimeframeForMultiDayEvents = autoExpandTimeframeForMultiDayEvents
        }
        let events = {}
        const header_search_value = search || $("#header_search").val()
        let eventsList = this.eventModel.getEventList(true)
        function compare( a, b ) {
            if (a.start < b.start) {
                return -1;
            }
            if (a.start > b.start) {
                return 1;
            }
            return 0;
        }
        eventsList.sort(compare)
        
        for (let event of eventsList) {
            event.columnColor = stringToHslColor(md5(event.resourceId))
            if (header_search_value) {
                let tags_search = event.tags?.reduce((accumulated, current) => `${accumulated} ${current?.id} ${current?.text}`, "")
                let search_in = [event.title, event.extendedProps?.comment, tags_search].join(" ")
                if (!search_in.toLowerCase().includes(header_search_value.toLowerCase())) {
                    continue
                }
            }

            if (!RenderListView.instance.autoExpandTimeframeForMultiDayEvents &&
                (
                    moment(event.start, CONFIG.DB_DATEFORMAT).isBefore(RenderListView.instance.starttime) ||
                    moment(event.end, CONFIG.DB_DATEFORMAT).isAfter(RenderListView.instance.endtime)
                )
            ) {
                continue
            }

            let datekey = moment(event.start, CONFIG.DB_DATEFORMAT).format(CONFIG.DB_DATEFORMAT_NOTIME)
            events[datekey] ? "" : events[datekey] = []
            events[datekey].push(this.setGridValues(event))
        }
        events = sortObject(events, this.sort_order)

        let statusColors = RenderListView.instance.eventStatusModel.getStatusColors()

        PbTpl.instance.renderIntoAsync('easy2schedule/templates/cards.tpl', {events: events, statusColors: statusColors}, "#list-view-container").then(() => RenderListView.instance.createEventListeners())
    }

    initListeners() {
        $("#reload_button").click(() => {
            RenderListView.instance.updateDataFromApi()
        })
        $("#starttime").datetimepicker({
            format: CONFIG.DATEFORMAT,
            locale: 'de',
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false
        }).on('dp.change', function (e) {
            let startDate = moment($("#starttime").val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)
            let endDate = moment($("#endtime").val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)

            endDate = fixDateRangeIfNeeded(startDate, endDate, 7, "days", "end")
            $("#endtime").data("DateTimePicker")?.date(endDate)

            RenderListView.instance.starttime = moment($("#starttime").val(), CONFIG.DATEFORMAT).startOf("day")
            RenderListView.instance.updateDataFromApi()
            setCookie("easy2schedule_listview_starttime", $("#starttime").val(), 1)
        })

        if(getCookie("easy2schedule_listview_starttime")) {
            $("#starttime").data("DateTimePicker").date(moment(getCookie("easy2schedule_listview_starttime"), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT))
        }

        $("#endtime").datetimepicker({
            format: CONFIG.DATEFORMAT,
            locale: 'de',
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false
        }).on('dp.change', function (e) {
            let startDate = moment($("#starttime").val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)
            let endDate = moment($("#endtime").val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)

            startDate = fixDateRangeIfNeeded(startDate, endDate, -7, "days", "start")
            $("#starttime").data("DateTimePicker")?.date(startDate)

            RenderListView.instance.endtime = moment($("#endtime").val(), CONFIG.DATEFORMAT).endOf('day')
            RenderListView.instance.updateDataFromApi()
            setCookie("easy2schedule_listview_endtime", $("#endtime").val(), 1)
        })

        if(getCookie("easy2schedule_listview_endtime")) {
            $("#endtime").data("DateTimePicker").date(moment(getCookie("easy2schedule_listview_endtime"), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT))
        }


        $("#header_search").keyup(() => {
            RenderListView.instance.renderAppointmentList()
            setCookie("easy2schedule_listview_search", $("#header_search").val(), 1)
        })

        if(getCookie("easy2schedule_listview_search")) {
            $("#header_search").val(getCookie("easy2schedule_listview_search"))
            RenderListView.instance.renderAppointmentList(false, getCookie("easy2schedule_listview_search"))
        }
        RenderListView.instance.socket.commandhandlers["addEvent"] = function (args) {
            RenderListView.instance.updateDataFromApi()
        }

        RenderListView.instance.socket.commandhandlers["removeEvent"] = function (args) {
            RenderListView.instance.updateDataFromApi()
        }

        $("#page_menu_button_1").click(() => {
            window.location.href = "../calendar/"
        })

        // $('#starttime').on("change", function () {
        //     RenderListView.instance.starttime = $(this).val()
        //     console.log("ENDTIMES 1", RenderListView.instance.starttime)
        //     RenderListView.instance.updateDataFromApi()
        // })
        //
        // $('#endtime').on("change", function () {
        //     RenderListView.instance.endtime = $(this).val();
        //     console.log("ENDTIMES", RenderListView.instance.endtime)
        //     RenderListView.instance.updateDataFromApi()
        // })

        $('#order-button').click(() => {
            RenderListView.instance.toggleSortOrder()
            RenderListView.instance.toggleSortIcons()
            RenderListView.instance.renderAppointmentList()
        })

    }

    createEventListeners() {
        let self = this
        $("*[data-entry-title-selector]").click((element) => {
            let id = $(element.currentTarget).data('entryTitleSelector')
            if ($(element.currentTarget).text().trim() != "Anonymized") {
                window.location.href = '../calendar/?event_id=' + `${id}`
            }
        })

        $("[data-entry-quickaction-edit]").click((element) => {
            let id = $(element.currentTarget).data('entryQuickactionEdit')
            let title = $(element.currentTarget).data('entryQuickactionTitle')
            if (title.trim() != "Anonymized") {
                window.location.href = '../calendar/?event_id=' + `${id}`
            } else {
                self.notifications.showHint(self.i18n.translate("You cannot edit anonymous entries."), "warning")
            }
        })

        $("[data-entry-quickaction-delete]").click((element) => {
            let id = $(element.currentTarget).data('entryQuickactionDelete')
            let title = $(element.currentTarget).data('entryQuickactionTitle')
            if (title.trim() != "Anonymized") {
                self.notifications.ask(self.i18n.translate("Do you really want to delete this entry?"), self.i18n.translate("Delete entry"), () => {
                    self.eventModel.deleteEvent(id)
                })
            } else {
                self.notifications.showHint(self.i18n.translate("You cannot delete anonymous entries."), "warning")
            }
        })
    }

    setGridValues(event) {
        let start = event.start.substr(11, 5)
        let end = event.end.substr(11, 5)

        let colsBeforeMainTimeline = 4
        let colsMainTimeline = 57
        let colsAfterMainTimeline = 4
        let startMainTimeline = 8
        let endMainTimeline = 21

        let eventColStart, eventSpan;

        let startHour = parseInt(start.substring(0, 2));
        let startMin = parseInt(start.substring(3));
        let endHour = parseInt(end.substring(0, 2));
        let endMin = parseInt(end.substring(3));

        if (startHour < startMainTimeline) {
            eventColStart = 1;
            if (endHour < startMainTimeline) {
                eventSpan = colsBeforeMainTimeline;
            } else {
                // all end and start dates are set to the same day 2000-01-01 to remove differences in the calculations when transforming from milliseconds into 15 min blocks
                // calculate remaining span with hard start time, 08:00 being the beginning time on the main timeline
                eventSpan = colsBeforeMainTimeline + Math.ceil((new Date(`2000-01-01T${end}`).getTime() - new Date(`2000-01-01T08:00`).getTime()) / (1000 * 60 * 15));
            }
        } else if (startHour >= endMainTimeline) {
            eventColStart = colsMainTimeline;
            eventSpan = colsAfterMainTimeline;
        } else {
            eventColStart = (colsBeforeMainTimeline + 1) + (startHour - startMainTimeline) * 4 + Math.floor(startMin / 15);
            if (endHour >= endMainTimeline && endMin > 0) {
                eventSpan = colsMainTimeline - eventColStart + colsBeforeMainTimeline;
            } else {
                // all end and start dates are set to the same day 2000-01-01 to remove differences in the calculations when transforming from milliseconds into 15 min blocks
                // calculate complete span by round down starting time and round up the end time
                eventSpan = Math.ceil(new Date(`2000-01-01T${end}`).getTime() / (1000 * 60 * 15)) - Math.floor(new Date(`2000-01-01T${start}`).getTime() / (1000 * 60 * 15));
            }
        }
        event["eventColStart"] = eventColStart
        event["eventSpan"] = eventSpan
        return event
    }

    calculateGridValues(start, end) {

        let colsBeforeMainTimeline = 4
        let colsMainTimeline = 57
        let colsAfterMainTimeline = 4
        let startMainTimeline = 8
        let endMainTimeline = 21

        let eventColStart, eventSpan;

        let startHour = parseInt(start.substring(0, 2));
        let startMin = parseInt(start.substring(3));
        let endHour = parseInt(end.substring(0, 2));
        let endMin = parseInt(end.substring(3));

        if (startHour < startMainTimeline) {
            eventColStart = 1;
            if (endHour < startMainTimeline) {
                eventSpan = colsBeforeMainTimeline;
            } else {
                // all end and start dates are set to the same day 2000-01-01 to remove differences in the calculations when transforming from milliseconds into 15 min blocks
                // calculate remaining span with hard start time, 08:00 being the beginning time on the main timeline
                eventSpan = colsBeforeMainTimeline + Math.ceil((new Date(`2000-01-01T${end}`).getTime() - new Date(`2000-01-01T08:00`).getTime()) / (1000 * 60 * 15));
            }
        } else if (startHour >= endMainTimeline) {
            eventColStart = colsMainTimeline;
            eventSpan = colsAfterMainTimeline;
        } else {
            eventColStart = (colsBeforeMainTimeline + 1) + (startHour - startMainTimeline) * 4 + Math.floor(startMin / 15);
            if (endHour >= endMainTimeline && endMin > 0) {
                eventSpan = colsMainTimeline - eventColStart + colsBeforeMainTimeline;
            } else {
                // all end and start dates are set to the same day 2000-01-01 to remove differences in the calculations when transforming from milliseconds into 15 min blocks
                // calculate complete span by round down starting time and round up the end time
                eventSpan = Math.ceil(new Date(`2000-01-01T${end}`).getTime() / (1000 * 60 * 15)) - Math.floor(new Date(`2000-01-01T${start}`).getTime() / (1000 * 60 * 15));
            }
        }
        return {eventColStart, eventSpan};
    }

}