import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"

export class SysAdminModule {
    actions = {}

    constructor() {
        if (!SysAdminModule.instance) {
            this.initListeners()
            this.initActions()
            $.unblockUI()
            SysAdminModule.instance = this
            SysAdminModule.instance.tpl = new PbTpl()
            SysAdminModule.instance.acc = new PbAccount()

        }

        return SysAdminModule.instance
    }

    initActions() {
        let self = this
        this.actions["sysadminModuleInitialised"] = function (myevent, callback = () => "") {
            self.acc.init().then(() => {
                if (!self.acc.hasPermission("sysadmin")) {
                    self.acc.logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Render().init()
                    callback()
                }
            })
        }
    }

    initListeners() {
        $(document).on('click', '*[data-consumeroptionlink]', function (e) {
            e.preventDefault()

            new PbNotifications().blockForLoading()
            let parts = e.currentTarget.id.split("_")
            let consumer_id = parts.pop()
            let kong_consumer_id = parts.pop()
            if (getCookie("consumer_id") == consumer_id) {
                new PbNotifications().showAlert("You cannot delete yourself!")
                new PbNotifications().unblockAll()
                return false
            }
            if (e.currentTarget.dataset.consumeroptionlink == "clear") {
                var settings = {
                    "url": `${CONFIG.API_BASE_URL}/es/cmd/deleteConsumer`,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey"),
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify({
                        "data": {
                            "cleanup_consumer": consumer_id
                        }
                    }),
                };
                new PbNotifications().ask("Do you really want to clear ALL data of this consumer?", "Clearing Consumer-Data",
                    () => {
                        $.ajax(settings).done(function (response) {
                            window.setTimeout(() => {
                                new Render().init();
                                new PbNotifications().unblockAll()
                            }, 1000)
                        })
                    },
                    () => new PbNotifications().unblockAll()
                )


            } else if (e.currentTarget.dataset.consumeroptionlink == "delete") {
                var settings = {
                    "url": `${CONFIG.API_BASE_URL}/es/cmd/deleteConsumer`,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey"),
                        "Content-Type": "application/json"
                    },
                    "data": JSON.stringify({
                        "data": {
                            "cleanup_consumer": consumer_id,
                            "kong_consumer_id": kong_consumer_id
                        }
                    }),
                };
                new PbNotifications().ask("Do you really want to delete ALL data and this consumer?", "Deleting Consumer", () => {
                        $.ajax(settings).done(function (response) {
                            window.setTimeout(() => {
                                new Render().init();
                                new PbNotifications().unblockAll()
                            }, 1000)
                        })
                    },
                    () => new PbNotifications().unblockAll())
            }
        })

        document.addEventListener("notification", function _(event) {
            if (event.data.data.event == "loginCreationSuccessfull") {
                new Render().init();
            }
        })



    }
}