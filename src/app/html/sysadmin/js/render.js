
export class Render {
    constructor() {
        if (!Render.instance) {
            Render.instance = this
        }

        return Render.instance
    }

    init() {
        switch (window.location.pathname) {
            case "/sysadmin/consumers.html":
                import("./renderConsumerPage.js?v=[cs_version]").then((Module) => {
                    new Module.RenderConsumerPage().init()
                })
                break;
            case "/sysadmin/user_rights.html":
                import("./renderUserRightsPage.js?v=[cs_version]").then((Module) => {
                    new Module.RenderUserRightsPage().init()
                })
                break;
            default:
                import("./renderSysadminPage.js?v=[cs_version]").then((Module) => {
                    new Module.RenderSysadminPage().init()
                })
        }
    }
}
