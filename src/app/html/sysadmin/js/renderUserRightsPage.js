import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]";

export class RenderUserRightsPage {
    rules = {}

    constructor() {
        if (!RenderUserRightsPage.instance) {
            RenderUserRightsPage.instance = this
        }

        return RenderUserRightsPage.instance
    }

    init() {
        let urlParams = new URLSearchParams(window.location.search);
        let login = urlParams.get('u');
        let consumer_id = urlParams.get('cid');
        $("#rules_login").html(login)

        this.fetchRightsFromApi(login, consumer_id).then(function (data) {
            PbTpl.instance.renderIntoAsync('sysadmin/templates/rule_table.html', {"rules": this.rules}, "#board-portlet-container").then(function() {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }.bind(this))
        }.bind(this))
    }

    fetchRightsFromApi(login, consumer_id) {
        let url = `${CONFIG.API_BASE_URL}/get_rights/${login}/${consumer_id}`
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": url,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => {
                        this.rules = response
                        resolve(response)
                },
                "error": (e) => {console.log("E", e);alert("Oooops, something went wrong. Sorry! please try again.")}
            };
            $.ajax(settings)
        }.bind(this))
    }
}
