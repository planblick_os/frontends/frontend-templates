import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {
    getQueryParam,
    fireEvent,
    objectDiff,
    getCookie,
    setCookie,
    array_merge_distinct,
    multiStringReplace
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {ContactManager} from "../../contact-manager/js/contact-manager.js?v=[cs_version]"
import {PbDocuments, PbDocument} from "./documents.js?v=[cs_version]"
import {PbSelect2Tags} from "../../csf-lib/pb-select2-tags.js?v=[cs_version]"
import {PbEditor} from "../../csf-lib/pb-editor.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {pbEventEntries} from "../../easy2schedule/js/pb-event-entries.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class Render {
    constructor() {
        if (!Render.instance) {
            let nj = nunjucks.configure(CONFIG.APP_BASE_URL, {autoescape: false})
            nj.addFilter('date', (data) => {
                let createtime = moment(data)
                return createtime.format(CONFIG.DATETIMEFORMAT)
            })
            Render.instance = this
            Render.instance.displayProperties = getCookie("dcp") == "true"
            Render.instance.source_id = getQueryParam("sid")
            Render.instance.initViewListener()
            Render.instance.notifications = new PbNotifications()
            Render.instance.tagListModel = new PbTagsList()
            Render.instance.handledTaskIds = []
            Render.instance.activeDocument = undefined
            Render.instance.documentationModuleInstance = undefined

            document.addEventListener("pbTagsListChanged", function _(event) {
                if (!Render.instance.handledTaskIds.includes(event.data.args.task_id)) {
                    Render.instance.handledTaskIds.push(event.data.args.task_id)
                    Render.instance.renderTagList(event.data.args.tags)
                }
            })
        }

        return Render.instance
    }

    init(DocumentationModuleInstance) {
        Render.instance.documentationModuleInstance = DocumentationModuleInstance
        let is_configured = false
        let is_admin = true
        if (!is_configured) {
            if (is_admin) {
                new PbGuide("./tours/other_tour.js")
            } else {
                new PbGuide("./tours/test.js")
            }
        }

        let renderer = new Render(getQueryParam("apikey"), CONFIG, DocumentationModuleInstance.socket, DocumentationModuleInstance.docs)

        let contact = new ContactManager()
        let eventEntries = new pbEventEntries()
        let promises = [contact.init(), eventEntries.init()]
        Promise.all(promises).then(function () {
            Render.instance.contacts_list = contact.contacts_list

            let fetchFunction = DocumentationModuleInstance.docs.fetchDocumentsBySourceId
            if (!DocumentationModuleInstance.source_id) {
                fetchFunction = DocumentationModuleInstance.docs.fetchAllDocuments
            }

            fetchFunction(DocumentationModuleInstance.source_id).then((docs) => {
                let eventEntries = new pbEventEntries()
                let sorted_groups = eventEntries.possible_groups;
                sorted_groups.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);

                let sorted_members = eventEntries.group_members;
                sorted_members.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
                let groupsandmembers = sorted_groups.concat(sorted_members);

                Render.instance.groupsandmembers = groupsandmembers
                DocumentationModuleInstance.docs.filter($("#documents_search_input").val())
                Render.instance.renderDocumentList(docs)
                Render.instance.renderContactsList()

                let url = new URL(window.location.href);
                let params = new URLSearchParams(url.search);
                let current_document_id = params.get("did");
                if (current_document_id) {
                    Render.instance.renderDetailView(docs.getDocumentById(current_document_id), docs)
                }
            })
        })

        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }
    }

    renderDocumentList(DocumentsInstance) {
        Render.instance.documentsInstance = DocumentsInstance
        $("#document_list").html('')
        if (Render.instance.source_id) {
            let sourceObject = {
                'module': Render.instance.source_id.split('|')[0],
                'id': Render.instance.source_id.split('|')[2]
            }

            $('#source_module_pill').html(`<i class="fa fa-filter mr-1"> ${sourceObject.module}`)
            $('#source_id_pill').html(`<i class="fa fa-filter mr-1"> ${sourceObject.id}`)
        }

        PbTpl.instance.renderInto('documentation/templates/documents-list.tpl', {documents: DocumentsInstance}, "#document_list")

        let self = this
        $("[data-listentry]").click((e) => {
            $("[data-listentry]").removeClass("selected")
            $(e.currentTarget).addClass("selected")
            self.renderDetailView(DocumentsInstance.getDocumentById(e.currentTarget.dataset.listentry), DocumentsInstance)
        })

        $("#button-new-document, [data-id='button-new-document']").click(function () {
            self.renderDetailView({}, DocumentsInstance)
        })
    }

    renderAttachments() {
        PbTpl.instance.renderInto('documentation/templates/sub-attachments.tpl', {document: Render.instance.activeDocument}, "#attachments_container")
    }

    renderTagList(selectedTags = []) {
        Render.instance.notifications.blockForLoading("#tag_container")
        let self = this
        let tags = array_merge_distinct(selectedTags || [], $("#tags").val() || [])
        return new Promise((resolve, reject) => {
            Render.instance.tagListModel.load(false, "documentation").then(() => {
                let entries = Render.instance.documentsInstance.documentations
                for (let entry of entries) {
                    if (entry.data?.tags) {
                        for (let localTag of entry.data.tags) {
                            let myTag = new PbTag().fromShort(localTag)
                            Render.instance.tagListModel.addTagToList(myTag)
                        }
                    }
                }

                for (let tag of tags) {
                    if (!tag) {
                        continue
                    }
                    if (typeof tag == "string") {
                        tag = new PbTag().fromShort(tag).getData()
                    }

                    tag = new PbTag({"name": tag.id || tag.name, "value": tag.text || tag.value})
                    if (!selectedTags.includes(tag.short())) {
                        selectedTags.push(tag.short())
                    }
                    Render.instance.tagListModel.addTagToList(tag)
                }

                let tagOptions = Render.instance.tagListModel.getSelectOptions(selectedTags)

                PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {
                    "multiple": true,
                    "addButton": true,
                    "propertyPath": "tags",
                    "options": tagOptions
                }, "#tag_container").then(() => {
                    $('[data-propertyname="tags"]').select2({
                        tags: true,
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })
                    $("#tags").attr("disabled", false)
                    $("#addGlobalTagButton").click(() => {
                        PbNotifications.instance.ask(
                            '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                            'New global tag',
                            () => {
                                Render.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["documentation"]), true)
                            })
                        $("#newGlobalTag").focus()
                    })
                    Render.instance.notifications.unblock("#tag_container")
                    resolve()
                })
            })
        })
    }

    renderDetailView(selected_document = {}, DocumentsInstance = undefined, force_reload = false, skipAutosaveCheck = false) {
        let urlParams = new URLSearchParams(window.location.search);
        urlParams.set("did", selected_document.document_id);
        let newUrl = "".concat(window.location.pathname, "?").concat(urlParams.toString());
        history.pushState({}, null, newUrl);

        if (force_reload) {
            location.reload()
        }

        Render.instance.documentDirty = false
        selected_document = new PbDocument(structuredClone(selected_document))

        if (Render.instance.autosave_interval) {
            window.clearInterval(Render.instance.autosave_interval)
        }
        Render.instance.autosave_interval = window.setInterval(() => {
                if (Render.instance.activeDocument && Render.instance.documentDirty) {
                    Render.instance.updateDocument(Render.instance.activeDocument)
                    Render.instance.documentationModuleInstance.docs.autosave(Render.instance.activeDocument)
                }
            }
            , 1000)
        DocumentsInstance.getAutosavedDocumentById(selected_document.document_id).then(function (data) {
            let document = undefined
            if (data) {
                document = new PbDocument(data)
                if (skipAutosaveCheck == false) {
                    PbNotifications.instance.ask(
                        'An autosaved version was found and loaded. Do you want to keep or delete the Autosaved version?',
                        'Autosaved version found',
                        () => {

                        },
                        () => {
                            DocumentsInstance.deleteAutosave(document.document_id).then(function () {
                                Render.instance.documentDirty = false
                                window.clearInterval(Render.instance.autosave_interval)
                                Render.instance.renderDetailView(selected_document, DocumentsInstance)
                            }.bind(this))
                        },
                        "warning",
                        "Keep",
                        "Delete")
                } else {
                    DocumentsInstance.deleteAutosave(document.document_id).then(function () {
                        Render.instance.documentDirty = false
                        window.clearInterval(Render.instance.autosave_interval)
                        Render.instance.renderDetailView(selected_document, DocumentsInstance)
                    }.bind(this))
                }
            } else {
                document = selected_document
            }
            if (document.data) {
                Render.instance.activeDocument = document
                PbTpl.instance.renderInto('documentation/templates/detail-view.tpl', {document: document}, "#azDocBody")

                if (document.data.type) {
                    $('#documentation_type').val(document.data.type.toLowerCase())
                }

                let tags = []
                if (document.data && document.data.tags) {
                    tags = document.data.tags
                }

                let editor = new PbEditor(CONFIG.DOCUMENTATION.EDITOR, "#content", this.onEditorUploadStart, this.onEditorUploadFinished).then((pbeditor) => {
                    pbeditor.init().then((initialisedEditor) => {
                        Render.instance.editor = initialisedEditor
                        if (document.data && document.data.content)
                            Render.instance.editor.setContent(document.data.content)

                        Render.instance.renderTagList(tags).then(() => {
                            Render.instance.setDetailViewListener(document, DocumentsInstance)
                            Render.instance.renderDocumentDetailPills(document)
                        })
                        Render.instance.addContactsToContactList(document.document_id ? false : true)
                        $("#contacts").select2()
                        Render.instance.selectAttachedContacts(document)
                    })
                })

                $("[data-attachmentdeletelink]").click(function (e) {
                    e.preventDefault()
                    let link = $(e.currentTarget).data("attachmentdeletelink")
                    Render.instance.notifications.ask("Willst du die Datei wirklich löschen?", "Datei löschen", () => {
                        Render.instance.activeDocument.data.attachments = Render.instance.activeDocument.data.attachments.filter(item => item && item != link)

                        Render.instance.editor.deleteRemoteFile(link).then(() => {
                            // $("#save_document_button").click()
                            // window.setTimeout(() => Render.instance.renderDetailView(Render.instance.activeDocument, DocumentsInstance, true), 1000)
                        }).catch(() => {
                            Render.instance.notifications.unblock("body")
                        })
                    })
                })

            } else {
                let newDocument = new PbDocument()
                Render.instance.activeDocument = newDocument

                PbTpl.instance.renderInto('documentation/templates/detail-view.tpl', {document: newDocument}, "#azDocBody")
                new PbEditor(CONFIG.DOCUMENTATION.EDITOR, "#content", this.onEditorUploadStart, this.onEditorUploadFinished).then((pbeditor) => {
                    pbeditor.init().then((initialisedEditor) => {
                        Render.instance.editor = initialisedEditor
                        Render.instance.renderTagList().then(() => {
                            Render.instance.setDetailViewListener(newDocument, DocumentsInstance)
                        })
                        Render.instance.addContactsToContactList(true)
                    })
                })
            }

            Render.instance.addEntriesToAccessrightsDropdown()
            Render.instance.addEntriesToObserverDropdown()

            Render.instance.selectDataOwners(document)
            Render.instance.preselectObservers(document)
            $("#save_document_button").prop('disabled', true)
            $("#document_title_input").keyup(function () {
                $("#save_document_button").prop('disabled', false)
            })

            $(document).ready(function () {
                //$('#contacts').dropdown({placeholder: 'Kein Kontakt ausgewählt'})
                $('#permissions').dropdown({placeholder: 'Für alle sichtbar'})
                $('#observers').dropdown({placeholder: 'Keine'})
            })

            if (Render.instance.displayProperties) {
                $("#collapseDocumentProperties").collapse('show')
            } else {
                $("#collapseDocumentProperties").collapse('hide')
            }

        }.bind(this))
    }

    onEditorUploadStart() {
        Render.instance.notifications.blockForLoading("#detail_view_tpl")
    }

    onEditorUploadFinished(link = undefined) {
        link = JSON.parse(link)
        if (!Render.instance.activeDocument.data.attachments) {
            Render.instance.activeDocument.data.attachments = []
        }
        if (link["link"]) {
            Render.instance.activeDocument.data.attachments.push(link["link"])
        }
        Render.instance.renderAttachments()
        Render.instance.notifications.unblock("#detail_view_tpl")
    }

    renderContactsList() {
        //gather list of contacts by H1 configuration from ContactManager and return them as titles 
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
        }
        // reset contactTitles in case of rerendering if contacts are refreshed
        Render.instance.contactTitles = []
        let contactObject = {}
        for (let entry of Render.instance.contacts_list) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
            }
            contactObject = {
                contact_id: contactId,
                title: contactTitleString
            }
            Render.instance.contactTitles.push(contactObject)
        }

        let tmp = Render.instance.contactTitles;

        tmp.sort(function (a, b) {
                a = a.title.toLowerCase()
                b = b.title.toLowerCase()
                return a < b ? -1 : 1
            }
        );

        Render.instance.contacts_list_sorted = tmp;
        Render.instance.addContactsToContactList()
    }


    addContactsToContactList(preselectSidId = false) {
        Render.instance.resetContactsList()
        if (document.getElementById("contacts")) {
            let dropdown = $('#contacts')
            let option = document.createElement('option')
            option.text = new pbI18n().translate("No contact linked")
            option.value = 'default' //JSON.stringify(item)
            option.setAttribute('data-contact-id', '')
            dropdown.append(option)
            for (let item of Render.instance.contacts_list_sorted) {
                let option = document.createElement("option");
                option.text = item.title
                if (preselectSidId && getQueryParam("sid") && getQueryParam("sid").includes("contact-manager|contact|") && getQueryParam("sid")?.split("|").pop() == item.contact_id) {
                    option.selected = true
                }
                option.value = item.contact_id//JSON.stringify(item)
                option.setAttribute("data-contact-id", item.contact_id)
                dropdown.append(option)
            }
        }
    }

    resetContactsList() {
        var dropdown = $('#contacts');
        dropdown.find('option').remove()
    }

    selectAttachedContacts(docu) {
        let selectedIds = []
        if (docu.links && docu.links.length > 0) {
            for (let link of docu.links) {
                if (link.type == "contact") {
                    selectedIds.push(link.external_id)
                }
            }
            $('#contacts').val(selectedIds).trigger('change')
        }
    }

    addEntriesToAccessrightsDropdown = function () {
        if ($("#permissions")) {
            let dropdown = document.getElementById("permissions");
            for (let item of Render.instance.groupsandmembers) {
                let option = document.createElement("option");
                option.text = item.replace("group.", new pbI18n().translate("Group") + ": ").replace("user.", new pbI18n().translate("User") + ": ")
                option.value = item
                option.setAttribute("data-accessrightid", item)
                dropdown.append(option);
            }
        }
    }

    addEntriesToObserverDropdown = function () {
        if ($("#observers")) {
            let dropdown = document.getElementById("observers");
            for (let item of Render.instance.groupsandmembers) {
                if (!item || item.includes("group.")) {
                    continue
                }
                let option = document.createElement("option");
                option.text = item.replace("user.", "")
                option.value = item.replace("user.", "")
                dropdown.append(option);
            }
        }
    }

    preselectObservers(docu) {
        if (!docu.document_id) {
            for (let option of document.getElementById("observers")) {
                option.selected = false
            }
        } else {
            if (docu.data?.observers && docu.data?.observers.length > 0) {
                for (let option of document.getElementById("observers")) {
                    docu.data?.observers.forEach(item => {
                        if (item == option.value) {
                            option.selected = true
                        }
                    })
                }
            }
        }
    }

    selectDataOwners(docu) {
        if (!docu.document_id) {
            for (let option of document.getElementById("permissions")) {
                if (getCookie("data_owners")) {
                    let owners = JSON.parse(decodeURIComponent(getCookie("data_owners")))
                    for (let owner of owners) {
                        if (option.value == owner) {
                            option.selected = true
                        }
                    }
                } else {
                    if (option.value.startsWith("group.")) {
                        option.selected = true
                        break;
                    }
                }
            }
        } else {
            if (docu.data_owners && docu.data_owners.length > 0) {
                for (let option of document.getElementById("permissions")) {
                    docu.data_owners.forEach(item => {
                        if (item == option.value) {
                            option.selected = true
                        }
                    })
                }
            }
        }
    }

    setDetailViewListener(docu, DocumentsInstance) {
        //TODO: teach editor class to accept callback-function for change since
        // this version only works with quill, but wouldn't with future froala
        let socket = new socketio()
        Render.instance.editor.onTextChanged(function (delta, old, source) {
            Render.instance.documentDirty = true
            if (!delta && !old && !source) {
                //Froala is the editor here
                // Update the internal Content on user-input
                docu.setContent(Render.instance.editor.getContent())
            } else {
                // Source can be "api" as well, but in this case we do nothing
                if (source == 'user') {
                    // Update the internal Content on user-input
                    docu.setContent(Render.instance.editor.getContent())
                }
            }
        })

        $('#observer_icon').unbind('click')
        $("#observer_icon").on('click', event => {
            alert($("#observer_icon").data("observeid"));
            alert(getCookie("username"))
        })
        $("*[data-key]").on('change', event => {
            Render.instance.documentDirty = true
            let target = $(event.target)
            let key = target.attr('data-key')
            let value = target.val()

            docu.setDataProperty(key, value)
        })

        $("select").on('change', event => {
            Render.instance.documentDirty = true
        })

        $('#save_document_button').unbind('click')
        $("#save_document_button").on('click', event => {
            let links = []

            if (document.getElementById("contacts"))
                for (let attachedContact of document.getElementById("contacts").selectedOptions) {
                    let id = attachedContact.getAttribute("data-contact-id")
                    let link_object = {
                        "resolver": "contact-manager",
                        "type": "contact",
                        "external_id": id
                    }
                    links.push(link_object)
                }

            let data_owners = []

            if (document.getElementById("permissions")) {
                for (let attachedAccessright of document.getElementById("permissions").selectedOptions) {
                    data_owners.push(attachedAccessright.getAttribute("data-accessrightid"))
                }
            }
            let observers = []
            if (document.getElementById("observers")) {
                for (let selected_observers of document.getElementById("observers").selectedOptions) {
                    observers.push(selected_observers.value)
                }
            }

            if (docu.document_id == "") {
                setCookie("data_owners", JSON.stringify(data_owners), {expires: 3650, path: '/'})
            }
            let tags = []
            let options = document.getElementById("tags").selectedOptions
            if (document.getElementById("tags"))
                for (let tag of document.getElementById("tags").selectedOptions) {
                    tags.push(tag.text)
                }


            if (docu.data.content) {
                docu.setSourceId(Render.instance.source_id)
                docu.setTags(tags)
                docu.setLinks(links)
                docu.setDataOwners(data_owners)
                docu.setObservers(observers)
                $('#save_icon').hide()
                $('#save_spinner').show()
                $("#save_document_button").off("click")
                DocumentsInstance.save(docu)
                Render.instance.documentDirty = false
            } else {
                Fnon.Alert.Danger({
                    title: new pbI18n().translate('Fehlender Dokumentationstext'),
                    message: new pbI18n().translate('Trage einen Wert in das Textfeld ein, um deine Dokumentation speichern zu können.'),
                    callback: () => {
                    }
                });
            }

        })

        $('#delete_document_button').unbind('click')
        $("#delete_document_button").on('click', event => {
            let deleteTitle = new pbI18n().translate("Löschen")
            let deleteQuestion = new pbI18n().translate("Möchten Sie das Dokument wirklich löschen?")

            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    DocumentsInstance.delete(docu).then(() => {
                        Render.instance.documentDirty = false
                        Render.instance.renderDetailView({}, DocumentsInstance)
                    })
                }
            })
        })

        $('#archive_document_button').unbind('click')
        $("#archive_document_button").on('click', function (e) {
            let archiveTitle = new pbI18n().translate("Archivieren")
            let archiveQuestion = new pbI18n().translate("Möchten Sie das Dokument wirklich archivieren?")

            Fnon.Ask.Danger(archiveQuestion, archiveTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    DocumentsInstance.archive(docu).then(() => {
                        Render.instance.documentDirty = false
                        Render.instance.renderDetailView({}, DocumentsInstance)
                    })
                }
            })

        })

        $('#advancedOptions').on('mousedown', function (ev) {
            if (ev.target.nodeName == 'A') {
                for (let c of Render.instance.contacts_list) {
                    if (c.contact_id == ev.target.dataset.value) {
                        window.open(
                            '/contact-manager/?ci=' + ev.target.dataset.value,
                            '_blank'
                        );
                        break;
                    }
                }
            }
        })

        $(document).on("showHistoryData", (data) => {
            new Render().showEntryHistory(data)
        })

        $(document).on("updatePBHistory", args => {
            let payload = args.originalEvent.data
            $('#save_icon').show()
            $('#save_spinner').hide()
            if ($('#history').children().attr('data-selector') == 'on') {
                let doc = DocumentsInstance.getDocumentById(args.document_id)
                if (docu.document_id == payload.document_id) {
                    docu.fetchDocumentHistoryFromApi(CONFIG.API_BASE_URL, getCookie('apikey')).then((response) => {
                        fireEvent("showHistoryData", response)
                    })
                }
            }
        })

        $(document).on("newPBHistory", args => {
            $('#save_icon').show()
            $('#save_spinner').hide()
        })
    }

    updateDocument(docu) {
        let links = []

        if (document.getElementById("contacts"))
            for (let attachedContact of document.getElementById("contacts").selectedOptions) {
                let id = attachedContact.getAttribute("data-contact-id")
                let link_object = {
                    "resolver": "contact-manager",
                    "type": "contact",
                    "external_id": id
                }
                links.push(link_object)
            }

        let data_owners = []

        if (document.getElementById("permissions")) {
            for (let attachedAccessright of document.getElementById("permissions").selectedOptions) {
                data_owners.push(attachedAccessright.getAttribute("data-accessrightid"))
            }
        }

        let observers = []
        if (document.getElementById("observers")) {
            for (let selected_observers of document.getElementById("observers").selectedOptions) {
                observers.push(selected_observers.value)
            }
        }

        let tags = []
        let options = document.getElementById("tags")?.selectedOptions
        if (options) {
            for (let tag of options) {
                tags.push(tag.text)
            }
        }

        docu.setSourceId(Render.instance.source_id)
        docu.setTags(tags)
        docu.setLinks(links)
        docu.setDataOwners(data_owners)
        docu.setObservers(observers)
        Render.instance.documentationModuleInstance.docs.autosave(docu)

    }

    renderDocumentDetailPills(document) {
        $("#history-list-container").html("")
        let params = {
            "headline": pbI18n.instance.translate("Activity"),
            "pills": [
                {
                    id: "history",
                    name: pbI18n.instance.translate("History"),
                    icon: "fa fa-history",
                    permission: "frontend.documentation",
                    toggle: "true",
                    onclick: function () {
                        if ($("#history_container").is(":visible")) {
                            $("#history_container").hide()
                        } else {
                            Fnon.Wait.Circle('', {
                                svgSize: {w: '50px', h: '50px'},
                                svgColor: '#3a22fa',
                            })
                            $(document).on("showHistoryData", (data) => {
                                new Render().showEntryHistory(data)
                            })
                            //TODO create new function to get data for docs
                            document.fetchDocumentHistoryFromApi(CONFIG.API_BASE_URL, getCookie('apikey')).then((response) => {
                                fireEvent("showHistoryData", response)
                            })
                            return false
                        }
                    }
                }
            ]
        }

        PbTpl.instance.renderInto('snippets/pills.tpl', params, "#interactions")

        for (let param of params.pills) {
            $(`#${param.id}`).click(param.onclick)
        }
    }

    showEntryHistory(data) {
        if (data.originalEvent.data.history && Array.isArray(data.originalEvent.data.history)) {
            $("#history_container").show()
            $("#history-list-container").html("")
            let previous_data = {}
            let newHistoryItem = $("#history-item").html()

            for (let {payload, type} of data.originalEvent.data.history) {
                Render.instance.currentlyShownHistoryId = payload.contact_id
                let {creator, create_time} = payload

                let localized_create_time = moment.utc(create_time).local().format();
                let local_create_time = new Date(localized_create_time).toLocaleString()

                let historyDetail = new pbI18n().translate("No details available")
                if (payload != null && previous_data != null) {
                    historyDetail = ""
                    let old_values = objectDiff(previous_data, payload, [previous_data.correlation_id, previous_data.create_time])
                    let new_values = objectDiff(payload, previous_data, [payload.correlation_id, payload.create_time])
                    if (old_values.data == undefined) {
                        old_values.data = {}
                    }
                    if (new_values.data == undefined) {
                        new_values.data = {}
                    }
                    let intersect_keys = array_merge_distinct(Object.keys(old_values.data), Object.keys(new_values.data))
                    for (let key of intersect_keys) {
                        if (key == "order") {
                            continue
                        }
                        let key_translated = new pbI18n().translate(key)
                        let old_value = old_values.data[key] ? old_values.data[key] : ""
                        let new_value = new_values.data[key] ? new_values.data[key] : false
                        if (new_value !== false && old_value != new_value) {
                            historyDetail += `<code><xmp> ${key_translated} : --> ${JSON.stringify(new_value)} </xmp></code>`
                        }
                    }
                }

                let details = {
                    'history-creator': creator,
                    'history-time': local_create_time,
                    'history-type': new pbI18n().translate(type),
                    'history-detail': historyDetail
                }

                previous_data = payload

                let historyItemContent = multiStringReplace(details, newHistoryItem)
                $("#history-list-container").prepend(historyItemContent)
            }
            Fnon.Wait.Remove(500)
        }
    }

    initViewListener() {
        let self = this
        document.addEventListener('froala.remoteFileDeleted', function (event) {
            console.log('Custom event remoteFileDeleted received:', event.detail.url);
            Render.instance.activeDocument.data.attachments = Render.instance.activeDocument.data.attachments.filter(item => item && item != event.detail.url)
            Render.instance.renderAttachments()
            //Render.instance.renderDetailView(Render.instance.activeDocument, Render.instance.documentsInstance)
            Render.instance.notifications.unblockAll("body")
        });

        $(document).on('click', '#documentPropertiesToggle', function (e) {
            self.displayProperties = !$("#documentPropertiesToggle").hasClass("collapsed")
            setCookie("dcp", self.displayProperties)
        })
    }
}
