import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {PbDocuments} from "./documents.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {RenderCallcenter} from "./renderCallcenter.js?v=[cs_version]"


export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init(DocumentationModuleInstance) {
        switch (window.location.pathname) {
            case "/documentation/":
            case "/documentation/index.html":
                DocumentationModuleInstance.docs = new PbDocuments(getCookie("apikey"), CONFIG, DocumentationModuleInstance.socket, (args) => alert("data ready"), DocumentationModuleInstance.onBackendUpdate, (args) => {
                    let fetchFunction = DocumentationModuleInstance.docs.fetchDocumentsBySourceId
                    if (!DocumentationModuleInstance.source_id) {
                        fetchFunction = DocumentationModuleInstance.docs.fetchAllDocuments
                    }
                    fetchFunction(DocumentationModuleInstance.source_id).then((DocumentsInstance) => {
                        Fnon.Hint.Success(new pbI18n().translate("Documentation has been updated"), {displayDuration: 2000})
                        Fnon.Wait.Remove(500)
                        DocumentationModuleInstance.docs.filter($("#documents_search_input").val())

                        let doc = DocumentsInstance.getDocumentById(args.document_id)
                        if (Render.instance) {
                            Render.instance.renderDocumentList(DocumentsInstance)
                            Render.instance.renderContactsList()
                            Render.instance.renderDetailView(doc, DocumentsInstance)
                        }
                        if (RenderCallcenter.instance) {
                            RenderCallcenter.instance.renderDocumentList(DocumentsInstance)
                            RenderCallcenter.instance.renderContactsList()
                            RenderCallcenter.instance.renderDetailView(doc, DocumentsInstance)
                        }
                    })
                })
                import("./render.js?v=[cs_version]").then((Module) => {
                    new PbGuide("./tours/user_getting_started.js")
                    new Module.Render(Controller.instance.socket).init(DocumentationModuleInstance)
                })
                break;
            case "/documentation/callcenter.html":
                DocumentationModuleInstance.docs = new PbDocuments(getCookie("apikey"), CONFIG, DocumentationModuleInstance.socket, (args) => alert("data ready"), DocumentationModuleInstance.onBackendUpdate, (args) => {
                    let fetchFunction = DocumentationModuleInstance.docs.fetchDocumentsBySourceId
                    if (!DocumentationModuleInstance.source_id) {
                        fetchFunction = DocumentationModuleInstance.docs.fetchAllDocuments
                    }
                    fetchFunction(DocumentationModuleInstance.source_id).then((DocumentsInstance) => {
                        Fnon.Hint.Success(new pbI18n().translate("Documentation has been updated"), {displayDuration: 2000})
                        Fnon.Wait.Remove(500)
                        DocumentationModuleInstance.docs.filter($("#documents_search_input").val())

                        let doc = DocumentsInstance.getDocumentById(args.document_id)
                        if (Render.instance) {
                            Render.instance.renderDocumentList(DocumentsInstance)
                            Render.instance.renderContactsList()
                            Render.instance.renderDetailView(doc, DocumentsInstance)
                        }
                        if (RenderCallcenter.instance) {
                            RenderCallcenter.instance.renderDocumentList(DocumentsInstance)
                            RenderCallcenter.instance.renderContactsList()
                            RenderCallcenter.instance.renderDetailView(doc, DocumentsInstance)
                        }
                    })
                })
                import("./renderCallcenter.js?v=[cs_version]").then((Module) => {
                    new Module.RenderCallcenter(Controller.instance.socket, DocumentationModuleInstance).init()
                })
                break;
            case "/documentation/archived.html":
                import("./render-archived.js?v=[cs_version]").then((Module) => {
                    DocumentationModuleInstance.docs = new PbDocuments(getCookie("apikey"), CONFIG, DocumentationModuleInstance.socket, (args) => alert("data ready"), DocumentationModuleInstance.onBackendUpdate, (args) => {
                        let fetchFunction = DocumentationModuleInstance.docs.fetchAllDocuments

                        fetchFunction(false, true).then((DocumentsInstance) => {
                            Fnon.Hint.Success(new pbI18n().translate("Documentation has been updated"), {displayDuration: 2000})
                            Fnon.Wait.Remove(500)
                            DocumentationModuleInstance.docs.filter($("#documents_search_input").val())

                            let doc = DocumentsInstance.getDocumentById(args.document_id)
                            // TODO: Check back whether this still makes sense
                            Module.RenderArchived.instance.renderDocumentList(DocumentsInstance)
                        })
                    })
                    new Module.RenderArchived(Controller.instance.socket, DocumentationModuleInstance).init()
                })
                break;
            default:
                import("./render.js?v=[cs_version]").then((Module) => {
                    new Module.Render(Controller.instance.socket).init(DocumentationModuleInstance)
                })
                break;
        }
    }
}

