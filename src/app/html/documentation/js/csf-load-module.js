
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {
    getQueryParam, getCookie, fireEvent
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {ContactManager} from "../../contact-manager/js/contact-manager.js?v=[cs_version]"
import {pbEventEntries} from "../../easy2schedule/js/pb-event-entries.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]"
import {RenderCallcenter} from "./renderCallcenter.js?v=[cs_version]"
import {RenderArchived} from "./render-archived.js?v=[cs_version]"
import {PbTaskHandler} from "../../csf-lib/pb-taskhandler.js?v=[cs_version]"


export class DocumentationModule {
    actions = {}
    config = []

    constructor() {
        if (!DocumentationModule.instance) {
            DocumentationModule.instance = this
            DocumentationModule.instance.taskhandler = new PbTaskHandler()
            DocumentationModule.instance.source_id = getQueryParam("sid")
            DocumentationModule.instance.socket = new socketio()

            DocumentationModule.instance.initListeners()
            DocumentationModule.instance.initActions()
            DocumentationModule.instance.acc = new PbAccount()
            $.unblockUI()
        }

        return DocumentationModule.instance
    }

    onBackendUpdate(args) {
        let fetchFunction = DocumentationModule.instance.docs.fetchDocumentsBySourceId
        if (!DocumentationModule.instance.source_id) {
            fetchFunction = DocumentationModule.instance.docs.fetchAllDocuments
        }
        fetchFunction(DocumentationModule.instance.source_id).then((DocumentsInstance) => {
            let renderInstance = Render.instance || RenderCallcenter.instance || RenderArchived.instance
            if (renderInstance.activeDocument != undefined && args.document_id == renderInstance.activeDocument.document_id && renderInstance.documentDirty) {
                renderInstance.notifications.ask("Der von dir geänderte Datensatz wurde gerade von einem anderen Benutzer geändert. Möchtest du den Datensatz aktualisieren. Achtung: Du verlierst dabei deine Änderungen!", "Datensatz wurde geändert", () => {
                    let doc = DocumentsInstance.getDocumentById(args.document_id)
                    if (Render.instance) {
                        Render.instance.renderDocumentList(DocumentsInstance)
                        Render.instance.renderContactsList()
                        Render.instance.renderDetailView(doc, DocumentsInstance, false, true)
                    }
                    if (RenderCallcenter.instance) {
                        RenderCallcenter.instance.renderDocumentList(DocumentsInstance)
                        RenderCallcenter.instance.renderContactsList()
                        RenderCallcenter.instance.renderDetailView(doc, DocumentsInstance, true)
                    }
                })
            } else if (renderInstance.activeDocument != undefined && args.document_id == renderInstance.activeDocument.document_id) {

                let doc = DocumentsInstance.getDocumentById(args.document_id)
                if (Render.instance) {
                    Render.instance.renderDocumentList(DocumentsInstance)
                    Render.instance.renderContactsList()
                    Render.instance.renderDetailView(doc, DocumentsInstance, false, true)
                }
                if (RenderCallcenter.instance) {
                    RenderCallcenter.instance.renderDocumentList(DocumentsInstance)
                    RenderCallcenter.instance.renderContactsList()
                    RenderCallcenter.instance.renderDetailView(doc, DocumentsInstance, true)
                }
            } else {
                let doc = DocumentsInstance.getDocumentById(args.document_id)
                if (Render.instance) {
                    Render.instance.renderDocumentList(DocumentsInstance)
                    Render.instance.renderContactsList()
                }
                if (RenderCallcenter.instance) {
                    RenderCallcenter.instance.renderDocumentList(DocumentsInstance)
                    RenderCallcenter.instance.renderContactsList()
                }
            }
        })

    }

    initActions() {
        let self = this
        this.actions["documentationModuleInitialised"] = function (myevent, callback = () => "") {
            self.acc.init().then(() => {
                if (!self.acc.hasPermission("frontend.documentation.editor")) {
                    self.acc.logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(DocumentationModule.instance.socket).init(DocumentationModule.instance)
                    callback()
                }
            })
        }

        this.actions["documentManagerListArchivedView"] = function (myevent, callback = () => "") {
            self.acc.init().then(() => {
                if (!self.acc.hasPermission("frontend.documentation.views.archive")) {
                    self.acc.logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(DocumentationModule.instance.socket).init(DocumentationModule.instance)
                    callback()
                }
            })
        }
    }

    initListeners() {
        $(document).on('click', '.az-contact-item, #button-new-document, [data-id="button-new-document"]', function (e) {
            $('body').addClass('az-content-body-show')
        })

        $('#documents_search_input').keyup(function() {
            DocumentationModule.instance.docs.filter($("#documents_search_input").val())
            Render.instance.renderDocumentList(DocumentationModule.instance.docs)
        })

        $('#search_input_archive').keyup(function() {
           DocumentationModule.instance.docs.filter($("#search_input_archive").val())
            RenderArchived.instance.renderDocumentList(DocumentationModule.instance.docs)
        })
    }
}
