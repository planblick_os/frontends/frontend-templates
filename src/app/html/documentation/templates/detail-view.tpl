<div id="detail_view_tpl">
    <!-- document view -->
    <div id="contactInfoHeader" class="az-contact-info-header">
        <div id="contactInfoHeaderContainer" class="contact-info-header">
            <!-- document title field to go here later -->
            <div id="contactInfoHeaderDetail" class="mr-2 edit-input-jumbo">
                <input placeholder="Enter a title for your document"  id="document_title_input" data-key="title"class="form-control input-borderless edit-input-jumbo edit-input-input " value="{{document.data.title}}">

            </div>
            <!--i id="observer_icon" data-observeid="{{document.document_id}}" class="far fa-eye tx-dark-blue tx-16 observer-icon cursor-finger" title="Beobachten"></i-->
        </div>
        <div class="az-contact-action">
            <a id="archive_document_button" data-permission='frontend.documentation.editor.archive' href="#" data-archiveid="{{document.document_id}}" class="hidden"><i class="typcn typcn-archive"></i><span data-i18n="Archive"></span></a>
            <a id="delete_document_button" href="#" data-deleteid="{{document.document_id}}"><i class="typcn typcn-trash"></i> <span data-i18n="delete"></span></a>
            <a id="save_document_button" href="#" data-documentid="{{document.document_id}}" class="nav-link"><i id="save_icon" class="typcn typcn-tick-outline"></i><span id="save_spinner" class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true" style="display: none"></span><span data-i18n="save"></span></a>
        </div><!-- az-contact-action -->
    </div><!-- az-contact-info-header -->

    <div id="docInfoBody" class="az-contact-info-body">
        <div id="documentPropertiesWrapper" class="accordion mt-4 mr-2">
            <div class="card options">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a id="documentPropertiesToggle" class="collapsed" data-toggle="collapse" data-target="#collapseDocumentProperties" aria-expanded="false" aria-controls="collapseDocumentProperties" data-i18n='advanced options'> Dokumenteigenschaften </a>
                    </h5>
                </div>
                <div id="collapseDocumentProperties" class="collapse" aria-labelledby="headingOne" data-parent="#documentPropertiesWrapper">
                    <div class="card-body p-2">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <!-- source links -->
                                <div class="px-3 bg-light mt-0">
                                    <h6 class="pt-3 pb-2 mb-2 border-bottom text-dark">
                                        <span class="fa fa-tag"></span>
                                        Merkmal
                                    </h6>
                                    <div class="row align-items-center">
<!--                                        <div class="col-3">-->
<!--                                            <label class="form-control-label pt-2">Quelle:</label>-->
<!--                                        </div>-->
<!--                                        <div class="col-9">-->
<!--                                            <span class="text-dark small" id="source">{{document.data.source_id}}</span>-->
<!--                                        </div>-->
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-3">
                                            <label class="form-control-label pt-2">Kontakte:</label>
                                        </div>
                                        <div class="col-9">
                                            <select name="contacts" id="contacts" class="ui fluid search dropdown" multiple></select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-3">
                                            <label class="form-control-label pt-0 mt-0">Anhänge:</label>
                                        </div>
                                        <div class="col-9" id="attachments_container">
                                            {% include "documentation/templates/sub-attachments.tpl" %}
<!--                                            <ul class="list-unstyled">-->
<!--                                                {% for attachment in document.data.attachments %}-->
<!--                                                {% if attachment %}-->
<!--                                                {% set parts = attachment.split('/') %}-->
<!--                                                    <li class="list">-->
<!--                                                        <a class="attachment_delete_link" href="#" data-attachmentdeletelink="{{attachment}}" target="_blank"><i class="typcn typcn-trash"></i></a>-->
<!--                                                        <a class="attachment_link" href="{{attachment}}" target="_blank">{{parts[parts.length-1]}}</a>-->
<!--                                                    </li>-->
<!--                                                {% endif %}-->
<!--                                                {% endfor %}-->
<!--                                            </ul>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <!-- extra details -->
                                <div class="px-3 bg-light mt-0 hidden" data-permission="frontend.documentation.editor.activity">
                                    <h6 class="pt-3 pb-2 mb-2 border-bottom text-dark">
                                        <span class="fa fa-chart-pie"></span>
                                        <span data-i18n="Erweiterte Details">Erweiterte Details</span>
                                    </h6>
                                    <div class="form-group" id="extended_props_wrapper">
                                        <div class="row align-items-center">
                                            <div class="col-3">
                                                <label for="ep_date">Aufgewendete Zeit:</label>
                                            </div>
                                            <div class="col-9">
                                                <input data-key="time_spent" type="number" class="form-control" id="time" value={{document.data.time_spent}} />
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-3">
                                                <label for="ep_termintyp">Tätigkeit:</label>
                                            </div>
                                            <div class="col-9">
                                                <select  data-key="type" id="documentation_type" name="documentation_type" class="form-control">
                                                    <option value="beratung">Beratung</option>
                                                    <option value="telefonat">Telefonat</option>
                                                    <option value="allgemein">Allgemein</option>
                                                    <option value="email">E-Mail</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row align-items-center pt-1">
                                            <div class="col-3">
                                                <label for="ep_tags">Tags:</label>
                                            </div>
                                            <div class="col-9 d-flex align-items-center" id="tag_container">

                                            </div>
                                        </div>
                                        <div class="row align-items-center pt-1">
                                            <div class="col-3">
                                                <label data-i18n="Observers" id="observer_label" for="observers"> :</label>
                                            </div>
                                            <div class="col-9">
                                                <select name="observers" id="observers" class="ui fluid search dropdown" multiple></select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- permissions -->
                                <div class="px-3 bg-light mt-3">
                                    <h6 class="pt-3 pb-2 mb-2 border-bottom text-dark">
                                        <span class="fa fa-lock"></span>
                                        Berechtigungen
                                    </h6>
                                    <div class="row align-items-center">
                                        <div class="col-3">
                                            <label data-i18n="Visible for" id="permissions_label" for="visible_for">Sichtbar für :</label>
                                        </div>
                                        <div class="col-9">
                                        <select name="permissions" id="permissions" class="ui fluid search dropdown" multiple></select> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> <!-- row -->

                    </div>
                </div>
            </div>
        </div>

        <div id="contactInfoBodyContainer" class="media-list">
            <div id="contactInfoBodyDetail" class="media pr-2">
                <!-- editor input area -->
                <div class="ql-wrapper ql-wrapper-100">
                    <div data-key="content" id="content">
                    </div>
                </div>
            </div><!-- media -->
        </div><!-- media-list -->

        <div id="interactions" class="pl-lg-3 pl-2 pb-2">
        </div>
        <div id="history_container" class="container pl-3 pr-3 pt-0" style="display:none;">
            <div class="w-100 pr-3">
                <ul class="timeline" id="history-list-container">
                    <!-- history list generated from snippet here -->
                </ul>
            </div>
        </div>

    </div><!-- az-contact-info-body -->

</div>
