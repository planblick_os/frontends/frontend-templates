import {socketio} from "/csf-lib/pb-socketio.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]"

export class PharmacyPrototypModule {
    actions = {}
    config = []

    constructor() {
        if (!PharmacyPrototypModule.instance) {
            PharmacyPrototypModule.instance = this
            PharmacyPrototypModule.instance.socket = new socketio()
            PharmacyPrototypModule.instance.initListeners()
            PharmacyPrototypModule.instance.initActions()
        }

        return PharmacyPrototypModule.instance
    }

    initActions() {
        let self = this
        this.actions["pharmacyPrototypModuleInitialised"] = function (myevent, callback = () => "") {
            new Controller(PharmacyPrototypModule.instance.socket).init(PharmacyPrototypModule.instance)
            callback()
        }
    }

    initListeners() {
        $(document).on('click', '.az-contact-item, #button-new-document, [data-id="button-new-document"]', function () {
            $('body').addClass('az-content-body-show')
        })
    }
}
