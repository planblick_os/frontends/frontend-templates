(window["webpackJsonpDistPbs"] = window["webpackJsonpDistPbs"] || []).push([[2],{

/***/ "./app/src/did.js?v=[cs_version]":
/*!***************************************!*\
  !*** ./app/src/did.js?v=[cs_version] ***!
  \***************************************/
/*! exports provided: Did */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Did", function() { return Did; });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw new Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw new Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

var Did = /*#__PURE__*/function (_getTalksList, _renderForm, _testURL, _startVideoGeneration, _renderTalksList) {
  function Did(options) {
    _classCallCheck(this, Did);
    this.apiurl = options.apiurl;
    this.tpl = options.tpl;
    this.targetSelector = options.targetSelector;
    this.formSelector = options.form_selector;
    this.talksList = [];
    this.talksListLength = this.talksList.length;
    this.renderForm();
  }
  _createClass(Did, [{
    key: "getCookie",
    value: function getCookie(cookieName) {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        if (cookie.startsWith(cookieName + '=')) {
          return cookie.substring(cookieName.length + 1);
        }
      }
      return null; // Cookie not found
    }
  }, {
    key: "setCookie",
    value: function setCookie(cookieName, cookieValue, expirationDays) {
      var date = new Date();
      date.setTime(date.getTime() + expirationDays * 24 * 60 * 60 * 1000); // Set expiration time

      var expires = "expires=" + date.toUTCString();
      document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
    }
  }, {
    key: "getTalksList",
    value: function getTalksList() {
      return (_getTalksList = _getTalksList || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var forceRefresh,
          _args = arguments;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              forceRefresh = _args.length > 0 && _args[0] !== undefined ? _args[0] : false;
              if (this.getCookie("did_apikey")) {
                _context.next = 3;
                break;
              }
              return _context.abrupt("return");
            case 3:
              return _context.abrupt("return", new Promise(function (resolve, reject) {
                if (forceRefresh === false && this.talksList.length > 0) {
                  this.renderTalksList();
                  resolve(this.talksList);
                } else {
                  var settings = {
                    "url": this.apiurl + "/talks",
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                      "Authorization": this.createBasicAuthHeaderFromApiKey(this.getCookie("did_apikey"))
                    },
                    "success": function (data) {
                      jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_options").show();
                      jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_table").show();
                      this.talksList = data["talks"];
                      if (this.taskListLength !== this.talksList.length) {
                        this.updateCredits();
                        this.stopInterval();
                      }
                      resolve(this.talksList);
                      this.renderTalksList();
                    }.bind(this),
                    "error": function error(data) {
                      alert("API-Key ungültig");
                    }
                  };
                  jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
                }
              }.bind(this)));
            case 4:
            case "end":
              return _context.stop();
          }
        }, _callee, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "updateCredits",
    value: function updateCredits() {
      return new Promise(function (resolve, reject) {
        var settings_credits = {
          "url": this.apiurl + "/credits",
          "method": "GET",
          "timeout": 0,
          "headers": {
            "Authorization": this.createBasicAuthHeaderFromApiKey(this.getCookie("did_apikey"))
          },
          "success": function (data) {
            resolve(data);
            jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_credits").html(data.remaining + " / " + data.total);
          }.bind(this),
          "error": function error(data) {
            alert("Cannot fetch credits");
          }
        };
        jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings_credits);
      }.bind(this));
    }
  }, {
    key: "createBasicAuthHeaderFromApiKey",
    value: function createBasicAuthHeaderFromApiKey(apikey) {
      var credentials = apikey;
      var encodedCredentials = btoa(credentials);
      var basicAuthHeader = 'Basic ' + encodedCredentials;
      return basicAuthHeader;
    }
  }, {
    key: "renderForm",
    value: function renderForm() {
      return (_renderForm = _renderForm || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
        var _ref;
        var template,
          _args4 = arguments;
        return _regeneratorRuntime().wrap(function _callee4$(_context4) {
          while (1) switch (_context4.prev = _context4.next) {
            case 0:
              template = _args4.length > 0 && _args4[0] !== undefined ? _args4[0] : "/did_table_form.html";
              return _context4.abrupt("return", new Promise(function (_x, _x2) {
                return (_ref = _ref || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(resolve, reject) {
                  return _regeneratorRuntime().wrap(function _callee3$(_context3) {
                    while (1) switch (_context3.prev = _context3.next) {
                      case 0:
                        _context3.next = 2;
                        return this.tpl.renderIntoAsync(template, {}, this.formSelector);
                      case 2:
                        if (this.getCookie("did_apikey")) {
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_apikey").val(this.getCookie("did_apikey"));
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#video_name").val(this.getCookie("video_name"));
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#tts_text").val(this.getCookie("tts_text"));
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#image_url").val(this.getCookie("image_url"));
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#voice").val(this.getCookie("voice"));
                        }
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_apikey").on("change", function () {
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_options").hide();
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_table").hide();
                          this.setCookie("did_apikey", jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_apikey").val(), 2);
                          this.getTalksList(true);
                        }.bind(this));
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()("#start_button").on("click", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
                          var url_valid;
                          return _regeneratorRuntime().wrap(function _callee2$(_context2) {
                            while (1) switch (_context2.prev = _context2.next) {
                              case 0:
                                this.setCookie("did_apikey", jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_apikey").val(), 2);
                                this.setCookie("video_name", jquery__WEBPACK_IMPORTED_MODULE_0___default()("#video_name").val(), 2);
                                this.setCookie("tts_text", jquery__WEBPACK_IMPORTED_MODULE_0___default()("#tts_text").val(), 2);
                                this.setCookie("image_url", jquery__WEBPACK_IMPORTED_MODULE_0___default()("#image_url").val(), 2);
                                this.setCookie("voice", jquery__WEBPACK_IMPORTED_MODULE_0___default()("#voice").val(), 2);
                                this.startInterval();
                                _context2.next = 8;
                                return this.testURL(jquery__WEBPACK_IMPORTED_MODULE_0___default()("#image_url").val());
                              case 8:
                                url_valid = _context2.sent;
                                if (!jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_generation_form")[0].reportValidity()) {
                                  _context2.next = 17;
                                  break;
                                }
                                if (!(!url_valid === true)) {
                                  _context2.next = 14;
                                  break;
                                }
                                this.stopInterval();
                                alert("Image-URL konnte nicht geladen werden");
                                return _context2.abrupt("return");
                              case 14:
                                this.startVideoGeneration(jquery__WEBPACK_IMPORTED_MODULE_0___default()("#did_apikey").val(), jquery__WEBPACK_IMPORTED_MODULE_0___default()("#video_name").val(), jquery__WEBPACK_IMPORTED_MODULE_0___default()("#tts_text").val(), jquery__WEBPACK_IMPORTED_MODULE_0___default()("#image_url").val(), jquery__WEBPACK_IMPORTED_MODULE_0___default()("#voice").val()).then(function (data) {}.bind(this));
                                _context2.next = 18;
                                break;
                              case 17:
                                this.stopInterval();
                              case 18:
                              case "end":
                                return _context2.stop();
                            }
                          }, _callee2, this);
                        })).bind(this));
                        resolve();
                      case 6:
                      case "end":
                        return _context3.stop();
                    }
                  }, _callee3, this);
                }))).apply(this, arguments);
              }.bind(this)));
            case 2:
            case "end":
              return _context4.stop();
          }
        }, _callee4, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "startInterval",
    value: function startInterval() {
      jquery__WEBPACK_IMPORTED_MODULE_0___default()("#generating_status").show();
      this.interval = setInterval(function () {
        this.getTalksList(true);
      }.bind(this), 5000);
    }
  }, {
    key: "stopInterval",
    value: function stopInterval() {
      jquery__WEBPACK_IMPORTED_MODULE_0___default()("#generating_status").hide();
      clearInterval(this.interval);
    }
  }, {
    key: "testURL",
    value: function testURL(_x3) {
      return (_testURL = _testURL || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(url) {
        var _ref3;
        return _regeneratorRuntime().wrap(function _callee6$(_context6) {
          while (1) switch (_context6.prev = _context6.next) {
            case 0:
              return _context6.abrupt("return", new Promise(function (_x4, _x5) {
                return (_ref3 = _ref3 || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(resolve, reject) {
                  return _regeneratorRuntime().wrap(function _callee5$(_context5) {
                    while (1) switch (_context5.prev = _context5.next) {
                      case 0:
                        fetch(url).then(function (response) {
                          if (response.ok) {
                            resolve(true);
                          } else {
                            resolve(false);
                          }
                        })["catch"](function (error) {
                          resolve(false);
                        });
                      case 1:
                      case "end":
                        return _context5.stop();
                    }
                  }, _callee5);
                }))).apply(this, arguments);
              }));
            case 1:
            case "end":
              return _context6.stop();
          }
        }, _callee6);
      }))).apply(this, arguments);
    }
  }, {
    key: "startVideoGeneration",
    value: function startVideoGeneration(_x6, _x7, _x8, _x9) {
      return (_startVideoGeneration = _startVideoGeneration || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7(did_apikey, name, input, image_source_url) {
        var voice_id,
          data,
          _args7 = arguments;
        return _regeneratorRuntime().wrap(function _callee7$(_context7) {
          while (1) switch (_context7.prev = _context7.next) {
            case 0:
              voice_id = _args7.length > 4 && _args7[4] !== undefined ? _args7[4] : "de-DE-ConradNeural";
              data = {
                "name": name,
                "source_url": image_source_url,
                "script": {
                  "type": "text",
                  "input": input,
                  "provider": {
                    "type": "microsoft",
                    "voice_id": voice_id
                  }
                },
                "config": {
                  "fluent": false,
                  "align_driver": true,
                  "stitch": true,
                  "driver_expressions": {
                    "expressions": [{
                      "expression": "neutral",
                      "intensity": 1,
                      "start_frame": 0
                    }]
                  }
                },
                "user_data": input
              };
              return _context7.abrupt("return", new Promise(function (resolve, reject) {
                var settings = {
                  "url": this.apiurl + "/talks",
                  "method": "POST",
                  "timeout": 0,
                  "headers": {
                    "Authorization": this.createBasicAuthHeaderFromApiKey(did_apikey),
                    "Content-Type": "application/json"
                  },
                  "data": JSON.stringify(data),
                  "success": function (data) {
                    resolve(data);
                  }.bind(this)
                };
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
              }.bind(this)));
            case 3:
            case "end":
              return _context7.stop();
          }
        }, _callee7, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "renderTalksList",
    value: function renderTalksList() {
      return (_renderTalksList = _renderTalksList || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8() {
        var entries, foundCreated, foundStarted;
        return _regeneratorRuntime().wrap(function _callee8$(_context8) {
          while (1) switch (_context8.prev = _context8.next) {
            case 0:
              entries = this.talksList.map(function (entry) {
                return {
                  "name": entry.name,
                  "id": entry.id,
                  "duration": entry.duration,
                  "started_at": this.convertUTCtoLocal(entry.created_at),
                  "status": entry.status,
                  "result_url": entry.result_url,
                  "source_url": entry.source_url,
                  "user_data": entry.user_data
                };
              }.bind(this));
              foundCreated = entries.find(function (item) {
                return item.status === "created";
              });
              foundStarted = entries.find(function (item) {
                return item.status === "started";
              });
              if (foundCreated || foundStarted) {
                this.startInterval();
              } else {
                this.stopInterval();
              }
              return _context8.abrupt("return", this.tpl.renderIntoAsync("/did_table.html", {
                "entries": entries
              }, this.targetSelector).then(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('[data-delete]').on("click", function (event) {
                  if (confirm("Soll der Eintrag wirklich gelöscht werden?")) {
                    this.deleteTalk(event.target.dataset["delete"]).then(function (data) {
                      this.getTalksList(true);
                    }.bind(this));
                  }
                }.bind(this));
                jquery__WEBPACK_IMPORTED_MODULE_0___default()('[data-preview]').on("click", function (event) {
                  this.tpl.renderIntoAsync("/did_preview.html", {
                    "url": event.target.dataset.previewurl
                  }, '#preview_' + event.target.dataset.preview);
                }.bind(this));
              }.bind(this)));
            case 5:
            case "end":
              return _context8.stop();
          }
        }, _callee8, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "convertUTCtoLocal",
    value: function convertUTCtoLocal(dateString) {
      var date = new Date(dateString);
      return new Intl.DateTimeFormat("de-DE", {
        dateStyle: 'short',
        timeStyle: 'short'
      }).format(date);
    }
  }, {
    key: "deleteTalk",
    value: function deleteTalk(id) {
      return new Promise(function (resolve, reject) {
        var settings = {
          "url": this.apiurl + "/talks/" + id,
          "method": "DELETE",
          "timeout": 0,
          "headers": {
            "Authorization": this.createBasicAuthHeaderFromApiKey(this.getCookie("did_apikey"))
          },
          "success": function (data) {
            resolve(data);
          }.bind(this)
        };
        jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
      }.bind(this));
    }
  }]);
  return Did;
}();

/***/ })

}]);
//# sourceMappingURL=2.main.js.map