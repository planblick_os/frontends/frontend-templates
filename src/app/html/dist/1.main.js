(window["webpackJsonpDistPbs"] = window["webpackJsonpDistPbs"] || []).push([[1],{

/***/ "./app/src/booking.js?v=[cs_version]":
/*!*******************************************!*\
  !*** ./app/src/booking.js?v=[cs_version] ***!
  \*******************************************/
/*! exports provided: Booking */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Booking", function() { return Booking; });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! datatables.net */ "./node_modules/datatables.net/js/jquery.dataTables.mjs");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw new Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw new Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
/* eslint-disable prettier/prettier */


function nationalDays(date) {
  var natDays = [
    // [1, 26, "au"],
    // [2, 6, "nz"],
    // [3, 17, "ie"],
    // [4, 27, "za"],
    // [5, 25, "ar"],
    // [6, 6, "se"],
    // [7, 4, "us"],
    // [8, 17, "id"],
    // [9, 7, "br"],
    // [10, 1, "cn"],
    // [11, 22, "lb"],
    // [12, 12, "ke"]
  ];
  for (var i = 0; i < natDays.length; i++) {
    if (date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1]) {
      return [false];
    }
  }
  return [true, ""];
}
function isNotWeekendsOrHolidays(date) {
  var noWeekend = jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.noWeekends(date);
  if (noWeekend[0]) {
    return nationalDays(date);
  } else {
    return noWeekend;
  }
}
var Booking = /*#__PURE__*/function (_loadFromApi, _confirmBooking, _getReservationDataByCorrelationId, _isSlotAvailable, _startBookingProcess, _renderTable) {
  function Booking(apiurl, apikey, tpl, target_selector, calendar_column_id) {
    var filters = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : [];
    var inline_datepicker = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : false;
    var attached_resources = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : [];
    var duration = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : 30;
    var extra_margin_start = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : 0;
    var extra_margin_end = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : 0;
    _classCallCheck(this, Booking);
    this.apikey = apikey;
    this.apiurl = apiurl;
    this.tpl = tpl;
    this.target_selector = target_selector;
    this.calendar_column_id = calendar_column_id;
    this.filters = filters;
    this.inline_datepicker = inline_datepicker;
    this.attached_resources = attached_resources;
    var date_format_options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      timezone: "UTC"
    };
    this.intl_dtf = new Intl.DateTimeFormat(undefined, date_format_options);
    this.data = [];
    this.event_template = {};
    this.event_template = {
      event_id: "",
      resourceId: this.calendar_column_id,
      textColor: "#ff0000",
      color: "#cccccc",
      attachedRessources: [],
      starttime: "",
      endtime: "",
      title: "test",
      savekind: "create",
      extended_props: {
        creator_login: "superuser",
        comment: "Created by booking-manager"
      },
      allDay: false,
      attachedContacts: [],
      data_owners: [],
      reminders: [],
      tags: [{
        "id": "Websitebuchung",
        "text": "Websitebuchung"
      }],
      data: {
        event_status: "pending",
        appointment_type: []
      }
    };
    this.table_id = "pbs_booking_table";
    this.lastDate = undefined;
    this.availability_config = {
      filters: [{
        ressource: this.calendar_column_id
      }].concat(this.filters),
      start: "",
      end: "",
      duration: duration,
      extra_margin_start: extra_margin_start,
      extra_margin_end: extra_margin_end
    };
    jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.regional.de = {
      closeText: "Schließen",
      prevText: "Zurück",
      nextText: "Vor",
      currentText: "Heute",
      monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
      monthNamesShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
      dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
      dayNamesShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
      dayNamesMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
      weekHeader: "KW",
      dateFormat: "dd.mm.yy",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };
    jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.setDefaults(jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.regional.de);
  }
  _createClass(Booking, [{
    key: "formatAttachedRessourcesForApi",
    value: function formatAttachedRessourcesForApi() {
      var formatted = [];
      var _iterator = _createForOfIteratorHelper(this.attached_resources),
        _step;
      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var entry = _step.value;
          formatted.push(JSON.stringify(entry));
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
      return formatted;
    }
  }, {
    key: "createBooking",
    value: function createBooking(data) {
      var self = this;
      return new Promise(function (resolve, reject) {
        this.startBookingProcess(data).then(function (result) {
          return resolve(result);
        });
      }.bind(this));
    }
  }, {
    key: "loadFromApi",
    value: function loadFromApi(_x) {
      return (_loadFromApi = _loadFromApi || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(date) {
        var settings;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              settings = {
                url: this.apiurl + "/cb/available_slots_for_date?date=" + date.toISOString().split("T")[0],
                method: "POST",
                timeout: 0,
                headers: {
                  apikey: this.apikey,
                  "Content-Type": "application/json"
                },
                data: JSON.stringify(this.availability_config),
                success: function (response) {
                  var currentTime = new Date();
                  this.data = response.filter(function (data) {
                    return new Date(data.start) > currentTime;
                  });
                }.bind(this)
              };
              _context.next = 3;
              return jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
            case 3:
              settings = {
                url: this.apiurl + "/cb/available_slots_for_date?date=" + date.toISOString().split("T")[0],
                method: "POST",
                timeout: 0,
                headers: {
                  apikey: this.apikey,
                  "Content-Type": "application/json"
                },
                data: JSON.stringify(this.availability_config),
                success: function (response) {}.bind(this)
              };
              _context.next = 6;
              return jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
            case 6:
            case "end":
              return _context.stop();
          }
        }, _callee, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "formatDateForApi",
    value: function formatDateForApi(date) {
      var year = date.getFullYear();
      var month = String(date.getMonth() + 1).padStart(2, "0");
      var day = String(date.getDate()).padStart(2, "0");
      var hours = String(date.getHours()).padStart(2, "0");
      var minutes = String(date.getMinutes()).padStart(2, "0");
      var seconds = String(date.getSeconds()).padStart(2, "0");
      return "".concat(year, "-").concat(month, "-").concat(day, " ").concat(hours, ":").concat(minutes, ":").concat(seconds);
    }
  }, {
    key: "formatDateForDisplay",
    value: function formatDateForDisplay(date) {
      var year = date.getFullYear();
      var month = String(date.getMonth() + 1).padStart(2, "0");
      var day = String(date.getDate()).padStart(2, "0");
      var hours = String(date.getHours()).padStart(2, "0");
      var minutes = String(date.getMinutes()).padStart(2, "0");
      var seconds = String(date.getSeconds()).padStart(2, "0");
      return "".concat(day, ".").concat(month, ".").concat(year, " ").concat(hours, ":").concat(minutes);
    }
  }, {
    key: "confirmBooking",
    value: function confirmBooking(_x2) {
      return (_confirmBooking = _confirmBooking || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(id) {
        var internal,
          _args2 = arguments;
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              internal = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : false;
              return _context2.abrupt("return", new Promise(function (resolve, reject) {
                var settings = {
                  url: "".concat(this.apiurl, "/es/saga/next/correlation_id/").concat(id),
                  method: "GET",
                  timeout: 0,
                  headers: {
                    apikey: this.apikey
                  },
                  success: function () {
                    if (!internal) {
                      this.tpl.renderIntoAsync("/confirmed.html", {}, this.target_selector).then(resolve);
                    } else {
                      this.tpl.renderIntoAsync("/reserved.html", {}, this.target_selector).then(resolve);
                    }
                  }.bind(this)
                };
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
              }.bind(this)));
            case 2:
            case "end":
              return _context2.stop();
          }
        }, _callee2, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "getReservationDataByCorrelationId",
    value: function getReservationDataByCorrelationId(_x3) {
      return (_getReservationDataByCorrelationId = _getReservationDataByCorrelationId || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(id) {
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt("return", new Promise(function (resolve, reject) {
                var settings = {
                  url: "".concat(this.apiurl, "/es/saga/correlation_id/").concat(id),
                  method: "GET",
                  timeout: 0,
                  headers: {
                    apikey: this.apikey
                  },
                  success: function success(response) {
                    return resolve(response);
                  }
                };
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
              }.bind(this)));
            case 1:
            case "end":
              return _context3.stop();
          }
        }, _callee3, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "isSlotAvailable",
    value: function isSlotAvailable(_x4, _x5) {
      return (_isSlotAvailable = _isSlotAvailable || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(startdate, enddate) {
        return _regeneratorRuntime().wrap(function _callee4$(_context4) {
          while (1) switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt("return", new Promise(function (resolve, reject) {
                var payload = this.availability_config;
                if (!startdate || !enddate) {
                  resolve(false);
                }
                payload["start"] = startdate;
                payload["end"] = enddate;
                var settings = {
                  url: "".concat(this.apiurl, "/cb/is_slot_available"),
                  method: "POST",
                  timeout: 0,
                  headers: {
                    apikey: this.apikey,
                    "Content-Type": "application/json"
                  },
                  data: JSON.stringify(payload),
                  success: function success(response) {
                    return resolve(response);
                  }
                };
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
              }.bind(this)));
            case 1:
            case "end":
              return _context4.stop();
          }
        }, _callee4, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "startBookingProcess",
    value: function startBookingProcess(_x6) {
      return (_startBookingProcess = _startBookingProcess || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(booking_data) {
        return _regeneratorRuntime().wrap(function _callee5$(_context5) {
          while (1) switch (_context5.prev = _context5.next) {
            case 0:
              return _context5.abrupt("return", new Promise(function (resolve, reject) {
                this.isSlotAvailable(this.formatDateForApi(new Date(booking_data.starttime)), this.formatDateForApi(new Date(booking_data.endtime))).then(function (result) {
                  if (!(result !== null && result !== void 0 && result.result)) {
                    resolve(false);
                  } else {
                    var saga = {
                      saga_name: "Book appointment",
                      saga_data: [{
                        step_name: "step_sendconfirmation_mail",
                        event_name: "sendAppointmentBookingConfirmationMail",
                        order: 0,
                        payload: {
                          data: {
                            firstname: jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_firstname_input").val(),
                            lastname: jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_lastname_input").val(),
                            email: jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_email_input").val(),
                            confirmation_page_url: window.location.href,
                            booking_data: booking_data
                          }
                        }
                      }, {
                        step_name: "step_create_new_appointment",
                        event_name: "createNewAppointment",
                        order: 1,
                        payload: booking_data
                      }],
                      data_owners: []
                    };
                    this.tpl.renderIntoAsync("/loading.html", {}, this.target_selector);
                    var settings = {
                      url: "".concat(this.apiurl, "/es/cmd/createNewSaga"),
                      method: "POST",
                      timeout: 0,
                      headers: {
                        apikey: this.apikey,
                        "Content-Type": "application/json"
                      },
                      data: JSON.stringify(saga),
                      success: function (response) {
                        setTimeout(function () {
                          this.confirmBooking(response.task_id, true).then(function () {
                            return resolve;
                          });
                        }.bind(this), 2000);
                      }.bind(this)
                    };
                    jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
                  }
                }.bind(this));
              }.bind(this)));
            case 1:
            case "end":
              return _context5.stop();
          }
        }, _callee5, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "renderTable",
    value: function renderTable() {
      return (_renderTable = _renderTable || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee9() {
        var _ref;
        var date,
          tpl,
          _args9 = arguments;
        return _regeneratorRuntime().wrap(function _callee9$(_context9) {
          while (1) switch (_context9.prev = _context9.next) {
            case 0:
              date = _args9.length > 0 && _args9[0] !== undefined ? _args9[0] : this.lastDate || new Date();
              this.lastDate = date;
              tpl = "/datepicker.html";
              if (this.inline_datepicker === true) {
                tpl = "/datepicker_inline.html";
              }
              this.tpl.renderIntoAsync(tpl, {
                data: {}
              }, this.target_selector).then(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker({
                  minDate: new Date(),
                  dateFormat: "dd.mm.yy",
                  changeMonth: true,
                  changeYear: true,
                  beforeShowDay: isNotWeekendsOrHolidays,
                  inline: this.inline_datepicker
                });
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.setDefaults(jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.regional.de);
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("setDate", date);
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").on("change", function () {
                  var newDate = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("getDate");
                  var userTimezoneOffset = newDate.getTimezoneOffset() * 60000;
                  newDate = new Date(newDate.getTime() - userTimezoneOffset);
                  this.renderTable(newDate);
                }.bind(this));
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#previous_day").click(function () {
                  var currentDate = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("getDate");
                  var previousDate = new Date(currentDate);
                  previousDate.setDate(currentDate.getDate() - 1);
                  while (!isNotWeekendsOrHolidays(previousDate)[0]) previousDate.setDate(previousDate.getDate() - 1);
                  jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("setDate", previousDate).change();
                });
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#next_day").click(function () {
                  var currentDate = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("getDate");
                  var nextDate = new Date(currentDate);
                  nextDate.setDate(currentDate.getDate() + 1);
                  while (!isNotWeekendsOrHolidays(nextDate)[0]) nextDate.setDate(nextDate.getDate() + 1);
                  jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("setDate", nextDate).change();
                });
              }.bind(this));
              _context9.next = 7;
              return this.loadFromApi(date);
            case 7:
              return _context9.abrupt("return", new Promise(function (_x7, _x8) {
                return (_ref = _ref || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8(resolve, reject) {
                  var _ref2;
                  var datatableOptions, table_id, table;
                  return _regeneratorRuntime().wrap(function _callee8$(_context8) {
                    while (1) switch (_context8.prev = _context8.next) {
                      case 0:
                        datatableOptions = {
                          language: {
                            emptyTable: this.tpl.i18n.translate("No slots available")
                          },
                          searching: false,
                          ordering: false,
                          pageLength: 1000,
                          stateSave: true,
                          paging: false,
                          info: false,
                          retrieve: true,
                          // rowId: function (a) {
                          //     return "uuid_" + a.uuid;
                          // },
                          // dom: 'Bfrtip',
                          // buttons: [
                          //     {
                          //         text: this.tpl.i18n.translate("Add"),
                          //         className: "btn btn-indigo",
                          //         action: function (e, dt, node, config) {
                          //             this.openFieldTypeEditor(undefined, {})
                          //         }.bind(this)
                          //     }
                          // ],
                          columns: [
                          // {title: this.tpl.i18n.translate('Start'), data: "start"},
                          // {title: this.tpl.i18n.translate('End'), data: "end"},
                          {
                            title: this.tpl.i18n.translate("Von"),
                            className: "dt-from-column",
                            data: "start",
                            render: function (value) {
                              return this.formatDateForDisplay(new Date(value)); //this.intl_dtf.format(new Date(value))
                            }.bind(this)
                          }, {
                            title: this.tpl.i18n.translate("bis"),
                            className: "dt-until-column",
                            data: "end",
                            render: function (value) {
                              return this.formatDateForDisplay(new Date(value)); //this.intl_dtf.format(new Date(value))
                            }.bind(this)
                          }, {
                            width: "40px",
                            data: null,
                            render: function render(data, slot, row, meta) {
                              return "<button class=\"btn btn-info btn-pills login-btn-primary\" data-selector=\"pbs_bookbutton\" data-start=\"".concat(row.start, "\" data-end=\"").concat(row.end, "\">Buchen</button>");
                            },
                            orderable: false
                          }],
                          responsive: true,
                          pagingType: "first_last_numbers",
                          lengthChange: false
                          // language: {
                          //     url: '/style-global/lib/datatables.net-dt/i18n/de-DE.json'
                          // },
                        };
                        table_id = this.table_id;
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()("#" + table_id).remove();
                        this.datatable = undefined;
                        table = jquery__WEBPACK_IMPORTED_MODULE_0___default()("<table>");
                        table.attr("id", table_id);
                        jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.target_selector).after(table);
                        if (!this.datatable) {
                          this.datatable = new datatables_net__WEBPACK_IMPORTED_MODULE_1__["default"]("#" + table_id, datatableOptions);
                        }
                        this.datatable.clear();
                        this.datatable.rows.add(this.data).draw();
                        //$(this.target_selector).hide()
                        this.datatable.off("click");
                        this.datatable.on("click", '[data-selector="pbs_bookbutton"]', function (_x9) {
                          return (_ref2 = _ref2 || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7(e) {
                            var data;
                            return _regeneratorRuntime().wrap(function _callee7$(_context7) {
                              while (1) switch (_context7.prev = _context7.next) {
                                case 0:
                                  e.preventDefault();
                                  jquery__WEBPACK_IMPORTED_MODULE_0___default()("#" + this.table_id).hide();
                                  data = {
                                    start_string: this.intl_dtf.format(new Date(e.target.dataset.start)),
                                    end_string: this.intl_dtf.format(new Date(e.target.dataset.end)),
                                    start_date: e.target.dataset.start,
                                    end_date: e.target.dataset.end
                                  };
                                  this.tpl.renderIntoAsync("booking_form.html", {
                                    data: data
                                  }, this.target_selector).then(function () {
                                    var _ref3;
                                    jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_submit_button").click(function (_x10) {
                                      return (_ref3 = _ref3 || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(e) {
                                        var result, start_date, end_date, tpl_data, payload, urlParams;
                                        return _regeneratorRuntime().wrap(function _callee6$(_context6) {
                                          while (1) switch (_context6.prev = _context6.next) {
                                            case 0:
                                              result = document.querySelector("#pbs_booking_form").reportValidity();
                                              if (result) {
                                                _context6.next = 3;
                                                break;
                                              }
                                              return _context6.abrupt("return");
                                            case 3:
                                              start_date = new Date(e.target.dataset.start);
                                              end_date = new Date(e.target.dataset.end);
                                              tpl_data = {
                                                start_string: this.intl_dtf.format(start_date),
                                                end_string: this.intl_dtf.format(end_date)
                                              };
                                              this.event_template.attachedRessources = this.formatAttachedRessourcesForApi();
                                              payload = Object.assign({}, this.event_template);
                                              payload.title = "".concat(jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_firstname_input").val(), " ").concat(jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_lastname_input").val(), " ").concat(jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_email_input").val());
                                              payload.starttime = this.formatDateForApi(start_date);
                                              payload.endtime = this.formatDateForApi(end_date);
                                              urlParams = new URLSearchParams(window.location.search);
                                              if (urlParams.get('cid')) {
                                                payload.attachedContacts = [urlParams.get('cid')];
                                              }
                                              this.createBooking(payload).then(function (result) {
                                                if (result) {
                                                  this.tpl.renderIntoAsync("/booking_success.html", {
                                                    data: tpl_data
                                                  }, this.target_selector).then(function () {}.bind(this));
                                                } else {
                                                  this.tpl.renderIntoAsync("/not_available.html", {}, this.target_selector).then(function () {
                                                    window.setTimeout(this.renderTable.bind(this), 3000);
                                                    resolve(false);
                                                  }.bind(this));
                                                }
                                              }.bind(this));
                                            case 14:
                                            case "end":
                                              return _context6.stop();
                                          }
                                        }, _callee6, this);
                                      }))).apply(this, arguments);
                                    }.bind(this));
                                    jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_booking_form_cancel_button").click(function (e) {
                                      this.renderTable();
                                    }.bind(this));
                                  }.bind(this));
                                  jquery__WEBPACK_IMPORTED_MODULE_0___default()("#" + table_id).hide();
                                  jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.target_selector).show();
                                case 6:
                                case "end":
                                  return _context7.stop();
                              }
                            }, _callee7, this);
                          }))).apply(this, arguments);
                        }.bind(this));

                        // this.datatable.on('click', '[data-selector="dt_delete"]', function (e) {
                        //     e.preventDefault();
                        //     new PbEventTypes().deleteByUUId(e.currentTarget.dataset?.id)
                        // }.bind(this));

                        resolve();
                      case 13:
                      case "end":
                        return _context8.stop();
                    }
                  }, _callee8, this);
                }))).apply(this, arguments);
              }.bind(this)));
            case 8:
            case "end":
              return _context9.stop();
          }
        }, _callee9, this);
      }))).apply(this, arguments);
    }
  }]);
  return Booking;
}();

/***/ })

}]);
//# sourceMappingURL=1.main.js.map