(window["webpackJsonpDistPbs"] = window["webpackJsonpDistPbs"] || []).push([[4],{

/***/ "./app/src/jitsi.js?v=[cs_version]":
/*!*****************************************!*\
  !*** ./app/src/jitsi.js?v=[cs_version] ***!
  \*****************************************/
/*! exports provided: Jitsi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Jitsi", function() { return Jitsi; });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw new Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw new Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
/* eslint-disable prettier/prettier */

var Jitsi = /*#__PURE__*/function (_init) {
  function Jitsi(apiurl, apikey, options) {
    var socketio = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
    _classCallCheck(this, Jitsi);
    this.options = options;
    this.api = undefined;
    this.init();
  }
  _createClass(Jitsi, [{
    key: "init",
    value: function init() {
      return (_init = _init || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var _this$options, _this$options2, _this$options3, _this$options4, _this$options5, _this$options6, _this$options7, _this$options8, _this$options9, _this$options10, _this$options11, _this$options12, _this$options13, _this$options14, _this$options15, _this$options16, _this$options17, _this$options18, _this$options19, _this$options20, _this$options21, _this$options22, _this$options23, _this$options24, _this$options25, _this$options26, _this$options27, _this$options28, _this$options29, _this$options30, _this$options31, _this$options32;
        var domain, toolbar_buttons, options;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return jquery__WEBPACK_IMPORTED_MODULE_0___default.a.getScript(this.options.jitsi_server + '/external_api.js');
            case 2:
              domain = this.options.jitsi_server.replace("https://", "") || 'jitsi.planblick.com';
              toolbar_buttons = ['camera', 'chat', 'closedcaptions', 'desktop', 'download', 'embedmeeting', 'etherpad', 'feedback', 'filmstrip', 'fullscreen', 'hangup', 'help', 'highlight', 'invite', 'linktosalesforce', 'livestreaming', 'microphone', 'noisesuppression', 'participants-pane', 'profile', 'raisehand', 'recording', 'security', 'select-background', 'settings', 'shareaudio', 'sharedvideo', 'shortcuts', 'stats', 'tileview', 'toggle-camera', 'videoquality', 'whiteboard'];
              options = {
                roomName: ((_this$options = this.options) === null || _this$options === void 0 ? void 0 : _this$options.room_id) || 'planblick',
                width: ((_this$options2 = this.options) === null || _this$options2 === void 0 ? void 0 : _this$options2.width) || "100%",
                height: ((_this$options3 = this.options) === null || _this$options3 === void 0 ? void 0 : _this$options3.height) || "400px",
                parentNode: document.querySelector(this.options.target_selector),
                interfaceConfigOverwrite: {
                  PROVIDER_NAME: ((_this$options4 = this.options) === null || _this$options4 === void 0 || (_this$options4 = _this$options4.interfaceConfigOverwrite) === null || _this$options4 === void 0 ? void 0 : _this$options4.PROVIDER_NAME) || 'planBLICK GmbH',
                  DEFAULT_BACKGROUND: ((_this$options5 = this.options) === null || _this$options5 === void 0 || (_this$options5 = _this$options5.interfaceConfigOverwrite) === null || _this$options5 === void 0 ? void 0 : _this$options5.DEFAULT_BACKGROUND) || '#000000',
                  RECENT_LIST_ENABLED: ((_this$options6 = this.options) === null || _this$options6 === void 0 || (_this$options6 = _this$options6.interfaceConfigOverwrite) === null || _this$options6 === void 0 ? void 0 : _this$options6.RECENT_LIST_ENABLED) || false,
                  TOOLBAR_ALWAYS_VISIBLE: ((_this$options7 = this.options) === null || _this$options7 === void 0 || (_this$options7 = _this$options7.interfaceConfigOverwrite) === null || _this$options7 === void 0 ? void 0 : _this$options7.TOOLBAR_ALWAYS_VISIBLE) || false,
                  DISABLE_JOIN_LEAVE_NOTIFICATIONS: ((_this$options8 = this.options) === null || _this$options8 === void 0 || (_this$options8 = _this$options8.interfaceConfigOverwrite) === null || _this$options8 === void 0 ? void 0 : _this$options8.DISABLE_JOIN_LEAVE_NOTIFICATIONS) || true,
                  MOBILE_APP_PROMO: ((_this$options9 = this.options) === null || _this$options9 === void 0 || (_this$options9 = _this$options9.interfaceConfigOverwrite) === null || _this$options9 === void 0 ? void 0 : _this$options9.MOBILE_APP_PROMO) || true,
                  VIDEO_LAYOUT_FIT: ((_this$options10 = this.options) === null || _this$options10 === void 0 || (_this$options10 = _this$options10.interfaceConfigOverwrite) === null || _this$options10 === void 0 ? void 0 : _this$options10.VIDEO_LAYOUT_FIT) || 'nocrop',
                  ENFORCE_NOTIFICATION_AUTO_DISMISS_TIMEOUT: ((_this$options11 = this.options) === null || _this$options11 === void 0 || (_this$options11 = _this$options11.interfaceConfigOverwrite) === null || _this$options11 === void 0 ? void 0 : _this$options11.ENFORCE_NOTIFICATION_AUTO_DISMISS_TIMEOUT) || 15,
                  TILE_VIEW_MAX_COLUMNS: ((_this$options12 = this.options) === null || _this$options12 === void 0 || (_this$options12 = _this$options12.interfaceConfigOverwrite) === null || _this$options12 === void 0 ? void 0 : _this$options12.TILE_VIEW_MAX_COLUMNS) || 2,
                  DISABLE_FOCUS_INDICATOR: ((_this$options13 = this.options) === null || _this$options13 === void 0 || (_this$options13 = _this$options13.interfaceConfigOverwrite) === null || _this$options13 === void 0 ? void 0 : _this$options13.DISABLE_FOCUS_INDICATOR) || true,
                  VERTICAL_FILMSTRIP: ((_this$options14 = this.options) === null || _this$options14 === void 0 || (_this$options14 = _this$options14.interfaceConfigOverwrite) === null || _this$options14 === void 0 ? void 0 : _this$options14.VERTICAL_FILMSTRIP) || true
                },
                configOverwrite: {
                  deeplinking: {
                    "disabled": ((_this$options15 = this.options) === null || _this$options15 === void 0 || (_this$options15 = _this$options15.configOverwrite) === null || _this$options15 === void 0 ? void 0 : _this$options15.disableDeepLinking) || true
                  },
                  prejoinConfig: {
                    enabled: ((_this$options16 = this.options) === null || _this$options16 === void 0 || (_this$options16 = _this$options16.configOverwrite) === null || _this$options16 === void 0 || (_this$options16 = _this$options16.prejoinConfig) === null || _this$options16 === void 0 ? void 0 : _this$options16.enabled) || true
                  },
                  toolbarButtons: ((_this$options17 = this.options) === null || _this$options17 === void 0 || (_this$options17 = _this$options17.configOverwrite) === null || _this$options17 === void 0 ? void 0 : _this$options17.toolbarButtons) || toolbar_buttons,
                  enableNoAudioDetection: ((_this$options18 = this.options) === null || _this$options18 === void 0 || (_this$options18 = _this$options18.configOverwrite) === null || _this$options18 === void 0 ? void 0 : _this$options18.enableNoAudioDetection) || true,
                  enableNoisyMicDetection: ((_this$options19 = this.options) === null || _this$options19 === void 0 || (_this$options19 = _this$options19.configOverwrite) === null || _this$options19 === void 0 ? void 0 : _this$options19.enableNoisyMicDetection) || true,
                  startAudioOnly: ((_this$options20 = this.options) === null || _this$options20 === void 0 || (_this$options20 = _this$options20.configOverwrite) === null || _this$options20 === void 0 ? void 0 : _this$options20.startAudioOnly) || false,
                  startAudioMuted: ((_this$options21 = this.options) === null || _this$options21 === void 0 || (_this$options21 = _this$options21.configOverwrite) === null || _this$options21 === void 0 ? void 0 : _this$options21.startAudioMuted) || false,
                  resolution: ((_this$options22 = this.options) === null || _this$options22 === void 0 || (_this$options22 = _this$options22.configOverwrite) === null || _this$options22 === void 0 ? void 0 : _this$options22.resolution) || 720,
                  disableRemoveRaisedHandOnFocus: ((_this$options23 = this.options) === null || _this$options23 === void 0 || (_this$options23 = _this$options23.configOverwrite) === null || _this$options23 === void 0 ? void 0 : _this$options23.disableRemoveRaisedHandOnFocus) || true,
                  startWithVideoMuted: ((_this$options24 = this.options) === null || _this$options24 === void 0 || (_this$options24 = _this$options24.configOverwrite) === null || _this$options24 === void 0 ? void 0 : _this$options24.startWithVideoMuted) || false,
                  localRecording: {
                    notifyAllParticipants: ((_this$options25 = this.options) === null || _this$options25 === void 0 || (_this$options25 = _this$options25.configOverwrite) === null || _this$options25 === void 0 ? void 0 : _this$options25.notifyAllParticipants) || true
                  },
                  liveStreaming: {
                    enabled: true
                  },
                  lobby: {
                    autoKnock: true,
                    enableChat: true
                  },
                  enableClosePage: ((_this$options26 = this.options) === null || _this$options26 === void 0 || (_this$options26 = _this$options26.configOverwrite) === null || _this$options26 === void 0 ? void 0 : _this$options26.enableClosePage) || false,
                  defaultRemoteDisplayName: ((_this$options27 = this.options) === null || _this$options27 === void 0 || (_this$options27 = _this$options27.configOverwrite) === null || _this$options27 === void 0 ? void 0 : _this$options27.defaultRemoteDisplayName) || 'Teilnehmer',
                  doNotStoreRoom: ((_this$options28 = this.options) === null || _this$options28 === void 0 || (_this$options28 = _this$options28.configOverwrite) === null || _this$options28 === void 0 ? void 0 : _this$options28.doNotStoreRoom) || true,
                  buttonsWithNotifyClick: [{
                    key: 'camera',
                    preventExecution: false
                  }, {
                    key: 'chat',
                    preventExecution: false
                  }, {
                    key: 'closedcaptions',
                    preventExecution: false
                  }, {
                    key: 'desktop',
                    preventExecution: false
                  }, {
                    key: 'download',
                    preventExecution: false
                  }, {
                    key: 'embedmeeting',
                    preventExecution: false
                  }, {
                    key: 'end-meeting',
                    preventExecution: false
                  }, {
                    key: 'etherpad',
                    preventExecution: false
                  }, {
                    key: 'feedback',
                    preventExecution: false
                  }, {
                    key: 'filmstrip',
                    preventExecution: false
                  }, {
                    key: 'fullscreen',
                    preventExecution: false
                  }, {
                    key: 'hangup',
                    preventExecution: false
                  }, {
                    key: 'hangup-menu',
                    preventExecution: false
                  }, {
                    key: 'help',
                    preventExecution: false
                  }, {
                    key: 'invite',
                    preventExecution: false
                  }, {
                    key: 'livestreaming',
                    preventExecution: false
                  }, {
                    key: 'microphone',
                    preventExecution: false
                  }, {
                    key: 'mute-everyone',
                    preventExecution: false
                  }, {
                    key: 'mute-video-everyone',
                    preventExecution: false
                  }, {
                    key: 'noisesuppression',
                    preventExecution: false
                  }, {
                    key: 'participants-pane',
                    preventExecution: false
                  }, {
                    key: 'profile',
                    preventExecution: false
                  }, {
                    key: 'raisehand',
                    preventExecution: false
                  }, {
                    key: 'recording',
                    preventExecution: false
                  }, {
                    key: 'security',
                    preventExecution: false
                  }, {
                    key: 'select-background',
                    preventExecution: false
                  }, {
                    key: 'settings',
                    preventExecution: false
                  }, {
                    key: 'shareaudio',
                    preventExecution: false
                  }, {
                    key: 'sharedvideo',
                    preventExecution: false
                  }, {
                    key: 'shortcuts',
                    preventExecution: false
                  }, {
                    key: 'stats',
                    preventExecution: false
                  }, {
                    key: 'tileview',
                    preventExecution: false
                  }, {
                    key: 'toggle-camera',
                    preventExecution: false
                  }, {
                    key: 'videoquality',
                    preventExecution: false
                  }, {
                    key: 'add-passcode',
                    preventExecution: false
                  }, {
                    key: 'whiteboard',
                    preventExecution: false
                  }],
                  hideConferenceSubject: ((_this$options29 = this.options) === null || _this$options29 === void 0 || (_this$options29 = _this$options29.configOverwrite) === null || _this$options29 === void 0 ? void 0 : _this$options29.hideConferenceSubject) || true,
                  subject: ((_this$options30 = this.options) === null || _this$options30 === void 0 || (_this$options30 = _this$options30.configOverwrite) === null || _this$options30 === void 0 ? void 0 : _this$options30.subject) || new Date().toLocaleDateString(),
                  useHostPageLocalStorage: true
                },
                userInfo: {
                  email: ((_this$options31 = this.options) === null || _this$options31 === void 0 || (_this$options31 = _this$options31.userInfo) === null || _this$options31 === void 0 ? void 0 : _this$options31.email) || '',
                  displayName: ((_this$options32 = this.options) === null || _this$options32 === void 0 || (_this$options32 = _this$options32.userInfo) === null || _this$options32 === void 0 ? void 0 : _this$options32.displayName) || ""
                }
              };
              this.api = new JitsiMeetExternalAPI(domain, options);
              // All available events: https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-iframe-events/
              this.api.addListener("videoConferenceJoined", function (e) {
                console.log("EVENT", e);
              });
              this.api.addListener("readyToClose", function (e) {
                document.querySelector(this.options.target_selector).innerHTML = "Vielen Dank für deine Teilnahme";
              }.bind(this));
              this.api.addListener("toolbarButtonClicked", function (e) {
                console.log("EVENT", e);
              });
              console.log("Jitsi init");
            case 10:
            case "end":
              return _context.stop();
          }
        }, _callee, this);
      }))).apply(this, arguments);
    }
  }]);
  return Jitsi;
}();

/***/ })

}]);
//# sourceMappingURL=4.main.js.map