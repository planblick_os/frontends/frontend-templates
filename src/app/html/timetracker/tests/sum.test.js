import { sum } from "../js/sum.js"
import { PbTimer } from "../js/timer.js"

global.moment = ()=>{
  return {utc: ()=>""}
}
jest.mock("../../csf-lib/pb-functions.js", () => () => false);

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('can create new instance of PbTimer', () => {
  let timer = new PbTimer()
  timer.setData({"foo": "bar"})
  expect(timer.getData()).toStrictEqual({"foo": "bar"})
});
//# sourceMappingURL=sum.test.js.map