<div id="activity-nav">
    <nav class="d-flex align-items-start pd-t-10">
        {% if headline and headline | length > 0 %}
            <h6 class="mg-t-7 ml-3">{{headline}}</h6>
        {% endif %}

        <div class="d-flex align-items-start w-100">
            {% set counter = 0 %}
            {% for pill in pills %}
                {% if counter < 3 %}
                    <div {% if 'permission' in pill and pill.permission|length > 1 %}class="hidden" data-permission="{{pill.permission}}"{% endif %} data-selector="pill_entry" id="{{pill.id}}">
                        <a id="pilllink_{{pill.id}}" {% if 'link' in pill and pill.link|length > 1 %} href="{{pill.link}}" target="{% if pill.target %}{{pill.target}}{% else %}_blank{% endif %}"{% endif %} class="btn-light-pill ml-2" {% if 'toggle' in pill and pill.toggle|length > 1 %} data-toggle="true" {% endif %}>
                            <span class="d-none d-lg-block" data-i18n='{{pill.name}}'>{{pill.name}}</span>
                            <span class="d-block d-lg-none"><i class="{{pill.icon}} mr-1 tx-14"></i></span>
                        </a>
                    </div>
                {% else %}
                    {% if counter == 3 %}
                        <!-- Burger menu button -->
                        <div id="burgerMenuButton" class="btn-indigo btn-light-pill ml-2" type="button" data-toggle="collapse" data-target="#burgerMenu" aria-expanded="false" aria-controls="burgerMenu">
                            <i id="burgerMenuIcon" class="fa fa-bars"></i>
                        </div>
                        <!-- Burger menu content -->
                        <div class="collapse" id="burgerMenu">
                    {% endif %}
                    <div class="other-pills d-flex align-items-start mb-2" id="other_{{pill.id}}">
                        <a id="pilllink_{{pill.id}}" {% if 'link' in pill and pill.link|length > 1 %} href="{{pill.link}}" target="{% if pill.target %}{{pill.target}}{% else %}_blank{% endif %}"{% endif %} class="btn-light-pill ml-2" {% if 'toggle' in pill and pill.toggle|length > 1 %} data-toggle="true" {% endif %}>
                            <span class="d-none d-lg-block" data-i18n='{{pill.name}}'>{{pill.name}}</span>
                            <span class="d-block d-lg-none"><i class="{{pill.icon}} mr-1 tx-14"></i></span>
                        </a>
                    </div>
                {% endif %}
                {% set counter = counter + 1 %}
            {% endfor %}

        </div>
    </nav>
</div>
<script type="text/javascript">
    $('[data-selector="pill_entry"]').click(function (event) {
        if(!$(event.currentTarget).children().attr('data-toggle') || $(event.currentTarget).children().attr('data-toggle') == 'false') {
            return;
        }
        if ($(event.currentTarget).children().attr('data-selector') == 'on') {
            $(event.currentTarget).children().attr('data-selector', 'off').removeClass("btn-primary-pill").addClass("btn-light-pill")
        } else {
            $('[data-selector="pill_entry"]').children().attr('data-selector', 'off').removeClass("btn-primary-pill").addClass("btn-light-pill")
            $(event.currentTarget).children().attr('data-selector', 'on').removeClass("btn-light-pill").addClass("btn-primary-pill")
        }
    });

    $('#burgerMenu').on('show.bs.collapse', function () {
        $('#burgerMenuIcon').removeClass('fa-bars').addClass('fa-times');
    });

    $('#burgerMenu').on('hide.bs.collapse', function () {
        $('#burgerMenuIcon').removeClass('fa-times').addClass('fa-bars');
    });
</script>
<style>
.other-pills {

}

.other-pills a {
    width: 100%;
}
</style>
