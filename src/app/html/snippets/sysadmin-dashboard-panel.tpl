{% for panel in panels %}
<li class="portlet-card" id="easy2schedule-card" data-selector="easy2schedule-card">
    <div class="card card-dashboard-app bg-light flex-grow-1">
        <div class="media justify-content-between">
            <div class="d-flex justify-content-start">
                <div class="img-app-icon"><img src="{{panel.icon_src}}" alt="">
                </div>
                <div class="card-title">{{panel.title | safe}}</div>
            </div>
        </div>
        <div class="media-body">
            <p class="tx-medium">
                {{panel.subtitle | safe}}
            </p>
            <div class="">
                {% for link in panel.links %}
                <div data-permission='{{ link.permission }}' class="d-flex align-items-center pb-2 hidden">
                    <i class="{{link.iconclasses}} tx-dark-blue tx-16 mr-2"></i>
                    <a id="{{ link.id }}" href="{{ link.href }}" class="tx-dark-blue">
                        {{ link.title  | safe}}
                    </a>
                </div>
                {% endfor %}

            </div>
        </div>
    </div>
</li>
{% endfor %}