window.tour_config = {
    header: "Touren im Kontaktmanager",
    intro: "Schnellstarthilfe für den planB-Kontaktmanager",
    tours: {
        firstSteps: {
            id: "firstSteps",
            title: "Erste Schritte im Kontaktmanager",
            desc: "Lerne die Oberfläche des Kontaktmanagers kennen.",
            startPage: "/contact-manager/index.html",
            startStep: 1,
            prevTour: null,
            nextTour: "newContact",
            permission: "contacts"
        },
        newContact:
        {
            id: "newContact",
            title: "Neue Kontakte erstellen und löschen",
            desc: "Lerne, wie man einen neuen Kontakt anlegen und wieder löschen kann.",
            startPage: "/contact-manager/index.html",
            startStep: 8,
            prevTour: "firstSteps",
            nextTour: "editContact",
            permission: "contacts"
        },
        editContact:
        {
            id: "editContact",
            title: "Kontakte bearbeiten",
            desc: "Lerne, wie man einen bestehenden Kontakt bearbeiten kann.",
            startPage: "/contact-manager/index.html",
            startStep: 17,
            prevTour: "newContact",
            nextTour: null,
            permission: "contacts"
        }
    },
    tourSteps: [
        {//1
            title: 'Willkommen',
            disableInteraction: true,
            intro: 'Willkommen in der <strong>planB-Kontaktverwaltung</strong>.<br/> Diese Tour führt dich durch die Oberfläche der Kontaktverwaltung.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>'
        },
        {//2
            title: 'Kontaktliste',
            element: '#list_container',
            disableInteraction: true,
            intro: 'Die Kontaktverwaltung hat zwei wesentliche Inhaltselemente. Auf der linken Seite wird dir die Liste aller für dich sichtbaren Kontakte angezeigt.',
            'myBeforeChangeFunction': function (PbGuide) {
            },
        },
        {//3
            title: 'Suche',
            element: '#search_input',
            disableInteraction: true,
            intro: 'Über den Kontakten befindet sich ein Eingabefeld mit dem du die Kontakt-Liste durchsuchen kannst. Wenn du nach etwas suchst, werden nur noch die Kontakte angezeigt, in denen der Suchbegriff vorkommt. Um wieder alle Einträge angezeigt zu bekommen, leere einfach das Suchfeld wieder. Die Suche startet automatisch bei jeder Änderung, die du am Suchbegriff vornimmst.'
        },
        {//4
            title: 'Kontakt anlegen',
            element: '#createContactButton',
            disableInteraction: true,
            intro: 'Wenn du dieses Plus-Symbol anklickst, kannst du einen neuen Kontakt anlegen. Hierfür wird im rechten Bereich eine Eingabemaske geöffnet.'
        },
        {//5
            title: 'Detail-Ansicht',
            element: '#azContactBody',
            disableInteraction: true,
            intro: 'Auf der rechten Seite der Kontaktverwaltung befindet sich die Detailansicht. Hier werden dir die Daten eines ausgewählten Kontaktes angezeigt bzw. das Formular zum ändern oder eintragen eines neuen Kontaktes.'
        },
        {//6
            title: 'Detail-Ansicht',
            element: '#contactInfoHeader',
            disableInteraction: true,
            intro: 'Im oberen Bereich werden dir, je nachdem ob du einen Kontakt gerade anzeigst, editierst oder neu anlegst, Optionen zum editieren, speichern oder löschen angezeigt.',
            'myBeforeChangeFunction': function (PbGuide) {
                $("#createContactButton").click()
            },
        },
        {//7
            title: 'Tour abgeschlossen',
            intro: 'Damit hast du unseren Rundgang abgeschlossen. Wenn du möchtest, können wir aber auch gleich mit der nächsten Tour weitermachen. Du kannst diese aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong>',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("contactmanager.firstSteps")
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.firstSteps.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            },
        },
        {//8
            title: 'Kontakt anlegen',
            intro: 'Diese Tour zeigt dir interaktiv, wie du ganz einfach einen <strong>Kontakt anlegen</strong> und wieder <strong>löschen</strong> kannst.<br/> Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>'
        },
        {//9
            title: 'Kontakt anlegen',
            element: '#createContactButton',
            intro: 'Um einen neuen Kontakt anzulegen, klicke jetzt bitte auf das <strong>+</strong>-Symbol.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()

                let object = document.querySelector('#createContactButton');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//10
            title: 'Neuen Kontakt anlegen',
            element: '#azContactBody',
            intro: 'Hier siehst du Felder, die zu einem Kontakt gespeichert werden können.<br/>Fülle diese jetzt bitte aus und klicke dann auf <strong>Weiter</strong>',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setNextButton("Weiter")
            },
        },
        {//11
            title: 'Neuen Kontakt speichern',
            element: '#save_contact_button',
            intro: 'Klicke jetzt bitte auf den <strong>Speichern</strong>-Button. Keine Sorge, wie man den Kontakt wieder löscht, erklären wir dir auch gleich.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()

                let object = document.querySelector('#save_contact_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//12
            title: 'Neuer Kontakt gespeichert',
            disableInteraction: true,
            intro: 'Dir sollte jetzt eine Erfolgsmeldung angezeigt werden, und der neue Kontakt sollte in der Liste angezeigt werden.'
        },
        {//13
            title: 'Kontakt auswählen',
            element: '#list_container',
            intro: 'Jetzt wollen wir den Kontakt wieder löschen. Klicke den Kontakt dafür bitte in der Liste an. Rechts sollten dir dann die Daten des Kontaktes angezeigt werden.'
        },
        {//14
            title: 'Kontakt löschen',
            element: '#delete_contact_button',
            intro: 'Klicke jetzt bitte den <strong>Löschen</strong>-Button und bestätige dass du den Kontakt löschen möchtest.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#delete_contact_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.exit()
                        },
                        {once: true}
                    )
                }

                document.addEventListener("command", function (event) {
                    PbGuide.introguide.goToStepNumber(15).start()
                },
        {once: true})
            }
        },
        {//15
            title: 'Kontakt gelöscht',
            element: 'body',
            intro: 'Der Kontakt wurde gelöscht und aus der Liste der Kontakte entfernt.'
        },
        {//16
            title: 'Tour abgeschlossen',
            intro: 'Sehr gut! Damit hast du die Tour zum Anlegen und Löschen von Kontakten abgeschlossen. Wenn du möchtest, können wir aber auch gleich mit der nächsten Tour weitermachen. Du kannst diese aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong>',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("contactmanager.firstSteps")
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.newContact.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            },
        },
        {//17
            title: 'Kontakt bearbeiten',
            intro: 'Diese Tour zeigt dir interaktiv, wie du ganz einfach einen <strong>Kontakt bearbeiten</strong> kannst.<br/> Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>'
        },
        {//18
            title: 'Kontakt auswählen',
            element: '#list_container',
            intro: 'Um einen Kontakt zu bearbeiten, wähle diesen bitte zuerst in der Liste aus. Rechts sollten dir dann die Daten des zu bearbeitenden Kontaktes angezeigt werden.'
        },
        {//19
            title: 'Kontakt bearbeiten',
            element: '#edit_contact_button',
            intro: 'Klicke jetzt bitte den <strong>Editieren</strong>-Button an.'
        },
        {//20
            title: 'Daten aktualisieren',
            element: '#azContactBody',
            intro: 'Jetzt kannst du alle Daten des Kontaktes ändern. Wenn du damit fertig bist, klicke bitte auf <strong>Weiter</strong>',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setNextButton("Weiter")
            },
        },
        {//21
            title: 'Änderungen speichern',
            element: '#save_contact_button',
            intro: 'Um die Änderungen jetzt abzuspeichern, klicke bitte auf den <strong>Speichern</strong>-Button',
            'myBeforeChangeFunction': function(PbGuide) {
                PbGuide.hideNextButton()

                let object = document.querySelector('#save_contact_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//22
            title: 'Daten aktualisiert',
            element: '#list_container',
            intro: 'Sobald das System die Daten erfolgreich gespeichert hat, wird die Liste aktualisiert und deine Änderungen sollten sichtbar sein.',
            'myBeforeChangeFunction': function (PbGuide) {},
        },
        {//23
            title: 'Tour abgeschlossen',
            intro: 'Sehr gut! Damit hast du die Tour zum Bearbeiten von Kontakten abgeschlossen.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("contactmanager.editContact")
                PbGuide.hidePreviousButton()

                // PbGuide.setNextButton("Weiter", function () {
                //     let nextTour = PbGuide.tourConfig.tours.firstSteps.nextTour
                //     let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                //     PbGuide.introguide.goToStepNumber(nextStartStep)
                // })
            },
        },
    ]
}
