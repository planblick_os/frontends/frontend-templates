window.tour_config = {
    header: "Admin-Touren im Kontaktmanager",
    intro: "Schnellstarthilfe für die Einstellungen des planB-Kontaktmanagers",
    tours: {
        firstStepsFields: {
            id: "firstStepsFields",
            title: "Eingabefelder verwalten",
            desc: "Lerne die Oberfläche zum Konfigurieren der Eingabefelder eines Kontakts kennen.",
            startPage: "/contact-manager/field-settings.html",
            startStep: 1,
            prevTour: null,
            nextTour: "fieldDetails",
            permission: "frontend.contactmanager.settings.list_order"
        },
        fieldDetails: {
            id: "fieldDetails",
            title: "Eingabefelder Details",
            desc: "Lerne, was die verschiedenen Einstellungen eines Feldes bedeuten und wie du diese ändern kannst.",
            startPage: "/contact-manager/field-settings.html",
            startStep: 12,
            prevTour: "firstStepsFields",
            nextTour: "listSettings",
            permission: "frontend.contactmanager.settings.list_order"
        },
        listSettings:
            {
                id: "listSettings",
                title: "Listen-Ansicht konfigurieren",
                desc: "Lerne, wie man die Ansicht der Kontaktliste konfigurieren kann.",
                startPage: "/contact-manager/list-settings.html",
                startStep: 27,
                prevTour: "fieldDetails",
                nextTour: "guest_access",
                permission: "frontend.contactmanager.settings.entries"
            },
        guest_access:
            {
                id: "guest_access",
                title: "Gast-Zugriff",
                desc: "Lerne, wie der Gast-Zugriff funktioniert und wofür man diesen verwenden kann.",
                startPage: "/contact-manager/guest-settings.html",
                startStep: 36,
                prevTour: "listSettings",
                nextTour: null,
                permission: "frontend.contactmanager.settings"
            }
    },
    tourSteps: [
        {//1
            title: 'Eingabefelder verwalten',
            element: '.nav-link.active',
            disableInteraction: true,
            intro: `Diese Tour führt dich durch die Oberfläche der Eingabefeld-Konfigurationsseite der Kontaktverwaltung.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.`,
            'myBeforeChangeFunction': function (PbGuide) {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.firstStepsFields.startPage, "firstStepsFields")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//2
            title: 'Eingabefelder Listenansicht',
            element: '#list-view',
            disableInteraction: true,
            intro: `In der Listenansicht werden dir alle Felder angezeigt, die derzeit für einen Kontakt angegeben werden können. Die Reihenfolge der Einträge in der Liste entspricht dabei der Reihenfolge in der die Eingabefelder eines Kontaktes angezeigt werden.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//3
            title: 'Listenansicht Spalten',
            element: '.typcn.typcn-arrow-unsorted',
            disableInteraction: true,
            intro: `Wenn du auf dieses Feld klickst und die Maustaste gedrückt hältst, kannst du das Feld verschieben und so die Reihenfolge der Felder verändern.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//3
            title: 'Listenansicht Spalten',
            element: '.col-4',
            disableInteraction: true,
            intro: `Die Spalte <strong>Feldbeschriftung</strong> legt fest, was neben dem Eingabefeld als Bezeichnung steht. Ggf. wird diese Bezeichnung noch über die automatische Übersetzung aktualisiert, du kannst aber auch einfach deutsche Bezeichnungen angeben.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//5
            title: 'Listenansicht Spalten',
            element: '.col-2',
            disableInteraction: true,
            intro: `Die Spalte <strong>Feldformat</strong> zeigt an, um welche Art von Feld es sich handelt. "Text" und "Textfeld" sind dabei die einfachste Form. Ausserdem gibt es "Telefonnummer", "E-Mail" und "Datum". Diese drei Feldtypen haben erweiterte Funktionen. So wird beim Feldtyp "Datum" beispielsweise automatisch ein Datepicker für das Feld eingeblendet. "Email" und "Telefon" werden in der Detailansicht als Links angezeigt und sind per Klick aufrufbar.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//6
            title: 'Listenansicht Spalten',
            element: '.col-3',
            disableInteraction: true,
            intro: `Die Spalte <strong>Feld-ID</strong> darf jeweils nur genau einmal vorkommen. Sie dient als interner Bezeichner für das Feld und wird z.B. auch in Exporten mit ausgegeben, um das Feld eindeutig zu benennen.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//7
            title: 'Listenansicht Optionen',
            element: '[data-selector="btn-field-edit"]',
            disableInteraction: true,
            intro: `Mittels eines Klicks auf diese Schaltfläche kannst du die Daten des zugehörigen Feldes ändern. Beachte dabei bitte unbedingt, dass eine Änderung der ID des Feldes zur Folge hat, dass alle bisher in das Feld eingebenen Daten nicht mehr abrufbar sind bis die jeweilige ID wieder verwendet wird (für das selbe oder ein anderes Feld).`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//8
            title: 'Listenansicht Optionen',
            element: '[data-selector="btn-field-delete"]',
            disableInteraction: true,
            intro: `Mittels eines Klicks auf diese Schaltfläche kannst du ein Feld löschen. Bereits gespeicherte Daten für das Feld werden zwar nicht mehr angezeigt, bleiben aber zunächst erhalten. Sobald der Kontakt das nächste Mal ohne das Feld abgespeichert wird, werden auch die Daten des Feldes gelöscht.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//9
            title: 'Neues Eingabefeld',
            element: '#field_template_selection',
            disableInteraction: true,
            intro: `Um ein neues Feld anzulegen, wähle hier bitte zunächst aus, was für ein Feld angelegt werden soll. Für viele Felder haben wir bereits Vorlagen, die man nur auswählen muss. Um komplett eigene Felder anzulegen, wähle bitte eines der beiden generischen Felder ganz unten aus.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//10
            title: 'Neues Eingabefeld',
            element: '#new_field_button',
            disableInteraction: true,
            intro: `Um das ausgewählte Eingabefeld zu erstellen, klicke bitte hier. Bei Auswahl einer Vorlage kannst du im nachfolgenden Dialog einfach Speichern auswählen. Ausserdem kannst du in diesem noch das verwendete Icon auswählen, und einstellen, ob es sich um ein Pflichtfeld handelt. Beide Einstellungen findest du, wenn du "Erweiterte Optionen" im Dialog öffnest.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//11
            title: 'Eingabefelder verwalten',
            disableInteraction: true,
            element: '#help',
            intro: `Prima, damit hast du die Einführung in die Oberfläche der Eingabefeld-Verwaltung abgeschlossen.<br/>Wenn Du möchtest, machen wir gleich weiter und sehen uns einmal die Detail-Einstellungen eines Feldes an. Du kannst diese Tour aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong> `,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.firstStepsFields.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            }
        },
        {//12
            title: 'Eingabefelder Details',
            disableInteraction: true,
            intro: `Diese Tour zeigt dir <strong>Interaktiv</strong>, wie du die Einstellungen eines Feldes ändern kannst.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.`,
            'myBeforeChangeFunction': function (PbGuide) {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.fieldDetails.startPage, "fieldDetails")){
                    PbGuide.introguide.exit()
                }
            }
        },
        {//13
            title: 'Eingabefelder Details',
            disableInteraction: true,
            intro: `Wir erklären die Feldeinstellungen am Beispiel eines neuen Feldes. Beim Bearbeiten eines Feldes stehen aber exakt die gleichen Möglichkeiten zur Wahl.`,
            'myBeforeChangeFunction': function (PbGuide) {

            }
        },
        {//14
            title: 'Eingabefelder Details',
            element: "#field_template_selection",
            intro: `Wähle bitte jetzt als erstes ein Format für das neue Feld aus. Welches Format kannst du selbst entscheiden. Weiter geht's sobald du eine Auswahl getroffen hast.`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#field_template_selection');
                if (object) {
                    object.addEventListener('change', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//15
            title: 'Eingabefelder Details',
            element: "#new_field_button",
            intro: `Klicke jetzt bitte auf <strong>neues Feld anlegen</strong>`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#new_field_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//16
            title: 'Eingabefelder Details',
            element: "#detail-view",
            disableInteraction: true,
            intro: `In dieser Maske kannst du nun die Eigenschaften des neuen Feldes festlegen. Die Bedeutung der Felder <strong>Feldformat</strong>, <strong>Feld-ID</strong> und <strong>Feldbeschriftung</strong> haben wir dir bereits in der ersten Tour erklärt.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//17
            title: 'Erweiterte Optionen',
            element: "#headingOne",
            intro: `Noch nicht ausführlich erklärt haben wir aber die Einstellungen die unter <strong>Erweiterte Optionen</strong> gemacht werden können. Klicke bitte jetzt auf das hervorgehobende Feld, um die <strong>Erweiterten Optionen</strong> anzuzeigen.`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#headingOne');
                if (object) {
                    object.addEventListener('click', e => {
                            window.setTimeout(() => PbGuide.introguide.nextStep(), 500)
                        },
                        {once: true}
                    )
                }
            }
        },
        {//18
            title: 'Erweiterte Optionen',
            element: "#collapseOne",
            disableInteraction: true,
            intro: `Du kannst hier für das Eingabefeld ein Icon auswählen, welches sowohl in der Detailansicht eines Kontakts als auch in der Bearbeitungsmaske vor dem Eingabefeld angezeigt wird.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//19
            title: 'Erweiterte Option: Icon',
            element: "#edit_field_icon_button",
            intro: `Klicke bitte jetzt auf den hervorgehobenen Link, um ein anderes Icon auszuwählen.`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#edit_field_icon_button');
                if (object) {
                    object.addEventListener('click', e => {
                            window.setTimeout(() => PbGuide.introguide.nextStep(), 800)
                        },
                        {once: true}
                    )
                }
            }
        },
        {//20
            title: 'Erweiterte Option: Icon',
            element: "#modal-icon",
            disableInteraction: false,
            position: "left",
            intro: `Wähle jetzt bitte ein Icon aus, indem du darauf klickst. `,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('body');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//21
            title: 'Erweiterte Option: Icon',
            element: "#modal-icon",
            position: "left",
            intro: `Bestätige deine Auswahl bitte, indem du auf <strong>Auswahl übernehmen</strong> klickst.`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#update_icon_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//22
            title: 'Erweiterte Option: Icon',
            element: "#temp_field_icon",
            intro: `Das von dir ausgewählte Icon wird zur Kontrolle hier noch einmal angezeigt.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//23
            title: 'Pflichtfelder',
            intro: `Die zweite Erweiterte Option ermöglicht es dir, ein Eingabefeld zum Pflichtfeld zu machen. Das heißt, ein Kontakt kann nicht gespeichert werden, solange das Feld nicht ausgefüllt wurde. `,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//24
            title: 'Pflichtfelder',
            element: '#field_required_value',
            intro: `Um ein Eingabefeld zum Pflichtfeld zu machen, setze bitte hier ein Häkchen.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//25
            title: 'Erweiterte Optionen',
            element: '#cancel_field_button',
            intro: `Als letztes musst du die Einstellungen des Feldes noch abspeichern, indem du auf die <strong>speichern</strong>-Schaltfläche klickst. Da wir das neue Feld jetzt nicht wirklich erstellen möchten, klicke dieses Mal bitte auf <strong>abbrechen</strong>.`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#cancel_field_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//26
            title: 'Erweiterte Optionen',
            element: '#help',
            intro: `Super. Damit hast die Tour zu den <strong>erweiterten Optionen abgeschlossen.</strong><br/>Wenn Du möchtest, machen wir gleich weiter und sehen uns einmal die Einstellungen zur Listenansicht an. Du kannst diese Tour aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong> `,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.fieldDetails.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            }
        },
        {//27
            title: 'Listenansicht',
            disableInteraction: true,
            intro: `Diese Tour führt dich durch die Oberfläche der Listenansicht-Konfigurationsseite der Kontaktverwaltung.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.`,
            'myBeforeChangeFunction': function (PbGuide) {
                if (!PbGuide.ensureUrl(PbGuide.tourConfig.tours.listSettings.startPage, "listSettings")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//28
            title: 'Listenansicht',
            element: '.az-content-body',
            disableInteraction: true,
            intro: `Mit den Einstellungen der Listenansicht kannst du beeinflussen, welche Felder in welcher Reihenfolge in der Kontaktliste dargestellt werden sollen.`,
            'myBeforeChangeFunction': function (PbGuide) {

            }
        },
        {//29
            title: 'Listenansicht',
            element: '.az-content-body',
            disableInteraction: true,
            intro: `Die beiden angezeigten Zeilen stehen für die beiden Zeilen, die für jeden Kontakt in der Listenansicht dargestellt werden.`,
            'myBeforeChangeFunction': function (PbGuide) {

            }
        },
        {//30
            title: 'Listenansicht',
            element: '#edit_list_view_button',
            intro: `Um die Felder, die angezeigt werden sollen, zu ändern, klicke auf bearbeiten.`,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()
                let object = document.querySelector('#edit_list_view_button');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            }
        },
        {//31
            title: 'Listenansicht',
            element: '#title-one',
            intro: `Um z.B. anstelle des Nachnamens ("Lastname") z.B. den Firmennamen ("Company") als erstes in der Liste der Kontakte anzuzeigen, wähle hier einfach <strong>Company</strong> aus.`,
            'myBeforeChangeFunction': function (PbGuide) {

            }
        },
        {//32
            title: 'Listenansicht',
            disableInteraction: true,
            intro: `Auf diese Weise kannst du später für beide Zeilen jeweils vier Felder auswählen, die dann entsprechend der ausgewählten Position in der Kontaktliste angezeigt werden. Um einen Platz leer zu lassen, wähle einfach den Eintrag <strong>Platz x</strong> aus der Auswahliste aus.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//33
            title: 'Listenansicht',
            disableInteraction: true,
            intro: `Um die Änderungen zu speichern, müsstest du dann noch einmal auf den Button <strong>speichern</strong> klicken. Um nicht versehentlich deine jetzigen Änderungen zu übernehmen, haben wir die Schaltfläche aber momentan deaktiviert.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//34
            title: 'Listenansicht',
            disableInteraction: true,
            intro: `Um die Änderungen zu verwerfen, kannst du auf die <strong>abbrechen</strong>-Schaltfläche klicken und die Rückfrage bestätigen.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//35
            title: 'Listenansicht',
            element: '#help',
            disableInteraction: true,
            intro: `Bravo, das war die Tour zu den Listenansicht Einstellungen.<br/>Wenn Du möchtest, machen wir gleich weiter und sehen uns an, was der Gast-Zugriff ist. Du kannst diese Tour aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong> `,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })

                PbGuide.setNextButton("Weiter", function () {
                    let nextTour = PbGuide.tourConfig.tours.listSettings.nextTour
                    let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                    PbGuide.introguide.goToStepNumber(nextStartStep)
                })
            }
        },
        {//36
            title: 'Gast-Zugriff',
            element: '.nav-link.active',
            disableInteraction: true,
            intro: `In dieser Tour erklären wir dir, was der Gast-Zugriff ist, und wofür du diesen verwenden kannst.`,
            'myBeforeChangeFunction': function (PbGuide) {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.guest_access.startPage, "guest_access")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//37
            title: 'Gast-Zugriff',
            element: '.az-content-body',
            disableInteraction: true,
            intro: `Mit Hilfe des Gast-Zugriffs kannst du anderen Personen ermöglichen, dass diese selbstständig ihre Kontaktdaten eingeben können. Damit stellst du sicher, dass bei der Erfassung der Daten möglichst keine Missverständnisse entstehen wie z.B. falsch verstandene Telefonnummern.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//38
            title: 'Gast-Zugriff',
            element: '.az-content-body',
            disableInteraction: true,
            intro: `Wenn du den Gast-Zugriff aktivierst, wird dir eine URL sowie ein QR-Code angezeigt. Wenn die andere Person nun die URL aufruft oder den QR-Code scannt, wird diese Person auf ein Formular geleitet, in das sie selbst, z.B. am Handy ihre Daten eintragen kann.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//39
            title: 'Gast-Zugriff',
            element: '.az-content-body',
            disableInteraction: true,
            intro: `Ausnahmsweise zeigen wir dir hier aber (noch) nicht, wie das funktioniert, da der Gast-Zugriff momentan noch nicht wieder deaktiviert werden kann.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//39
            title: 'Gast-Zugriff',
            element: '.az-content-body',
            disableInteraction: true,
            intro: `Wenn du dir jetzt nicht sicher bist, ob der Gast-Zugriff für dich relevant ist, melde dich bitte einfach bei uns im Forum oder wende dich an den Support und wir beraten dich gerne.`,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        },
        {//39
            title: 'Das Ende',
            disableInteraction: true,
            intro: `Hervorragend! Wenn du das liest, hast du die aktuell letzte Tour erfolgreich beendet.<br/>Wir hoffen, wir konnten dir etwas helfen beim Einstieg in die Verwaltung des Kontaktmanagers. <br/><br/>Viel Spaß weiterhin. `,
            'myBeforeChangeFunction': function (PbGuide) {
            }
        }
    ]
}