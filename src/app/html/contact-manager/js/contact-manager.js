﻿import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbEventEntries} from "../../easy2schedule/js/pb-event-entries.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]";


export class ContactManager {
    fieldconfig = []
    contacts_list = []
    active_contact = undefined
    active_contact_data_owners = []
    visible_contacts_list = []
    data_owner_toremove = []
    possible_groups = []
    group_members = []
    groupsandmembers = []

    constructor() {
        if (!ContactManager.instance) {
            ContactManager.instance = this
            ContactManager.instance.ee = new pbEventEntries();
        }

        return ContactManager.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            let arr = [ContactManager.instance.fetchFieldConfig(), ContactManager.instance.fetchContactList(), ContactManager.instance.ee.init()]
            Promise.all(arr)
                .then(function (res) {
                    let arr2 = [ContactManager.instance.setGroupMembers(), ContactManager.instance.setPossibleGroups()]
                    Promise.all(arr2).then(function () {
                        ContactManager.instance.setSortedGroupsandMembers()
                        resolve()
                    })
                })
                .catch(function (err) {
                    reject(err)
                }) // This is executed
        })

    }

    setActiveContactById(contact_id) {
        ContactManager.instance.active_contact = undefined
        if (!contact_id) {
            ContactManager.instance.active_contact = undefined
        } else {
            for (let contact of ContactManager.instance.contacts_list) {
                if (contact.contact_id == contact_id) {
                    ContactManager.instance.active_contact = contact
                    break
                }
            }
        }
    }

    getActiveContactId(contact_id) {
        if (ContactManager.instance.active_contact) {
            return ContactManager.instance.active_contact.contact_id
        } else {
            return false
        }
    }

    setFieldConfig(response) {
        ContactManager.instance.fieldconfig = response.data
    }

    fetchFieldConfig() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/contacts/fieldconfig",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": function (response) {
                    ContactManager.instance.setFieldConfig(response)
                    console.log("Fetched config", response)
                    //resolve(response)
                },
                "complete": resolve

            };

            $.ajax(settings)
        })
    }

    updateFieldConfig() {
        console.log("Updated config", ContactManager.instance.fieldconfig)
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateContactConfiguration",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "contact_configuration_id": "system",
                    "data": ContactManager.instance.fieldconfig
                }),
                "success": (response) => {
                    resolve(response)
                },
                "error": () => reject(),
            };

            $.ajax(settings)
        })
    }

    addFieldToConfig(newFieldObject) {
        console.log("Add FIEld", newFieldObject)
        ContactManager.instance.fieldconfig.push(newFieldObject)
        console.log("Expanded config", ContactManager.instance.fieldconfig)
    }

    removeFieldFromConfig(id) {
        if (id != undefined) {
            console.log("In remove method", typeof id, id)
            console.log("REMOVE method before", ContactManager.instance.fieldconfig)
            for (var i = 0; i < ContactManager.instance.fieldconfig.length; i++) {
                if (ContactManager.instance.fieldconfig[i]["field_id"] === id) {
                    ContactManager.instance.fieldconfig.splice(i, 1);
                    i--;
                }
            }
            console.log("REMOVE method after", ContactManager.instance.fieldconfig)
        }

    }

    updateField(updatedFieldObject) {
        ContactManager.instance.fieldconfig.forEach(element => {
            if (element.field_id == updatedFieldObject.field_id) {
                element = updatedFieldObject
            }
        });
    }

    setContactList(response) {
        ContactManager.instance.contacts_list = response

        //find out from the config settings, which details are to be shown in the list view
        let tmp_titles = {}
        let tmp_subtitles = {}

        for (let configEntry of this.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
            if (configEntry.display_pos_in_list_h2 > 0) {
                tmp_subtitles[configEntry.display_pos_in_list_h2] = configEntry.field_id
            }
        }

        ContactManager.instance.contacts_list.forEach(entry => {
            entry.data.displayTitle = this.formatContactTitle(entry, tmp_titles);
            entry.data.displaySubtitle = this.formatContactSubtitle(entry, tmp_subtitles);
        });
    }

    formatContactTitle(configEntry, tmp_titles) {
        let contactTitleString = ""

        for (let titlefield_key of Object.keys(tmp_titles)) {
            if (configEntry.data[tmp_titles[titlefield_key]]) {
                contactTitleString += configEntry.data[tmp_titles[titlefield_key]] + " "
            }
        }

        return contactTitleString
    }

    formatContactSubtitle(configEntry, tmp_subtitles) {
        let contactSubtitleString = ""

        for (let titlefield_key of Object.keys(tmp_subtitles)) {
            if (configEntry.data[tmp_subtitles[titlefield_key]]) {
                contactSubtitleString += configEntry.data[tmp_subtitles[titlefield_key]] + " "
            }
        }

        return contactSubtitleString
    }

    fetchContactList(include_archive = false, only_archived = false) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + `/contacts/list?include_archived=${include_archive ? 1 : 0}&only_archived=${only_archived ? 1 : 0}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": function (response) {
                    ContactManager.instance.setContactList(response)
                    resolve(response)
                },
            }

            $.ajax(settings)
        })
    }

    setNewContact(data) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewContact",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(data),

            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                resolve(response)
            });
        })
    }

    setContact(contact) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateContact",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(contact),
            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                resolve(response)
            });
        })
    }

    deleteContact(contact_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteContact",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({"contact_id": contact_id}),
            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                resolve(response)
            });
        })
    }

    archiveContact(contact_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/archiveContact",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "contact_id": ContactManager.instance.active_contact.contact_id
                }),
                "success": resolve,
                "error": reject
            };

            $.ajax(settings)
        })
    }

    restoreContact(contact_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/restoreContact",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "contact_id": contact_id
                }),
                "success": resolve,
                "error": reject
            };

            $.ajax(settings)
        })
    }


    addDataOwners() {
        let payload = {
            "contact_id": ContactManager.instance.active_contact.contact_id,
            "owners": ContactManager.instance.active_contact_data_owners
        }
        console.log("DATA OWNERS ADDED", ContactManager.instance.active_contact_data_owners)
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/contactAddDataOwner",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "data": payload
                }),
                "error": reject,
                "success": (response) => resolve(response)
            };

            $.ajax(settings)
        });


    }

    deleteDataOwner(data_owner_toremove) {
        let payload = {
            "contact_id": ContactManager.instance.active_contact.contact_id,
            "owners": [data_owner_toremove]
        }
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/contactRemoveDataOwner",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "data": payload
                }),
                "error": reject,
                "success": (response) => resolve(response)
            };


            $.ajax(settings)
        });
    }

    setPossibleGroups() {
        ContactManager.instance.possible_groups = ContactManager.instance.ee.possible_groups
    }

    getPossibleGroups() {
        return ContactManager.instance.possible_groups
    }

    setGroupMembers() {
        ContactManager.instance.group_members = ContactManager.instance.ee.group_members
    }

    getGroupMembers() {
        return ContactManager.instance.group_members
    }

    setSortedGroupsandMembers() {
        let sorted_groups = ContactManager.instance.getPossibleGroups()
        sorted_groups.sort((a, b) =>
            a.toLowerCase() > b.toLowerCase() ? 1 : -1
        );

        let sorted_members = ContactManager.instance.getGroupMembers()
        sorted_members.sort((a, b) =>
            a.toLowerCase() > b.toLowerCase() ? 1 : -1
        );
        ContactManager.instance.groupsandmembers = sorted_groups.concat(sorted_members);
    }

    getSortedGroupsandMembers() {
        return ContactManager.instance.groupsandmembers
    }

    setActiveContactDataowners(contact_id) {
        ContactManager.instance.active_contact_dataowners = []
        if (!contact_id) {
            ContactManager.instance.active_contact_data_owners = []
        } else {
            for (let contact of ContactManager.instance.contacts_list) {
                if (contact.contact_id == contact_id) {
                    ContactManager.instance.active_contact_data_owners = contact.data_owners
                    break
                }
            }
        }
    }

    addDataOwner(new_dataowner) {
        ContactManager.instance.active_contact_dataowners.push(new_dataowner)
    }

    setDataOwnerToRemove(data_owner) {
        ContactManager.instance.data_owner_toremove.push(data_owner)
    }

    fetchContactsHistoryFromApi(contact_id = undefined) {
        if (!contact_id) {
            contact_id = ContactManager.instance.active_contact.contact_id
        }
        let event_types = ["newContactCreated", "contactUpdated", "contactDeleted"]
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + `/es/event/history?search=${ContactManager.instance.active_contact.contact_id}&event_types=${event_types.join(",")}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => resolve(response)
            }

            $.ajax(settings)
        })
    }
}