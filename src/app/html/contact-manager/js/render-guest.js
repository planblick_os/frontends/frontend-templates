import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]";
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]";
import {getQueryParam} from "../../csf-lib/pb-functions.js?v=[cs_version]"

export class RenderGuest {

    constructor() {
        if (!RenderGuest.instance) {
            RenderGuest.instance = this;
            RenderGuest.instance.initListeners()
        }

        return RenderGuest.instance;
    }


    renderContactGuestDetailEditor(field_config) {
        $("#contactInfoBodyContainer").html("")
        if (field_config) {
            for (let [key, entry] of Object.entries(field_config)) {
                RenderGuest.instance.addRowToGuestForm(entry)
            }
        }
    }

    addRowToGuestForm(entry) {
        console.log("field_config", entry)
        let disable_field = false
        let field_class = "form-control detail-inputs wd-sm-400 wd-md-300"
        let newField = $("#contactInfoBodyDetail").clone()
        let field_value = ""

        let newInput = RenderGuest.instance.renderGuestInputField(entry.input_type, entry.field_id, entry.name, entry.optional, field_value, disable_field, field_class)
        newField.attr("id", "fieldentry_" + entry.field_id)
        newInput = ($(newInput).prop('outerHTML'))

        let fieldContent = newField.html().replace("[{contact-detail-label}]", entry.name).replace("[{contact-detail-field}]", newInput).replace("[{contact-detail-icon}]", RenderGuest.instance.renderIcon(entry.css_classes))

        newField.html(fieldContent)
        $("#contactInfoBodyContainer").append(newField)
    }


    renderGuestInputField(input_type, id, name, optional, value, disable_field, field_class) {
        let field_type = input_type
        let field_id = id
        let field_name = name
        let field_required = false

        if (optional == false) {
            field_required = true
        }

        let field_value = value
        let common_attr = {
            'id': field_id,
            'name': field_name,
            'value': field_value,
            'class': field_class,
            'required': field_required,
            'disabled': disable_field
        }

        if (input_type == "textarea") {
            let field = $('<textarea>').attr(common_attr).text(field_value)
            return field
        } else {
            let field = $('<input>').attr(common_attr).attr({
                'type': field_type,
            })
            return field
        }


    }

    renderIcon(icon_class) {
        icon_class = icon_class[0]
        let icon = ""

        //catch wrong icon format but allow other icon types
        if (icon_class.startsWith("fa-") == true) {
            icon_class = "fas fa-address-card"
        }

        icon = `<i class="${icon_class}"></i>`
        return icon
    }

    getDetailsFromForm(field_config = undefined) {
        let details = {}
        if (field_config) {
            for (let entry of field_config) {
                details[entry.field_id] = $("#" + entry.field_id).val()
            }
            return details
        }

    }

    initListeners() {
        let socket = new socketio()
        socket.commandhandlers["contactVerifiedSuccessfully"] = function (args) {

            if(args.task_id==getQueryParam("corid")){
                $('#success-display-message').prop('hidden', false)
            }
            
        }

        socket.commandhandlers["contactVerifiedFailed"] = function (args) {
            // Backend says verification failed for whatever reasons
            if(args.task_id==getQueryParam("corid")){
                $('#error-display-message').prop('hidden', false)
            }
            
        } 



    }
}