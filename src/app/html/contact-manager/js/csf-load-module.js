import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {Saga, SagaStep} from "../../csf-lib/pb-saga.js?v=[cs_version]"
import {
    getCookie,
    fireEvent,
    showNotification,
    getQueryParam,
    get_news_for,
    renderNews
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {ContactManager} from "./contact-manager.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {RenderConfig} from "./render-config.js?v=[cs_version]"
import {RenderGuest} from "./render-guest.js?v=[cs_version]"
import {QrCodeLinker} from "../../csf-lib/pb-qrcodelinker.js?v=[cs_version]"
import {ContactManagerGuest} from "./contact-manager-guest.js?v=[cs_version]"
import {RenderGuestConfirmation} from "./render-contact-confirmation.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {RenderArchived} from "./render-archived.js?v=[cs_version]";


export class ContactManagerModule {
    actions = {}
    config = []

    constructor() {
        if (!ContactManagerModule.instance) {
            ContactManagerModule.instance = this
            ContactManagerModule.tagmanager = undefined
            this.initListeners()
            this.initActions()
            $.unblockUI()

        }

        return ContactManagerModule.instance
    }

    initActions() {
        this.actions["contactManagerGuestInitialised"] = function (myevent, callback = () => "") {
            //if corid is set in url, we are on the confirmation-page
            let socket = new socketio()
            let guest_renderer = new RenderGuest()

            if (getQueryParam("corid")) {

                console.log("Before wait circle")
                Fnon.Wait.Circle("Ihre Daten werden bestätigt....", {
                    position: 'center-top',
                    fontSize: '14px',
                    width: '300px'
                })
                let mySaga = new Saga(socket)


                Fnon.Wait.Remove(5000)


                mySaga.setTaskId(getQueryParam("corid")).fastForward(99).then(function () {
                    console.log("Confirmation status received...")

                    console.log("Listeners active....")
                    /* Fnon.Wait.Remove() */
                })
            } else {

                let apikey = getCookie('apikey')

                let cmg = new ContactManagerGuest()

                cmg.fetchConfigAsGuest(apikey).then(function () {
                    guest_renderer.renderContactGuestDetailEditor(cmg.config)
                    ContactManagerModule.instance.config = cmg.config

                }).catch()

                if ($("#loader-overlay")) {
                    $("#loader-overlay").hide()
                    $("#contentwrapper").show()
                }
            }


        }

        this.actions["contactManagerInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("contacts")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    if ($("#loader-overlay")) {
                        $("#loader-overlay").hide()
                        $("#contentwrapper").show()
                    }
                    new ContactManager().init().then(function () {
                        callback(myevent)

                    })
                }

            })

        }

        this.actions["contactManagerListView"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("frontend.contactmanager.views")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new PbTagsList().load().then(function (tagsList) {
                        ContactManagerModule.instance.tags = tagsList
                        new Render(ContactManager.instance, ContactManagerModule.instance.tags).initial_load_list_view()
                    }).catch(() => {
                        $("#contactInfoHeader").remove()
                        $("#createContactButton").attr("disabled", "disabled")
                        $("#createContactButton").attr("title", new pbI18n().translate("Disabled to prevent data-loss due to an error while page load. Please reload the page and try again."))
                        $("#tag_container").html(new pbI18n().translate("Not available"))
                        new PbNotifications().ask("We were unable to retrieve tags. To prevent data-loss, editing and adding contacts is disabled. Please reload this page as soon as possible.", "Unable to load tags", () => "", () => location.reload(true), "warning", "Okay", "Reload")
                        new Render(ContactManager.instance, ContactManagerModule.instance.tags).initial_load_list_view()
                    }).then(() => {
                        Render.instance.includesQueryParam()
                        if (getQueryParam("new") != undefined) {
                            $("#createContactButton").click()
                        }
                    })
                    new PbGuide("./tours/user_getting_started.js")
                    callback(myevent)
                }
            })
            callback(myevent)
        }

        this.actions["contactManagerListArchivedView"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("frontend.contactmanager.views.archive")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    import('./render-archived.js?v=[cs_version]').then(() => {
                        ContactManager.instance.fetchContactList(false, true).then(function ([]) {
                            new RenderArchived(ContactManager.instance, []).initial_load_list_archived()
                        })
                    })

                    callback(myevent)
                }
            })
            callback(myevent)
        }

        this.actions["contactManagerConfigurationView"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                    if (!new PbAccount().hasPermission("frontend.contactmanager.views")) {
                        new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                    } else {
                        new PbGuide("./tours/admin_getting_started.js")

                        if ($("#loader-overlay")) {
                            $("#loader-overlay").hide()
                            $("#contentwrapper").show()
                        }

                        new RenderConfig().renderFieldList()
                        new RenderConfig().fillSelectPosition()
                        new RenderConfig().preselectValuesForPositionView()
                        callback(myevent)
                    }
                }
            )
            callback(myevent)
        }

        // added action locally so that template logout path is correct
        this.actions["logoutButtonClicked"] = function (myevent, callback = () => "") {
            new PbAccount().logout()
            callback(myevent)
        }
    }

    initListeners() {
        let socket = new socketio()
        socket.commandhandlers["refreshContactsAfterRestore"] = function (args) {
            ContactManager.instance.fetchContactList(false, true).then(function () {
                new RenderArchived(ContactManager.instance, []).initial_load_list_archived()
            })
        }

        socket.commandhandlers["refreshContacts"] = function (args) {
            if (Render.instance?.currentlyShownHistoryId && Render.instance?.currentlyShownHistoryId == args.contact_id) {
                ContactManager.instance.fetchContactsHistoryFromApi(Render.instance?.currentlyShownHistoryId).then((response) => {
                    fireEvent("showHistoryData", response)
                })
            }

            if (ContactManager.instance) {
                let active_contact_id = ContactManager.instance.getActiveContactId()
                //console.log("REFRESHING CONTACTS")
                new ContactManager().fetchContactList().then(function () {
                    new Render().refreshContacts()

                    if (Render.instance.pending_tasks.includes(args.task_id)) {
                        if (args.contact_id) {
                            //console.log("IF CLAUSE IN REFRESH CONTACTS")
                            ContactManager.instance.setActiveContactById(args.contact_id)
                            Render.instance.renderContactDetailEditor(args.contact_id)
                            Render.instance.highlightSelectedListItem(args.contact_id)
                            if (Render.instance.pending_data_owner_delete_tasks.includes(args.task_id)) {
                                Render.instance.editMode()
                            }


                        } else {
                            //console.log("ELSE CLAUSE IN REFRESH CONTACTS")
                            ContactManager.instance.setActiveContactById()
                            Render.instance.resetDetailViewContainer()
                        }

                    } else {
                        if (ContactManager.instance.active_contact != undefined && ContactManager.instance.active_contact.contact_id != undefined) {
                            ContactManager.instance.setActiveContactById()
                            Render.instance.resetDetailViewContainer()
                        } else if (ContactManager.instance.active_contact != undefined && ContactManager.instance.active_contact.contact_id == args.contact_id) {
                            let confirmation_result = confirm("Die Daten für diesen Kontakte haben sich geändert. Möchten sie die Ansicht aktualisieren (nicht gespeicherte Änderungen gehen verloren)?")
                            if (confirmation_result == true) {
                                Render.instance.renderContactDetailEditor(ContactManager.instance.active_contact.contact_id)
                                Render.instance.highlightSelectedListItem(ContactManager.instance.active_contact.contact_id)
                            }
                        } else if (ContactManager.instance.active_contact != undefined) {
                            Render.instance.highlightSelectedListItem(ContactManager.instance.active_contact.contact_id)

                        }
                    }
                    if (active_contact_id) {
                        ContactManager.instance.setActiveContactById(active_contact_id)
                        if (ContactManager.instance.active_contact) {
                            Render.instance.renderContactDetailEditor(active_contact_id)
                            Render.instance.highlightSelectedListItem(active_contact_id)
                        }
                    }
                })
            }

        }

        socket.commandhandlers["refreshContactConfiguration"] = function (args) {
            ContactManager.instance.fetchFieldConfig().then(function () {
                console.log("New Render")
                RenderConfig.instance.renderFieldList()
            })
        }

        socket.commandhandlers["contactVerifiedSuccessfully"] = function (args) {
            console.log("args", args)
            $('#success-display-message').prop('hidden', false)
            alert("Contact was successfully verified in csf")
        }

        socket.commandhandlers["contactVerifiedFailed"] = function (args) {
            // Backend says verification failed for whatever reasons
            console.log("args", args)
            $('#error-display-message').prop('hidden', false)
            alert("Contact verification failed csf")
        }

        $("#logout_button").click(function (event) {
            fireEvent("logoutButtonClicked", {
                "login": getCookie("username"),
                "consumer": getCookie("consumer_name")
            })
        })

        $(document).on('click', '#createContactButton', function (e) {
            e.preventDefault()
            $("#history-list-container").html("")
            ContactManager.instance.setActiveContactById()
            new Render().renderContactDetailEditor()
            $('.az-contact-item').removeClass('selected')
            $('.az-contact-star').removeClass('active')

        })

        $(document).on('click', '.az-contact-item', function (e) {
            ContactManager.instance.setActiveContactById(e.currentTarget.id)
            new Render().highlightSelectedListItem(e.currentTarget.id)
            $('body').addClass('az-content-body-show')
            ContactManager.instance.setActiveContactById(e.currentTarget.id)
            new Render().renderContactDetailEditor(e.currentTarget.id)
        })

        $(document).on('click', '#save_contact_button', function (e) {
            e.preventDefault()
            new Render().saveContactDetails()
        })


        $(document).on('click', '#delete_contact_button', function (e) {
            e.preventDefault()
            let deleteTitle = new pbI18n().translate("Löschen")
            let deleteQuestion = new pbI18n().translate("Möchten Sie den Kontakt wirklich löschen?")

            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {

                    ContactManager.instance.deleteContact(ContactManager.instance.active_contact.contact_id).then(
                        function (response) {
                            Render.instance.pending_tasks.push(response.task_id)
                        }
                    )

                    $('.az-contact-item').removeClass('selected')
                    Fnon.Hint.Success('Kontakt gelöscht')
                }
            })

        })

        $(document).on('click', '#archive_contact_button', function (e) {
            e.preventDefault()
            let archiveTitle = new pbI18n().translate("Archivieren")
            let archiveQuestion = new pbI18n().translate("Möchten Sie den Kontakt wirklich archivieren?")

            Fnon.Ask.Danger(archiveQuestion, archiveTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    ContactManager.instance.archiveContact(ContactManager.instance.active_contact.id).then(
                        (response) => {
                            console.info("Archive command for contact was sent successfully", response)
                        }
                    ).catch((params) => {
                        console.info("Error while sending command to archive contact", params)
                    })
                }
            })
        })

        $("#search_input").keyup(function (e) {
            e.preventDefault()
            Render.instance.sort_contact_list("asc", $("#search_input").val())
        })

        $("#search_input_archive").keyup(function (e) {
            e.preventDefault()
            RenderArchived.instance.sort_contact_list("asc", $("#search_input_archive").val())
        })

        $(document).on('click', '#edit_contact_button', function (e) {
            e.preventDefault()
            Render.instance.editMode()
        })

        $(document).on('click', '#cancel_edit_contact_button', function (e) {
            e.preventDefault()
            let deleteTitle = new pbI18n().translate("Sie haben nicht gespeichert.")
            let deleteQuestion = new pbI18n().translate("Möchten Sie wirklich abbrechen? Alle Änderungen gehen verloren.")

            Fnon.Ask.Warning(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    if (getQueryParam("r") != undefined) {
                        location.href = getQueryParam("r")
                    }
                    if (ContactManager.instance.active_contact) {
                        Render.instance.renderContactDetailEditor(ContactManager.instance.active_contact.contact_id)
                    } else {
                        Render.instance.resetDetailViewContainer()
                    }
                }

            })


            console.log("ACTIVE CONTACT", ContactManager.instance.active_contact)

        })


        $(document).on('click', "[data-listener='password_change_click']", function (e) {
            e.preventDefault();
            if (!new PbAccount().hasPermission("dashboard.management.change_own_password")) {
                showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum ändern ihres Passworts."))
            } else {
                $.blockUI({message: $('#password_change_modal')});
            }

        })

        $(document).on('click', ".delete", function (event) {
                event.preventDefault()
                if (event.target.parentNode.dataset.value.includes('group') || event.target.parentNode.dataset.value.includes('user')) {
                    if (ContactManager.instance.active_contact != undefined) {
                        let deleteTitle = new pbI18n().translate("Delete")
                        let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the dataowner?")
                        Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                            if (result == true) {
                                ContactManager.instance.deleteDataOwner(event.target.parentNode.dataset.value).then(response => {
                                    return new Promise((resolve, reject) => {
                                        Render.instance.pending_tasks.push(response.task_id)
                                        Render.instance.pending_data_owner_delete_tasks.push(response.task_id)
                                        resolve()
                                    })
                                        .then(() => {
                                            Fnon.Hint.Success('Freigabe gelöscht')
                                            Render.instance.editMode()
                                            Render.instance.addEntriesToAccessrightsDropdown(Render.instance.groupsandmembers)
                                        })

                                })
                            } else {
                                Render.instance.renderContactDetailEditor(ContactManager.instance.active_contact.contact_id)
                                Render.instance.editMode()
                            }
                        })
                    } else {
                        Fnon.Hint.Warning('Kontakt existiert noch nicht!')
                    }
                }
            }
        )

        $(document).on('click', '#activate_contact_guest_form', function (e) {
            e.preventDefault()
        })

        $(document).on('click', "#save_contact_guest_button", async e => {
            e.preventDefault()
            let data = new RenderGuest().getDetailsFromForm(ContactManagerModule.instance.config)

            let external_id = crypto.randomUUID();
            let step1 = new SagaStep()
            step1.setName("step_create_contact")
            step1.setEventName("createNewContact")
            step1.setOrder(0)
            step1.setPayload(data, {"external_id": external_id})


            let step2 = new SagaStep()
            step2.setName("step_start_contact_verification_process")
            step2.setEventName("verifyContact")
            step2.setOrder(1)
            step2.setPayload({}, {"external_id": external_id})
            console.log("Step2 data:", typeof step2.getJSON(), step2.getJSON())


            let step3 = new SagaStep()
            step3.setName("step_finish_contact_verification_process")
            step3.setEventName("contactVerified")
            step3.setOrder(2)
            step3.setPayload({}, {"external_id": external_id})
            console.log("Step2 data:", typeof step3.getJSON(), step3.getJSON())


            let step4 = new SagaStep()
            step4.setName("step_create_login")
            step4.setEventName("createLogin")
            step4.setOrder(3)
            step4.payload = {
                "username": "",
                "password": "randomgenerated",
                "email": "",
                "roles": [
                    "role.guest_user"
                ],
                "links": [
                    {
                        "resolver": "contact-manager",
                        "type": "contact",
                        "external_id": external_id
                    }
                ]
            }

            let socket = new socketio()
            let mySaga = new Saga(socket)
            mySaga.setSagaName('test_saga')
            mySaga.addStep(step1)
            mySaga.addStep(step2)
            mySaga.addStep(step3)

            let callback_on_server_message = function (response) {
                mySaga.fastForward("step_start_contact_verification_process")
            }
            mySaga.create(callback_on_server_message)

            Fnon.Hint.Success('Kontaktdaten hinterlegt. Du erhälst in Kürze eine Email, um deine Daten zu verifizieren:')


        })

        $(document).on('click', '#activate_contact_guest_form', function (e) {
            e.preventDefault()
            let name = "Contact-Form-Guests"
            let value = CONFIG.APP_BASE_URL + "/contact-manager/guest_add_contact.html"
            let guest_login = true
            let type = "url"
            let error_correct = "H"
            let kind = "default"
            let logo_url = ""
            let qrcode = new QrCodeLinker(name, value, socket, guest_login, type, error_correct, kind, logo_url).createQRCode().then((response) => console.log("RESPONSE", response))
        })
    }


}