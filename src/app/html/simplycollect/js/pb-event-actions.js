import {PbBaseEventActions} from "./pbjs/pb-base-event-actions.js?v=[cs_version]"
import {
    getCookie,
    setCookie,
    getQueryParam,
    getLocaleFormattedTime, fireEvent, makeId
} from "./pbjs/pb-functions.js?v=[cs_version]"
import {pbApp} from "./pb-app.js?v=[cs_version]";
import {PbVideoconference} from "./pbjs/pb-videoconference.js?v=[cs_version]"
import {pbShoppingSession} from "./pbjs/pb-shoppingsession.js?v=[cs_version]"
import {pbI18n} from "./pbjs/pb-i18n.js?v=[cs_version]"
import {socketio} from "./pbjs/pb-socketio.js?v=[cs_version]";
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]";


export class PbEventActions extends PbBaseEventActions {
    constructor() {
        self
        super();
        this.initActions()
        if (!PbEventActions.instance) {
            PbEventActions.instance = this;
            self = PbEventActions.instance
        }
        new pbApp()

        return PbEventActions.instance;
    }

    initActions() {
        PbEventActions.instance.conference = null
        document.addEventListener("i18nLanguageLoaded", function _(event) {
            let logoImageSrc = new pbI18n().translate("merchantLogoImageSrc")
            console.log("LOGO", logoImageSrc)
            $('.merchant-logo').attr("src", logoImageSrc)
            if ("paypal-me-address" != new pbI18n().translate("paypal-me-address")) {
                $('#sendPaypalMeLink').show()
            }
        })

        $('#customer_queue_wrapper').bind('DOMSubtreeModified', function () {
            $('#queue_length').html(($('.queueEntryContainer').length - 1)) // Subtract one to account for the template
            $('#queue_length_tab').html(($('.queueEntryContainer').length - 1))// Place the value on the tab too
            $('.queueEntryAcceptButton').off('click')
            $('.queueEntryAcceptButton').on('click', function (event) {
                event.stopPropagation();
                event.stopImmediatePropagation();
                jQuery('#bell').prop("muted", true);
                let srid = event.target.getAttribute("data-srid")
                new pbShoppingSession().acceptedBy(srid, getCookie("username"))
                new pbShoppingSession().connectToConference(srid)
                $(event.target).next().show()
                // $('.queueEntryContainer[data-srid="' + srid + '"]').css("background-color", "#3bb001")
                $('.queueEntryContainer[data-srid="' + srid + '"]').removeClass('new')
                $('.queueEntryContainer[data-srid="' + srid + '"]').addClass('selected')


            });

            $('.queueEntryCloseButton').off('click')
            $('.queueEntryCloseButton').on('click', function (event) {
                event.stopPropagation();
                event.stopImmediatePropagation();
                jQuery('#bell').prop("muted", false);
                new pbShoppingSession().quit("Abgelehnt", event.target.getAttribute("data-srid"))
                $(event.target).hide()
            });
        });

        this.actions["init"] = function (myevent, callback = () => "") {
            console.log("INIT")

            new pbI18n().init()
            $("#language_change_test").on("click", function (event) {
                new pbI18n().getTranslations("en")
            })

            $("#language_change_en").on("click", function (event) {
                new pbI18n().getTranslations("en")
            })
            $("#language_change_de").on("click", function (event) {
                new pbI18n().getTranslations("de")
            })


            $('*[data-dynamicvalue="username"]').html(getCookie("username"))

            if (getQueryParam("mid")) {
                $(window).off('beforeunload');
                $(window).bind('beforeunload', function () {
                    new pbShoppingSession().aboutToLeave()
                    new PbVideoconference().hangup()
                    var myInterval = window.setInterval(function () {
                        new pbShoppingSession().reconnectToConference()
                        clearInterval(myInterval)
                    }, 5000)
                    //return 'Willst Du die Seite wirklich verlassen?';
                });

                $("#sessionTopicModal").modal()

                let merchantOnlineTimer = setTimeout(function () {
                    fireEvent("merchantIsOffline", {"timeout_id": merchantOnlineTimer}, getCookie("consumer_name"))
                }, 3000)
                fireEvent("isMerchantOnline", {"timeout_id": merchantOnlineTimer}, getCookie("consumer_name"))
            }

            callback(myevent)
        }

        this.actions["initMerchant"] = function (myevent, callback = () => "") {
            if (!new PbAccount().hasPermission("simplycollect.merchant_view")) {
                $("#loader-overlay").show()
                $("#contentwrapper").hide()
                new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))

            } else {
                new PbAccount().handlePermissionAttributes()
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
                callback(myevent)
            }

            setInterval(function () {
                fireEvent("merchantStatus", {
                    "online": true,
                    "queue_length": $('#queue_length').html()
                }, getCookie("consumer_name"))
            }, 5000)
        }

        this.actions["merchantStatus"] = function (myevent, callback = () => "") {
            $('#queueEntries').html(myevent.data.queue_length)
        }

        this.actions["isMerchantOnline"] = function (myevent, callback = () => "") {
            fireEvent("merchantIsOnline", myevent.data, getCookie("consumer_name"))
        }

        this.actions["merchantIsOnline"] = function (myevent, callback = () => "") {
            clearTimeout(myevent.data.timeout_id)
            $("#waitingForSessionMessage").show()
            $("#merchantNotOnlineMessage").hide()
            let merchant = getCookie("consumer_name")
            let customerName = getCookie("username")
            let customerInterest = ""
            var interval = window.setInterval(function () {
                customerInterest = $("#session_topic").val()
                new pbShoppingSession().request(merchant, customerName, customerInterest, interval)
            }, 5000)

            new pbShoppingSession().request(merchant, customerName, customerInterest, interval)

        }

        this.actions["customerInRoomIsLeaving"] = function (myevent, callback = () => "") {
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').css("background-color", "#ff0000")
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
            window.setTimeout(function () {
                $('.last_status[data-srid="' + myevent.data.room_id + '"]').html("vermutlich gegangen")
                $('.queueEntryCloseButton[data-srid="' + myevent.data.room_id + '"]').show()
                new pbShoppingSession().cleanUp(false, false)
            }, 15000)
            callback(myevent)
        }

        this.actions["customerInRoomHasLeft"] = function (myevent, callback = () => "") {
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').css("background-color", "#ff0000")
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
            window.setTimeout(function () {
                $('.last_status[data-srid="' + myevent.data.room_id + '"]').html("gegangen")
            }, 5000)
            callback(myevent)
        }

        this.actions["shopSessionQuit"] = function (myevent, callback = () => "") {
            console.log("QUIT", myevent.data.room_id, new pbShoppingSession())

            new pbShoppingSession().cleanUp()

            $('[data-srid="' + myevent.data.room_id + '"]').remove()
            $('#queue_length').html(($('.queueEntryContainer').length - 1)) // Subtract one to account for the template
            $('#queue_length_tab').html(($('.queueEntryContainer').length - 1)) // Place the value on the tab too
            $('.in_session_with').html("niemand") //clear active customer name in page header

            callback(myevent)
        }

        this.actions["newShopSessionRequest"] = function (myevent, callback = () => "") {
            let connection_lost_message = "hatte verbindung verloren"
            let waiting_message = "wartet"
            if ($('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]') && $('.last_status[data-srid="' + myevent.data.room_id + '"]').html() != waiting_message) {
                $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').css("background-color", "initial")
                $('.last_status[data-srid="' + myevent.data.room_id + '"]').html(connection_lost_message)
            }

            let startedTime = 0
            let currentTime = Math.round((new Date()).getTime() / 1000)

            //$('[data-srid="' + myevent.data.room_id + '"]').remove()
            if ($('[data-srid="' + myevent.data.room_id + '"]').length == 0) {
                let customerName = decodeURIComponent(myevent.data.customer_name) == "false" ? "Kein Name angegeben" : decodeURIComponent(myevent.data.customer_name)
                let newEntry = $("#queueEntryTemplate").clone().css("display", "block").html().replaceAll("[Customer-Name]", customerName)
                newEntry = newEntry.replaceAll("[Customer-Interest]", decodeURIComponent(myevent.data.customer_interest))
                newEntry = newEntry.replaceAll("[Session-Room-Id]", decodeURIComponent(myevent.data.room_id))
                $("#customer_queue_wrapper").append(newEntry)
                startedTime = myevent.data.time

                var sound = document.getElementById("bell");
                if (sound) {
                    sound.src = "./sounds/doorbell.mp3"
                    sound.play();
                }

            } else {
                $('.customer-interest[data-srid="' + myevent.data.room_id + '"]').html(decodeURIComponent(myevent.data.customer_interest))
                startedTime = myevent.data.time
            }

            let delta = Math.floor((currentTime - startedTime) / 60)
            let statusColor = "#17a2b8 "
            if (delta == 0) {
                delta = "< 1"
            } else {
                statusColor = "#ffc107"
            }

            if (delta >= 5) {
                statusColor = "#dc3545"
            }
            if ($('.last_status[data-srid="' + myevent.data.room_id + '"]').html() == "")
                $('.last_status[data-srid="' + myevent.data.room_id + '"]').html(waiting_message)
            //$('.event-indicator[data-srid="' + myevent.data.room_id + '"]').attr("style", "background-color: " + statusColor + " !important")
            $('.waiting_time[data-srid="' + myevent.data.room_id + '"]').html(delta)
            $('.az-content-label[data-srid="' + myevent.data.room_id + '"]').attr("style", "color: " + statusColor + " !important")
        }

        this.actions["addToBasket"] = function (myevent, callback = () => "") {
            let table = $(myevent.data.table_selector).DataTable()
            console.log("EVENT addToBasket", myevent)

            table.row($("#" + myevent.data.new_row_id)).remove();
            table.row($("#sumrow")).remove()

            table.row.add(myevent.data.new_row_data).node().id = myevent.data.new_row_id
            let counter = 0
            let sum = 0
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                let data = this.data()
                data[0] = ++counter
                sum += parseFloat(data[5].replace(".", "").replace(",", "."))
                this.data(data)
            })
            sum = ["Summe", "", "", "", "", new Intl.NumberFormat('de-DE', {
                style: 'currency',
                currency: 'EUR'
            }).format(sum), ""]
            table.row.add(sum).node().id = "sumrow"
            table.draw()

            callback(myevent)
        }

        this.actions["removeFromBasket"] = function (myevent, callback = () => "") {
            let table = $(myevent.data.table_selector).DataTable()
            table.row($("#sumrow")).remove()

            console.log("EVENT removeFromBasket", myevent.data.table, myevent)
            table.row($("#" + myevent.data.rowid)).remove().draw();
            let counter = 0
            let sum = 0
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                let data = this.data()
                data[0] = ++counter
                sum += parseFloat(data[5].replace(".", "").replace(",", "."))
                this.data(data)
            })
            sum = ["Summe", "", "", "", "", new Intl.NumberFormat('de-DE', {
                style: 'currency',
                currency: 'EUR'
            }).format(sum), ""]
            table.row.add(sum).node().id = "sumrow"
            table.draw()
            callback(myevent)
        }

        this.actions["newChatMessage"] = function (myevent, callback = () => "") {
            if ($("#messageid_" + myevent.data.message_id).length == 0) {
                let template = ""
                let message = decodeURIComponent(myevent.data.from_username + ": " + myevent.data.message)

                if (myevent.data.from_username == getCookie("username")) {
                    template = $("#right_chat_message").children().first().clone()
                } else {
                    template = $("#left_chat_message").children().first().clone()
                }

                template.attr("id", "messageid_" + myevent.data.message_id)
                template.addClass("active-chat-message")
                template.html(template.html().replace(/\[MESSAGE\]/g, message).replace(/\[TIME\]/g, getLocaleFormattedTime()))

                $("#chat_messages_container").append(template)


            }
            $('#azCollectBody').scrollTop($('#azCollectBody').prop('scrollHeight'))

            callback(myevent)

        }

        this.actions["orderNoticeUpdate"] = function (myevent, callback = () => "") {
            $("#orderNoticeWrapper").show()
            let newcontent = myevent.data.newcontent.replace(/(?:\r\n|\r|\n)/g, '<br>');
            $("#orderNotices").html(newcontent)
            callback(myevent)
        }

        this.actions["orderSubmitted"] = function (myevent, callback = () => "") {
            $("#printOrderButton").show()
            callback(myevent)
        }

        this.actions["shopSessionAcceptedBy"] = function (myevent, callback = () => "") {
            let currentCustomer = $('.customer-name[data-srid="' + myevent.data.srid + '"]').html()
            let innerHTML = $('.inSessionWith[data-srid="' + myevent.data.srid + '"]').html()
            if (innerHTML) {
                innerHTML = innerHTML.replaceAll(myevent.data.supporter_name + ", ", "").replace(myevent.data.supporter_name, "")
                innerHTML += myevent.data.supporter_name + ", ".replace(/, $/g, '');
                $('.inSessionWith[data-srid="' + myevent.data.srid + '"]').html(innerHTML)
            }
            $('.last_status[data-srid="' + myevent.data.srid + '"]').html("im Gespräch")
            $('.in_session_with').html(currentCustomer)//set active customer name in page header
            if (myevent.data.supporter_name != getCookie("username")) {
                //$('.queueEntryContainer[data-srid="' + myevent.data.srid + '"]').css("background-color", "#1e6200")
            } else {
                fireEvent("orderNoticeUpdate", {
                    "newcontent": $('#order_notices').val(),
                    "sent_time": new Date(),
                    "message_id": makeId(10)
                }, new pbShoppingSession().roomName)

                let table = $("#basket").DataTable()
                let data = []
                table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (data["DT_RowId"] != "artnr_Summe") {
                        console.log("DATA", data)
                        data.push(this.data())
                    }
                })
                //Remove last element in list since this is the sum which is added automatically again

                for (let dat of data) {
                    console.log("DAT", dat)
                    if (!dat["DT_RowId"] || dat["DT_RowId"] == "artnr_Summe") {
                        continue
                    }
                    fireEvent("addToBasket", {
                        "table_selector": '#basket',
                        "new_row_id": dat["DT_RowId"],
                        "new_row_data": dat
                    }, new pbShoppingSession().roomName)
                }


            }
            new pbShoppingSession().start()
            $('#customerQueue').hide()


            callback(myevent)
        }


        //ToDo: Easy2Gether Functions, need to be extracted later on
        this.actions["initMeet"] = function (myevent, callback = () => "") {
            let room = "tmp_" + getQueryParam("sid")
            new socketio().joinRoom(room)


            $('#meetName').val(getCookie("username"))
            if (getCookie("meeting_username")) {
                $('#meetName').val(getCookie("meeting_username"))
            }


            console.log("InitMeet", myevent)
            new pbI18n().init()
            $('*[data-dynamicvalue="username"]').html(getCookie("username"))

            if (getQueryParam("sid")) {
                let customerName = getCookie("meeting_username") || getCookie("username")
                let conference = new PbVideoconference(getQueryParam("sid"), "#meet", customerName)
                    .setOptions(
                        customerName,
                        getQueryParam("sid"),
                        "#meet",
                        null,
                        null,
                        ['microphone', 'camera', 'desktop', 'fullscreen',
                            'fodeviceselection', 'settings',
                            'videoquality', 'filmstrip', 'stats', 'shortcuts',
                            'tileview', 'invite'])
                    .init()
                conference.api.addListener("videoConferenceJoined", function () {
                    fireEvent("initVideochatForParticipant", {"force": true}, room)
                    setInterval(function () {
                        fireEvent("initVideochatForParticipant", {}, room)
                    }, 5000)

                });
                $('#lobbyModeButton').click(function () {
                    if (!$('#lobbyModeButton').data("lockedState") || $('#lobbyModeButton').data("lockedState") == "false") {
                        conference.setLobbyMode(true)
                        $('#lobbyModeButton').data("lockedState", "true")
                        $('#lobbyModeButton').html(new pbI18n().translate("Unlock room"))
                    } else {
                        conference.setLobbyMode(false)
                        $('#lobbyModeButton').data("lockedState", "false")
                        $('#lobbyModeButton').html(new pbI18n().translate("Lock room"))
                    }
                })
                $('#meetName').keyup(function (e) {
                    conference.setDisplayName($('#meetName').val())
                    setCookie("meeting_username", $('#meetName').val())
                });
            }


            callback(myevent)

        }

        this.actions["initPart"] = function (myevent, callback = () => "") {
            let room = "tmp_" + getQueryParam("sid")
            new socketio().joinRoom(room)
        }

        this.actions["initVideochatForParticipant"] = function (myevent, callback = () => "") {
            if (!PbEventActions.instance.conference || (myevent.data.force && myevent.data.force == true)) {
                let customerName = getCookie("meeting_username") || getCookie("username")
                if (PbEventActions.instance.conference) {
                    PbEventActions.instance.conference.hangup()
                }
                setTimeout(function () {
                    PbEventActions.instance.conference = new PbVideoconference(getQueryParam("sid"), "#meet", customerName)
                        .setOptions(
                            customerName,
                            getQueryParam("sid"),
                            "#meet",
                            null,
                            null,
                            ['microphone', 'camera', 'desktop', 'fullscreen',
                                'fodeviceselection', 'settings',
                                'videoquality', 'filmstrip', 'stats', 'shortcuts',
                                'tileview', 'invite'])
                        .init()
                    $("#creatorNotReadyYet").hide()
                }, 1000)

            }

        }


        this.actions["protocolUpdate"] = function (myevent, callback = () => "") {
            console.log("PROTOCOL UPDATE", myevent)
            $('#preview').html(myevent.data.protocolContent);

            let editor = $('#example')[0]['data-froala.editor']
            if (editor) {
                if (myevent.data.editorName != getCookie("username")) {
                    $("#editorTypeStatus").html(myevent.data.editorName + " gibt gerade etwas ein...")
                    editor.html.set(myevent.data.protocolContent);
                    editor.edit.off()
                    if (this.typeTimeout) {
                        clearTimeout(this.typeTimeout)
                    }
                    this.typeTimeout = setTimeout(function () {
                        editor.edit.on()
                        $("#editorTypeStatus").html("&nbsp;")
                    }, 2000)

                }
            }
            callback(myevent)
        }
    }
}


