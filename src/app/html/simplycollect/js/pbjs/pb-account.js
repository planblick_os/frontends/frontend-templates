import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {getCookie, isEmail, makeId, showNotification} from "./pb-functions.js?v=[cs_version]";

export class PbAccount {
    logins = []

    constructor() {
        if (!PbAccount.instance) {
            PbAccount.instance = this;
        }

        return PbAccount.instance;
    }

    init() {
        return new Promise(function (resolve, reject) {
            PbAccount.instance.getLogins((response) => {
                PbAccount.instance.logins = response
                resolve(response)
            })
        })
    }

    requestAccess(appName, right = null) {

        let url = CONFIG.API_BASE_URL + "/access/" + appName
        if (right != null) {
            url += "/" + right
        }
        var requestHeaders = new Headers();
        requestHeaders.append("apikey", $.cookie("apikey"));

        var requestOptions = {
            method: 'GET',
            headers: requestHeaders,
            redirect: 'follow'
        };

        return fetch(url, requestOptions)
            .then(response => response.json())
            .then(result => {
                return result.has_access || false
            })
            .catch(error => console.log('error', error));
    }

    sendInvitationLink(email, success=()=>{showNotification("Invitation sent")}, error=(message)=>{showNotification("Invitation sending failed")}) {
        if(!isEmail(email)) {
            error("not an email address")
            return false;
        }

        var settings = {
            "url": CONFIG.API_BASE_URL + "/login_invite",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "apikey": $.cookie("apikey")
            },
            "data": JSON.stringify({"username": email}),
            "success": success,
            "error": error
        };

        $.ajax(settings).done(function (response) {

        });

    }

    startRegistrationProcess(login, password, email, successCallback, errorCallback) {
        var data = {"username": login, "email": email, "password": password}

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": CONFIG.API_BASE_URL + "/registration",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
            },
            "processData": false,
            "data": JSON.stringify(data),
            "success": (response) => successCallback(response),
            "error": (response) => errorCallback(response)
        }

        $.ajax(settings)
    }

    startLostPasswordProcess(login, successCallback, errorCallback) {
        var data = {"username": login}

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": CONFIG.API_BASE_URL + "/init_credentials_lost_process",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
            },
            "processData": false,
            "data": JSON.stringify(data),
            "success": (response) => successCallback(response),
            "error": (response) => errorCallback(response)
        }

        $.ajax(settings)
    }

    getLogins(callback = () => "") {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/get_users",
            "method": "GET",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "apikey": $.cookie("apikey")
            }
        };

        $.ajax(settings).done(function (response) {
            callback(response)
        });
    }

    deleteAccount(consumer_id, callback = () => "") {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/deleteAccount",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "apikey": $.cookie("apikey")
            },
            "data": JSON.stringify({"id": consumer_id}),
        };

        $.ajax(settings).done(function (response) {
            callback(response)
        });
    }

    deleteLogin(loginName, callback = () => "") {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/deleteLogin",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "apikey": $.cookie("apikey")
            },
            "data": JSON.stringify({"username": loginName}),
        };

        $.ajax(settings).done(function (response) {
            callback(response)
        });
    }

    createLogin(username, password, successCallback = () => {
        alert("Login creation successfull")
    }, errorCallback = () => {
        alert("Login creation failed")
    }) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/newlogin",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/json",
                "apikey": $.cookie("apikey"),
            },
            "data": JSON.stringify({"username": username, "password": password}),
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
            document.addEventListener("notification", function _(event) {
                if (event.data.data.args.correlation_id == response.taskid) {
                    if (event.data.data.event == "loginCreationFailed") {
                        errorCallback()
                        console.log("loginCreationFailed", event.data.data.response)
                    }
                    if (event.data.data.event == "loginCreated") {
                        successCallback()
                    }

                    document.removeEventListener("notification", _)
                }
            })
        });
    }

    createTemporaryLogin(valid_until_unix_timestamp) {
        let username = "guest_" + makeId(10) + "_" + valid_until_unix_timestamp
        let password = makeId(15)
        PbAccount.instance.createLogin(username, password)
        return {"username": username, "password": password}
    }

    checkLogin(isLoginPage = false, checkagainTime = 30000, health_endpoint = "/has_valid_login") {
        console.log("Checking login...")
        let url_for_redirect_after_login = CONFIG.LOGIN_URL + '?r=' + window.location.href
        let apikey = $.cookie("apikey");
        if (typeof (apikey) == "undefined") {
            let redirect_file = "index.html"
            let current_url = window.location.pathname;
            let current_filename = current_url.substring(current_url.lastIndexOf('/') + 1);

            if (redirect_file != current_filename) {
                $.blockUI({
                    message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>',
                    timeout: 3000,
                    baseZ: 9999999999999999,
                    onUnblock: function () {
                        $(location).attr('href', url_for_redirect_after_login)
                    }
                });
            }
        } else {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": CONFIG.API_BASE_URL + health_endpoint,
                "method": "GET",
                "headers": {
                    "apikey": $.cookie("apikey"),
                    "Cache-Control": "no-cache"
                },
                "success": function (response) {
                    //setTimeout(PbAccount.instance.checkLogin, checkagainTime, isLoginPage, checkagainTime);
                    if (isLoginPage) {
                        PbAccount.instance.redirectDialog()
                    }
                },
                "error": function (response) {
                    if (!isLoginPage) {
                        $.blockUI({
                            message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>',
                            timeout: 3000,
                            baseZ: 9999999999999999,
                            onUnblock: function () {
                                $(location).attr('href', url_for_redirect_after_login)
                            }
                        });
                    }
                }
            }
            $.ajax(settings).done();
        }
    }

    logout(redirect = null, message = null, justInvalidateKey=false) {
        let apikey = $.cookie("apikey");
        let result = false

        let postUrl = CONFIG.API_BASE_URL + "/logout";
        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('apikey', apikey);
            },
            success: function (response) {
                let realRedirect = '/login/'
                let timeout = 500
                if (redirect == false || redirect == null) {
                    redirect = "#"
                    timeout = 60000000
                }
                if (redirect.length > 0) {
                    realRedirect = redirect
                }
                let realMessage = "Sie wurden abgemeldet."
                if (message != null) {
                    realMessage = message
                }

                if (typeof (response) != "undefined") {
                    $.removeCookie("apikey", {path: '/'});
                    if(!justInvalidateKey) {
                        $.blockUI({
                            message: '<div class="responseMessage">' + realMessage + '</div>',
                            timeout: timeout,
                            onUnblock: function () {
                                $(location).attr('href', realRedirect)
                            }
                        });
                    }
                } else {
                    $.removeCookie("apikey", {path: '/'});
                    if(!justInvalidateKey) {
                        $.blockUI({message: '<div class="responseMessage">' + realMessage + '</div>'});
                    }

                }
            },
            error: function () {
                $.removeCookie("apikey", {path: '/'});
                if(!justInvalidateKey) {
                    $.blockUI({message: '<div class="responseMessage">' + message + '</div>'});
                }
            }
        }).done(function (o) {

        });


    }

    getapikey(doRedirect = true, successCallback = () => "") {
        let apikey = $.cookie("apikey");
        console.debug(apikey)
        let username = document.getElementById("login").value;
        let password = document.getElementById("password").value;

        $.blockUI({message: '<div class="responseMessage">Ihre Zugangsdaten werden überprüft ...</div>'});
        let postUrl = CONFIG.API_BASE_URL + "/login";

        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
            },
            success: function (response) {
                if (typeof (response) != "undefined") {
                    //console.debug(response)
                    $.blockUI({message: '<div class="responseMessage">Login successfull</div>', timeout: 1000});
                    $.cookie("apikey", response.apikey, {path: '/'});
                    $.cookie("consumer_name", response.consumer_name, {path: '/'});
                    $.cookie("consumer_id", response.consumer_id, {path: '/'})
                    $.cookie("username", response.username, {path: '/'});
                    //$(location).attr('href', 'beta.html')
                    if (doRedirect) {
                        let urlParams = new URLSearchParams(window.location.search)
                        if (urlParams.has("r")) {
                            PbAccount.instance.redirectDialog(urlParams.get("r"))
                        } else {
                            PbAccount.instance.redirectDialog(CONFIG.DEFAULT_LOGIN_REDIRECT)
                        }
                    } else {
                        $.blockUI({
                            message: '<div class="responseMessage">Successfull login</div>',
                            timeout: 3000
                        });
                    }
                    successCallback()
                } else {
                    $.removeCookie("apikey", {path: '/'});
                    $.unblockUI()
                    $.blockUI({
                        message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',
                        timeout: 3000
                    });
                }
            },
            error: function () {
                $.removeCookie("apikey", {path: '/'});
                $.unblockUI()
                $.blockUI({
                    message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',
                    timeout: 3000
                });

            }
        }).done(function (o) {

        });
    }

    getapikeyfor(username, password, callback) {
        let postUrl = CONFIG.API_BASE_URL + "/login";
        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
            },
            success: function (response) {
                //console.log("RESPONSE", response)
                if (typeof (response) != "undefined") {
                    callback(response.apikey)
                } else {
                    callback(response.apikey)
                }
            },
            error: function () {
                callback(false)
            }
        }).done(function (o) {

        });
    }

    redirectDialog(url = null) {
        if (url == null) {
            location.href = "/login/"
        } else {
            location.href = url
        }
    }
}