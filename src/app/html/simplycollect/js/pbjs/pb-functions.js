import {PbVoiceRecognition} from "./pb-voice-recognition.js?v=[cs_version]"
import {pbBatch} from "./pb-batch.js?v=[cs_version]"
import {pbSmartspeech} from "./pb-smartspeech.js?v=[cs_version]";
import {socketio} from "./pb-socketio.js?v=[cs_version]";

export function getLocaleFormattedTime(date = new Date(), hours=true, minutes=true, seconds=false, locale=navigator.language || navigator.browserLanguage || navigator.userLanguage || "de_DE", hour12=false) {
    let options = {
      hour12: hour12
    };
    if(hours)
        options["hour"]='numeric'
    if(minutes)
        options["minute"]='numeric'
    if(seconds)
        options["second"]='numeric'

    return new Intl.DateTimeFormat(locale, options).format(date)
}

export function getLocaleFormattedDate(date = new Date(), locale=navigator.language || navigator.browserLanguage || navigator.userLanguage || "de_DE", hour12=false) {
    let options = {
      year: 'numeric', month: 'numeric', day: 'numeric',
    };
    return new Intl.DateTimeFormat(locale, options).format(date)
}

export function getQueryParam(name) {
    let urlParams = new URLSearchParams(window.location.search)
    if (urlParams.has(name)) {
        return urlParams.get(name)
    } else {
        return undefined
    }

}

export function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export function checkPasswordStrength(password) {
    //Regular Expressions
    var regex = new Array();
    regex.push("[A-Z]"); //For Uppercase Alphabet
    regex.push("[a-z]"); //For Lowercase Alphabet
    regex.push("[0-9]"); //For Numeric Digits
    regex.push("[$@$!%*#?&]"); //For Special Characters

    var passed = 0;

    //Validation for each Regular Expression
    for (var i = 0; i < regex.length; i++) {
        if((new RegExp (regex[i])).test(password)){
            passed++;
        }
    }

    //Validation for Length of Password
    if(passed > 2 && password.length > 8){
        passed++;
    }

    return passed
}

export function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

export function showNotification(message) {
    alert(message)
}

export function parseJsonOrReturnData(data) {
    try {
        return JSON.parse(data);
    } catch (e) {
        return data;
    }
}

export function fireEvent(event_name, data = {}, transmitToRoom=false) {
    let evt = new Event(event_name);
    evt.data = parseJsonOrReturnData(data)
    console.log("Firing event: ", event_name, data)
    if ([].includes(event_name))
        console.trace();
    document.addEventListener(event_name, function _event_logger(event) {
        //console.log("Event caught: ", event_name, event.data)
        document.removeEventListener(event_name, _event_logger)
    }, false);
    document.dispatchEvent(evt);

    if(transmitToRoom) {
        new socketio().socket.emit("command", {
            command: "fireEvent",
            args: {"eventname": event_name, "eventdata": data},
            room: transmitToRoom
        })
    }

}

export function waitFor(condition, callback, recheckIntervall=250) {
    if (!condition()) {
        console.log('waiting for', condition);
        window.setTimeout(waitFor.bind(null, condition, callback), recheckIntervall); /* this checks the flag every 100 milliseconds*/
    } else {
        console.log('stop waiting for', condition);
        callback();
    }
}

export function speech(text, callback = () => "", init_voice = false) {
    if (!callback) {
        callback = () => ""
    }
    new pbSmartspeech().say(text, callback);
}

export function localspeech(text, callback = null, init_voice = false) {
    let potentialFileExtension = text.split(".").pop()
    console.log(potentialFileExtension)
    let supportedFileExtensions = ["wav", "mp3"]
    if (supportedFileExtensions.includes(potentialFileExtension)) {
        playSpeechWav(text, callback)
        return
    }

    let speechTextPotentialAudioFile = md5(text) + ".mp3"


    let stimmen = window.speechSynthesis.getVoices();

    for (var i = 0; i < stimmen.length; i++) {
        console.log("Stimme " + i.toString() + " " + stimmen[i].name);
    }
    var worte = new SpeechSynthesisUtterance(text);
    worte.voice = stimmen[2];
    worte.lang = "de-DE";
    if (callback != null) {
        worte.onend = callback
    }
    if (init_voice === false) {
        window.speechSynthesis.cancel();
        window.speechSynthesis.speak(worte);
    }
}

export function playSpeechWav(srcUrl, callback = null) {
    let soundFile = undefined
    let src = undefined
    if (document.getElementById("audioplayer")) {
        soundFile = document.getElementById("audioplayer")
    } else {
        soundFile = document.createElement("audio")
        soundFile.id = "audioplayer"
        soundFile.preload = "auto";
        src = document.createElement("source");
        src.id = "audioplayerSource"
        src.src = "";
        soundFile.appendChild(src);
    }

    src.src = srcUrl;
    soundFile.load();
    soundFile.currentTime = 0.01;
    soundFile.volume = 1.000000;
    soundFile.play();
    if (callback) {
        soundFile.onended = callback
    } else {
        soundFile.onended = () => ""
    }

}

export function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

export function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) {
        return decodeURIComponent(match[2])
    } else {
        return false;
    }
}

export function setCookie(name, value) {
    $.cookie(name, value)
}

export function fillFormFieldsInteractively(callback) {
    let form = document.getElementsByTagName("form")[0]

    if (form) {
        for (let element of Array.from(form.getElementsByTagName("input"))) {
            let id = element.id
            new pbBatch().add(id, function () {
                getValueForInputElement(element)
            })
            //let result = await getValueForInputElement(element)

            //element.value = result
        }
        new pbBatch().add("form_submit", function () {
            form.submit()
        }, true)
        // if(form) {
        //     Array.from(form.getElementsByTagName("input")).forEach(inputElement => {
        //         console.log("Input element:", inputElement.labels)
        //         //speech("Was soll ich für " + inputElement.labels[0].innerText + " als Wert einsetzen?")
        //         await getValueForInputElement(inputElement)
        //     })
        // }
    }
    //callback()
}

export async function getValueForInputElement(inputElement) {
    let promise = new Promise((resolve, reject) => {
        speech("Was soll ich für " + inputElement.labels[0].innerText + " als Wert einsetzen?", function () {
            new PbVoiceRecognition().startRecording({
                "type": "syncInputValueGathering",
                "data": {"element_id": inputElement.id}
            })
        })
    })
    return promise

}

export function makeId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

export function encrypt(value, passphrase) {
    return CryptoJS.AES.encrypt(value, passphrase)
}

export function decrypt(value, passphrase) {
    return CryptoJS.AES.decrypt(value, passphrase).toString(CryptoJS.enc.Utf8)
}

export function md5(value) {
    return CryptoJS.MD5(value).toString();
}

export function imagesEqual(imageElementA, imageElementB, tolerance = 245) {
    //console.log("using tolerance of: ", tolerance)
    return imagediff.equal(imageElementA, imageElementB, tolerance);

}

export function watchForChangesInVideo(videoElement, onChange = () => "", onNoChange = () => "", tolerance = 245, interval = 250) {
    setInterval(function () {
        _watchForChangesInVideo(videoElement, onChange, onNoChange(), tolerance, interval)
    }, interval)
}

function _watchForChangesInVideo(videoElement, onChange = () => "", onNoChange = () => "", tolerance, interval) {
    const canvas = document.createElement("canvas");
    // scale the canvas accordingly
    canvas.width = videoElement.videoWidth;
    canvas.height = videoElement.videoHeight;
    // draw the video at that frame
    canvas.getContext('2d').drawImage(videoElement, 0, 0, canvas.width, canvas.height);
    // convert it to a usable data URL
    const dataURL = canvas.toDataURL("image/jpeg", 1.0);
    var img = document.getElementById("cmpImage1") || document.createElement("img");
    img.id = "cmpImage1"
    img.src = dataURL;
    img.style.display = "none"
    if (!document.getElementById("cmpImage1")) {
        document.getElementById("mediaOptions").append(img)
    }

    let img2 = document.getElementById("cmpImage2")

    let isEqual = null
    if (img && img2) {
        isEqual = imagesEqual(img, img2, tolerance)
        //console.log("IMAGES", img, img2, img.src == img2.src, isEqual)
        if (!isEqual) {
            onChange(img2, img)
        } else {
            onNoChange(img2, img)
        }
    }

    setTimeout(function () {
        img2 = document.getElementById("cmpImage2") || document.createElement("img");
        img2.id = "cmpImage2"
        img2.src = dataURL
        img2.style.display = "none"
        if (!document.getElementById("cmpImage2")) {
            document.getElementById("mediaOptions").append(img2)
        }
    }, interval)

    return isEqual
}

export function enterFullscreen(element=document.documentElement) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  }
}
