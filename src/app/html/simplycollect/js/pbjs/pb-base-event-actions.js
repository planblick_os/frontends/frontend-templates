import {pbFacetracker} from "./pb-facetracker.js";
import {
    fireEvent,
    speech,
    fillFormFieldsInteractively,
    makeId,
    playSpeechWav,
    watchForChangesInVideo, getCookie
} from "./pb-functions.js?v=[cs_version]";
import {PbVoiceRecognition} from "./pb-voice-recognition.js";
import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {pbBatch} from "./pb-batch.js?v=[cs_version]";
import {pbMedia} from "./pb-media.js?v=[cs_version]";


export class PbBaseEventActions {
    actions = {}
    self = null
    constructor() {
        this.initBaseActions()
        if (!PbBaseEventActions.instance) {
            PbBaseEventActions.instance = this
            self = PbBaseEventActions.instance
        }

        return PbBaseEventActions.instance;
    }

    getAll() {
        return this.actions
    }

    getByName(name, callback = undefined) {
        if (!callback) {
            callback = function (myevent) {
            }
        }

        if (self.actions[name]) {
            let result = self.actions[name]
            result = function (myevent) {
                self.actions[name](myevent, callback);
            }

            return result
        } else {
            //console.warn("No Action defined for event '" + name + "'")
            let defaultFunc = function (myevent) {
                console.warn("No Action defined for event '" + name + "'");
                callback(myevent);
            }
            return defaultFunc
        }
    }

    setByName(name, myfunction) {
        this.actions[name] = myfunction
    }

    initBaseActions() {
        this.actions["testAction"] = function (myevent, callback = () => "") {
            $("#overlay").hide()
        }

        this.actions["scanForFaces"] = function (myevent, callback = () => "") {
            $("#overlay").hide()
            new pbMedia($("#mediaOptions").get(0), "videoWithAudioInput").init().start()
            new pbFacetracker(false).startTracking();

            callback(myevent)
        }

        this.actions["fillFormFields"] = function (myevent, callback = () => "") {
            new pbMedia().init().start(function () {
                fillFormFieldsInteractively(() => callback(myevent))
                $("#overlay").hide()
            })
        }
        this.actions["formFieldsFilled"] = function (myevent, callback = () => "") {
            callback(myevent)
        }

        this.actions["noAudioInWhenExpected"] = function (myevent, callback = () => "") {
            speech("Ich konnte Dich leider nicht verstehen. Sage abbrechen wenn Du nicht weitermachen möchtest.", function () {
                console.log("NOT UNDERSTAND EVENT", myevent)
                callback(myevent);
                fireEvent(myevent.type, myevent.data)
            });
        }
        this.actions["audioInputNotUnderstood"] = function (myevent, callback = () => "") {
            speech("Ich konnte Dich leider nicht verstehen.", function () {
                callback(myevent);
                fireEvent(myevent.type, myevent.data)
            });
        }

        this.actions["noIntentionFound"] = function (myevent, callback = () => "") {
            speech("Ich konnte Dich leider nicht verstehen.", function () {
                callback(myevent);
                fireEvent(myevent.type, myevent.data)
            });
        }

        this.actions["sayGoodbye"] = function (myevent, callback) {
            speech("Okay. Ich wünsche Dir noch einen schönen Tag.", function () {
                callback(myevent)
            });
        }

        this.actions["visitorAborted"] = function (myevent, callback) {
            speech("Okay, ich breche den Vorgang ab. Um neu zu Starten musst Du meinen Sichtbereich einmal verlassen.", function () {
                callback(myevent)
            });
        }
        this.actions["faceTrackerSetNewFace"] = function (myevent, callback) {
            let freezed_image = document.getElementById("freezedImage")
            freezed_image.src = ""
            setTimeout(() => pbFacetracker.instance.newFace = true, 3000)
        }
        this.actions["syncInputValueGathering"] = function (myevent, callback) {
            console.log("SYNC", myevent)
            if (!myevent["id"] || !myevent["value"]) {
                new pbBatch().restart(myevent["data"]["element_id"])
            } else {
                new pbBatch().resolve(myevent["id"])
                document.getElementById(myevent["id"]).value = myevent["value"]
            }
            callback(myevent)
        }

        this.actions["redirectToStartPage"] = function (myevent, callback = () => "") {
            location.href = CONFIG.LOGIN_URL
        }

        this.actions["loginSuccessfull"] = function (myevent, callback = () => "") {
            $("#overlay").hide()
            speech("Willkommen zurück.", function () {
                callback(myevent)
            });
        }


    }
}


