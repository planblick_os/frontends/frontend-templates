<form action="">
    <div id="wizard" class="wizard">
        <div class="steps">
            <ul class="nav nav-tabs" id="wizard_steps_overview_container">

            </ul>
        </div>
        <section class="content step-content">
            <!-- select product step 0 -->
            <section id="wizard-p-0" class="body">
                <!-- order title -->
                <h4 class="text-center" id="wizard_step_title"></h4>
                <p class="text-center" id="wizard_step_subtitle"></p>

                <div id="step_bodycontent" class="row">
                    { include 'dashboard/templates/orderwizard/step_1_bodycontent.tpl' }
                </div>
            </section>
        </section>
        <div id="step_footer" class="actions">

        </div>
    </div>
</form>