<div class="az-header">
    <div class="container"> <!-- change to container-fluid for full browser width-->
        <div class="az-header-left">
            {% if show_logo %}
                <a href="{{index_link}}" class="az-logo"><span></span>
                    <!-- logo for large screens -->
                    <img class="az-logo" src="{{logo}}" alt="{{title}}"/>
                </a>
            {% endif %}
                <a href="" id="azContentBodyHide" class="az-header-arrow d-md-none">
                    <i class="icon ion-md-arrow-back"></i>
                </a>
            {% if left_nav_links %}
                <a href="" id="azContentLeftShow" class="az-header-arrow d-block d-lg-none">
                    <i class="icon ion-md-arrow-back"></i>
                </a>
            {% endif %}
            {% if nav_links %}
                <a href="" id="azMenuShow" class="az-header-menu-icon d-lg-none"><span></span></a>
            {% endif %}
             <!-- logo for mobile screens -->
             {% if show_mobilelogo %}
             <a href="{{index_link}}">
                <img class="sub-branding-logo d-block d-lg-none pl-2" src="{{mobilelogo}}" alt="{{title}}">
            </a>
            {% endif %}
            {% if show_title %}
            <!-- Text logo not shown in mobile screens -->
            <a href="{{index_link}}">
                <span class="nav-link text-dark tx-20 d-none d-lg-block" data-i18n="{{title}}">{{title}}</span>
            </a>
            {% endif %}
        </div><!-- az-header-left -->
        <div class="az-header-menu">
            <div class="az-header-menu-header">
                <!-- Sub nav burger menu and logo for mobile screens -->
                <a href="{{index_link}}" class="az-logo"><span></span>
                    <img class="az-logo" src="{{logo}}" alt="{{title}}"/>
                </a>
                <a href="" class="close">&times;</a>
            </div><!-- az-header-menu-header -->
            <ul class="nav">
                <!-- if there are links here, then set nav_links = true so that burger menu shown in mobile view" -->
            </ul>
        </div><!-- az-header-menu -->
        <div class="az-header-right">
            {% if show_help_icon %}
                {% include 'snippets/pb-header-help-menu.tpl' %}
            {% endif %}
            {% if show_news_icon %}
                {% include 'snippets/header-news-menu.tpl' %}
            {% endif %}
            {% if show_apps_menu_icon %}
                {% include 'snippets/header-apps-menu.tpl' %}
            {% endif %}
            {% if show_profile_menu_icon %}
                {% include 'snippets/header-profile-menu.tpl' %}
            {% endif %}
        </div><!-- az-header-right -->
    </div> <!-- container -->
</div><!-- az-header -->