<!-- snippet start parametric footer.tpl -->
<div class="az-footer">
    {% if show_menu %}
    <div class="container-fluid row py-3 ht-100p">
        <div class="col-sm-6 text-center">
            <h6>Quick Links</h6>
            <a class="tx-dark" href="https://www.planblick.com" target="_blank">planBLICK</a><br>
            <a class="tx-dark" href="https://www.crowdsoft.net" target="_blank">CROWDSOFT</a>
        </div>
        <div class="col-sm-6 text-center">
            <h6>Legal</h6>
            <a class="tx-dark" href="https://impressum.planblick.com" target="_blank">Impressum</a><br>
            <a class="tx-dark" href="https://datenschutz.planblick.com" target="_blank">Datenschutz</a>
        </div>
    </div>
    {% endif %}
    {% if show_copyright %}
    <div onclick="alert('Version: [cs_version]')" class="az-footer d-flex ht-40 align-items-center justify-content-center text-center tx-12">
            <span>&copy; 2018 - 2024 <span data-i18n="serviceBLICK"></span> by planBLICK GmbH</span>
    </div>
    {% endif %}
</div>
<!-- snippet end parametric footer.tpl -->