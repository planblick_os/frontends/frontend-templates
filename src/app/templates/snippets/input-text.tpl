<div id="fieldentry_firstname" class="media">
    <div class="media-icon"> <i class="fas fa-address-card"></i></div>
    <div class="media-body">
        <div class="contact-detail">
            <label>Firstname</label>
            <span class="tx-medium"> <input id="firstname" name="Firstname" value="" class="form-control detail-inputs wd-sm-400 wd-md-300" required="required" type="text"></span>
        </div>
    </div>
</div>