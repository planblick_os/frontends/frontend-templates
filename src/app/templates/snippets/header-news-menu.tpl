<!-- snippet start header-news-menu -->
<div class="dropdown az-header-notification mg-l-10" id="system_message" data-title="system_message" data-intro="System Message" data-step="">
    <a href="#" data-toggle="modal" data-target="#news_modal" id="news_bell" class="">
        <i class="icon ion-ios-information-circle-outline" data-toggle="tooltip" title="" data-original-title="News and information"></i>
    </a>
</div>
<div class="dropdown az-header-notification mg-l-10" id="notification_center_icon" data-title="system_message" data-intro="System Message" data-step="">
    <a href="#" data-toggle="modal" data-target="#notifications_header_modal" id="notification_header_icon" class="">
        <i id="notification_icon" class="icon ion-ios-mail" data-toggle="tooltip" title="" data-original-title="Benachrichtigungen"></i>
    </a>
</div>


