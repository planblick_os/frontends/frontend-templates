<!-- snippet start help-dialog -->
<div id="modal_help" class="modal fixed-right fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-aside" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hilfe-Center</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Hilfe und Touren</h5>
                <p>Schnellstarthilfe für jeden Bereich Ihres Veranstaltungsplaners.</p>

                <ul id="column_element_list" class="column-element-list pl-0">
                    <li class="column-element tickets-card row hidden" data-permission='eventmanager'>
                        <div class="col-1 pd-0">
                            <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                        </div>
                        <div class="col-11">
                            <a id="help_tour_get_started" href="#" class="tx-dark-blue" >
                                <span >Willkommen in Ihrem Veranstaltungsplaners</span>
                                <br>
                                <span class="tx-12 tx-gray-700" >
                                    Sind Sie neu hier? Dies ist ein guter Ausgangspunkt.
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- snippet end  -->