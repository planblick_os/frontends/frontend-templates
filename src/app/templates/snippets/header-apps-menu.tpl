<!-- snippet start header-apps-menu -->
<div class="dropdown az-header-notification" id="header_apps_menu">
    <a href="" data-toggle="tooltip" title="" data-original-title="My Apps"><i
            class="icon ion-ios-keypad tx-gray-700" style="width: .75em; margin: 0px 0px 0px 30px;"></i></a>
            
    <div class="dropdown-menu">
        <div class="az-dropdown-header d-sm-none">
            <a href="" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
        </div>
        <div class="az-notification-list" style="display:flex; flex-direction: column">
            <div class="media new">
                <div class="img-app-icon"><img src="/style-global/img/icons/dashboard.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/dashboard/index.html" class="text-dark tx-18">
                    Dashboard
                </a>
                </div><!-- media-body -->
            </div><!-- media -->

            <div id="easy2schedule-anchor" data-permission='easy2schedule' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/calendar.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a id="easy2schedule-anchor-link" href="/easy2schedule/" class="text-dark tx-18" data-i18n="CS:Kalender">
                        Easy2Schedule
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->

            <div id="easy2track-anchor" data-permission='easy2track' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/track.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/easy2track/client.html" class="text-dark tx-18">
                        Easy2Track
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            <div id="easy2order-anchor" data-permission='easy2order' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/order.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/prototypes/easy2order/entries.html" class="text-dark tx-18">
                    Easy2Order
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            <div id="simplycollect-anchor" data-permission='simplycollect' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/chat.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/simplycollect/merchant.html" class="text-dark tx-18">
                        Simplycollect
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            <div id="contactmanager-anchor" data-permission='frontend.contactmanager.views' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/contact.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/contact-manager/" class="text-dark tx-18" data-i18n="CS:Kontakte">
                    Kontaktmanager
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            <div id="taskmanager-anchor" data-permission='taskmanager' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/todo.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/task-manager/" class="text-dark tx-18" data-i18n="CS:Aufgaben">
                     Taskmanager
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            <div id="documentation-anchor" data-permission='frontend.documentation.dashboard' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/clipboard.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/documentation/index.html" class="text-dark tx-18" data-i18n="CS:Dokumentation">
                       Dokumentation
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            <div id="eventmanager-anchor"data-permission='eventmanager' class="media new hidden">
                <div class="img-app-icon"><img src="/style-global/img/icons/event.png" alt="">
                </div>
                <div class="media-body" style="height: 36px;  display: flex; align-items: center;">
                    <a href="/eventmanager/" class="text-dark tx-18" data-i18n="CS:Veranstaltungen">
                        Eventmanager
                    </a>
                </div><!-- media-body -->
            </div><!-- media -->
            
        </div><!-- az-notification-list -->
    </div><!-- dropdown-menu -->
</div>
<script defer type="module">
    import {PbAccount} from '/csf-lib/pb-account.js?v=[cs_version]';
    import {getCookie, mobileCheck} from "/csf-lib/pb-functions.js?v=[cs_version]";
    let settings =""
    new PbAccount().init(getCookie("consumer_id")).then(() => {
        let dashboard_settings = PbAccount.instance.getAccountProfileData()["dashboard_settings"]
        if(dashboard_settings) {
            settings = JSON.parse(dashboard_settings)
            settings.forEach((settingsobject, index) => updateOrder(settingsobject, index))
        }
    })
    $(document).on('newDashBoardOrder', e => {
        JSON.parse(e.detail.settings).forEach((settingsobject, index) => updateOrder(settingsobject, index))
    })
    //Stick to naming convention for element ids for easy updates
    const updateOrder = (settingsobject, index) => {
        let obj = document.getElementById(settingsobject["id"].replace("card", "anchor"))
        if(obj) {
            obj.style.order = index + 1
        }
    }

    if(mobileCheck()) {
        $("#easy2schedule-anchor-link").attr("href", "/easy2schedule/list-view.html")
    }
</script>
<!-- snippet end -->
