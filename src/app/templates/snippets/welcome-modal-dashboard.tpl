<!-- snippet start welcome-dialog -->
<div data-permission="" class="alert alert-secondary hidden" role="alert" id="dashboard_welcome" style="border: 1px solid #cdd4e0;
border-radius: 4px; display: none;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="dashboard_welcome-close">
        <span aria-hidden="true">×</span>
      </button>
    <div class="pl-0 pt-0 ">
        <div class="d-block align-items-center d-sm-flex">
            <div class="p-2 text-center">
                <div class="large_icon" style="color: #7275d2; font-size: 60px; line-height: 80px; margin: 0 10px;">
                    <i class="fa fa-rocket"></i>
                </div>
            </div>
            <div class="p-2 text-center text-sm-left">
                <h5 class="page-title" data-i18n="welcome onboard title">First Steps with your Serviceblick Account</h5>
                <p data-i18n="welcome onboard text">Let's discover the main features of your Planner. We'll explore your dashboard, learn how to maintain account users and thier permissions and fine tune your user profile. You can stop at any time and individual tours are always available from the help icon in the header menu.</p>
                <a href="#" id="button_dismiss" data-i18n="welcome onboard dismiss">Close and don't show again</a>
            </div>
            <div class="ml-auto p-2  text-center" style="white-space: nowrap;">
                <div data-dismiss="alert" data-tour="firstStepsUser" class="col-11" id="firstStepsUser">
                    <a href="javascript:void(0)" class="btn btn-outline-indigo">
                    <!-- tour name -->Tour starten <i class="typcn icon typcn-chevron-right-outline"></i></a>                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- snippet end  -->