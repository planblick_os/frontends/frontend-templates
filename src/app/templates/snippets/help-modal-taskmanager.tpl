<!-- snippet start help-dialog -->
<div id="modal_help" class="modal fixed-right fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-aside" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Hilfe-Center</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <h5>Hilfe und Touren</h5>
          <p>Schnellstarthilfe für jeden Bereich Ihres Serviceblick-Kontos und Ihrer Anwendung.</p>
          <br>
          <ul id="column_element_list" class="column-element-list pl-0">
              <li class="column-element tickets-card row hidden" data-permission='taskmanager'>
                  <div class="col-1 pd-0">
                      <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                  </div>
                  <div class="col-11" id="help_tour_get_started"> 
                    <!--  -->
                    <a href="javascript:void(0)" class="tx-dark-blue">
                          <!-- tour name --><strong>Erste Schritte mit Aufgabenmanager</strong></a>
                          <br>
                          <span class="tx-12 tx-gray-700">Am besten fangen Sie hier an, wenn Sie neu im Aufgabenmanager sind.</span>
                      </a>
                  </div>
              </li>
              <li class="column-element tickets-card row hidden" data-permission='frontend.taskmanager.views.settings'>
                  <div class="col-1 pd-0">
                      <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                  </div>
                  <div class="col-11" id="help_tour_lists"> 
                      <a href="javascript:void(0)" class="tx-dark-blue">
                          <!-- tour name --><strong>Listen bringen Ordnung</strong>
                          <br>
                          <span class="tx-12 tx-gray-700">Erfahren Sie, wie Sie Ihre Aufgabenlisten verwalten können.</span>
                      </a>
                  </div>
              </li>
              <li class="column-element tickets-card row hidden" data-permission='taskmanager'>
                  <div class="col-1 pd-0">
                      <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                  </div>
                  <div class="col-11" id="help_tour_tasks" > 
                      <a href="javascript:void(0)" class="tx-dark-blue">
                          <!-- app link --><strong>Aufgaben für Alles</strong>
                          <br>
                          <span class="tx-12 tx-gray-700">Lernen Sie alle Features kennen, die zu einem Aufgabenelement gehören.</span>
                      </a>
                  </div>
              </li>
              <li class="column-element tickets-card row hidden" data-permission='frontend.taskmanager.views.settings'>
                <div class="col-1 pd-0">
                    <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                </div>
                <div class="col-11" id="help_tour_templates" > 
                    <a href="javascript:void(0)" class="tx-dark-blue">
                        <!-- app link --><strong>Vorlagen und ihre QR-Codes</strong>
                        <br>
                        <span class="tx-12 tx-gray-700">Entdecken Sie das Konzept von QR-Codes und wie sie zum Erstellen von Aufgaben verwendet werden können.</span>
                    </a>
                </div>
            </li>
              <li class="column-element tickets-card row hidden" data-permission='frontend.taskmanager.views.settings'>
                <div class="col-1 pd-0">
                    <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                </div>
                <div class="col-11" id="help_tour_template_groups" > 
                    <a href="javascript:void(0)" class="tx-dark-blue">
                        <!-- app link --><strong>Automatisieren mit Aufgabenpakete</strong>
                        <br>
                        <span class="tx-12 tx-gray-700">Erfahren Sie, wie Sie Ihre Arbeit mit Vorlagen erleichtern können.</span>
                    </a>
                </div>
            </li>
            <li class="column-element tickets-card row hidden" data-permission='frontend.taskmanager.views.settings'>
              <div class="col-1 pd-0">
                  <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
              </div>
              <div class="col-11" id="help_tour_resources" > 
                  <a href="javascript:void(0)" class="tx-dark-blue">
                      <!-- app link --><strong>Ressourcen sind Key</strong>
                      <br>
                      <span class="tx-12 tx-gray-700">Erfahren Sie, wie Sie Ihre Ressourcen verwalten können.</span>
                  </a>
              </div>
          </li>
          </ul>
          <br>
         

        </div>
        <div class="p-3 d-flex justify-content-between align-items-center">
          <a href="https://forum.crowdsoft.net/" target"_blank" class="tx-dark-blue tx-13">Noch Fragen? Das Community Support-Forum besuchen.</a><span><i class="typcn typcn-messages tx-24 tx-gray-700 pr-2"></i></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div> 
  </div> 
<!-- snippet end  -->