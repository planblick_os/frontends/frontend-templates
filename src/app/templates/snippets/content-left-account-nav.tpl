<!-- snippet start content-left-account-nav -->
<div class="component-item">
    <h5 class="az-profile-name force-wrap" id="account_company_name"><!-- Consumer name rendered from Cookie --></h5>
    <p class="az-profile-name-text mg-b-20">Kundenkonto</p>
    <hr class="mg-y-30 mg-r-30">
    <nav class="nav flex-column" id="use-activate-menu">
        <label data-i18n="Settings"></label>
        <a data-permission='system.account.profile' href="./account-settings.html#benachrichtigung" class="nav-link hidden" data-i18n="E-Mail Signature">E-Mail Signature</a>
        <a data-permission='system.account.profile' href="./account-logo-settings.html#logo" class="nav-link hidden" data-i18n="Logo"></a>
        <a data-permission='system.account.backups' href="./backups.html" class="nav-link hidden" data-i18n="Backup"></a>
        <a data-permission='system.account.filestorage' href="./filestorage-settings.html" class="nav-link hidden" data-i18n="Filestorage"></a>
        <a data-permission='beta_access' href="./tag-settings.html" class="nav-link hidden" data-i18n="Tags"></a>

        <label data-i18n="Permissions">Permissions</label>
        <a data-permission='dashboard.management.usermanagement' href="./account.html" class="nav-link" data-i18n="Benutzerverwaltung">Users</a>
        <a data-permission='dashboard.management.rolemanagement' href="./roles.html" class="nav-link" data-i18n="Rollenverwaltung">Roles</a>
        <a data-permission='system.groups.management' href="./groups.html" class="nav-link" data-i18n="Gruppenverwaltung">Groups</a>

        <label data-i18n="Billing">Billing</label>
        <a data-permission='system.account.profile.management.billingaddress.read' href="./account-address.html" class="nav-link" data-i18n="Billing Address">Billing Address</a>
        <a data-permission='beta_access' href="./account-plan.html" class="nav-link" data-i18n="Actual Plan">Actual Plan</a>
    </nav>
</div><!-- component-item -->
<!-- snippet end  -->

