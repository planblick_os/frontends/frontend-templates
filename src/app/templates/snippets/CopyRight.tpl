        <!-- snippet start CopyRight.tpl -->
        <div class="az-footer d-flex ht-40 align-items-center justify-content-center text-center tx-12">
            <span onclick="alert('Version: [cs_version]')">&copy; 2018 - 2024 </span>&nbsp;
            <span data-i18n="serviceBLICK"></span>&nbsp;by planBLICK GmbH
        </div>
