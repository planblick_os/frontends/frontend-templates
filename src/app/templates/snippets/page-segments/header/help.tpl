<div data-permission="dashboard" class="az-header-notification mg-l-10 hidden" data-hint="Start a tour at anytime from the help menu." data-hint-position="middle-top">
    <a href="#" data-toggle="modal" data-target="#modal_help" id="help">
        <i class="icon ion-ios-help-circle-outline" data-toggle="tooltip" title="" data-original-title="Hilfe"></i></a>
</div>