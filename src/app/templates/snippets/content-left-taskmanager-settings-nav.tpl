<!-- snippet start content-left-taskmanger-nav -->
<div class="component-item">

    <label >Einstellungen</label>
    <nav class="nav flex-column" id="use-activate-menu">
        <a data-permission='frontend.taskmanager.views.settings' href="./list-settings.html" class="nav-link hidden" id="list_nav">Liste</a>
        <a data-permission='frontend.taskmanager.views.settings' href="./template-settings.html" class="nav-link hidden" id="template_nav">Vorlagen</a>
        <a data-permission='frontend.taskmanager.views.settings' href="./template-group-settings.html" class="nav-link hidden" id="package_nav">Aufgabenpakete</a>
        <a data-permission='frontend.taskmanager.views.settings' href="./resource-settings.html" class="nav-link hidden" id="resource_nav">Ressourcen</a>
    </nav>
   
</div><!-- component-item -->
<!-- snippet end  -->

