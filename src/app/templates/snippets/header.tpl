<div class="az-header">
    <div class="container">
        <div class="az-header-left">
            <a href="{{logo_link}}" class="az-logo">
                <span></span><img class="az-logo" src="{{logo_url}}" alt="{{logo_alt}}" />
            </a>
            <a href="" id="azContentBodyHide" class="az-header-arrow d-md-none">
                <i class="icon ion-md-arrow-back"></i>
            </a>
            <!-- use if there is main menu that needs to be displayed in mobile view -->
            <a href="#" id="azMenuShow" class="az-header-menu-icon d-lg-none"><span></span></a>
            <span class="nav-link text-dark tx-20  d-none d-lg-block" data-i18n="{{title}}">{{title}}</span>
        </div>
        <div class="az-header-menu">
            <div class="az-header-menu-header">
                <a href="index.html" class="az-logo">
                    <span></span><img class="az-logo" src="{{logo_url}}" alt="{{logo_alt}}">
                </a>
                <a href="" class="close">&times;</a>
            </div>
        </div>
        <div class="az-header-right">
            <div class="az-header-notification mg-l-4 hidden">
                   <a href="" data-toggle="tooltip"
                       data-original-title="Einstellungen und Verwaltung" >
                        <i class="la la-cog tx-24 tx-gray-900"></i>
                    </a>
                </a>
            </div>
            {% include 'snippets/header-apps-menu.tpl' %}
            {% include 'snippets/header-profile-menu.tpl' %}
        </div>
    </div>
</div>
