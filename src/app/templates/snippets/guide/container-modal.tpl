<!-- snippet start help-dialog -->
<div id="modal_help" class="modal fixed-right fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-aside" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Hilfe-Center</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="guide_modal_body" class="modal-body">


        </div>
        <div class="p-3 d-flex justify-content-between align-items-center">
          <a href="https://forum.crowdsoft.net/" target="_blank" class="tx-dark-blue tx-13">Noch Fragen? Das Community Support-Forum besuchen.</a><span><i class="typcn typcn-messages tx-24 tx-gray-700 pr-2"></i></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div> 
  </div> 
<!-- snippet end  -->