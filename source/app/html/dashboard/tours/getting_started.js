
window.tour_config = {
    header: "Lerne serviceBLICK kennen",
    intro: "Schnellstarthilfe zur allgemeinen Systemsteuerung",
    tours: {
        firstStepsUser: {
            id: "firstStepsUser",
            title: "Erste Schritt im Dashboard",
            desc: "Lerne die Dashboard und Benutzeroberfläche kennen",
            startPage: "/dashboard/index.html",
            startStep: 1,
            prevTour: null,
            nextTour: "appsUser",
            permission: "dashboard"
        },
        appsUser:
            {
            id: "appsUser",
            title: "Deine Apps",
            desc: "Erfahr mehr über die Anwendungen, die in deinem Dashboard verfügbar sind.",
            startPage: "/dashboard/index.html",
            startStep: 13,
            prevTour: null,
            nextTour: null,
            permission: "dashboard"
        },

    },
    tourSteps: [
        {//1
            title: "Wichtiges im Überblick",
            intro: "Dein Dashboard ermöglicht den Zugriff auf deine Apps und kontorelevante Informationen. Schauen wir uns um. Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu gelangen. Klicke auf <strong> x</strong>, um die Tour abzubrechen."
        },
        {   
            title: "Wieder zuhause",
            element: ".az-header-left",
            intro: "Du kannst jederzeit zur Hauptseite der App zurückkehren, indem du auf diesen Bereich mit dem Logo und dem Namen der App klickst.",            
        },
        {   
            title: "News und Infos",
            element: "#news_bell",
            intro: "Informationen über neue Funktionen oder mögliche Störungen erhaltst du über das News-Icon."
        },
        {   
            title: "Hilfe-Center",
            element: "#help",
            intro: "Start jederzeit eine Tour über das Hilfemenü."
        },
        { //5
            title: "App-Menü",
            element: "#header_apps_menu",
            intro: "Wechsel schnell zwischen alle deinen Apps und dem Dashboard über das App-Menü."
        },
        {   
            title: "Profilmenü",
            element: " #header_profile_menu",
            intro: "Alle Funktionen und Einstellungen für dein Benutzerprofil erreichst du hier über das Profilmenü"
        },
        {
            title: "Deine Anwendungen",
            element: "#board-portlet-container",
            intro: "In diesem Bereich sehst du alle deine verf\u00fcgbaren Anwendungen als Kachel mit Aktionslinks."
        }, 
        {
            title: "Drag & Drop",
            element: "#edit_dashboard_button",
            intro: "Klick Bearbeitungsmodus, um die Dashboard-Kacheln in Ihre persönliche Reihenfolge zu verschieben und dann zu speichern."
        }, 
        {
            title: "Kontrolle über deinen Konto",
            element: "#account_content",
            intro: "In diesem Bereich findest du Zugriff auf verschiedene Kontoeinstellungen."
        },
        {//10
            title: "Allgemeine Anpassungen",
            element: "#account-settings",
            intro: "Verwende die Quicklinks in diesem Bereich, um allgemeine Kontoeinstellungen festzulegen, die sich auf alle Kontobenutzer auswirken."
        },
        {
            title: "Zugriff und Berechtigung",
            element: "#account-permissions",
            intro: "Hier kannst du Kontobenutzern und deren Berechtigungen einrichten und ändern ."
        },
        {  
            title: 'Prima!',
            disableInteraction: true,
            intro: "Damit hast du die Einführung abgeschlossen.<br/>Wenn Du möchtest, machen wir gleich weiter und geben dir eine Überblick über deine Anwendungen.  <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong>",
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("dashboard")
                $("#dashboard_welcome").hide()
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
                 PbGuide.setNextButton("Weiter", function () {
                        PbGuide.introguide.goToStepNumber(13)
                })
            }   
        },
        {
            title: "Deine Anwendungen",
            element: "#board-portlet-container",
            intro: "In diesem Bereich sehst du alle deine verf\u00fcgbaren Anwendungen als Kachel mit Aktionslinks.",
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()
            }   

        }, 
        {
            element: "#easy2schedule-card",
            intro: "Easy2Schedule ist unsere einfache Kalender-Verwaltung."
        },
        {//15
            element: "#easy2track-card",
            intro: "Easy2Track ist unsere einfache Kontakt-Nachverfolgung."
        },
        {
            element: "#simplycollect-card",
            intro: "Simplycollect ist dein einfacher Helfer f\u00fcr Click&Collect Gesch\u00e4fte ohne Webshop."
        },
        {
            element: "#easy2order-card",
            intro: "Easy2Order ist unser einfacher Helfer um Gast-Bestellungen schnell und einfach ausf\u00fchren zu k\u00f6nnen."
        },
        {
            element: "#contactmanager-card",
            intro: "Kontaktmanager ist unser einfacher Helfer um Kontakte zu verwalten."
        },
        {
            element: "#taskmanager-card",
            intro: "Der Aufgabenmanager ermöglicht die digitale Erstellung, Nachverfolgung und Abarbeitung von Aufgaben."
        },
        { //20
            title: 'Einführung abgeschlossen',
            disableInteraction: true,
            intro: `Prima, damit hast du die Einführung abgeschlossen.<br/>Wenn Du möchtest, machen wir gleich weiter und geben dir eine Einführung in die Kontoverwaltung. Du kannst diese aber auch später starten indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong> `,
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
                if(!PbGuide.hasPermission("dashboard.management.usermanagement")) {
                    PbGuide.updateIntro("Prima, damit hast du die Einführung abgeschlossen. Wähl einfach eine App aus, um loszulegen.") 
                    PbGuide.hideNextButton()
                }else {
                    PbGuide.setNextButton("Weiter", function () {
                        PbGuide.ensureUrl("/dashboard/account.html", "firstStepsAdmin")
                        PbGuide.introguide.exit()
                })
                
            }

            }

        }
        
    ]
}
