

window.tour_config = {
    header: "Lerne deinen Familien Planer kennen",
    intro: "Schnellstarthilfe zur allgemeinen Systemsteuerung",
    tours: {
        firstStepsUser: {
            id: "firstStepsUser",
            title: "Erste Schritte im Familien Planer",
            desc: "Lerne die Oberfläche des Familien Planers kennen.",
            startPage: "/dashboard/index.html",
            startStep: 1,
            prevTour: null,
            nextTour: "appsUser",
            permission: "frontend.dashboard.familyplanner"
        },
        appsUser: {
            id: "appsUser",
            title: "Familien Planer Apps",
            desc: "Erfahre mehr über die Familien Planer Funktionen, die in deinem Dashboard verfügbar sind.",
            startPage: "/dashboard/index.html",
            startStep: 13,
            prevTour: null,
            nextTour: null,
            permission: "frontend.dashboard.familyplanner"
        }
    },
    tourSteps: [
        {//1
            title: "Wichtiges im Überblick",
            intro: "Sobald du angemeldet bist, ist das Dashboard dein Ausgangspunkt. Du kannst auf die Funktionen im Header von allen Apps aus zugreifen. Schauen wir uns um. Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu gelangen. Klicke auf <strong> x</strong>, um die Tour abzubrechen."
        },
        {   
            title: "Wieder zuhause",
            element: ".az-header-left",
            intro: "Du kannst jederzeit zur Hauptseite der App zurückkehren, indem du auf diesen Bereich mit dem Logo und dem Namen der App klickst.",            
        },
        {   
            title: "News und Infos",
            element: "#news_bell",
            intro: "Informationen über neue Funktionen oder mögliche Störungen erhältst du über das News-Icon."
        },
        {   
            title: "Hilfe-Center",
            element: "#help",
            intro: "Start jederzeit eine Tour über das Hilfemenü im jeweiligen Bereich."
        },
        { //5
            title: "App-Menü",
            element: "#header_apps_menu",
            intro: "Wechsle schnell zwischen all deinen Apps und dem Dashboard über das App-Menü."
        },
        {   
            title: "Profilmenü",
            element: " #header_profile_menu",
            intro: "Alle Funktionen und Einstellungen für dein Benutzerprofil erreichst du hier über das Profilmenü."
        },
        {
            title: "Familien Planer Toolbox",
            element: "#board-portlet-container",
            intro: "In diesem Bereich siehst du alle deine verf\u00fcgbaren Anwendungen als Kachel mit Aktionslink."
        }, 
        {
            title: "Drag & Drop",
            element: "#edit_dashboard_button",
            intro: "Klicke auf den Bearbeitungsmodus, um die Dashboard-Kacheln in deine persönliche Reihenfolge zu verschieben und dann zu speichern."
        }, 
        {
            title: "Kontrolle über dein Familien Planer Konto",
            element: "#account_content",
            intro: "In diesem Bereich findest du Zugriff auf verschiedene Kontoeinstellungen."
        },
        {//10
            title: "Allgemeine Anpassungen",
            element: "#account-settings",
            intro: "Verwende die Quicklinks in diesem Bereich, um allgemeine Kontoeinstellungen festzulegen, die sich auf alle Kontobenutzer auswirken."
        },
        {
            title: "Zugriff und Berechtigung",
            element: "#account-permissions",
            intro: "Hier kannst du Familien Kontobenutzer und deren Berechtigungen einrichten und ändern ."
        },
        {  
            title: 'Großartig!',
            disableInteraction: true,
            intro: "Jetzt kennst du dich in deinem Familien Planer Dashboard aus. <strong>Möchtest du jetzt mehr über Benutzer und Gruppen erfahren?</strong> Wenn ja, klick Weiter.",
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
                if(!PbGuide.hasPermission("dashboard.management.usermanagement")) {
                    PbGuide.updateIntro("Jetzt kennst du dich in deiner Familien Planer Dashboard aus. Wähl einfach eine App aus, um loszulegen.") 
                    PbGuide.hideNextButton()
                }else {
                    PbGuide.setNextButton("Weiter", function () {
                        PbGuide.ensureUrl("/dashboard/account.html", "firstStepsAdmin")
                        PbGuide.introguide.exit()
                })
                
            }

            }
        },
        {
            title: "Familien Planer Toolbox",
            element: "#board-portlet-container",
            intro: "In diesem Bereich siehst du alle deine verf\u00fcgbaren Anwendungen als Kachel mit den Aktionslinks."
        }, 
        {
            title: "Familien Kalender",
            element: "#easy2schedule-card",
            intro: "Über den Link 'Dein Kalender' gelangst du direkt zu deiner Wochenübersicht."
        },
        {
            title: "Familien Kalender Export",
            element: "#easy2schedule_link_export",
            intro: "Über den Datenexport-Link wird ein Dialogfeld geöffnet, in dem du einen Datumsbereich auswählst und einen CSV-Bericht herunterladen kannst."
        },
        {//15 
            title: "Kontakt Manager",
            element: "#contactmanager-card",
            intro: "Der Aktionslink 'Deine Kontakte' führt dich zu einer durchsuchbaren Liste deiner Kontakte."
        },
        {
            title: "Aufgaben",
            element: "#taskmanager-card",
            intro: "Der Quick-Link auf der Aufgaben-Kachel bringt dich direkt zur zuletzt besuchten Liste oder Aufgabe."
        },
        {
            title: "Notizen",
            element: "#documentation-card",
            intro: "Alle deine Notizen sind über den Schnell-Link 'Deine Notizen' auf der Notiz-Kachel zugänglich."
        },
        {  
            title: 'Großartig!',
            intro: "Jetzt bist du mit all deinen verfügbaren Anwendungen vertraut. Wähl einfach eine App aus, um loszulegen.",
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()
                PbGuide.setNextButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
            }
        },

    ]
}


