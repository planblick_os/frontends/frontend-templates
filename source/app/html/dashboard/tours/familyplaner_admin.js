window.tour_config = {
    header: "Lerne deinen Familien Planer kennen",
    intro: "Schnellstarthilfe zur allgemeinen Systemsteuerung",
    tours: {
        firstStepsAdmin:
            {
            id: "firstStepsAdmin",
            title: "Logins und Benutzer einrichten",
            desc: "Erstellen und Verwalten von Familienmitglied-Logins",
            startPage: "/dashboard/account.html",
            startStep: 1,
            prevTour: null,
            nextTour: "firstStepsAdminGroups",
            permission: "dashboard.management.usermanagement"
        },
        firstStepsAdminGroups: {
            id: "firstStepsAdminGroups",
            title: "Gruppen festlegen",
            desc: "Mit Gruppen kannst du den Benutzerzugriff auf bestimmte Informationen steuern",
            startPage: "/dashboard/groups.html",
            startStep: 9,
            prevTour: null,
            nextTour: null,
            permission: "dashboard.management.usermanagement"
        }
    },
    tourSteps: [     
       
        {//1
            title: 'Benutzerverwaltung',
            disableInteraction: true,
            intro: 'In der Benutzerverwaltung steuern Benutzer mit einer Administratorrolle, wer Zugriff auf das Familienkonto hat. ',
            'myBeforeChangeFunction': function (PbGuide) {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.firstStepsAdmin.startPage, "firstStepsAdmin")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {
            title: 'Benutzer einladen',
            element: '#invite-login-wrapper',
            intro: 'Eine Möglichkeit, einen neuen Benutzer zu deinem Konto hinzuzufügen, ist ihn einzuladen. Gib einfach eine E-Mailadresse ein. Eine Einladungs-E-Mail wird automatisch versendet. Der neue Benutzer kann mit dem Link zur Registrierung seinen Login für dein Familienkonto erstellen. ',
        },
        {
            title: 'Anlegen von Benutzern',
            element: '#create-login-wrapper',
            intro: 'Asl Administrator kannst du selbst neue Benutzer erstellen, indem du einen Login und ein Kennwort festlegst und die Rolle (Berechtigungsstufe) auswählst. Der neue Benutzer wird in der Liste unten eingetragen.',
        },
        {
            title: 'Benutzertabelle',
            element: '#manage-login-wrapper',
            intro: 'Hier kannst du die zusätzlich angelegten Logins/Nutzer zu deinem Konto verwalten',
        },
        {//5
            title: 'Bearbeiten',
            element: '.btn-with-icon',
            intro: 'Bearbeite und überprüfe Berechtigungen, indem du auf die Schaltfläche mit dem Stift klickst.', 
        },
        {
            title: 'Passwort zurücksetzen',
            element: '.typcn-key-outline',
            intro: 'Durch Anklicken der Schaltfläche mit dem Schlüssel kannst du ein neues Passwort für den Benutzer erstellen. ', 
        },
        {
            title: 'Löschen',
            element: '.typcn-trash',
            intro: 'Die Schaltfläche mit dem Papierkorb entfernt den Benutzer aus dem System. Es gibt eine Bestätigung, bevor der Benutzer vollständig gelöscht wird.', 

        },
        {
            title: 'Sehr gut!',
  
            intro: 'Das war die Einführung in die Benutzerverwaltung. <strong>Möchtest du jetzt mehr über Gruppen erfahren?</strong> Wenn ja klick Weiter.', 
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
                PbGuide.setNextButton("Weiter", function () {
                        let nextTour = PbGuide.tourConfig.tours.firstStepsAdmin.nextTour
                        let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                        PbGuide.introguide.goToStepNumber(nextStartStep)
                })
                
            }

        },

        {
            title: 'Gruppenverwaltung',
            disableInteraction: true,
            intro: 'Mithilfe von Gruppen kannst du ähnliche Benutzertypen kategorisieren. Sie sind nützlich, um schnell Berechtigungen und Delegierungen innerhalb der verschiedenen Apps festzulegen.',
            'myBeforeChangeFunction': function (PbGuide) {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.firstStepsAdminGroups.startPage, "firstStepsAdminGroups")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//10
            title: 'Neue Gruppe',
            element: '#create-group-wrapper',
            intro: 'Eine neue Gruppe benötigt einen eindeutigen Namen aus Kleinbuchstaben ohne Leerzeichen und eine kurze Beschreibung des Zwecks dieser Gruppe.',
            'myBeforeChangeFunction': function () {
            }   
        },
        {
            title: 'Gruppentabelle',
            element: '#manage-group-wrapper',
            intro: 'Hier kannst du die zusätzlich angelegten Gruppen zu deinem Konto verwalten.',
            'myBeforeChangeFunction': function () {
            }            
        },
        {
            title: 'Gruppenmitglieder',
            element: '.btn-indigo',
            intro: 'Um festzulegen, welche Benutzer Teil der Gruppe sein sollen, klick auf "Bearbeiten".',
            'myBeforeChangeFunction': function () {
            }            
        },
        {
            title: 'Löschen',
            element: '.typcn-trash',
            intro: 'Die Schaltfläche mit dem Papierkorb entfernt die Gruppe (nicht die Benutzer in der Gruppe) aus dem System. Es wird nach einer Bestätigung gefragt, bevor die Gruppe vollständig gelöscht wird.',
            'myBeforeChangeFunction': function () {
            }            
        },
        {
            title: 'Tour abgeschlossen',
            intro: 'Sehr gut! Das war die Einführung in die Benutzerverwaltung. Jetzt kannst du beginnen, eigene Gruppen zu definieren.',
            'myBeforeChangeFunction': function (PbGuide) {
                // PbGuide.markTourSeen("contactmanager.editContact")
                PbGuide.hidePreviousButton()

                PbGuide.setNextButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
            },
        }
    ]
}
