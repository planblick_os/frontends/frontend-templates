import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]";
import {PbLogin} from "../../csf-lib/pb-login.js?v=[cs_version]";
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]";
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]";
import {validateLetNumbUnderField} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie, makePassword} from "../../csf-lib/pb-functions.js?v=[cs_version]"

export class Render {
    static logins(containerElement, forceReload = false, logins = undefined) {
        // prepare data for display
        if (forceReload) {
            new PbAccount().init(getCookie("consumer_id"), true).then(() => {
                Render.logins(containerElement, false)
            })
            return
        }

        if (!logins) {
            logins = new PbAccount().logins
        }
        logins.sort(function (a, b) {
            let textA = a.username.toUpperCase();
            let textB = b.username.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        })


        // We still have to create the buttons because bootstrap cannot handle this properly
        // Although we could use html-strings here as well.


        for (let idx in logins) {
            let login = logins[idx]
            logins[idx].options = ""

            let rolesEditButton = document.createElement("button")
            rolesEditButton.id = "rolesEditButtonBootstrap_" + login.userid
            rolesEditButton.name = "rolesEditButtonBootstrap"
            rolesEditButton.innerHTML = "Bearbeiten"
            rolesEditButton.setAttribute('data-loginid', login.userid);
            rolesEditButton.setAttribute('data-loginname', login.username);
            rolesEditButton.setAttribute('class', "btn btn-indigo btn-with-icon");
            let rolesEditIcon = document.createElement("i")
            rolesEditIcon.setAttribute('class', "typcn typcn-edit mg-l-5");
            rolesEditIcon.setAttribute('data-loginid', login.userid);
            rolesEditIcon.setAttribute('data-loginname', login.username);
            rolesEditButton.appendChild(rolesEditIcon)
            logins[idx].options = rolesEditButton.outerHTML

            let changePasswordButton = document.createElement("button")
            changePasswordButton.id = "changePasswordButtonBootstrap_" + login.userid
            changePasswordButton.name = "changePasswordButtonBootstrap"
            let changePasswordIcon = document.createElement("i")
            changePasswordIcon.setAttribute('class', "typcn typcn-key-outline");
            changePasswordIcon.setAttribute('data-loginid', login.userid);
            changePasswordIcon.setAttribute('data-loginname', login.username);
            changePasswordButton.appendChild(changePasswordIcon)
            changePasswordButton.setAttribute('data-loginid', login.userid);
            changePasswordButton.setAttribute('data-loginname', login.username);
            changePasswordButton.setAttribute('class', 'btn btn-indigo btn-icon');
            logins[idx].options += changePasswordButton.outerHTML

            let deleteLoginButton = document.createElement("button")
            deleteLoginButton.id = "deleteLoginButtonBootstrap_" + login.userid
            deleteLoginButton.name = "deleteLoginButtonBootstrap"
            let rolesDeleteIcon = document.createElement("i")
            rolesDeleteIcon.setAttribute('class', "typcn typcn-trash");
            rolesDeleteIcon.setAttribute('data-loginid', login.userid);
            rolesDeleteIcon.setAttribute('data-loginname', login.username);
            deleteLoginButton.appendChild(rolesDeleteIcon)
            deleteLoginButton.setAttribute('data-loginid', login.userid);
            deleteLoginButton.setAttribute('data-loginname', login.username);
            deleteLoginButton.setAttribute('class', "btn btn-outline-indigo btn-icon");
            logins[idx].options += deleteLoginButton.outerHTML

        }

        $(function () {
            $(containerElement).bootstrapTable({
                data: logins
            });
            $(containerElement).bootstrapTable('load', logins)
        });

        $(document).off("click", 'button[name ="rolesEditButtonBootstrap"]')
        $(document).on("click", 'button[name ="rolesEditButtonBootstrap"]', function (event) {
            let state = {'user': event.target.dataset.loginname}
            history.pushState(state, "")
            window.location.href = "./user-rights.html?user=" + event.target.dataset.loginname
        })

        // We have to remove the click handlers in case we already went through here already
        $(document).off("click", 'button[name ="deleteLoginButtonBootstrap"]')
        // Then (re-)add the click handler performing the delete action
        $(document).on("click", 'button[name ="deleteLoginButtonBootstrap"]', function (event) {
                console.log(event.target)

                let deleteQuestion = new pbI18n().translate("Do you really want to delete the login {LOGIN}?").replace("{LOGIN}", event.target.dataset.loginname)
                Fnon.Ask.Danger(deleteQuestion, new pbI18n().translate('Login löschen'), new pbI18n().translate('Okay'), new pbI18n().translate('Abbrechen'), (result) => {
                    if (result == true) {
                        Fnon.Box.Circle('.az-content-body', '', {
                            svgSize: {w: '50px', h: '50px'},
                            svgColor: '#3a22fa',
                        })
                        new PbAccount().deleteLogin(
                            event.target.dataset.loginid,
                            function () {
                            })
                    }
                })
            }
        )


        $(document).off("click", 'button[name ="changePasswordButtonBootstrap"]')
        // Then (re-)add the click handler performing the delete action
        $(document).on("click", 'button[name ="changePasswordButtonBootstrap"]', function (event) {
                let deleteQuestion = new pbI18n().translate("Do you really want to reset the password for the login {LOGIN}?").replace("{LOGIN}", event.target.dataset.loginname)
                Fnon.Ask.Danger(deleteQuestion, new pbI18n().translate('Passwort zurücksetzen'), new pbI18n().translate('Okay'), new pbI18n().translate('Abbrechen'), (result) => {
                    if (result == true) {
                        Fnon.Box.Circle('.az-content-body', '', {
                            svgSize: {w: '50px', h: '50px'},
                            svgColor: '#3a22fa',
                        })
                        let newPassword = makePassword(15)
                        new PbLogin().resetPassword(event.target.dataset.loginid, newPassword).then(function (result) {
                            new PbNotifications().waitForCorrelationId(result.task_id).then((result) => {
                                Fnon.Box.Remove('.az-content-body', 500)
                                if(result.data.event == "loginPasswordResetted") {
                                    Fnon.Alert.Success(new pbI18n().translate("Password resetted. New password is {NEWPASSWORD}").replace("{NEWPASSWORD}",newPassword), new pbI18n().translate("Password resetted"), new pbI18n().translate('Okay'), () => {});
                                }
                                else {
                                    Fnon.Alert.Danger(new pbI18n().translate("Something went wrong, please try again"), new pbI18n().translate("Ooops..."), new pbI18n().translate('Okay'), () => {});
                                }
                            }).catch(function (error) {
                                Fnon.Box.Remove('.az-content-body', 500)
                                console.log("ERROR", error)
                                Fnon.Alert.Danger(new pbI18n().translate("Something went wrong, please try again"), new pbI18n().translate("Ooops..."), new pbI18n().translate('Okay'), () => {});
                            })
                        })
                    }
                })
            }
        )
    }

    static customPreloadsForDataset(value, type, role) {


        $("#edited_name").html(value)

        if (type == "user") {
            let translation = new pbI18n().translate("#hint1_user")
            translation = translation.replace("[{username}]", value)
            let translation2 = new pbI18n().translate("#hint3").replace("[{typ}]", "Benutzer")

            $('#back-link').attr('href', '/dashboard/account.html')
            $("#hint1_user").html(translation)
            $("#edited_desc").hide()
            $("#edited_link_div").hide()
            $("#headline_editor").html("Benutzer")
            $("#hint1_user").show()
            $("#hint1_role").hide()
            new pbI18n().translate("#hint2_user")
            $("#hint2_user").show()
            $("#hint2_role").hide()
            $("#hint3").html(translation2)

        }
        if (type == "role") {
            let translation = new pbI18n().translate("#hint1_role")
            translation = translation.replace("[{rolename}]", value)
            let translation2 = new pbI18n().translate("#hint3").replace("[{typ}]", "Rolle")


            let edited_desc = (new pbPermissions().roleset.roles[role].description.length > 0) ? new pbPermissions().roleset.roles[role].description : "keine";

            $('#back-link').attr('href', '/dashboard/roles.html')
            $("#hint1_role").html(translation)
            $("#edited_desc").show()
            $("#description").html(edited_desc)
            $("#headline_editor").html("Rolle")
            $("#hint1_user").hide()
            $("#hint1_role").show()
            new pbI18n().translate("#hint2_role")
            $("#hint2_user").hide()
            $("#hint2_role").show()
            $("#hint3").html(translation2)
        }
    }

    static renderRoleMembershipTagsFor(rolename) {
        let import_roles = new pbPermissions().userrules[rolename].definition.import

        for (let idx = 0; idx < import_roles.length; idx++) {
            if (import_roles[idx].startsWith("role.") != false) {
                Render.addRoleMembershipTags(import_roles[idx])
            }

        }
    }

    static addRoleMembershipTags(clicked_role) {
        let id = "roletag_" + clicked_role
        let clickedRole = document.getElementById(id)
        if (clickedRole == undefined) {
            let newButton = $("#role_button_template").clone()
            newButton.attr("id", "roletag_" + clicked_role)
            newButton.attr("data-roleid", clicked_role)
            let buttonContent = newButton.html().replace("[{role_name}]", clicked_role)
            newButton.html(buttonContent)
            $("#role_memberships_container").append(newButton)
        }
    }

    static removeRoleMembershipTags(clicked_role) {
        let id = "roletag_" + clicked_role
        $(document.getElementById(id)).remove()
    }

    static renderRoleListFor(rolename) {
        let set_of_roles = new pbPermissions().roleset.roles
        for (let [key, value] of Object.entries(set_of_roles)) {
            if (key == rolename || key.startsWith("role.") == false) {
                continue
            }

            if(PbAccount.instance.hasPermission("frontend.dashboard.familyplannerpremium") && !PbAccount.instance.hasPermission("dashboard.management.rolemanagement.giveallrights") && !key.includes("familyPlanerPremium")) {
                continue
            }
            if(PbAccount.instance.hasPermission("frontend.dashboard.familyplanner") && !PbAccount.instance.hasPermission("frontend.dashboard.familyplannerpremium") && !PbAccount.instance.hasPermission("dashboard.management.rolemanagement.giveallrights") && !key.includes("familyPlaner_")) {
                continue
            }

            let newListItem = $("#role_list_item").clone()
            newListItem.attr("id", "rolelistentry_" + key)
            let listItemName = newListItem.html().replace("[{role_name}]", key).replace("[{role_description}]", value.description).replace("[{role_id}]", key)
            newListItem.html(listItemName)

            //pre check the box if role is already assigned
            let alreadyAssignedRoles = document.getElementById("roletag_" + key)

            if (typeof (alreadyAssignedRoles) != 'undefined' && alreadyAssignedRoles != null) {
                var myDomElement = $(newListItem).find("input");
                myDomElement.prop({checked: true})
            }

            $("#check-list-box").append(newListItem)
        }
    }

    static renderUserRuleListFor(rolename) {
        let assigned_user_rules = new pbPermissions().userrules[rolename].definition.rules || {}
        $("#check-list-box-user-rules").html("")
        for (let [key, value] of Object.entries(assigned_user_rules)) {
            let newListItem = $("#user_rule_list_item").clone()
            newListItem.attr("id", "user_rule_item_" + key)
            newListItem.attr("data-ruleid", key)
            let listItemEnty = newListItem.html().replace("[{rule_name}]", key).replace("[{rule_action}]", value.action).replace("[{rule_weight}]", value.weight)
            newListItem.html(listItemEnty)
            $("#check-list-box-user-rules").append(newListItem)

        }
    }

    static addUserRuleListItem(clicked_rule) {
        let id = "user_rule_item_" + clicked_rule
        let clickedRule = document.getElementById(id)
        if (clickedRule == undefined) {
            let newListItem = $("#user_rule_list_item").clone()
            newListItem.attr("id", "user_rule_item_" + clicked_rule)
            newListItem.attr("data-ruleid", clicked_rule)
            let article = document.getElementById(clicked_rule)
            let listItemEnty = newListItem.html().replace("[{rule_name}]", clicked_rule).replace("[{rule_action}]", article.dataset.ruleAction).replace("[{rule_weight}]", parseInt(article.dataset.ruleWeight))
            newListItem.html(listItemEnty)
            $("#check-list-box-user-rules").append(newListItem)
        }
    }

    static removeUserRuleListItem(clicked_rule) {
        let id = "user_rule_item_" + clicked_rule
        let clickedRule = document.getElementById(id)
        clickedRule.remove()
    }

    static renderRuleListFor(rolename) {
        let set_of_rules = new pbPermissions().rulesset[rolename].rules
        $("#check-list-box-rules").html("")



        let allowedRights = Object.entries(new pbPermissions().rulesset[rolename].rules).map(([key, value]) => {
            if(value.action.includes("allow"))
                return key
        })

        for (let [key, value] of Object.entries(set_of_rules)) {
            let newListItem = $("#rule_list_item").clone()
            newListItem.attr("id", "rulelistentry_" + key)
            let listItemName = newListItem.html().replace("[{rule_name}]", key).replace("[{rule_id}]", key).replace("[{rule_action}]", value.action).replace("[{rule_weight}]", value.weight)
            newListItem.html(listItemName)

            //pre check the box if rule is already assigned
            let alreadyAssignedRules = document.getElementById("user_rule_item_" + key)
            if (typeof (alreadyAssignedRules) != 'undefined' && alreadyAssignedRules != null) {
                var myDomElement = $(newListItem).find("input");
                myDomElement.prop({checked: true})
            }

            if(
                PbAccount.instance.hasPermission("dashboard.management.usermanagement.giveallrights") ||
                (PbAccount.instance.hasPermission("dashboard.management.usermanagement.giveownrights") && allowedRights.includes(key))
            ) {
                $("#check-list-box-rules").append(newListItem)
            }
        }
    }

    static renderUserRightsTableFor(rolename) {
        let set_of_user_rules = new pbPermissions().rulesset[rolename].rules
        let tableData = []
        for (let [key, value] of Object.entries(set_of_user_rules)) {
            let tableRow = [key, value.action, value.weight, value.source]
            tableData.push(tableRow)
        }

        $(function () {
            $('#rights_table').bootstrapTable({
                data: tableData
            });
            $('#rights_table').bootstrapTable('load', tableData);
        });
    }


    static editUserRules(clicked_button, id) {
        let weightItemToEdit = $(document.getElementById(id)).find('[data-selector="assigned-rule-weight"]')
        let actionItemToEdit = $(document.getElementById(id)).find('[data-selector="assigned-rule-action"]')
        let weight = weightItemToEdit.text()

        weightItemToEdit.html('')
        $('<input>').attr({
            'type': 'text',
            'class': 'input-rule-edit',
            'name': 'fname',
            'id': 'txt-rule-weight',
            'value': weight
        }).appendTo(weightItemToEdit)

        let preselected_action = actionItemToEdit.text()
        actionItemToEdit.html('')

        let dropdown = $('<select>').attr({"id": 'txt-rule-action', 'class': 'select-rule-edit'})
        let actions = ["deny", "forcedeny", "allow", "forceallow"]
        for (let action of actions) {
            $('<option>').attr({
                "value": action,
                "selected": true ? action == preselected_action : false
            }).text(action).appendTo(dropdown)
        }
        dropdown.appendTo(actionItemToEdit)
        $("#txt-rule-action").focus()

        $(clicked_button).text("save").attr("data-selector", "assigned-rule-save");
        Render.disableRuleEditButtons()
    }

    static saveUserRules(clicked_button, id, rule, role) {
        let weightItemToEdit = $(document.getElementById(id)).find('[data-selector="assigned-rule-weight"]')
        let actionItemToEdit = $(document.getElementById(id)).find('[data-selector="assigned-rule-action"]')
        let weight = $('#txt-rule-weight').val()
        let action = $('#txt-rule-action').val()
        weightItemToEdit.text(weight)
        actionItemToEdit.text(action)

        let new_rule = {
            "action": action,
            "weight": parseInt(weight)
        }
        new pbPermissions().userrules[role].definition.rules[rule] = new_rule

        pbPermissions.instance.setRole(role)
        $(clicked_button).text("edit").attr("data-selector", "assigned-rule-edit")
        Render.enableRuleEditButtons()
    }

    static enableRuleEditButtons() {
        $('a[data-selector="assigned-rule-disabled"]').attr('data-selector', 'assigned-rule-edit').attr('class', 'text-primary nav-link');
    }

    static disableRuleEditButtons() {
        $('a[data-selector="assigned-rule-edit"]').attr('data-selector', 'assigned-rule-disabled').attr('class', 'nav-link disabled');
    }

    static refreshContent(role) {
        new pbPermissions().init(role, true).then(function () {
            Render.renderUserRuleListFor(role)
            Render.renderRuleListFor(role)
            Render.renderUserRightsTableFor(role)
        })
    }

    static roles(containerElement, forceReload = false, roles = undefined) {

        if (forceReload) {
            new pbPermissions().init().then(() => {
                Render.roles(containerElement, false)
            })
            return
        }

        if (!roles) {
            roles = new pbPermissions().roleset.roles
        }

        let currentRoleSet = [];

        for (let [key, ] of Object.entries(roles)) {
            if (key.startsWith("role.") != false) {
                currentRoleSet.push({
                    "rolename": key
                });
            }
        }


        //create option buttons in bootstraptable
        for (let idx in currentRoleSet) {
            let role = currentRoleSet[idx]
            currentRoleSet[idx].options = ""
            if (new PbAccount().hasPermission("dashboard.management.rolemanagement.role.update")) {
                let rolesEditButton = document.createElement("button")
                rolesEditButton.id = "rolesEditButtonBootstrap_" + role.rolename
                rolesEditButton.name = "rolesEditButtonBootstrap"
                rolesEditButton.innerHTML = "Bearbeiten"
                rolesEditButton.setAttribute('data-rolename', role.rolename);
                rolesEditButton.setAttribute('data-permission', "dashboard.management.rolemanagement.role.update");
                rolesEditButton.setAttribute('class', "btn btn-indigo");
                currentRoleSet[idx].options = rolesEditButton.outerHTML
            }

            if (new PbAccount().hasPermission("dashboard.management.rolemanagement.role.delete")) {
                let deleteRolesButton = document.createElement("button")
                deleteRolesButton.id = "deleteRolesButtonBootstrap_" + role.rolename
                deleteRolesButton.name = "deleteRolesButtonBootstrap"
                deleteRolesButton.setAttribute('data-rolename', role.rolename);
                deleteRolesButton.setAttribute('class', "btn btn-outline-indigo btn-icon");
                deleteRolesButton.setAttribute('data-permission', "dashboard.management.rolemanagement.role.delete");
                let rolesDeleteIcon = document.createElement("i")
                rolesDeleteIcon.setAttribute('class', "typcn typcn-trash");
                rolesDeleteIcon.setAttribute('data-rolename', role.rolename);
                deleteRolesButton.appendChild(rolesDeleteIcon)
                currentRoleSet[idx].options += deleteRolesButton.outerHTML
            }
        }

        $(function () {
            $(containerElement).bootstrapTable({
                data: currentRoleSet
            });
            $(containerElement).bootstrapTable('load', currentRoleSet)
        });

        $(document).off("click", 'button[name ="rolesEditButtonBootstrap"]')
        $(document).on("click", 'button[name ="rolesEditButtonBootstrap"]', function (event) {
            let state = {'user': event.target.dataset.rolename}
            history.pushState(state, "")
            window.location.href = "./user-rights.html?role=" + event.target.dataset.rolename.replace('role.', '')
        })

        $(document).off("click", 'button[name ="deleteRolesButtonBootstrap"]')
        $(document).on("click", 'button[name ="deleteRolesButtonBootstrap"]', function (event) {
            let clicked_role = event.target.dataset.rolename
            let deleteQuestion = new pbI18n().translate("Möchten Sie die Rolle <strong>{LOGIN}</strong> wirklich löschen?").replace("{LOGIN}", clicked_role)

            Fnon.Ask.Danger(deleteQuestion, 'Rolle löschen', 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    Fnon.Box.Circle('.az-content-body', '', {
                        svgSize: {w: '50px', h: '50px'},
                        svgColor: '#3a22fa',
                    })
                    pbPermissions.instance.deleteRole(clicked_role)
                }
            })
            // if (confirm(deleteQuestion)) {
            //     pbPermissions.instance.deleteRole(clicked_role).then(function () {
            //         alert("Rolle " + event.target.dataset.rolename + " wurde gelöscht.");
            //         Render.roles(document.getElementById("bootstraptable"), true)
            //     })
            // }
        })
    }

    static editRoleDetails(clicked_button, id) {
        let detailItemToEdit = document.getElementById(id)
        let description = detailItemToEdit.innerText

        detailItemToEdit.innerHTML = ""
        $('<textarea>').attr({
            //'type': 'text',
            'class': 'textarea-description-edit',
            'name': 'fname',
            'id': 'txt-role-detail',
            'value': description,
            'rows': '3'
        }).appendTo(detailItemToEdit)

        $("#txt-role-detail").text(description)
        $("#txt-role-detail").focus()
        $(clicked_button).text("save").attr("data-selector", "role-details-save");
        $(clicked_button).text("save");
    }

    static editGroupDetails(clicked_button, id) {
        let detailItemToEdit = document.getElementById(id)
        let description = detailItemToEdit.innerText

        detailItemToEdit.innerHTML = ""
        $('<textarea>').attr({
            //'type': 'text',
            'class': 'textarea-description-edit',
            'name': 'fname',
            'id': 'txt-group-detail',
            'value': description,
            'rows': '3'
        }).html(description).appendTo(detailItemToEdit)

        $("#txt-role-detail").text(description)
        $("#txt-role-detail").focus()
        $(clicked_button).text("save").attr("data-selector", "group-details-save");
        $(clicked_button).text("save");
    }

    static saveRoleDetails(clicked_button, id, role) {
        let current_ruleset = new pbPermissions().userrules[role]
        let detailItemToEdit = document.getElementById(id)
        let description = $("#txt-role-detail").val()

        current_ruleset.description = description
        new pbPermissions().userrules[role] = current_ruleset
        detailItemToEdit.innerText = description
        pbPermissions.instance.setRole(role)
        $(clicked_button).text("edit").attr("data-selector", "role-details-edit")

    }

    static saveGroupDetails(clicked_button, id, group) {
        let group_set = pbPermissions.instance.groups[group]
        let detailItemToEdit = document.getElementById(id)
        group_set.name = group
        delete group_set.readonly
        let description = $("#txt-group-detail").val()

        group_set.description = description
        detailItemToEdit.innerText = description

        pbPermissions.instance.setGroup(group)
        $(clicked_button).text("edit").attr("data-selector", "group-details-edit")

    }

    static validateRoleName(elem_id) {
        document.getElementById('role-error-msg').style.opacity = 0

        if (validateLetNumbUnderField(elem_id) != true) {
            let fieldName = elem_id
            let fieldWithId = document.getElementById(elem_id)
            let errorId = document.getElementById(fieldName + "Error")

            errorId.classList.replace("error", "error-visible")
            fieldWithId.classList.add("invalid")
            errorId.setAttribute("aria-hidden", false)
            errorId.setAttribute("aria-invalid", true)

        } else {
            let newrolename = document.getElementById(elem_id).value
            if (newrolename.startsWith("role.") != true) {
                newrolename = "role." + newrolename
            }
            //console.log("newrolename: ", newrolename, "role: ", role)
            return newrolename
        }
    }

    static validateGroupName(elem_id) {

        if (validateLetNumbUnderField(elem_id) != true) {
            let fieldName = elem_id
            let fieldWithId = document.getElementById(elem_id)
            let errorId = document.getElementById(fieldName + "Error")

            errorId.classList.replace("error", "error-visible")
            fieldWithId.classList.add("invalid")
            errorId.setAttribute("aria-hidden", false)
            errorId.setAttribute("aria-invalid", true)
        } else {
            let newgroupname = document.getElementById(elem_id).value
            if (newgroupname.startsWith("group.") != true) {
                newgroupname = "group." + newgroupname
            }
            return newgroupname
        }
    }

    static groups(containerElement, forceReload = false, groups = undefined) {

        if (forceReload) {
            new pbPermissions().init().then(() => {
                Render.groups(containerElement, false)
            })
            return
        }

        if (!groups) {
            groups = new pbPermissions().groups
        }

        let currentGroupSet = [];
        for (let [key, value] of Object.entries(groups)) {
            if (key.startsWith("group.") != false) {
                currentGroupSet.push({
                    "groupname": key,
                    "groupdescription": value.description
                });
            }

        }

        //create option buttons in bootstraptable
        for (let idx in currentGroupSet) {
            let group = currentGroupSet[idx]
            currentGroupSet[idx].options = ""
            if (new PbAccount().hasPermission("system.groups.management.update")) {
                let groupsEditButton = document.createElement("button")
                groupsEditButton.id = "groupsEditButtonBootstrap_" + group.groupname
                groupsEditButton.name = "groupsEditButtonBootstrap"
                groupsEditButton.innerHTML = "Bearbeiten"
                groupsEditButton.setAttribute('data-rolename', group.groupname);
                groupsEditButton.setAttribute('data-permission', "system.groups.management.update");
                groupsEditButton.setAttribute('class', "btn btn-indigo");
                currentGroupSet[idx].options = groupsEditButton.outerHTML
            }

            if (new PbAccount().hasPermission("system.groups.management.delete")) {
                let deleteGroupsButton = document.createElement("button")
                deleteGroupsButton.id = "deleteGroupsButtonBootstrap_" + group.groupname
                deleteGroupsButton.name = "deleteGroupsButtonBootstrap"
                deleteGroupsButton.setAttribute('data-groupname', group.groupname);
                deleteGroupsButton.setAttribute('class', "btn btn-outline-indigo btn-icon");
                deleteGroupsButton.setAttribute('data-permission', "system.groups.management.delete");
                let groupsDeleteIcon = document.createElement("i")
                groupsDeleteIcon.setAttribute('class', "typcn typcn-trash");
                groupsDeleteIcon.setAttribute('data-groupname', group.groupname);
                deleteGroupsButton.appendChild(groupsDeleteIcon)
                currentGroupSet[idx].options += deleteGroupsButton.outerHTML
            }
        }
        console.log("SET", currentGroupSet)
        $(function () {
            $(containerElement).bootstrapTable({
                data: currentGroupSet
            });
            $(containerElement).bootstrapTable('load', currentGroupSet)
        });

        $(document).off("click", 'button[name ="groupsEditButtonBootstrap"]')
        $(document).on("click", 'button[name ="groupsEditButtonBootstrap"]', function (event) {
            let state = {'group': event.target.dataset.rolename.replace('role.', '')}
            history.pushState(state, "")
            window.location.href = "./group-users.html?group=" + event.target.dataset.rolename.replace('role.', '')
        })

        $(document).off("click", 'button[name ="deleteGroupsButtonBootstrap"]')
        $(document).on("click", 'button[name ="deleteGroupsButtonBootstrap"]', function (event) {
            console.log(event.target.dataset)
            let clicked_group = event.target.dataset.groupname
            let deleteQuestion = new pbI18n().translate("Möchten Sie die Gruppe {GROUP} wirklich löschen?").replace("{GROUP}", clicked_group)
            if (confirm(deleteQuestion)) {
                pbPermissions.instance.deleteGroup(clicked_group).then(function () {
                    alert(new pbI18n().translate("Gruppe " + event.target.dataset.groupname + " wurde gelöscht."))
                    Render.groups(document.getElementById("bootstraptable"), true)
                })
            }
        })
    }

    static addGroupMembershipTags(groupname, groupid) {
        let id = "grouptag_" + groupid
        let groupElement = document.getElementById(id)
        if (groupElement == undefined) {
            let newButton = $("#group_button_template").clone()
            newButton.attr("id", "grouptag_" + groupid)
            newButton.attr("data-groupid", groupid)
            let buttonContent = newButton.html().replace("[{group_name}]", groupname)
            newButton.html(buttonContent)
            $("#group_memberships_container").append(newButton)
        }
    }


    static dataExportRange() {
        var start = moment().subtract(29, 'days')
        var end = moment()

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'))
            $('#easy2schedule_data_export').html('Datenexport von ' + start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY') + ' herunterladen' + '<i class="typcn icon typcn-download-outline tx-20 pl-2"></i>')
            $('#easy2schedule_data_export').attr('data-start', start.format('YYYY-MM-DD'));
            $('#easy2schedule_data_export').attr('data-end', end.format('YYYY-MM-DD'));

        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            maxDate: end,
            ranges: {
                //'Today': [moment(), moment()],
                //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                //'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Letzte 30 Tage': [moment().subtract(29, 'days'), moment()],
                'Diesen Monat': [moment().startOf('month'), moment().endOf('month')],
                'Letzten Monat': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Letzte 90 Tage': [moment().subtract(89, 'days'), moment()],
                'Dieses Jahr bis heute': [moment().startOf('year'), moment()],
                'Letztes Jahr': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
            locale: {
                format: "DD.MM.YYYY",
                // separator: " . ",
                applyLabel: "anwenden",
                cancelLabel: "abbrechen",
                fromLabel: "von",
                toLabel: "bis",
                customRangeLabel: "Benutzerdefinierte",
                weekLabel: "W",
                daysOfWeek: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
                "firstDay": 1
            },
            linkedCalendars: false,
            opens: "center",
            drops: "auto"

        }, cb);

        cb(start, end)
    }

    static dataExportLink(start, end) {
        let startDate = start
        let endDate = end
        let apikey = $.cookie("apikey")
        let calenderExportUri = CONFIG.API_BASE_URL + "/cb/events?apikey=" + apikey +
            "&start=" +
            startDate + "T00%3A00%3A00" +
            "&end=" +
            endDate + "T23%3A59%3A59" +
            "&timeZone=Europe%2FBerlin&format=csv"
        let a = document.createElement('a')
        a.target = '_blank'
        location.assign(calenderExportUri)
        // a.href = calenderExportUri
        //a.click()


    }

    static fromCookietoString(cookie, elemid, makeShort = false) {
        let raw_cookie_value = getCookie(cookie)
        let encode_cookie_value = encodeURIComponent(raw_cookie_value)
        let cookie_value = decodeURIComponent(encode_cookie_value)

        let tag = document.getElementById(elemid)

        if (tag == null) {
            return console.log("Warn: no element with ID" + elemid)
        }
        if (makeShort !== false) {
            tag.innerHTML = Render.toInitials(cookie_value)
        }

        return tag.innerHTML = cookie_value
    }

    static toInitials(my_string) {
        var firstChar = my_string.charAt(0);
        var lastChar = my_string.slice(-1);
        return my_string = firstChar + lastChar;
    }


}
