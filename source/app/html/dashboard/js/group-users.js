import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]";
import {getQueryParam} from "../../csf-lib/pb-functions.js?v=[cs_version]";
import {Render} from "./render.js?v=[cs_version]";


export class GroupUsers {
    userList = {}

    constructor() {
        if (!GroupUsers.instance) {
            GroupUsers.instance = this
            GroupUsers.instance.am = new PbAccount()
            GroupUsers.instance.perm = new pbPermissions()
            GroupUsers.instance.initListeners()
        }

        return GroupUsers.instance
    }


    init() {
        return new Promise(function (resolve, reject) {
            let group = getQueryParam("group")
            var arr = [GroupUsers.instance.am.getLogins(), GroupUsers.instance.perm.fetchGroupsFromApi(), GroupUsers.instance.perm.fetchRolesFromApi()]

            Promise.all(arr)
                .then(function (res) {
                    let [logins, groups, ] = res

                    console.log("INITIAL user_groups", groups)
                    $("#edited_name").html(group.replace("group.", ""))
                    $("#description").html(groups[group].description)
                    for (let login of logins) {
                        GroupUsers.instance.userList[login.userid] = login.username
                    }

                    resolve(GroupUsers.instance)
                })
                .catch(function () {
                    reject(GroupUsers.instance)
                }) // This is executed
        })
    }

    run() {
        let group = getQueryParam("group")
        $("#check-list-box").html("")

        $("#group_memberships_container").html("")
        for (let [id, login] of Object.entries(GroupUsers.instance.userList)) {
            let newUserListItem = $("#user_list_item").clone()
            newUserListItem.attr("id", "userlistentry_" + id)
            newUserListItem.html(newUserListItem.html().replace("[{user_name}]", login).replace("[{user_id}]", id))
            let alreadyAssignedUser = false
            if(GroupUsers.instance.perm.roleset.roles["user."+login] && GroupUsers.instance.perm.roleset.roles["user."+login].definition.import.includes(group))
            {
                alreadyAssignedUser = true
                Render.addGroupMembershipTags(login, id)
            }

            if (alreadyAssignedUser) {
                var myDomElement = $(newUserListItem).find("input");
                myDomElement.prop({checked: true})
            }

            $("#check-list-box").append(newUserListItem)
        }
    }

    initListeners() {
        $(document).on('click', 'input.user-check-box', function (e) {
            let clicked_user = $(this).attr("id")
            let users_role = "user." + GroupUsers.instance.userList[clicked_user]
            let users_role_set = GroupUsers.instance.perm.roleset.roles[users_role] || GroupUsers.instance.perm.userRuleSetTemplate
            if(users_role_set.readonly) {
                delete users_role_set.readonly
            }
            users_role_set.name = users_role
            let group = getQueryParam("group")

            if (e.target.checked == true) {
                if (!users_role_set.definition.import.includes(group)) {
                    users_role_set.definition.import.push(group)
                }
            } else {
                users_role_set.definition.import = users_role_set.definition.import.remove(group)
            }
            GroupUsers.instance.perm.groups[users_role] = users_role_set
            GroupUsers.instance.perm.setUserToGroup(users_role_set.name).then(GroupUsers.instance.run())

        })

        $(document).on('click', 'button[data-selector="btn-group"]', function () {
            let id = $(this).attr('data-groupid')

            $("#" + id).click()
            $(this).remove()
        })

        //edit role description in detail display area
        $(document).on('click', 'a[data-selector="group-details-edit"]', function () {
            let clicked_button = $(this)
            let id = "description"
            Render.editGroupDetails(clicked_button, id)
        })

        //save role descrition in detail display area
        $(document).on('click', 'a[data-selector="group-details-save"]', function () {
            let clicked_button = $(this)
            let id = "description"
            //let rule = $(this).closest('li').attr('data-ruleid')
            let group = getQueryParam("group")
            Render.saveGroupDetails(clicked_button, id, group)
        })
    }
}