
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"

export class RenderAccountSettings {

    account_id =  undefined

    constructor(account_id) {
        if (!RenderAccountSettings.instance) {
            RenderAccountSettings.instance = this
            RenderAccountSettings.instance.account_id = account_id
        }

        return RenderAccountSettings.instance
    }   

    setInputFields() {

        var toolbarOptions = [
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            ['bold', 'italic', 'underline'],
            [{ 'align': '' }, { 'align': 'center' }, { 'align': 'right' }],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            ['link'],
            ['clean']
        ]
    
        let quill = new Quill('#quillEditor', {
            modules: {
                toolbar: toolbarOptions
            },
            placeholder: 'Die Standardsignatur wird derzeit verwendet. Schreiben Sie etwas, um Ihr eigenes zu erstellen...',
            theme: 'snow'
        })

        //The official way for importing raw HTML
        quill.clipboard.dangerouslyPasteHTML(0, RenderAccountSettings.instance.account_id.accountProfileData.email_signature)
        
    }

    updateMessagingAccountSettings() {
    
    let quill = new Quill('#quillEditor')
    let signatureHtml = quill.root.innerHTML

    let css_style = 
    `<style>
    .ql-align-right  {text-align: right;}
    .ql-align-center {text-align: center;}
    </style>`

        //check if editor is empty except for the default return, and clear it on save if so
        if (signatureHtml == "<p><br></p>"){
            RenderAccountSettings.instance.clearQuillClipboard()
            RenderAccountSettings.instance.account_id.accountProfileData.email_signature = ""
            } else {
                RenderAccountSettings.instance.account_id.accountProfileData.email_signature = css_style + signatureHtml
        }
        RenderAccountSettings.instance.account_id.saveAccountProfileData(getCookie("consumer_id")).then(function () {
            Fnon.Hint.Success('Neue Einstellungen wird gespeichert.')
        return false
        })
    }

    clearQuillClipboard() {
        let quill = new Quill('#quillEditor', {
            placeholder: 'Die Standardsignatur wird derzeit verwendet. Schreiben Sie etwas, um Ihr eigenes zu erstellen...',
        })
        quill.root.innerHTML = "" 
    } 

}


 
        