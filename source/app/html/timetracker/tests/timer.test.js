import {PbTimer} from "../js/timer.js"

let utcReturnValue = Math.floor(Math.random() * 1000);
global.moment = () => {
    return {utc: () => utcReturnValue}
}

let mock_internal_fireEvent = undefined

jest.mock("../../csf-lib/pb-functions.js", () => {
    return {
        fireEvent: function (name, data) {
            mock_internal_fireEvent = name
        }
    }
});

test('Can create new instance of PbTimer', () => {
    let onBackendUpdateFuncCall = false
    let onBackendUpdateFunc = ()=> onBackendUpdateFuncCall = true
    let onCommandErrorCall = false
    let onCommandErrorFunc = ()=> onCommandErrorCall = true
    let onEventErrorCall = false
    let onEventErrorFunc = ()=> onEventErrorCall = true
    let onTimertickCall = false
    let onTimertickFunc = ()=> onTimertickCall = true

    let timer = new PbTimer("fakeapikey", {"test": "atest"}, undefined, onBackendUpdateFunc,onCommandErrorFunc,onEventErrorFunc, onTimertickFunc)
    timer.setData({
        "id": 1,
        "times": [],
        "time": 0,
        "title": ""
    })
    expect(timer.getData()).toStrictEqual({
        "id": 1,
        "times": [],
        "time": 0,
        "title": ""
    })
});

test('Cannot create new instance of PbTimer if id is undefined', () => {
    let timer = new PbTimer()
    let func = () => timer.setData({
        "id": undefined,
        "times": [],
        "time": 0,
        "title": ""
    })
    expect(func).toThrow(Error);
});

test('Cannot create new instance of PbTimer if id is undefined', () => {
    let timer = new PbTimer()
    timer.setData({
        "id": "123",
        "times": [],
        "time": 0,
        "title": ""
    })
    timer.tick()
});

test('Test whether tick function updates time', () => {
    let timer = new PbTimer()
    timer.setData({
        "id": 1,
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })
    timer.tick()
    expect(timer.getData().times[0].end).toBe(utcReturnValue)
});

test('Test whether starting and stopping of ticking works', () => {
    let timer = new PbTimer()
    timer.setData({
        "id": 1,
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    expect(timer.interval).toBeDefined()
    timer.finishTimer()
    expect(timer.interval).not.toBeDefined()
    timer.startTicking()
    expect(timer.interval).toBeDefined()
    timer.startTicking()
});

test('Test whether timetrackerCreated gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerCreated", function _(e) {
        mock_internal_fireEvent = "timetrackerCreated"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerCreated", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerCreated").toBe(true)
    mock_internal_fireEvent = undefined
});

test('Test whether timetrackerCreateFailed gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerCreateFailed", function _(e) {
        mock_internal_fireEvent = "timetrackerCreateFailed"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerCreateFailed", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerCreateFailed").toBe(true)
    mock_internal_fireEvent = undefined
});

test('Test whether timetrackerUpdated gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerUpdated", function _(e) {
        mock_internal_fireEvent = "timetrackerUpdated"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerUpdated", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerUpdated").toBe(true)
    mock_internal_fireEvent = undefined
});

test('Test whether timetrackerUpdated gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerUpdateFailed", function _(e) {
        mock_internal_fireEvent = "timetrackerUpdateFailed"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerUpdateFailed", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerUpdateFailed").toBe(true)
    mock_internal_fireEvent = undefined
});

test('Test whether timetrackerDeleted gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerDeleted", function _(e) {
        mock_internal_fireEvent = "timetrackerDeleted"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerDeleted", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerDeleted").toBe(true)
    mock_internal_fireEvent = undefined
});

test('Test whether timetrackerDeleteFailed gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerDeleteFailed", function _(e) {
        mock_internal_fireEvent = "timetrackerDeleteFailed"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerDeleteFailed", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerDeleteFailed").toBe(true)
    mock_internal_fireEvent = undefined
});

