import {getQueryParam} from "/csf-lib/pb-functions.js?v=[cs_version]"


export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init(TimeTrackerModuleInstance) {
        switch (getQueryParam("app")) {
            case "timetracker":
                import("./render.js?v=[cs_version]").then((Module) => {
                    new Module.Render(Controller.instance.socket).init(TimeTrackerModuleInstance)
                })
                break;
            default:
                import("./render.js?v=[cs_version]").then((Module) => {
                    new Module.Render(Controller.instance.socket).init(TimeTrackerModuleInstance)
                })
                break;
        }
    }
}

