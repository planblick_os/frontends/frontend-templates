import {getCookie, fireEvent} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"

export class PbTimer {
    #data = {}

    /* istanbul ignore next */
    constructor(apikey = "", config = {}, socket = undefined, onBackendUpdateFunc = () => "", onCommandError = () => "", onEventError = () => "", onTimerTick = () => "") {
        this.apikey = apikey
        this.config = config
        this.socket = socket
        this.#data = {
            "id": undefined,
            "times": [],

            "title": "",
            "state": "running"
        }
        this.onBackendUpdateFunc = onBackendUpdateFunc
        this.onTimerTick = onTimerTick
        this.onCommandError = onCommandError
        this.onEventError = onEventError
        this.initBackendListeners()
        this.interval = undefined
        this.db = undefined
        return this
    }

    /* istanbul ignore next */
    getDurationAsString() {
        return moment.utc(this.getDuration().as('milliseconds')).format('HH:mm:ss')
    }

    /* istanbul ignore next */
    getDuration() {
        let durations_sum = new moment.duration()
        for (let loggedTimeSpan of this.#data.times) {
            let end = new moment(loggedTimeSpan.end)
            durations_sum.add(moment.duration(end.diff(loggedTimeSpan.start)))
        }
        return durations_sum
    }

    startTicking(auto_store = true, addTimeSpan = false) {
        if (!this.interval) {
            this.#data.state = "running"
            if(addTimeSpan) {
                this.#data.times.push({"start": moment().utc()})
            }
            this.interval = window.setInterval(this.tick.bind(this), 1000)
            if (auto_store) {
                this.store()
            }
        }
    }

    finishTimer(auto_store = true) {
        clearInterval(this.interval)
        this.#data.state = "finished"
        this.interval = undefined
        if (auto_store) {
            this.store()
        }
    }

    pauseTicking(auto_store = true) {
        clearInterval(this.interval)
        this.#data.state = "paused"
        this.interval = undefined
        if (auto_store) {
            this.store()
        }
    }

    tick() {
        console.log("TICK", this.#data.times)
        let lastTimeSpan = this.#data.times.pop()
        if (!lastTimeSpan?.end) {
            lastTimeSpan = {"start": moment().utc(), "end": moment().utc()}
        }
        lastTimeSpan.end = moment().utc()
        this.#data.times.push(lastTimeSpan)
        this.onTimerTick(this)
    }

    setData(data) {
        if (data?.id == undefined || data?.times == undefined || data?.title == undefined) {
            throw new Error("Data has to include the fields id, times, time and title")
        }

        this.#data = data
        return this
    }

    getData() {
        return this.#data
    }

    store() {
        clearInterval(this.interval)
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/updateTimetracker",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "data": this.#data,
                "data_owners": []
            }),
            "success": () => {
            },
            "error": this.onCommandError
        };

        $.ajax(settings)
    }

    initBackendListeners() {

        document.addEventListener("event", function _(e) {
            if (e.data && e.data.data && e.data.data.event == "timetrackerUpdated") {
                if (e.data.data?.args?.data?.id == this.#data.id) {
                    fireEvent("timetrackerUpdated", e.data.data)
                }
            }
        }.bind(this))

        return this
    }
}
