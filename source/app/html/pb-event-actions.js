import {PbBaseEventActions} from "/csf-lib/pb-base-event-actions.js?v=[cs_version]"
import {PbAccount} from "/csf-lib/pb-account.js?v=[cs_version]";
import {getCookie, setCookie} from "/csf-lib/pb-functions.js?v=[cs_version]";
import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {CONFIG} from "./pb-config.js?v=[cs_version]";


if (window.matchMedia('(display-mode: standalone)').matches) {
    // if webapp installed, remove 'target' attribute of links
    document.querySelectorAll('a').forEach(function (a) {
        a.removeAttribute('target');
    });
}

// Select the target node (usually the parent element where changes might occur)
const targetNode = document.body;

// Options for the observer (which mutations to observe)
const config = {childList: true, subtree: true};

// Callback function to execute when mutations are observed
const callback = function (mutationsList, observer) {
    // Iterate through mutations
    for (let mutation of mutationsList) {
        // Check if nodes were added or removed
        if (mutation.type === 'childList' && window.matchMedia('(display-mode: standalone)').matches) {
            // Check each added node for anchor elements and add click event listener
            document.querySelectorAll('a').forEach(function (a) {
                a.removeAttribute('target');
            });
        }
    }
};

// Create a new observer with the specified configuration and callback
const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(targetNode, config);

export class PbEventActions extends PbBaseEventActions {
    constructor() {
        if (getCookie("r") != 1) {
            setCookie("r", 1, {expires: 1, path: window.location.pathname})
            location.reload(true)
        }
        super();
        this.initActions()
        this.initGlobalListeners()
        new PbTpl().init()
        if (!PbEventActions.instance) {
            PbEventActions.instance = this;
        }

        PbEventActions.instance.notification_or_audio = true
        return PbEventActions.instance;
    }

    initActions() {
        this.actions["init"] = function (myevent, callback = () => "") {
            callback()
        }

        this.actions["initCalendarModule"] = function (myevent, callback = () => "") {
            import("./calendar/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.CalendarModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleLogin"] = function (myevent, callback = () => "") {
            import("./login/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.LoginModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleRegistration"] = function (myevent, callback = () => "") {
            import("./registration/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.RegistrationModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleDashboard"] = function (myevent, callback = () => "") {
            import("./dashboard/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.DashboardModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleE2OForm"] = function (myevent, callback = () => "") {
            import("./prototypes/easy2order/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.E2OFormModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleEasy2Schedule"] = function (myevent, callback = () => "") {
            import("./easy2schedule/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.Easy2ScheduleModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleEasy2TrackVisitors"] = function (myevent, callback = () => "") {
            import("./easy2track/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.Easy2TrackModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["contactManagerInit"] = function (myevent, callback = () => "") {
            import("./contact-manager/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.ContactManagerModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initEventManager"] = function (myevent, callback = () => "") {
            import("./eventmanager/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.EventManagerModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["taskManagerInit"] = function (myevent, callback = () => "") {
            import("./task-manager/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.TaskManagerModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                Fnon.Wait.Circle('', {
                    svgSize: {w: '0px', h: '0px'},
                    svgColor: '#3a22fa',
                })
                Fnon.Wait.Init({
                    fontFamily: '"Roboto", sans-serif',
                    textColor: '#3a22fa',
                    svgSize: {w: '50px', h: '50px'},
                    svgColor: '#3a22fa',
                    textSize: '12px'
                })
                callback(myevent)
            })
        }
        this.actions["initModuleEasy2Gether"] = function (myevent, callback = () => "") {
            import("./easy2gether/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.Easy2GetherModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleEasy2GetherPro"] = function (myevent, callback = () => "") {
            import("./easy2gether/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.Easy2GetherProModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleProtoype"] = function (myevent, callback = () => "") {
            import("./prototypes/roommanagement/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.PrototypeModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleDocumentation"] = function (myevent, callback = () => "") {
            import("./documentation/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.DocumentationModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }
        this.actions["initModuleTimetracker"] = function (myevent, callback = () => "") {
            import("/timetracker/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.TimetrackerModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initModulePharmacyPrototyp"] = function (myevent, callback = () => "") {
            import("./pharmacy/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.PharmacyPrototypModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initModuleQuestionaire"] = function (myevent, callback = () => "") {
            import("/questionaire/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.QuestionaireModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initModuleCruises"] = function (myevent, callback = () => "") {
            import("/cruises/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.CruisesModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initModuleSysAdmin"] = function (myevent, callback = () => "") {
            import("./sysadmin/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.SysAdminModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initSystemAccountAddress"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account.profile")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new PbAccount().init(getCookie("consumer_id")).then(
                        function () {
                            callback(myevent)
                        }
                    )
                }
            })
        }
        this.actions["systemAccountAddressInitialized"] = function (myevent, callback = () => "") {
            new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
            new RenderAccountAddress(new PbAccount()).setAddressData()
            Controller.fromCookietoString("consumer_name", "account_company_name")

            callback(myevent)
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
        }

        this.actions["initOrderWizard"] = function (myevent, callback = () => "") {
            import("./dashboard/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.DashboardModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initDashboardModule"] = function (myevent, callback = () => "") {
            import("./dashboard/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.DashboardModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initModuleThreecolumn"] = function (myevent, callback = () => "") {
            import("./prototypes/threecolumn/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.ThreecolumnModule().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }

        this.actions["initAppCrm"] = function (myevent, callback = () => "") {
            import("./apps/crm/js/csf-load-module.js?v=[cs_version]").then((Module) => {
                let actions = new Module.CrmApp().actions
                PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
                callback(myevent)
            })
        }


        // this.actions["initSimplyCollectMerchant"] = function (myevent, callback = () => "") {
        //     let actions = new SimplyCollectModule().actions
        //     PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
        //     callback(myevent)
        // }

        // this.actions["initSimplyCollect"] = function (myevent, callback = () => "") {
        //     let actions = new SimplyCollectModule().actions
        //     PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
        //     callback(myevent)
        // }


        this.actions["logoutButtonClicked"] = function (myevent, callback = () => "") {
            new PbAccount().logout()
            callback(myevent)
        }
    }

    initGlobalListeners() {
        document.addEventListener("browser_notification", function (event) {
            console.log("EVENT", event)
            PbEventActions.instance.notifyUser(event.data.title, {body: event.data.body})
        })

        document.addEventListener("notification", function (event) {
            if (event.data.data.event == "showEasy2scheduleEventReminder") {

                var settings = {
                    "url": CONFIG.API_BASE_URL + "/cb/event/" + event.data.data.args.event_id,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey")
                    },
                    "success": function (response) {
                        let startdate = new Date(response.starttime.replace(" ", "T"));
                        let options = {
                            year: 'numeric',
                            month: '2-digit',
                            day: '2-digit',
                            hour: '2-digit',
                            minute: '2-digit'
                        };
                        startdate = new Intl.DateTimeFormat('de-DE', options).format(startdate)
                        let enddate = new Date(response.endtime.replace(" ", "T"));
                        enddate = new Intl.DateTimeFormat('de-DE', options).format(enddate)
                        PbEventActions.instance.notification_or_audio = !PbEventActions.instance.notification_or_audio
                        Fnon.Hint.Success(`${startdate} - ${enddate}<br/>Titel: ${response.title}`, {displayDuration: 30000})
                        // fireEvent("browser_notification", {
                        //     "title": 'Easy2Schedule: Termin-Erinnerung',
                        //     "body": `${startdate} - ${enddate}\nTitel: ${response.title}`
                        // })
                        // let audio = new Audio('https://www.thesoundarchive.com/email/IGORMAIL.mp3');
                        // audio.play();
                        // Fnon.Alert.Info({
                        //     title: 'Easy2Schedule: Termin-Erinnerung',
                        //     message: `${startdate} - ${enddate}<br/>Titel: ${response.title}`,
                        //     callback: () => {
                        //         // do some thing
                        //         console.log('Dismissed');
                        //     }
                        // });
                    }
                };

                $.ajax(settings).done(function () {
                });
            }
            if (event.data.data.event == "showTaskReminder") {
                console.log("TASK DATA", event.data.data)
                var settings = {
                    "url": CONFIG.API_BASE_URL + "/tasks/by_id/" + event.data.data.args.task_id,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey")
                    },
                    "success": function (response) {
                        let due_date = new Date(response.task_data.due_date.replace(" ", "T"));
                        let options = {
                            year: 'numeric',
                            month: '2-digit',
                            day: '2-digit',
                            hour: '2-digit',
                            minute: '2-digit'
                        };
                        due_date = new Intl.DateTimeFormat('de-DE', options).format(due_date)
                        PbEventActions.instance.notification_or_audio = !PbEventActions.instance.notification_or_audio
                        Fnon.Hint.Success(`Aufgaben-Erinnerung:<br/>Ablaufdatum: ${due_date}<br/>Titel: ${response.task_data.title}`, {displayDuration: 30000})
                    }
                };

                $.ajax(settings).done(function () {
                });


            }
        })
    }

    notifyUser(title, options) {
        var regex = /<br>/gi;
        title = title.replace(regex, "\n");
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
            alert("This browser does not support desktop notification");
        }

        // Let's check whether notification permissions have alredy been granted
        else if (Notification.permission === "granted") {
            // If it's okay let's create a notification
            new Notification(title, options);
        }

        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                // If the user accepts, let's create a notification
                if (permission === "granted") {
                    new Notification(title, options);
                }
            });
        }

        // At last, if the user has denied notifications, and you
        // want to be respectful there is no need to bother them any more.
    }
}


