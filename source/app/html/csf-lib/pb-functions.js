import {PbVoiceRecognition} from "./pb-voice-recognition.js?v=[cs_version]"
import {pbBatch} from "./pb-batch.js?v=[cs_version]"
import {pbSmartspeech} from "./pb-smartspeech.js?v=[cs_version]"
import {socketio} from "./pb-socketio.js?v=[cs_version]"
import {CONFIG} from "../pb-config.js?v=[cs_version]"
import {PbAccount} from "./pb-account.js?v=[cs_version]"
import {PbLogin} from "./pb-login.js?v=[cs_version]"
import {PbGuide} from "./pb-guide.js?v=[cs_version]"


export function getStackTrace() {
    const error = new Error();
    return error.stack;
}
export function checkPasswordStrength(password) {
    //Regular Expressions
    var regex = new Array();
    regex.push("[A-Z]"); //For Uppercase Alphabet
    regex.push("[a-z]"); //For Lowercase Alphabet
    regex.push("[0-9]"); //For Numeric Digits
    regex.push("[$@$!%*#?&]"); //For Special Characters

    var passed = 0;

    //Validation for each Regular Expression
    for (var i = 0; i < regex.length; i++) {
        if ((new RegExp(regex[i])).test(password)) {
            passed++;
        }
    }

    //Validation for Length of Password
    if (passed > 2 && password.length > 8) {
        passed++;
    }

    return passed
}

export function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

export function showNotification(message) {
    alert(message)
}

export function parseJsonOrReturnData(data) {
    try {
        return JSON.parse(data);
    } catch (e) {
        return data;
    }
}

export function fireEvent(event_name, data = {}, transmitToRoom = false, supress_locally = false) {
    let evt = new Event(event_name);
    evt.data = parseJsonOrReturnData(data)
    console.info("Firing event: ", event_name, data)
    if ([].includes(event_name))
        console.trace();
    document.addEventListener(event_name, function _event_logger(event) {
        //console.log("Event caught: ", event_name, event.data)
        document.removeEventListener(event_name, _event_logger)
    }, false);

    if (!supress_locally)
        document.dispatchEvent(evt);

    if (transmitToRoom) {
        new socketio().socket.emit("command", {
            command: "fireEvent",
            args: {"eventname": event_name, "eventdata": data},
            room: transmitToRoom
        })
    }

}

export function waitFor(condition, callback) {
    if (!condition()) {
        console.info('waiting for', condition);
        window.setTimeout(waitFor.bind(null, condition, callback), 100); /* this checks the flag every 100 milliseconds*/
    } else {
        console.info('stop waiting for', condition);
        callback();
    }
}

export function speech(text, callback = () => "", init_voice = false) {
    if (!callback) {
        callback = () => ""
    }
    new pbSmartspeech().say(text, callback);
}

export function localspeech(text, callback = null, init_voice = false) {
    let potentialFileExtension = text.split(".").pop()
    console.log(potentialFileExtension)
    let supportedFileExtensions = ["wav", "mp3"]
    if (supportedFileExtensions.includes(potentialFileExtension)) {
        playSpeechWav(text, callback)
        return
    }

    let speechTextPotentialAudioFile = md5(text) + ".mp3"


    let stimmen = window.speechSynthesis.getVoices();

    for (var i = 0; i < stimmen.length; i++) {
        console.log("Stimme " + i.toString() + " " + stimmen[i].name);
    }
    var worte = new SpeechSynthesisUtterance(text);
    worte.voice = stimmen[2];
    worte.lang = "de-DE";
    if (callback != null) {
        worte.onend = callback
    }
    if (init_voice === false) {
        window.speechSynthesis.cancel();
        window.speechSynthesis.speak(worte);
    }
}

export function playSpeechWav(srcUrl, callback = null) {
    let soundFile = undefined
    let src = undefined
    if (document.getElementById("audioplayer")) {
        soundFile = document.getElementById("audioplayer")
    } else {
        soundFile = document.createElement("audio")
        soundFile.id = "audioplayer"
        soundFile.preload = "auto";
        src = document.createElement("source");
        src.id = "audioplayerSource"
        src.src = "";
        soundFile.appendChild(src);
    }

    src.src = srcUrl;
    soundFile.load();
    soundFile.currentTime = 0.01;
    soundFile.volume = 1.000000;
    soundFile.play();
    if (callback) {
        soundFile.onended = callback
    } else {
        soundFile.onended = () => ""
    }

}

export function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) {
        return match[2]
    } else {
        return false;
    }
}

export function setCookie(name, value, options = {}) {
    window.jQuery.cookie(name, value, options)
}

export function fillFormFieldsInteractively(callback) {
    let form = document.getElementsByTagName("form")[0]

    if (form) {
        for (let element of Array.from(form.getElementsByTagName("input"))) {
            let id = element.id
            new pbBatch().add(id, function () {
                getValueForInputElement(element)
            })
            //let result = await getValueForInputElement(element)

            //element.value = result
        }
        new pbBatch().add("form_submit", function () {
            form.submit()
        }, true)
        // if(form) {
        //     Array.from(form.getElementsByTagName("input")).forEach(inputElement => {
        //         console.log("Input element:", inputElement.labels)
        //         //speech("Was soll ich für " + inputElement.labels[0].innerText + " als Wert einsetzen?")
        //         await getValueForInputElement(inputElement)
        //     })
        // }
    }
    //callback()
}

export async function getValueForInputElement(inputElement) {
    let promise = new Promise((resolve, reject) => {
        speech("Was soll ich für " + inputElement.labels[0].innerText + " als Wert einsetzen?", function () {
            new PbVoiceRecognition().startRecording({
                "type": "syncInputValueGathering",
                "data": {"element_id": inputElement.id}
            })
        })
    })
    return promise

}

export function makeId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

export function makePassword(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!()#@$%^&*';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

export function encrypt(value, passphrase) {
    return CryptoJS.AES.encrypt(value, passphrase)
}

export function decrypt(value, passphrase) {
    return CryptoJS.AES.decrypt(value, passphrase).toString(CryptoJS.enc.Utf8)
}

export function md5(value) {
    return CryptoJS.MD5(value).toString();
}

export function imagesEqual(imageElementA, imageElementB, tolerance = 245) {
    //console.log("using tolerance of: ", tolerance)
    return imagediff.equal(imageElementA, imageElementB, tolerance);

}

export function watchForChangesInVideo(videoElement, onChange = () => "", onNoChange = () => "", tolerance = 245, interval = 250) {
    setInterval(function () {
        _watchForChangesInVideo(videoElement, onChange, onNoChange(), tolerance, interval)
    }, interval)
}

function _watchForChangesInVideo(videoElement, onChange = () => "", onNoChange = () => "", tolerance, interval) {
    const canvas = document.createElement("canvas");
    // scale the canvas accordingly
    canvas.width = videoElement.videoWidth;
    canvas.height = videoElement.videoHeight;
    // draw the video at that frame
    canvas.getContext('2d').drawImage(videoElement, 0, 0, canvas.width, canvas.height);
    // convert it to a usable data URL
    const dataURL = canvas.toDataURL("image/jpeg", 1.0);
    var img = document.getElementById("cmpImage1") || document.createElement("img");
    img.id = "cmpImage1"
    img.src = dataURL;
    img.style.display = "none"
    if (!document.getElementById("cmpImage1")) {
        document.getElementById("mediaOptions").append(img)
    }

    let img2 = document.getElementById("cmpImage2")

    let isEqual = null
    if (img && img2) {
        isEqual = imagesEqual(img, img2, tolerance)
        //console.log("IMAGES", img, img2, img.src == img2.src, isEqual)
        if (!isEqual) {
            onChange(img2, img)
        } else {
            onNoChange(img2, img)
        }
    }

    setTimeout(function () {
        img2 = document.getElementById("cmpImage2") || document.createElement("img");
        img2.id = "cmpImage2"
        img2.src = dataURL
        img2.style.display = "none"
        if (!document.getElementById("cmpImage2")) {
            document.getElementById("mediaOptions").append(img2)
        }
    }, interval)

    return isEqual
}

export function enterFullscreen(element = document.documentElement) {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    }
}

export function getQueryParam(name) {
    let urlParams = new URLSearchParams(window.location.search)
    if (urlParams.has(name)) {
        return urlParams.get(name)
    } else {
        return undefined
    }
}

export function setQueryParam(name, value) {
    let urlParams = new URLSearchParams(window.location.search);
    urlParams.set(name, value);
    window.history.replaceState({}, '', `${window.location.pathname}?${urlParams}`);
}


export function getLocaleFormattedTime(date = new Date(), hours = true, minutes = true, seconds = false, locale = navigator.language || navigator.browserLanguage || navigator.userLanguage || "de_DE", hour12 = false) {
    let options = {
        hour12: hour12
    };
    if (hours)
        options["hour"] = 'numeric'
    if (minutes)
        options["minute"] = 'numeric'
    if (seconds)
        options["second"] = 'numeric'

    return new Intl.DateTimeFormat(locale, options).format(date)
}

export function getLocaleFormattedDate(date = new Date(), year = true, month = true, day = true, hours = true, minutes = true, seconds = false, locale = navigator.language || navigator.browserLanguage || navigator.userLanguage || "de_DE", hour12 = false) {
    let options = {
        hour12: hour12
    };

    if (year)
        options["year"] = 'numeric'
    if (month)
        options["month"] = '2-digit'
    if (day)
        options["day"] = '2-digit'

    if (hours)
        options["hour"] = 'numeric'
    if (minutes)
        options["minute"] = 'numeric'
    if (seconds)
        options["second"] = 'numeric'

    return new Intl.DateTimeFormat(locale, options).format(date)
}

export function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

export async function loadNotifications(target_selector = "#notification_container") {
    new Promise((resolve, reject) => {
        let settings = {
            "url": CONFIG.API_BASE_URL + "/notifications/list",
            "method": "GET",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
            },
            "error": ()=>"",
            "success": function (response) {
                response.sort((a, b) => {
                    return new Date(a.date) - new Date(b.date);
                }).reverse()

                $("#notification_icon").removeClass("ion-ios-mail-unread")
                $("#notification_icon").addClass("ion-ios-mail")
                $("#notification_icon").css("color", "initial")

                $(target_selector).html("")
                if (!target_selector) {
                    resolve(response)
                } else {
                    let unread_message = false
                    for (let notification of response) {
                        //let read_unread = $.parseHTML(`<i onclick="setNotificationRead('${notification.id}')" class="far fa-eye tx-dark-blue tx-16 mr-2" title="Als gelesen markieren"></i>`)
                        let read_unread = $("<i>").addClass("far fa-eye tx-dark-blue tx-16 mr-2 cursor-finger").attr("title", "Als gelesen markieren").click(function () {
                            setNotificationRead(notification.id)
                        })
                        if (notification.data.read) {
                            read_unread = $("<i>").addClass("far fa-eye-slash tx-dark-blue tx-16 mr-2 cursor-finger").attr("title", "Als ungelesen markieren").click(function () {
                                setNotificationUnread(notification.id)
                            })
                        } else {
                            unread_message = true
                        }
                        let deleteNotificationIcon = $("<i>").addClass("fa fa-trash tx-dark-blue tx-16 mr-2").attr("title", "Benachrichtigung löschen").click(function () {
                            deleteNotification(notification.id)
                        })

                        $(target_selector).append(read_unread)
                        $(target_selector).append(deleteNotificationIcon)
                        $(target_selector).append(
                            (notification.data.read ? "<strike>" : "") +
                            "<strong>" +
                            getLocaleFormattedDate(new Date(notification.date)) +
                            " " + notification.data.params.title +
                            "</strong><br/>" +
                            notification.data.params.message + "<br/>" + (notification.data.read ? "</strike>" : "") +
                            "<hr/>")
                    }
                    if(unread_message) {
                        $("#notification_icon").removeClass("ion-ios-mail")
                        $("#notification_icon").addClass("ion-ios-mail-unread")
                        $("#notification_icon").css("color", "green")
                    }
                    resolve(response)
                }
            }
        };
        $.ajax(settings)
    })
}

export function setNotificationRead(notification_id) {
    let settings = {
        "url":  CONFIG.API_BASE_URL + "/notifications/put?update_only=true",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "apikey": getCookie("apikey"),
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "id": notification_id,
            "data": {
                "read": true,
            }
        }),
    };

    $.ajax(settings)
}

export function setNotificationUnread(notification_id) {
    let settings = {
        "url":  CONFIG.API_BASE_URL + "/notifications/put?update_only=true",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "apikey": getCookie("apikey"),
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "id": notification_id,
            "data": {
                "read": false,
            }
        }),
    };

    $.ajax(settings)
}

export function deleteNotification(notification_id) {
    let settings = {
        "url": CONFIG.API_BASE_URL + `/notifications/delete/${notification_id}`,
        "method": "DELETE",
        "timeout": 0,
        "headers": {
            "apikey": getCookie("apikey"),
        },
    };
    $.ajax(settings)
}

export function get_news_for(app) {
    for (let news of CONFIG.NEWS) {
        if (news.show_in_apps.includes(app)) {
            $.ajax({
                type: 'GET',
                url: news.content_uri,
                dataType: 'html',
                success: function (data) {
                    fireEvent("news_available", {"config": news, "data": data})
                    // let evt = new Event('news_available');
                    // evt.content_id = news.content_id
                    // evt.force_display = news.force_display
                    // evt.data = data
                    // document.dispatchEvent(evt);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ERROR NEWS", errorThrown)
                }
            });
        }
    }
}


export function checkAccountForNews(event, account_data) {
    event = event.data
    account_data = account_data
    if (account_data["read_news"] && account_data("read_news").includes(event.config.content_id))
        return
    console.log("Marked in account as read", account_data)
}

export function renderNews(event) {
    event = event.data
    if ($.cookie("read_news") && $.cookie("read_news").includes(event.config.content_id))
        return
    $("#news_bell").addClass("active")
    let doc = new DOMParser().parseFromString(event.data, "text/xml");
    let news_content = doc.getElementById(event.config.content_id)

    let approve_button = $('<div id="button_' + event.config.content_id + '" class="ui approve button news_read_button btn btn-primary" onclick="setNewsCookieRead(\'' + event.config.content_id + '\')">Als gelesen markieren</div>')

    $("#news_container").append(news_content.innerHTML)
    $("#news_container").append(approve_button)
    $("#news_container").append("<br/><br/><br/>")

    if (event.config.force_display) {
        $("#news_modal").modal('show')
    }
}

export function setNewsCookieRead(news_content_id) {
    $("#button_" + news_content_id).prev().remove()
    $("#button_" + news_content_id).remove()

    $("#news_bell").removeClass("active")
    let cookie_content = $.cookie("read_news")
    if (!cookie_content)
        cookie_content = ""
    cookie_content += "/" + news_content_id
    $.cookie("read_news", cookie_content, {expires: 730, path: '/'})
    setNewsAccountRead(news_content_id)
    //$("#news_modal").modal('hide')
}

export function setNewsAccountRead(news_content_id, account_data) {
    let news_read = account_data["read_news"]
    if (!news_read) {
        news_read = ""
    }
    news_read += "/" + news_content_id
    account_data["read_news"] = news_read
}

export function handle_mousedown(e) {
    console.log(e)
    if (e.target.tagName != "H2" && e.target.id != "icon_menu_wrapper" && !e.target.classList.contains("dragger"))
        return;
    window.my_dragging = {};
    my_dragging.pageX0 = e.pageX;
    my_dragging.pageY0 = e.pageY;
    my_dragging.elem = this;
    my_dragging.offset0 = $(this).offset();

    function handle_dragging(e) {
        var left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0);
        var top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
        $(my_dragging.elem)
            .offset({top: top, left: left});
    }

    function handle_mouseup(e) {
        $('body')
            .off('mousemove', handle_dragging)
            .off('mouseup', handle_mouseup);
    }

    $('body')
        .on('mouseup', handle_mouseup)
        .on('mousemove', handle_dragging);
}

export function multiStringReplace(object, string) {
    var val = string
    var entries = Object.entries(object);
    entries.forEach((para) => {
        var find = '\\[{' + para[0] + '}\\]'
        var regExp = new RegExp(find, 'g')
        val = val.replace(regExp, para[1])
    })
    return val;
}

Array.prototype.remove = function (i) {
    if (this != undefined && this.filter != undefined) {
        return this.filter(f => f !== i)
    } else {
        return false
    }
}

export function removeFromArray(array, val) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === val) {
            array.splice(i, 1);
            i--;
        }
    }
    return array;
}


export function array_merge_distinct(array1, array2) {
    const array3 = [...new Set([...array1, ...array2])];
    return array3
}


export function array_intersect(array1, array2) {
    var mergedElems = {},
        result = [],
        firstArray, secondArray,
        firstN = 0,
        secondN = 0;

    function generateStrKey(elem) {
        var typeOfElem = typeof elem;
        if (typeOfElem === 'object') {
            typeOfElem += Object.prototype.toString.call(elem);
        }
        return [typeOfElem, elem.toString(), JSON.stringify(elem)].join('__');
    }

    // Executes the loops only if both arrays have values
    if (array1.length && array2.length) {
        // Begins with the shortest array to optimize the algorithm
        if (array1.length < array2.length) {
            firstArray = array1;
            secondArray = array2;
        } else {
            firstArray = array2;
            secondArray = array1;
        }

        firstArray.forEach(function (elem) {
            var key = generateStrKey(elem);
            if (!(key in mergedElems)) {
                mergedElems[key] = {elem: elem, inArray2: false};
                // Increases the counter of unique values in the first array
                firstN++;
            }
        });

        secondArray.some(function (elem) {
            var key = generateStrKey(elem);
            if (key in mergedElems) {
                if (!mergedElems[key].inArray2) {
                    mergedElems[key].inArray2 = true;
                    // Increases the counter of matches
                    secondN++;
                    // If all elements of first array have coincidence, then exits the loop
                    return (secondN === firstN);
                }
            }
        });

        Object.values(mergedElems).forEach(function (elem) {
            if (elem.inArray2) {
                result.push(elem.elem);
            }
        });
    }

    return result;
}

export function objectDiff(obj1, obj2, exclude) {
    var r = {};

    if (obj1 == null) {
        obj1 = {}
    }
    if (obj2 == null) {
        obj2 = {}
    }
    if (!exclude) exclude = [];

    for (var prop in obj1) {
        if (obj1.hasOwnProperty(prop) && prop != '__proto__') {
            if (exclude.indexOf(obj1[prop]) == -1) {

                // check if obj2 has prop
                if (!obj2.hasOwnProperty(prop)) r[prop] = obj1[prop];

                    // check if prop is object and
                // NOT a JavaScript engine object (i.e. __proto__), if so, recursive diff
                else if (obj1[prop] === Object(obj1[prop])) {
                    var difference = objectDiff(obj1[prop], obj2[prop]);
                    if (Object.keys(difference).length > 0) r[prop] = difference;
                }

                // check if obj1 and obj2 are equal
                else if (obj1[prop] !== obj2[prop]) {
                    if (obj1[prop] === undefined)
                        r[prop] = 'undefined';
                    if (obj1[prop] === null)
                        r[prop] = null;
                    else if (typeof obj1[prop] === 'function')
                        r[prop] = 'function';
                    else if (typeof obj1[prop] === 'object')
                        r[prop] = 'object';
                    else
                        r[prop] = obj1[prop];
                }
            }
        }

    }

    return r;

}

export function getObjectDiffPaths(obj1, obj2, path = []) {
    let diffPaths = [];
    for (const key in obj1) {
        if (obj2.hasOwnProperty(key)) {
            if (typeof obj1[key] === 'object' && typeof obj2[key] === 'object') {
                diffPaths = diffPaths.concat(getObjectDiffPaths(obj1[key], obj2[key], path.concat(key)));
            } else {
                if (obj1[key] !== obj2[key]) {
                    diffPaths.push(path.concat(key).join('.'));
                }
            }
        } else {
            diffPaths.push(path.concat(key).join('.'));
        }
    }
    diffPaths = diffPaths.filter(path => !path.includes(".remove"))
    return diffPaths;
}


export function getWelcome(module_name) {
    new PbAccount().init().then(() => {
            if (PbLogin.instance?.profileData?.tours_seen?.[module_name]) {
                return
            }

            let welcome_modal_id = "#" + module_name + "_welcome"

            $(welcome_modal_id).show()
            $("#button_dismiss").on('click', function () {
                let guide = new PbGuide()
                guide.markTourSeen(module_name)
                $(welcome_modal_id).hide()
            })
        }
    )

}

export function getTourParam() {
    let current_tour = undefined
    if (window.location.toString().includes("tour")) {
        current_tour = getQueryParam("tour")
        // alert(current_tour)
        $('#' + current_tour).trigger("click");
    }
}

const coefficients = {
    "minutes": 60,
    "hours": 3600,
    "days": 86400,
    "weeks": 604800
}

export function calculateInterval(value, unit, timeCoefficients = coefficients) {
    let scheduling_interval = value * timeCoefficients[unit]
    return scheduling_interval
}

export function interpretInterval(value, timeCoefficients = coefficients) {
    let intervalObject = {}
    let reversedCoefficients = Object.keys(timeCoefficients).reverse()
    for (let i = 0; i < reversedCoefficients.length; i++) {
        if (value % timeCoefficients[reversedCoefficients[i]] == 0) {
            intervalObject[reversedCoefficients[i]] = value / timeCoefficients[reversedCoefficients[i]]
        }
    }
    return intervalObject

}

export function dateDiff(delta_date, basedate = new Date()) {
    const secondsInAMin = 60
    const secondsInAnHour = 60 * secondsInAMin
    const secondsInADay = 24 * secondsInAnHour
    let remainingSecondsInDateDiff = (delta_date - basedate) / 1000
    let days = Math.floor(remainingSecondsInDateDiff / secondsInADay)
    let remainingSecondsAfterDays = remainingSecondsInDateDiff - (days * secondsInADay)
    let hours = Math.floor(remainingSecondsAfterDays / secondsInAnHour)
    let remainingSecondsAfterhours = remainingSecondsAfterDays - (hours * secondsInAnHour)
    let mins = Math.floor(remainingSecondsAfterhours / secondsInAMin)
    let seconds = Math.floor(remainingSecondsAfterhours - (mins * secondsInAMin))

    return {"days": days, "hours": hours, "mins": mins, "seconds": seconds, "diff_in_seconds": Math.floor(remainingSecondsInDateDiff)}
}

export function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

export function removeParamFromAddressBar(parameter) {
    var url = document.location.href;
    var urlparts = url.split('?');

    if (urlparts.length >= 2) {
        var urlBase = urlparts.shift();
        var queryString = urlparts.join("?");

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = queryString.split(/[&;]/g);
        for (var i = pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        if (pars.length == 0) {
            url = urlBase;
        } else {
            url = urlBase + '?' + pars.join('&');
        }

        window.history.pushState('', document.title, url); // push the new url in address bar
    }
    return url;
}

export function togglePassword(formInputId, buttonId) {
    var x = $("#" + formInputId);
    var y = $("#" + buttonId);
    if (x.is("[type=password]")) {
        x.attr("type", "text");
        y.html("<i class='far fa-eye-slash'></i>");
    } else {
        x.attr("type", "password");
        y.html("<i class='far fa-eye'></i>");
    }
}

export const setPath = (object, path, value) => path
    .split('.')
    .reduce((o, p, i) => o[p] = path.split('.').length === ++i ? value : o[p] || {}, object)

export const resolvePath = (object, path, defaultValue) => path
    .split('.')
    .reduce((o, p) => o ? o[p] : defaultValue, object)

export function sortObject(obj, order = "ascending") {
    let sorted = {}
    if (order == "ascending") {
        sorted = Object.keys(obj).sort().reduce(function (result, key) {
            result[key] = obj[key];
            return result;
        }, {})
    } else {
        sorted = Object.keys(obj).sort().reverse().reduce(function (result, key) {
            result[key] = obj[key];
            return result;
        }, {})
    }
    return sorted
}


export function fixDateRangeIfNeeded(start, end, correction_value = 1, correction_unit = "hours", fixPlace = "end") {
    let startDate = start
    let endDate = end

    if (fixPlace == "end") {
        if (endDate.isBefore(startDate))
            endDate = startDate.add(correction_value, correction_unit)
        return endDate
    }

    if (fixPlace == "start") {
        if (startDate.isAfter(endDate))
            startDate = endDate.add(correction_value, correction_unit)
        return startDate
    }


}

export function mobileCheck() {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

export function mobileOrTabletCheck() {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

export function getNextParentElement(elem, elementKind = "LI") {
    if (elem.tagName === elementKind) {
        return (elem);
    }
    // If not check the parent
    const parent = elem.parentElement;
    if (parent.tagName != elementKind) {
        // If still not LI element, recall function but pass in the parent element
        return (getNextParentElement(parent, elementKind));

    } else {
        // Return the parent if it's the LI element
        return (parent);
    }
};


export function stringToHslColor(string, saturation = 10, lightness = 50) {
    let hash = 0;
    for (let i = 0; i < string.length; i++) {
        hash = string.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    return `hsl(${(Math.abs(hash) % 360)}, ${saturation * (hash.toString().slice(-1))}%, ${lightness}%)`;
}

//# sourceMappingURL=pb-functions.js.map