import {DtWizardScenario, DtWizardStep} from "./pb-wizard-datatypes.js"
import {
    getCookie
} from "/csf-lib/pb-functions.js?v=[cs_version]"

export class Wizard {
    #scenario = {}
    #currentStepNumber = undefined

    constructor(pbtpl, pbi18n) {
        this.pbtpl = pbtpl
        this.pbi18n = pbi18n
        this.databaseBasename = "temp_Wizard_"
    }

    init() {
        window.onpopstate = function (event) {
            const url = new URL(location);
            let step = parseInt(url.searchParams.get("step") || 1);
            this.renderStep(step, event.state, true)
        }.bind(this)

        return Dexie.getDatabaseNames().then(function (data) {
            let currentDatabaseName = this.databaseBasename + getCookie("consumer_id")
            for (let databaseName of data) {
                if (databaseName.startsWith(this.databaseBasename) && databaseName != currentDatabaseName) {
                    Dexie.delete(databaseName).then(() => console.log("Deleted old temp_database", databaseName));
                }
            }
            this.db = new Dexie(currentDatabaseName)
            this.db.version(1).stores({
                formdata: `
                    id,
                    data`,
            });
        }.bind(this))
    }

    newScenario(name, steps) {
        this.#scenario = new DtWizardScenario(name, steps)
        return this
    }

    getCurrentStep() {
        return this.#currentStepNumber
    }

    renderStep(stepNumber, render_data = undefined) {
        this.#currentStepNumber = parseInt(stepNumber)
        this.renderStepOverviewContent()
        let currentStep = this.#scenario.getStep(this.#currentStepNumber)
        render_data = render_data || currentStep.renderData
        return this.pbtpl.renderIntoAsync(currentStep.bodytemplate, render_data, "#step_bodycontent").then(() => {
            this.renderStepTitles()
            this.db.formdata.toArray().then(function (data) {
                for (let field of data) {
                    $("#" + field.id).val(field.data.value)
                    $("#" + field.id).prop("checked", field.data.checked)
                }
                currentStep.onload({"stepData": currentStep, "formdata": data})
            }.bind(this))
        })
    }

    setPushState(step = 1) {
        this.storeData()
        const url = new URL(location);
        url.searchParams.set("step", step);
        history.pushState({}, "", url);
    }

    storeData() {
        $("input").each((idx, elem) => {
            this.db.formdata.bulkPut([{
                id: elem.id,
                data: {"value": $(elem).val(), "type": elem.type, "checked": elem.checked}
            }])
        })
    }

    disableHeaderLinks() {
        $("*[data-steplink]").off('click')
    }

    enableHeaderLinks() {
        $("*[data-steplink]").click(function (event) {
            if (event.currentTarget.dataset["steplink"] < this.#currentStepNumber) {
                this.setPushState(event.currentTarget.dataset["steplink"])
                this.renderStep(event.currentTarget.dataset["steplink"])
                return false
            }
        }.bind(this))
    }

    renderStepOverviewContent() {
        this.pbtpl.renderIntoAsync("csf-lib/pb/wizard/tpl/step_header.html", {
            steps: this.#scenario.steps,
            current_step: this.#currentStepNumber
        }, "#wizard_steps_overview_container").then(function () {
            this.enableHeaderLinks()
        }.bind(this))

        this.pbtpl.renderIntoAsync("csf-lib/pb/wizard/tpl/step_footer.html", {
            steps: this.#scenario.steps,
            current_step: this.#currentStepNumber
        }, "#step_footer").then(function () {
            $("#wizard_next_btn").click(function () {
                this.next().then(() => {
                })
            }.bind(this))

            $("#wizard_prev_btn").click(function () {
                this.previous().then(() => {
                })
            }.bind(this))
        }.bind(this))
    }

    renderStepTitles() {
        let currentStep = this.#scenario.getStep(this.#currentStepNumber)
        $("#wizard_step_title").html(this.pbi18n.translate(currentStep.title))
        $("#wizard_step_subtitle").html(this.pbi18n.translate(currentStep.subtitle))
    }

    getScenario() {
        return this.#scenario
    }

    addStep(step_number, shortname, title, subtitle, bodytemplate, onload, onleave, renderData = {}) {
        this.#scenario.addStep(new DtWizardStep(step_number, shortname, title, subtitle, bodytemplate, onload, onleave, renderData))
    }

    next() {
        return new Promise(function (resolve, reject) {
            let prevStepNumber = this.#currentStepNumber
            let prevStep = this.getScenario().getStep(prevStepNumber)
            let nextStep = parseInt(this.#currentStepNumber) + 1
            this.db.formdata.toArray().then((formdata) => {
                if (prevStep?.onleave && prevStep?.onleave({"stepData": prevStep, "formdata": formdata}) !== false) {
                    this.setPushState(nextStep)
                    this.renderStep(nextStep)
                    resolve()
                }

                if (!prevStep?.onleave) {
                    this.setPushState(nextStep)
                    this.renderStep(nextStep).then(() => resolve())
                }
            })
        }.bind(this))
    }

    previous() {
        return new Promise(function (resolve, reject) {
            this.storeData()
            let prevStepNumber = this.#currentStepNumber
            let prevStep = this.getScenario().getStep(prevStepNumber)
            this.db.formdata.toArray().then((formdata) => {
                prevStep.onleave({"stepData": prevStep, "formdata": formdata})
                this.#currentStepNumber--
                this.setPushState(this.#currentStepNumber)
                this.renderStep(this.#currentStepNumber).then(() => resolve())
            })
        }.bind(this))
    }

    initListeners() {

    }
}