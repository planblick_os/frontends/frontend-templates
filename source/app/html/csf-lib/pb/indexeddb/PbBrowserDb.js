import {
    getCookie
} from "/csf-lib/pb-functions.js?v=[cs_version]"

export class PbBrowserDb {
    constructor(databaseName, stores, version) {
        this.databaseName = databaseName + "_" + getCookie("username")
        this.stores = stores
        this.version = version
    }

    init() {
        return Dexie.getDatabaseNames().then(function (data) {
            let already_exists = false
            for (let databaseName of data) {
                if (databaseName == this.databaseName) {
                    already_exists = true
                }
            }
            this.db = new Dexie(this.databaseName)
            this.db.version(this.version).stores(this.stores)
            this.db.open().then(() => {
                console.info("IndexDB Opened", this.databaseName, this.stores)
            }).catch(function (e) {
                console.error("Open failed: " + e)
            })
        }.bind(this))
    }

    add(tableName, data) {
        return this.db[tableName].add(data);
    }

    bulkPut(tableName, dataArray) {
        return this.db[tableName].bulkPut(dataArray)
    }

    read(tableName, where=undefined) {
        if(where === undefined) {
            return this.db[tableName].toArray()
        } else {
            return this.db[tableName].where(where).toArray()
        }
    }

    delete(tableName, where) {
            return this.db[tableName].where(where).delete()
    }

    clear(tableName) {
        return this.db[tableName].clear()
    }


}