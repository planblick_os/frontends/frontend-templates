export class PbQuestionaire {
    title;
    description;
    image_url;
    questions = [];
    currentQuestionIndex;
    constructor(questionaireConfig) {
        this.title = questionaireConfig?.title;
        this.description = questionaireConfig?.description;
        this.image_url = questionaireConfig?.image_url;
        for (let question of questionaireConfig?.questions || []) {
            let newQuestion = new PbQuestion(question.question);
            let answer = new PbAnswer(question.answer);
            newQuestion.setAnswer(answer);
            this.addQuestion(newQuestion);
        }
    }
    getQuestionById(id) {
        let result = this.questions.find((el) => el.id == id);
        return result;
    }
    addQuestion(question) {
        this.questions.push(question);
        return this;
    }
}
export class PbAnswer {
    data;
    response;
    constructor(options) {
        if (options) {
            this.data = options;
        }
    }
    setResponse(response) {
        this.response = response;
        return this;
    }
    getResponse() {
        return this.response;
    }
}
export class PbQuestion {
    get id() {
        return this._id;
    }
    _id;
    title;
    text;
    next;
    previous;
    answer;
    hidden;
    options;
    constructor(options) {
        if (options) {
            this._id = options.id;
            this.title = options.title;
            this.text = options.text;
            this.next = options.next;
            this.previous = options.previous;
            this.hidden = options.hidden;
            this.options = options.options;
        }
    }
    ;
    getId() {
        return this._id;
    }
    setAnswer(answer) {
        this.answer = answer;
        return this;
    }
    getAnswer() {
        return this.answer;
    }
}
//# sourceMappingURL=classes.js.map
