import {expect, test} from '@jest/globals';
import {PbQuestion, PbAnswer, PbQuestionaire} from "./classes.js";


let qData = {
    "id": "q1ID",
    "text": "Text1",
    "title": 1,
    "next": "",
    "previous": ""
}

test('Create question', () => {
    let question = new PbQuestion(qData)
    expect(question.id).toStrictEqual(qData.id)
    expect(question.text).toStrictEqual(qData.text)
    expect(question.title).toStrictEqual(qData.title)
    expect(question.next).toStrictEqual(qData.next)
    expect(question.previous).toStrictEqual(qData.previous)
});

test("Test whether setting id of questions after constructing throws an error", () => {
    let question = new PbQuestion(qData)
    expect(() => {
        question.id = "foo"
    }).toThrow(TypeError);
})

test("Test whether changing question-values aside from id works", () => {
    let question = new PbQuestion(qData)
    question.text = "changed"
    question.text = "changed"
    question.title = "changed"
    question.next = "changed"
    question.previous = "changed"

    expect(question.text).toStrictEqual("changed")
    expect(question.title).toStrictEqual("changed")
    expect(question.next).toStrictEqual("changed")
    expect(question.previous).toStrictEqual("changed")
})

test('Create question', () => {
    let question = new PbQuestion(qData)
    expect(question.id).toStrictEqual(qData.id)
    expect(question.text).toStrictEqual(qData.text)
    expect(question.title).toStrictEqual(qData.title)
    expect(question.next).toStrictEqual(qData.next)
    expect(question.previous).toStrictEqual(qData.previous)
});

test('Test creation of all answer type objects', () => {
    let answerData = {
        "type": "select",
        "select_options": [{"id": 1, "value": "Oben"}, {"id": 2, "value": "Unten"}],
        "values": [{"id": 2, "value": "Unten"}]
    }
    let answer = new PbAnswer(answerData)
    expect(answer.data).toStrictEqual(answerData)
    expect(answer.response).toStrictEqual(undefined)
});

test('Test setting response to answer type object', () => {
    let answerData = {
        "type": "select",
        "select_options": [{"id": 1, "value": "Oben"}, {"id": 2, "value": "Unten"}],
        "values": [{"id": 2, "value": "Unten"}]
    }
    let answer = new PbAnswer(answerData)
    let response = {"id": qData.id, "value": "Unten"}
    answer.setResponse(response)
    expect(answer.data).toStrictEqual(answerData)
    expect(answer.response).toStrictEqual(response)
});

test("Test whether we can add a question to, and get a certain question from questionaire by id", () => {
    let qnair = new PbQuestionaire()
    let question = new PbQuestion(qData)

    qnair.addQuestion(question)
    expect(qnair.getQuestionById(question.getId()).title).toStrictEqual(qData.title)
    expect(qnair.getQuestionById("none")?.title).toStrictEqual(undefined)
})

test("Test whether we can add an answer and an answer response to a question", () => {
    let qnair = new PbQuestionaire()
    let question = new PbQuestion(qData)

    qnair.addQuestion(question)

    let addedQuestion = qnair.getQuestionById(qData.id)

    let answerData = {
        "id": "a1",
        "type": "select",
        "select_options": [{"id": 1, "value": "Oben"}, {"id": 2, "value": "Unten"}],
        "values": [{"id": 2, "value": "Unten"}]
    }
    let answerData2 = {
        "id": "a2",
        "type": "multiselect",
        "select_options": [{"id": 3, "value": "Links"}, {"id": 4, "value": "Rechts"}],
        "values": [{"id": 3, "value": "Links"}, {"id": 4, "value": "Rechts"}]
    }
    let answer = new PbAnswer(answerData)
    let response = {"id": "foobar", "value": "Unten"}
    answer.setResponse(response)

    addedQuestion.setAnswer(answer)

    expect(addedQuestion.answer).toStrictEqual(answer)
    let answer_response = addedQuestion.getAnswer().getResponse()
    expect(answer_response).toStrictEqual(response)
})
//# sourceMappingURL=type.question.test.js.map
