import {DtSelectAnswer, DtInputAnswer, DtQuestion, DtAnswerResponse} from "./types.js";

export class PbQuestionaire {
    public title: string
    public description: string
    public image_url: string
    protected questions: PbQuestion[] = []
    protected currentQuestionIndex: bigint

    constructor(questionaireConfig?:any) {
        this.title = questionaireConfig?.title
        this.description = questionaireConfig?.description
        this.image_url = questionaireConfig?.image_url
        for(let question of questionaireConfig?.questions || []) {
            let newQuestion = new PbQuestion(question.question)
            let answer = new PbAnswer(question.answer)
            newQuestion.setAnswer(answer)
            this.addQuestion(newQuestion)
        }
    }

    getQuestionById(id:string):PbQuestion | undefined {
        let result = this.questions.find((el)=> el.id == id)
        return result
    }

    addQuestion(question:PbQuestion):PbQuestionaire {
        this.questions.push(question)
        return this
    }
}

export class PbAnswer {
    data: DtSelectAnswer | DtInputAnswer
    response: DtAnswerResponse

    constructor(options?: DtSelectAnswer | DtInputAnswer) {
        if(options) {
            this.data = options
        }
    }

    setResponse(response:DtAnswerResponse):PbAnswer {
        this.response = response
        return this
    }
    getResponse():DtAnswerResponse | undefined {
        return this.response
    }
}
export class PbQuestion {
    get id(): string {
        return this._id;
    }
    private _id: string
    title: string
    text: string
    next: string
    previous: string
    answer: PbAnswer
    hidden: boolean
    options: object

    constructor(options?: DtQuestion) {
        if(options) {
            this._id = options.id
            this.title = options.title
            this.text = options.text
            this.next = options.next
            this.previous = options.previous
            this.hidden = options.hidden
            this.options = options.options
        }
    };

    getId(): string {
        return this._id;
    }
    setAnswer(answer: PbAnswer): PbQuestion {
        this.answer = answer
        return this
    }

    getAnswer():PbAnswer | undefined {
        return this.answer
    }
}