import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import {array_merge_distinct} from "/csf-lib/pb-functions.js?v=[cs_version]"

export class PbGroups {
    #groups = {}
    #lastTaskId = undefined
    #group_members = {}


    constructor(socket, onBackendUpdateFunc = () => "") {
        this.socket = socket
        this.onBackendUpdateFunc = onBackendUpdateFunc
        this.initBackendListeners()

    }

    loadFromApi(loadGroupMembers = false) {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest(`/get_groups`).execute().then((response) => {
                self.#groups = response.groups
                if (loadGroupMembers) {
                    self.loadGroupMembersFromApi().then(() => {
                        resolve(self)
                    })
                } else {
                    resolve(self)
                }
            })
        })
    }

    getGroupMembers(group = undefined) {
        if (group) {
            return this.#group_members[group] || this.#group_members["group." + group]
        } else {
            let all_members = []
            for (let group of Object.values(this.#group_members)) {
                all_members = array_merge_distinct(all_members, group)
            }
            return all_members
        }
    }

    getSelectOptions(selectedIds = []) {
        let select_options = []


        // TODO: add members
        for (let group_id of Object.keys(this.#groups)) {
            let option = {
                "id": group_id,
                "name": group_id,
                "selected": selectedIds.includes(group_id) ? "selected" : ""
            }
            select_options.push(option)
        }
         for (let member_id of this.getGroupMembers()) {
            let option = {
                "id": member_id,
                "name": member_id,
                "selected": selectedIds.includes(member_id) ? "selected" : ""
            }
            select_options.push(option)
        }
        return select_options
    }

    loadGroupMembersFromApi() {
        let self = this
        let apiParams = Object.keys(this.#groups)

        let promises = []
        apiParams.forEach(param => {
            promises.push(self.loadGroupMembersByGroupName(param.split(".")[1]))
        })

        return Promise.all(promises).then((results) => {})
    }

    loadGroupMembersByGroupName(groupname) {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest('/get_group_members/' + groupname).execute().then((response) => {
                let all_members = []
                for (let member of response.members) {
                    all_members.push("user." + member.username)
                }
                self.#group_members["group." + groupname] = all_members
                resolve(response)
            })
        })
    }

    updateGroups() {
        let self = this
        this.loadFromApi().then(() => {
            this.onBackendUpdateFunc(skipOnBackendUpdateFunc)
        })
    }

    initBackendListeners() {
        if (this.socket) {
            let self = this
            // this.socket.commandhandlers["addEvent"] = function (args) {
            //     if (self.#lastTaskId) {
            //         self.#lastTaskId = undefined
            //         self.updateActiveEvent(args.id, true)
            //     } else {
            //         self.updateActiveEvent(args.id, false)
            //     }
            // }
        }
    }
}
