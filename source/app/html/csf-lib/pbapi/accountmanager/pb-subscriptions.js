import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import {array_merge_distinct} from "/csf-lib/pb-functions.js?v=[cs_version]"

export class PbSubscriptions {
    #subscriptions = {}
    #lastTaskId = undefined

    constructor(socket, onBackendUpdateFunc = () => "") {
        this.socket = socket
        this.onBackendUpdateFunc = onBackendUpdateFunc
        this.initBackendListeners()
    }

    loadFromApi() {
        return new Promise(function (resolve, reject) {
            new PbApiRequest(`/get_account_subscriptions`).execute().then((response) => {
                this.#subscriptions = response
                resolve()
            })
        }.bind(this))
    }

    addAccountPackage(packageName) {
        return new Promise(function (resolve, reject) {
            let payload = {
                "package": packageName
            }

            new PbApiRequest("/es/cmd/addAccountPackage", "POST", payload).execute().then((response) => {
                this.#lastTaskId = response.task_id
                resolve(this)
            })
        }.bind(this))
    }

    getActive() {
        return this.#subscriptions?.active
    }

    getExpired() {
        return this.#subscriptions?.expired
    }

    initBackendListeners() {
        document.addEventListener("command", function _(e) {
            if (e.data?.data?.command == "refreshAccessRights") {
                this.loadFromApi()
            }
        }.bind(this))

    }
}
