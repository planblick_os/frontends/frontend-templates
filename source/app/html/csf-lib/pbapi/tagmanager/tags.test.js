import {PbTag} from "./pb-tag.js"

test('create new PbTag from short', () => {
    let tag = new PbTag().fromShort("foo:bar")
    expect(tag.getData().name).toStrictEqual("foo")
    expect(tag.getData().value).toStrictEqual("bar")

    tag = new PbTag().fromShort("foo")
    expect(tag.getData().name).toStrictEqual("foo")
    expect(tag.getData().value).toStrictEqual("foo")
});

test('set usableFor', () => {
    let tag = new PbTag().fromShort("foo:bar")
    let usableForData = ["me", "myself", "and i"]
    tag.setUsableFor(usableForData)
    expect(tag.getUsableFor()).toStrictEqual(usableForData)
    expect(tag.isUsableFor("myself")).toBe(true)
});

test('set data by constructor', () => {
    let data = {
        "name": "foo",
        "value": "bar",
        "create_time": "2023-06-07 11:01:41",
        "creator": "superuser",
        "usable_for": ["eventmanager"],
        "data_owners": ["group.one", "user.superuser"]
    }

    let tag = new PbTag(data)
    expect(tag.getUsableFor()).toStrictEqual(["eventmanager"])
    expect(tag.getDataOwners()).toStrictEqual(["group.one", "user.superuser"])
    expect(tag.getData().name).toStrictEqual("foo")
    expect(tag.getData().value).toStrictEqual("bar")
    expect(tag.getData().create_time).toStrictEqual("2023-06-07 11:01:41")
    expect(tag.getData().creator).toStrictEqual("superuser")
})

test('from short to short', () => {
    let tag = new PbTag().fromShort("foo:bar")
    expect(tag.short()).toStrictEqual("foo:bar")
    tag = new PbTag().fromShort("foo")
    expect(tag.short()).toStrictEqual("foo")
})

test('from short to long', () => {
    let tag = new PbTag().fromShort("foo:bar")
    expect(tag.long()).toStrictEqual({"name": "foo", "usable_for": [], "value": "bar"})
})