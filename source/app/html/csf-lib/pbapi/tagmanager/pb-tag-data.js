export class PbTagData {
    constructor(name, value, create_time = undefined, creator = undefined, usable_for=[], data_owners=[]) {
        this.name = name
        this.value = value
        this.create_time = create_time
        this.creator = creator
        this.usable_for = usable_for
        this.data_owners = data_owners
    }
}