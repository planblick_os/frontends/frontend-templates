export class Booking {
    constructor(apiurl, apikey, tpl, target_selector) {
        this.apikey = apikey
        this.apiurl = apiurl
        this.tpl = tpl
        this.target_selector = target_selector
        const date_format_options = {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: "2-digit",
            minute: "2-digit",
            timezone: "UTC"
        };
        this.intl_dtf = new Intl.DateTimeFormat(undefined, date_format_options)
        this.data = []
        this.event_template = {}
        this.table_id = "pbs_booking_table"
    }

    createBooking(data) {
        let self = this
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": self.apiurl + "/es/cmd/createNewAppointment",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": self.apikey,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(data),
                "success": function (response) {
                    console.log("createNewAppointment reply", response)
                    resolve()
                }
            }
            $.ajax(settings)
        })
    }

    async loadFromApi(date) {
        var settings = {
            "url": this.apiurl + "/cb/available_slots_for_date?date=" + date.toISOString().split('T')[0],
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": this.apikey,
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "filters": [
                    {
                        "ressource": "ResourceV1_ID_4"
                    }
                ],
                "duration": 30,
                "extra_margin_start": 0,
                "extra_margin_end": 0
            }),
            "success": function (response) {
                let currentTime = new Date()
                this.data = response.filter((data) => new Date(data.start) > currentTime)
            }.bind(this)
        }
        await $.ajax(settings)

        var settings = {
            "url": this.apiurl + "/cb/available_slots_for_date?date=" + date.toISOString().split('T')[0],
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": this.apikey,
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "filters": [
                    {
                        "ressource": "ResourceV1_ID_4"
                    }
                ],
                "duration": 20,
                "extra_margin_start": 10,
                "extra_margin_end": 10
            }),
            "success": function (response) {
                this.event_template = {
                    "event_id": "",
                    "resourceId": "ResourceV1_ID_4",
                    "textColor": "#ff0000",
                    "color": "#cccccc",
                    "attachedRessources": [],
                    "starttime": "",
                    "endtime": "",
                    "title": "test",
                    "savekind": "create",
                    "extended_props": {
                        "creator_login": "superuser",
                        "comment": "Created by booking-manager"
                    },
                    "allDay": false,
                    "attachedContacts": [],
                    "data_owners": [],
                    "reminders": [],
                    "tags": [],
                    "data": {
                        "event_status": "pending",
                        "appointment_type": []
                    }
                }
            }.bind(this)
        }
        await $.ajax(settings)
    }

    formatDateForApi(date) {
        const year = date.getFullYear()
        const month = String(date.getMonth() + 1).padStart(2, '0')
        const day = String(date.getDate()).padStart(2, '0')
        const hours = String(date.getHours()).padStart(2, '0')
        const minutes = String(date.getMinutes()).padStart(2, '0')
        const seconds = String(date.getSeconds()).padStart(2, '0')
        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
    }

    formatDateForDisplay(date) {
        const year = date.getFullYear()
        const month = String(date.getMonth() + 1).padStart(2, '0')
        const day = String(date.getDate()).padStart(2, '0')
        const hours = String(date.getHours()).padStart(2, '0')
        const minutes = String(date.getMinutes()).padStart(2, '0')
        const seconds = String(date.getSeconds()).padStart(2, '0')
        return `${day}.${month}.${year} ${hours}:${minutes}`
    }

    async renderTable(date = new Date()) {
        this.tpl.renderIntoAsync('/datepicker.html', {"data": {}}, this.target_selector).then(function () {
            $("#pbs_datepicker").datepicker({
                minDate: new Date(),
                dateFormat: "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                lang: 'de',
                i18n: {
                    de: { // German
                        months: [
                            'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'
                        ],
                        dayOfWeekShort: [
                            "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"
                        ],
                        dayOfWeek: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"]
                    },
                },
            })

            $('#pbs_datepicker').datepicker("setDate", date);
            $("#pbs_datepicker").on("change", function () {
                let newDate = $("#pbs_datepicker").datepicker('getDate')
                var userTimezoneOffset = newDate.getTimezoneOffset() * 60000;
                newDate = new Date(newDate.getTime() - userTimezoneOffset);
                this.renderTable(newDate)
            }.bind(this))
        }.bind(this))
        await this.loadFromApi(date)

        return new Promise(async function (resolve, reject) {
            let datatableOptions = {
                 language: {
                  "emptyTable": this.tpl.i18n.translate("No slots available")
                },
                searching: false,
                pageLength: 1000,
                stateSave: true,
                paging: false,
                info: false,
                retrieve: true,
                // rowId: function (a) {
                //     return "uuid_" + a.uuid;
                // },
                // dom: 'Bfrtip',
                // buttons: [
                //     {
                //         text: this.tpl.i18n.translate("Add"),
                //         className: "btn btn-indigo",
                //         action: function (e, dt, node, config) {
                //             this.openFieldTypeEditor(undefined, {})
                //         }.bind(this)
                //     }
                // ],
                columns: [
                    // {title: this.tpl.i18n.translate('Start'), data: "start"},
                    // {title: this.tpl.i18n.translate('End'), data: "end"},
                    {
                        title: this.tpl.i18n.translate('Von'),
                        data: "start",
                        render: function (value) {
                            return this.formatDateForDisplay(new Date(value))//this.intl_dtf.format(new Date(value))
                        }.bind(this)
                    },
                    {
                        title: this.tpl.i18n.translate('bis'),
                        data: "end",
                        render: function (value) {
                            return this.formatDateForDisplay(new Date(value))//this.intl_dtf.format(new Date(value))
                        }.bind(this)
                    },
                    {
                        width: "40px",
                        data: null,
                        render: function (data, slot, row, meta) {
                            return `<button class="btn btn-info btn-pills login-btn-primary" data-selector="pbs_bookbutton" data-start="${row.start}" data-end="${row.end}">Buchen</button>`
                        },
                        orderable: false
                    }
                ],
                responsive: true,
                pagingType: 'first_last_numbers',
                lengthChange: false,
                // language: {
                //     url: '/style-global/lib/datatables.net-dt/i18n/de-DE.json'
                // },
            }

            let table_id = this.table_id

            let table = $('<table>');
            table.attr("id", table_id)
            $(this.target_selector).after(table);

            if (!this.datatable) {
                this.datatable = new DataTable("#" + table_id, datatableOptions);
            }
            this.datatable.clear()
            this.datatable.rows.add(this.data).draw()
            //$(this.target_selector).hide()
            this.datatable.off("click")
            this.datatable.on('click', '[data-selector="pbs_bookbutton"]', async function (e) {
                e.preventDefault();
                $("#"+this.table_id).hide()
                let data = {
                    "start_string": this.intl_dtf.format(new Date(e.target.dataset.start)),
                    "end_string": this.intl_dtf.format(new Date(e.target.dataset.end)),
                    "start_date": e.target.dataset.start,
                    "end_date": e.target.dataset.end
                }
                this.tpl.renderIntoAsync('/booking_form.html', {"data": data}, this.target_selector).then(function () {
                    $("#pbs_booking_form_submit_button").click(function (e) {
                        let start_date = new Date(e.target.dataset.start)
                        let end_date = new Date(e.target.dataset.end)
                        let tpl_data = {
                            "start_string": this.intl_dtf.format(start_date),
                            "end_string": this.intl_dtf.format(end_date)
                        }
                        let payload = Object.assign({}, this.event_template)
                        payload.title = `${$("#pbs_booking_form_firstname_input").val()} ${$("#pbs_booking_form_lastname_input").val()} ${$("#pbs_booking_form_email_input").val()}`
                        payload.starttime = this.formatDateForApi(start_date)
                        payload.endtime = this.formatDateForApi(end_date)
                        this.createBooking(payload).then(function () {
                            this.tpl.renderIntoAsync('/booking_success.html', {"data": tpl_data}, this.target_selector).then(function () {
                            }.bind(this))

                        }.bind(this))
                    }.bind(this))
                    $("#pbs_booking_form_cancel_button").click(function (e) {
                        $(this.target_selector).hide()
                        $("#" + table_id).show()
                        $(this.target_selector).html("")
                    }.bind(this))
                }.bind(this))

                $("#" + table_id).hide()
                $(this.target_selector).show()

            }.bind(this))

            // this.datatable.on('click', '[data-selector="dt_delete"]', function (e) {
            //     e.preventDefault();
            //     new PbEventTypes().deleteByUUId(e.currentTarget.dataset?.id)
            // }.bind(this));

            resolve()
        }.bind(this))
    }


}