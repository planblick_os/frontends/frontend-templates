import gulp from 'gulp';
import dartSass from 'sass'
import gulpSass from 'gulp-sass';

const sass = gulpSass(dartSass);
import postcss from 'gulp-postcss';
import cssnano from 'cssnano';
import terser from 'gulp-terser';
import typescript from 'gulp-typescript';

const project = typescript.createProject('.tsconfig.json')
import eslint from 'gulp-eslint';
import jest from '@jest/core';
const outputDir = "../src/"

gulp.task('jest', async function () {
    await jest.runCLI({moduleDirectories: ['node_modules', 'app/html'], 'changedSince': 'origin/master', 'onlyChanged': true, 'forceExit': false, 'expand':false, 'coverage': true}, ['app']);
});

gulp.task('watch', () => {
    gulp.watch(['app/**/*.js'], gulp.series('lint', 'copy-js', 'jest'));
    gulp.watch(['app/**/*.ts*'], gulp.series('compile-typescript'));
    gulp.watch(['app/**/*.json'], gulp.series('copy-json'));
    gulp.watch(['app/**/*.css'], gulp.series('minifiy-css'));
    gulp.watch(['app/**/*.scss'], gulp.series('compile-sass'));
    gulp.watch(['app/**/img/*.'], gulp.series('copy-images'));
    gulp.watch(['app/**/*.html'], gulp.series('copy-html'));
});

gulp.task('lint', () => {
    return gulp.src('app/**/*.js', {base: ".", since: gulp.lastRun('lint')})
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
});

gulp.task('copy-json', () => {
    return gulp.src('app/**/*.json', {base: ".", since: gulp.lastRun('copy-json')})
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});

gulp.task('copy-html', () => {
    return gulp.src('app/**/*.html', {base: ".", since: gulp.lastRun('copy-html')})
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});

gulp.task('copy-templates', () => {
    return gulp.src('app/**/*.tpl', {base: ".", since: gulp.lastRun('copy-templates')})
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});

gulp.task('copy-js', () => {
    return gulp.src('app/**/*.js', {sourcemaps: true, base: ".", since: gulp.lastRun('copy-js')})
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});

gulp.task('minify-js', () => {
    return gulp.src('app/**/*.js', {sourcemaps: true, base: ".", since: gulp.lastRun('minify-js')})
        .pipe(terser())
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});

gulp.task('copy-images', () => {
    return gulp.src('app/**/img/*.*', {base: ".", since: gulp.lastRun('copy-images')})
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});


gulp.task('copy-css', () => {
    return gulp.src('app/**/*.css', {base: ".", since: gulp.lastRun('copy-css')})
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}))
});

gulp.task('minifiy-css', () => {
    return gulp.src('app/**/*.css', {sourcemaps: true, base: ".", since: gulp.lastRun('minifiy-css')})
        .pipe(postcss([cssnano()]))
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}));
});

gulp.task('compile-sass', () => {
    return gulp.src('app/**/*.scss', {sourcemaps: true, base: ".", since: gulp.lastRun('compile-sass')})
        .pipe(sass())
        .pipe(postcss([cssnano()]))
        .pipe(gulp.dest(outputDir, {sourcemaps: '.'}));
});


gulp.task('compile-typescript', () => {
    return gulp.src('app/**/*.ts*', {sourcemaps: true, base: ".", since: gulp.lastRun('compile-typescript')})
        .pipe(project())
        .pipe(gulp.dest("../source/", {sourcemaps: '.'}));
});

gulp.task('build',
    gulp.series(
        'lint',
        'jest',
        gulp.parallel('compile-typescript', 'compile-sass', 'minifiy-css', 'copy-images', 'copy-templates', 'copy-html', 'copy-json'),
        'minify-js'
    )
);

gulp.task('dev',
    gulp.series(
        gulp.parallel('compile-typescript', 'compile-sass',  'minifiy-css', 'copy-images', 'copy-templates', 'copy-html', 'copy-json'),
        'copy-js',
        'lint',
        'jest',
        'watch'
    )
);
