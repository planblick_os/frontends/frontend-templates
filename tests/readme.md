# How to run tests?
### 1.) Install python 3.x
https://www.python.org/downloads/
### 2.) Download needed Webdriver and place it into a PATH-accessible directory
Depending on which browsers you want to use for testing you need different drivers.
For Firefox: https://github.com/mozilla/geckodriver/releases/

For Chrome: https://chromedriver.chromium.org/downloads

On top of the driver, the browser itself has to be installed as well. 
### 3.) Install required python packages
```pip install pytest pytest-bdd pytest-html pytest-xdist pytest-xdist[psutil] pytest-splinter pillow requests sqlalchemy --user```
### 4.) cd into the test-directory
Example: `cd ./tests`
### 5.) Run tests
`python -m pytest`

For local: `python -m pytest --environment local -v -n 1`

OR

`python -m pytest --environment local -v -n 1 --browser_engine chrome --credentials demo_3:pb2019! --admin_credentials demo:pb2019! -x --lf -s -vvv`

For remote: `python -m pytest --environment staging -v -n 1 --browser_engine remote --test_auth login:password@`

#Additional info
Since we are using pytest and pytest-bdd, all resources about those can be used for further information.

Some flags which may come in handy:
```
python -m pytest --environment local|staging|live  //Default: local. Selects which environment the test shall be executed against.
python -m pytest -v  //Set verbosity level of pytest. Can also be -vv for even more chatty test-results
python -m pytest -s  //Outputs prints which are in tests
python -m pytest -k "filterstring"  //Filter tests by keyword which has to be in description
python -m pytest -m "module_login"  //Filter tests by markers. Get all available markers with pytest --markers 
python -m pytest --headless yes|no  //Default: no. Decides whether browser should be run in headless mode or not
python -m pytest --browser_engine firefox|chrome|remote  //Default: firefox. Selects to beused browser_engine. Be aware, the browser as well as the driver has to be installed.
python -m pytest --size 800x600  //Default: max. Set the viewport-size. Keep in mind, this is different than the windows-size.
python -m pytest --help // Shows all flags including custom ones of our test-suite
python -m pytest --lf // Starts with last failed tests and skips successfull
python -m pytest -x // Stops tests on first fail

```