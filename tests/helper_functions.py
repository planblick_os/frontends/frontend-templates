from PIL import Image
import json
from selenium.webdriver.common.by import By
import os

def set_viewport_size(driver, width, height):
    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + parseInt(arguments[0]),
          window.outerHeight - window.innerHeight + parseInt(arguments[1])];
        """, width, height)
    driver.set_window_size(*window_size)


def make_tour_step(tour_name, id, desc, clear_previous=False):
    file_path = f"../src/app/templates/tours/{tour_name}.json"

    if clear_previous:
        with open(file_path, "w") as f:
            f.write("")

    current_tour_data = []
    if os.path.exists(file_path):
        with open(file_path, "r") as f:
            try:
                current_tour_data = json.load(f)
            except Exception as e:
                pass

    next_step = {
        "element": id,
        "intro": desc
    }

    current_tour_data.append(next_step)
    with open(file_path, "w") as f:
        f.write(json.dumps(current_tour_data, indent=4))


def make_screenshot(browser, step_name, feature_name="", mouse_click_element_id_or_xpath=None, description="",  next=None, previous=None):
    image_name = step_name.replace(" ", "_").lower() + ".png"
    json_name = step_name.replace(" ", "_").lower() + ".json"
    browser.driver.save_screenshot('./docs/images/' + image_name)

    if mouse_click_element_id_or_xpath is not None:
        pil_image = Image.open('./docs/images/' + image_name).convert('RGBA')
        mousepointer_image = Image.open('./docs/images/mousepointer.png').convert('RGBA')

        e = None
        try:
            e = browser.driver.find_element(by=By.ID, value=mouse_click_element_id_or_xpath)
        except Exception:
            pass
        try:
            e = browser.driver.find_element(by=By.XPATH, value=mouse_click_element_id_or_xpath)
        except Exception:
            pass

        if e is not None:
            location = e.location
            size = e.size
            w, h = size['width'], size['height']

            position = (location["x"] + int(w / 2), location["y"] + int(h / 2 - 15))
            pil_image.paste(mousepointer_image, position, mask=mousepointer_image)

            pil_image.save("./docs/images/" + image_name, format="png")
        else:
            raise Exception("Cannot find elment to place-mousemarker on for documentation image.")

    step_definition = {
        "en": {
            "feature_name": feature_name,
            "step_name": step_name,
            "step_description": description,
            "step_image": image_name,
            "step_next": next,
            "step_previous": previous
        }
    }

    f = open("./docs/jsons/" + json_name, "w")
    f.write(json.dumps(step_definition, indent=4, sort_keys=True))
    f.close()


