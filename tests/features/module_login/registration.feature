@module_registration
Feature: Register
  As a visitor without account,
  I want to be able to register myself with my email-address,
  So that I can login to the system.

  Scenario: Get to registration page
    Given I'm on the login page
    When I press the register button
    Then I want to be redirected to the registration-page

  Scenario Outline: Filling out the form on the registration-page
    Given I'm on the registration page
    When I fill in my "<username>", "<email>" and "<password>"
    When I checked the data-privacy-checkbox
    When I press the form submit button
    Then I want that an account with the given data is created

    Examples: Credentials
      | username | email            | password |
      | foobar | fk@planblick.com | Planblick2022!  |

  Scenario: Get back to login-page when on registration page
    Given I'm on the registration page
    When I click on the "Anmelden" link
    Then I want to be redirected to the login-screen

