@module_simplycollect
Feature: Simplycollect
  As a registered user with simplycollect license,
  I want to be able to start the simplycollect application from the dashboard,
  So that I can use the tool.

  Scenario: Try to access simplycollect merchant application without permissions
    Given I'm a user without simplycollect.merchant_view permissions
    When I visit the simplycollect-merchant page
    Then I want to be redirected to the login-page

  Scenario: Get to simplycollect traders-page
    Given I'm logging in
    When I press the "Haendleransicht" link
    Then I want to be redirected to the simplycollect traders-page

  Scenario: Get to simplycollect customers-page
    Given I'm not logged in
    When I go to the simplycollect customers-website
    Then I want to see simplycollect customers-website
