@module_easy2schedule
Feature: Easy2Schedule Ressource Settings
  SUMMARY DESCRIPTION WHAT FEATURES SHALL BE TESTET HERE RESULTING IN N Scenario


  Scenario: Visit settings-page without permissions
    Given I don't have permissions to visit the settings-page
    When I try to open the settings-page by entering the url directly
    Then I want to be redirected to the login-page

  Scenario: Add new ressource
    Given I'm on the ressource settings page and i have suitable permission
    When I click on the button "<Ressource Anlegen>"
    Then A modal should be displayed
    When I enter the name <Test-Resource>
    When I click on the <Speichern> Button
    Then I want to see a new entry in the table named "<Test-Resource>"

  Scenario: Update existing ressource
    Given I'm on the ressource settings page and i have suitable permission
    When I click on the button "<Ressource Anlegen>"
    Then A modal should be displayed
    When I enter the name <Test-Resource>
    When I click on the <Speichern> Button
    Then I want to see a new entry in the table named "<Test-Resource>"
    When I click the update button next to row named "<Test-Resource>"
    When I enter the name <Test-Resource updated>
    When I click on the <Speichern> Button
    Then I want to see a updated entry in the table named "<Test-Resource updated>"

  Scenario: Delete existing ressource
    Given I'm on the ressource settings page and i have suitable permission
    When I click on the button "<Ressource Anlegen>"
    Then A modal should be displayed
    When I enter the name <Test-Resource to delete>
    When I click on the <Speichern> Button
    Then I want to see a new entry in the table named "<Test-Resource to delete>"
    When I click the delete button next to row named "<Test-Resource to delete>"
    Then I don't want to see an entry named "<Test-Resource to delete>" anymore

