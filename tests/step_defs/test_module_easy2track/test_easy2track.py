from time import sleep

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers
from tests.helper_functions import make_screenshot


scenarios("../../features/module_easy2track/easy2track.feature")

@given("I'm logging in")
def step_impl(easy2track_user, login, no_js_error):
    pass


@when('I press the "zu Ihrem QR-Code" link')
def step_impl(easy2track_user, browser):
    link = browser.find_by_id("easy2track_qrcode_link")
    make_screenshot(browser, "Easy2Track client mode", "Easy2Track", "easy2track_qrcode_link",
                    description="Click on the QR-Code link to get on the page which should be visible to customers.",
                    next="Easy2Track client mode: QR-Code-Page")
    link.click()


@then("I want to be redirected to the easy2track client-page")
def step_impl(browser, no_js_error):
    make_screenshot(browser, "Easy2Track client mode: QR-Code-Page","Easy2Track", None,
                    previous="Easy2Track client mode",
                    description="The customer can now scan the qr-code with a cellphone and will be redirected to the contact-form.",
                    next="Easy2Track client mode: QR-Code-Page 2"
                    )
    assert browser.is_text_present('Classic QR') is True
    no_js_error()


@then("I want a qrcode to be shown to me")
def step_impl(browser, no_js_error):

    image = browser.find_by_xpath('//*[@id="qrcode"]/img')
    assert len(image) == 1
    no_js_error()


@when("I click on the qrcode")
def step_impl(browser):
    make_screenshot(browser, "Easy2Track client mode: QR-Code-Page 2", "Easy2Track", "current_value_link",
                    previous="Easy2Track client mode: QR-Code-Page",
                    description="Alternatively, a click on the qr-code will redirect to the contact-form. If shown on a terminal, users without a cellphone can enter their data on the terminal this way.",
                    next="Easy2Track client mode: Classic Form"
                    )

    browser.find_by_id("current_value_link").click()



@then("I want to be redirected to the Easy2Track Classic form")
def step_impl(browser, no_js_error):
    browser.windows.current = browser.windows[1]
    make_screenshot(browser, "Easy2Track client mode: Classic Form", "Easy2Track", "save_contact_button",
                    previous="Easy2Track client mode: QR-Code-Page 2",
                    description="On the classic-form page, the customer has to enter his contact-details, set the data-privacy checkbox and click the save-button.")
    browser.is_text_present("verschlüsselt gespeichert")
    browser.is_text_present("Speichern")
    no_js_error()
    browser.windows.current = browser.windows[0]


@when('I press the "zum Formular" link')
def step_impl(browser):
    browser.find_by_id("easy2track_form_link").click()


@then("I want to be redirected to the easy2track client-form")
def step_impl(browser, no_js_error):
    browser.is_text_present("Kostenlosen Checkin QR-Code erstellen")
    no_js_error()

@when('I press the "zum Scanner" link')
def step_impl(browser):
    browser.find_by_id("easy2track_scanner_link").click()

@then("I want to be redirected to the easy2track scanner-page")
def step_impl(browser, no_js_error):
    browser.is_text_present("Checkin QR-Code Scannen")
    #no_js_error()

@when('I press the "zur Statistik" link')
def step_impl(browser):
    browser.find_by_id("easy2track_statistics_link").click()

@then("I want to be redirected to the easy2track statistics-page")
def step_impl(browser, no_js_error):
    browser.is_text_present("Admin Panel")
    #no_js_error()


@given("I'm a user without easy2track permissions")
def step_impl(default_user, login):
    pass


@when("I visit the easy2track statistics page")
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/easy2track/visitors.html"
    browser.visit(url)
