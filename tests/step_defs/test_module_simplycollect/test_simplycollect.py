from time import sleep

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers

from tests.helper_functions import make_screenshot

scenarios("../../features/module_simplycollect/simplycollect.feature")

@given("I'm logging in")
def step_impl(simplycollect_user, login):
    pass

@when('I press the "Haendleransicht" link')
def step_impl(browser):
    link = browser.find_by_id("simplycollect_traders_view_link")
    make_screenshot(browser, "Simplycollect traders-view", "Simplycollect", "simplycollect_traders_view_link",
                    description="Click on the merchant link to get on the page for the merchant.")
    link.click()

@then("I want to be redirected to the simplycollect traders-page")
def step_impl(browser, no_js_error):
    browser.is_text_present("Sie bedienen jetzt:")
    #Not possible yet because of video-stream error
    #no_js_error()


@when('I go to the simplycollect customers-website')
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/simplycollect/customer.html?mid=demo"
    browser.visit(url)


@then("I want to see simplycollect customers-website")
def step_impl(browser, no_js_error):
    browser.is_text_present("Willkommen bei simplycollect")



@given("I'm a user without simplycollect.merchant_view permissions")
def step_impl(default_user, login):
    pass

@when("I visit the simplycollect-merchant page")
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/simplycollect/merchant.html"
    browser.visit(url)


