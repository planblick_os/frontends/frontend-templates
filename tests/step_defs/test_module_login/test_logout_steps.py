import sys
import pytest
from pytest_bdd import scenarios, scenario, given, when, then, parsers
from time import sleep
from tests.conftest import make_screenshot

scenarios("../../features/module_login/logout.feature")


@given("I'm on the dashboard page")
def step_impl(login):
    pass


@when("I press the logout button")
def step_impl(browser, window_size, login, no_js_error):

    if window_size.get("width") < 992:
        browser.find_by_id("azMenuShow").click()

    profile_button = browser.find_by_id("header_profile_menu")
    profile_button.click()

    button = browser.find_by_id('logout_button')
    make_screenshot(browser, "Logout from Dashboard", "System", '//*[@id="logout_button"]',
                    description="Click on the logout button to quit your session safely.")
    button.click()
    #no_js_error()


@then("I want to be redirected to the login-page")
def step_impl(browser):
    assert browser.is_text_present('Melden Sie sich an') is True
