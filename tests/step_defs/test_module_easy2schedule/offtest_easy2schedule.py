from time import sleep

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers

from tests.helper_functions import make_screenshot


scenarios("../../features/module_easy2schedule/easy2schedule.feature")

@given("I'm on the dashboard page")
def step_impl(easy2schedule_user, login):
    pass


@when('I press the "zu Kalendar" link')
def step_impl(browser, environment):
    link = browser.links.find_by_partial_text('Dein Kalender')
    make_screenshot(browser, "Easy2Schedule Start", "Easy2Schedule", "easy2schedule_link",
                    description="Click on the register link to get on the registration page.")
    link.click()


@then("I want to be redirected to the easy2schedule application")
def step_impl(browser):
    assert browser.is_text_present('Wochenansicht Räume') is True
    assert browser.is_text_present('Tagesansicht Räume') is True


@given("I'm on the easy2schedule page")
def step_impl(browser, environment, login):
    sleep(0.5)
    url = environment.get("baseurl") + "/easy2schedule/"
    browser.visit(url)
    sleep(0.2)


@when('I click on "Neuer Termin"')
def step_impl(browser):
    link = browser.links.find_by_partial_text('Neuer Termin')
    make_screenshot(browser, "Easy2Schedule Create new event step 1", "Easy2Schedule", '//*[@id="full_side_menu_newEvent"]',
                    description="Click on the new event button in the menu.",
                    next="Easy2Schedule Create new event step 2")
    link.click()


@when(parsers.cfparse('I fill in the "{title}"'))
def step_impl(browser, title):
    browser.fill('title', title)
    make_screenshot(browser, "Easy2Schedule Create new event step 2", "Easy2Schedule", None,
                    description="A form opens in which you can enter the details of your event.",
                    previous="Easy2Schedule Create new event step 1",
                    next="Easy2Schedule Create new event step 3")


@when('I press the "Anlegen" button')
def step_impl(browser):
    button = browser.find_by_value('Anlegen')
    make_screenshot(browser, "Easy2Schedule Create new event step 3", "Easy2Schedule", 'save',
                    description="Click on the save-button to save the new event.",
                    previous="Easy2Schedule Create new event step 2",
                    next="Easy2Schedule Create new event step 4")
    button.click()


@then(parsers.cfparse("I want that an a new appointment with title {title} is created"))
def step_impl(browser, title, environment):
    browser.reload()
    sleep(1)
    assert browser.is_text_present(title) is True
    make_screenshot(browser, "Easy2Schedule Create new event step 4", "Easy2Schedule", None,
                    description="After a short moment, your event should appear in the calendar.",
                    previous="Easy2Schedule Create new event step 3",
                    next="Easy2Schedule Delete an event step 1")


@when("I doubleclick on an event")
def step_impl():
    raise NotImplementedError(u'STEP: When I doubleclick on an event')


@then("I want to see to edit event form")
def step_impl():
    raise NotImplementedError(u'STEP: Then I want to see to edit event form')


@when(parsers.cfparse("I delete the event with {title}"))
def step_impl(browser, title, environment):
    browser.find_by_css('a.fc-time-grid-event').mouse_over()
    event = browser.find_by_text(title)
    sleep(1)
    make_screenshot(browser, "Easy2Schedule Delete an event step 1" , "Easy2Schedule", None,
                    description="To delete an event, first mark the event by clicking on it.",
                    next="Easy2Schedule Delete an event step 2")
    event.click()
    
    link = browser.links.find_by_partial_text('Termin löschen')
    make_screenshot(browser, "Easy2Schedule Delete an event step 2", "Easy2Schedule", '//*[@id="full_side_menu_deleteEvent"]',
                    description="Now click on the button delete event.",
                    previous="Easy2Schedule Delete an event step 1",
                    next="Easy2Schedule Delete an event step 3")
    link.click()
    WebDriverWait(browser.driver, 5).until(EC.alert_is_present(), 'Timed out waiting for alert to appear')
    alert = browser.driver.switch_to.alert
    msg_from_alert = alert.text
    assert msg_from_alert == "Wollen Sie den ausgewählten Termin löschen?"
    alert.accept()


@then(parsers.cfparse("No appointment with title {title} does exist"))
def step_impl(browser, title):
    browser.driver.refresh()
    sleep(1)
    assert browser.is_text_present(title) is False
    make_screenshot(browser, "Easy2Schedule Delete an event step 3", "Easy2Schedule", '//*[@id="full_side_menu_deleteEvent"]',
                    description="After confirming the message asking if you really want to delete the event, the event disappears in the calendar.",
                    previous="Easy2Schedule Delete an event step 2"
                    )


@when("I click the dashboard link in the menu")
def step_impl(browser):
    link = browser.links.find_by_partial_text('Dashboard')
    link.click()


@when("I click the export-button")
def step_impl(easy2schedule_user_admin, browser):
    try:
        WebDriverWait(browser.driver, 2).until_not(EC.presence_of_element_located((By.XPATH, '//*[@id="export_loading"]')))
    except:
        pass

    browser.find_by_id("easy2schedule_data_export").first.click()
    browser.windows.current = browser.windows[0]

@then(parsers.cfparse("I want the new event with title {title} to be in the export"))
def step_impl(browser, title):
    browser.windows.current = browser.windows[1]
    content = browser.html

    assert (title in content) is True


@when("I go back to the calendar")
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/easy2schedule/"
    browser.visit(url)
    sleep(0.2)


@given("I'm a user without easy2schedule permissions")
def step_impl(default_user, login):
    pass


@when("I visit the easy2schedule index page")
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/easy2schedule/"
    browser.visit(url)
    sleep(0.2)
