from time import sleep

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers

from tests.helper_functions import make_tour_step

scenarios("../../features/module_dashboard/dashboard_display.feature")


@given("I'm not logged in")
def step_impl():
    pass

@when('I\'m entering the url /dashboard/ into the browser-address bar')
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/dashboard/"
    browser.visit(url)

@then("I want to be redirected to the login-page")
def step_impl(browser):
    assert browser.is_text_present('Melden Sie sich an') is True

@given("I'm logged in as a user without any special permissions")
def step_impl(default_user, login):
    pass


@then('I want to see the headline "Willkommen in Deinem Dashboard"')
def step_impl(browser):
    assert browser.is_text_present('in Deinem Dashboard') is True


@then('I want to see the application "Verwaltung"')
def step_impl(browser):
    assert browser.is_text_present('Verwaltung') is True


@given("I'm logged in as demo_1")
def step_impl(login):
    pass


@then('I want to see a link labeled "Benutzerverwaltung"')
def step_impl(browser):
    links = browser.links.find_by_partial_text('Benutzerverwaltung')
    list_length = len(links)
    assert list_length > 0


@then('I don\'t want to see the application "Easy2Track"')
def step_impl(browser):
    make_tour_step("dashboard", "#board-portlet-container", "Hier sind alle für verfügbaren Anwendungen aufgelistet.", True)
    make_tour_step("dashboard", "#header_apps_menu", "Auch hier finden sie nochmal alle Anwendungen. Dieser Menüpunkt steht in allen Anwendungen zur Verfügung. So können sie schnell zwischen ANwendungen hin und her wechseln.")
    make_tour_step("dashboard", "#card-one", "Easy2Schedule ist unsere einfache Kalender-Verwaltung.")
    make_tour_step("dashboard", "#card-two", "Easy2Track ist unsere einfache Kontakt-Nachverfolgung.")
    make_tour_step("dashboard", "#card-four", "Simplycollect ist ihr einfacher Helfer für Click&Collect Geschäfte ohne Webshop.")
    make_tour_step("dashboard", "#card-three", "Easy2Order ist unser einfacher Helfer um Gast-Bestellungen schnell und einfach ausführen zu können.")
    make_tour_step("dashboard", "#card-five", "Kontaktmanger ist unser einfacher Helfer um Kontakte zu verwalten")

    assert browser.is_text_not_present('Scanner') is True



@then('I don\'t want to see the application "Easy2Schedule"')
def step_impl(browser):
    assert browser.is_text_not_present('Dein Kalender') is True


@then('I don\'t want to see the application "Simplycollect"')
def step_impl(browser):
    assert browser.is_text_not_present('Händleransicht') is True