from time import sleep

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers

from tests.helper_functions import make_screenshot

scenarios("../../features/module_dashboard/dashboard_accounts.feature")

@given("I'm not logged in")
def step_impl():
    pass

