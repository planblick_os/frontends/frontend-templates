from time import sleep
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers
from tests.helper_functions import make_screenshot

scenarios("../../features/module_dashboard/dashboard_groups.feature")

@given("I'm logged in as admin")
def step_impl(admin_login):
    pass

@when('I press the "Gruppenverwaltung" link')
def step_impl(browser):
    element = browser.links.find_by_href('./groups.html').last
    element.click()


@then("I want to see the group management page")
def step_impl(browser, no_js_error):
    browser.is_text_present("Gruppen-Verwaltung")
    no_js_error()


@given("I am on the group management page")
def step_impl(admin_login, environment, browser):
    url = environment.get("baseurl") + "/dashboard/groups.html"
    browser.visit(url)

@when(parsers.cfparse('I fill in the "<name>" and "<description>" form fields'))
def step_impl(browser, name, description):
    browser.driver.implicitly_wait(15)
    browser.fill('newgroupname', name)
    browser.fill('newgroupdescription', description)


@when('I click on "Neue Gruppe erstellen" button')
def step_impl(browser):
    button = browser.find_by_id("createGroupButton")
    button.click()


@then(parsers.cfparse('A new group with name "<name>" and description "<description>" will show up in the groups table'))
def step_impl(browser, no_js_error, name, description):
    browser.is_text_present("group." + name)
    browser.is_text_present(description)
    no_js_error()


@when(parsers.cfparse('I click the "Bearbeiten" button for role with name <name>'))
def step_impl(browser, name):
    button = browser.find_by_id("groupsEditButtonBootstrap_group." + name)
    button.click()


@then(parsers.cfparse("I want to be redirected to the edit group page"))
def step_impl(browser, no_js_error, name, description):
    browser.is_text_present("Details")
    browser.is_text_present("Gruppenmitglieder")
    browser.is_text_present(name)
    browser.is_text_present(description)
    no_js_error()

@when('I click on the back arrow')
def step_impl(browser):
    links_found = browser.find_by_id('back-link')
    links_found.click()

@when(parsers.cfparse('I click on the trashbin button for "<name>"'))
def step_impl(browser, name):
    button = browser.find_by_id("deleteGroupsButtonBootstrap_group." + name)
    button.click()


@then(parsers.cfparse('I want the group with name "<name>" to be deleted and not shown in the group-table anymore'))
def step_impl(browser, name):
    WebDriverWait(browser.driver, 5).until(EC.alert_is_present(), 'Timed out waiting for alert to appear')
    alert = browser.driver.switch_to.alert
    assert name + " wirklich löschen" in alert.text

    alert.accept()
    WebDriverWait(browser.driver, 5).until(EC.alert_is_present(), 'Timed out waiting for alert to appear')
    alert = browser.driver.switch_to.alert
    assert "wurde gelöscht" in alert.text
    alert.accept()