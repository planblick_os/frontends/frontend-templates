from shutil import copyfile

import boto3
import os
import json

from pathvalidate import sanitize_filename
from pydub import AudioSegment


def createSpeech(text, filename, language, force_regenerate=False):
    print("Start tts synth...")
    languages = {"en": "en-US", "de": "de-DE"}
    languageCode = languages.get(language)

    voices = {"en": {"engine": "neural", "voiceid": "Salli"}, "de": {"engine": "standard", "voiceid": "Vicki"}}

    folder = os.path.dirname(os.path.abspath(filename))
    if not os.path.exists(folder):
        os.makedirs(folder)

    file = open(filename, 'wb')
    polly_client = boto3.Session(region_name='eu-central-1').client('polly')

    response = polly_client.synthesize_speech(
        Engine=voices.get(language).get("engine"),  # 'standard' | 'neural'
        VoiceId=voices.get(language).get("voiceid"),
        OutputFormat='mp3',
        LanguageCode=languageCode,
        Text=text)

    file.write(response['AudioStream'].read())
    file.close()
    print("Tts synth finished")
    return filename


def parse_json_files(path="../jsons/", language="en", force_regenerate=False):
    with os.scandir(path) as iterator:
        for entry in iterator:
            if entry.name.endswith(".json") and entry.is_file():
                fullpath = os.path.join(path, entry.name)
                with open(fullpath, 'r') as jsonfile:
                    print(fullpath)
                    data = json.load(jsonfile)
                    filename_base = data[language]["step_name"].replace(" ", "_").lower()

                    mp3_filename = filename_base + ".mp3"
                    if not os.path.exists("../dist/mp3/" + filename_base + ".mp3") or force_regenerate:
                        title_mp3_filename = createSpeech(data[language]["step_name"],
                                                          "../dist/mp3/" + filename_base + "_title.mp3", language)
                        description_mp3_filename = createSpeech(data[language]["step_description"],
                                                                "../dist/mp3/" + filename_base + "_description.mp3",
                                                                language)
                        print(description_mp3_filename)
                        try:
                            title_mp3 = AudioSegment.from_mp3(title_mp3_filename)
                            description_mp3 = AudioSegment.from_mp3(description_mp3_filename)
                            two_sec_silence = AudioSegment.silent(duration=1000)
                            complete_mp3 = title_mp3 + two_sec_silence + description_mp3
                            complete_mp3.export("../dist/mp3/" + mp3_filename, format="mp3")
                            os.remove(title_mp3_filename)
                            os.remove(description_mp3_filename)
                        except(Exception):
                            print("Unable to process audio file", title_mp3_filename)

                    create_docpage(data[language]["step_name"], data[language]["feature_name"], data[language]["step_description"], data[language]["step_image"], mp3_filename, data[language]["step_next"], data[language]["step_previous"])

global index
index = {}

def create_docpage(title, feature, description, image_source, mp3_source, next, previous):
    print("Creating docpage")
    print("NEXT", next)
    if next is None:
        next = "start"
    if previous is None:
        previous = "start"

    mp4_source = mp3_source.replace(".mp3", ".mp4")
    global index
    with open("template.html", "r") as tpl:
        content = tpl.read()
        content = content.replace("{FEATURE}", feature)
        content = content.replace("{TITLE}", title)
        content = content.replace("{DESCRIPTION}", description)
        content = content.replace("{IMGSOURCE}", "../img/" + image_source)
        content = content.replace("{MP3SOURCE}", "../mp3/" + mp3_source)
        content = content.replace("{MP4SOURCE}", "../mp4/" + mp4_source)
        content = content.replace("{PREVIOUS}", "./" + sanitize_filename(previous) + ".html")
        content = content.replace("{NEXT}", "./" + sanitize_filename(next) + ".html")
        filename = "../dist/html/" + sanitize_filename(title) + ".html"
        folder = os.path.dirname(os.path.abspath(filename))
        print("folder", folder)
        if not os.path.exists(folder):
            os.makedirs(folder)

        with open(filename, "w") as out:
            out.write(content)

        index[title] = sanitize_filename(title) + ".html"
        if not os.path.exists("../dist/img/"):
            os.makedirs("../dist/img/")
        copyfile("../images/" + image_source, "../dist/img/" + image_source)


def create_index_file():
    list = '<div ><nav class="nav flex-column">'
    global index
    for key, value in index.items():
        list += f'<a href="./{value}" class="doc-nav-item" target="content_page">{key}</a>'
    list += "</nav></div>"
    with open("template.html", "r") as tpl:
        content = tpl.read()
        content = content.replace('<div class="container mb-5 pt-5">', '<div class="container mb-5 pt-5 hidden">')
        content = content.replace('<div class="col-md-4">', '<div class="col-md-4 p-0">')
        content = content.replace("{DESCRIPTION}", list)
        content = content.replace('<p id="video">', '<p id="video" class="hidden">')
        content = content.replace('<div class="card">', '<div class="card hidden">')
        content = content.replace('<div class="media pt-5 mt-5">', '<div class="media hidden">')
        content = content.replace("{MP3SOURCE}", "../mp3/index.mp3")
        content = content.replace("{MP4SOURCE}", "../mp4/index.mp4")
        with open("../dist/html/index.html", "w") as out:
            out.write(content)
    pass

def createDocumentation(language, force_regenerate=False):
    #createSpeech("Welcome to our documentation.", "../dist/mp3/index.mp3", "en")
    parse_json_files(language=language, force_regenerate=force_regenerate)
    create_index_file()


force_regenerate = False
createDocumentation("en", force_regenerate)
